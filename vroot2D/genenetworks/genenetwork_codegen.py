import time
import ast
import codegen
import os
from odesparse import odeints_cfunc, dlopen, dlclose
from vroot2D.utils.db_utilities import get_wall_decomp, \
                         get_tissue_maps, get_parameters, \
                         add_property, get_graph
from vroot2D.utils.db_geom import updateVS
import numpy as np
import scipy.sparse
from math import exp, log, sqrt
from collections import defaultdict

def add_ast(ast1, ast2):
    return ast.Expression(body=ast.BinOp(left=ast1.body, op=ast.Add(), right=ast2.body))

class AbstractCGModel(object):
    """
    Abstract object used to document Model interface -
    implement this interface rather than subclass
    Here for documentation purposes
    """
    
    name = 'AbstractGCModel'
    # Name of this model - used for storing parameters in TissueDB
    
    def pre_step(self, db):	
        pass

    def post_step(self, db):	
        pass

    def post_iteratiions(self, db):
        pass

    def get_species(self):
        """
        Species used in model
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        pass

    def get_modifier_species(self):
        """
        Species used by model, but not changed in the reactions
        """
        pass

    def get_modified_species(self):
        """
        Species used by model, but not changed in the reactions
        """
        pass

    def get_parameters(self):
        pass

    def set_default_parameters(self, db):
        """
        Set default parameters in TissueDB
        :param db: Tissue database
        :type db: TissueDB
        """
        pass

    def deriv(self):
        """
        RHS of gene network ODEs
        """
        pass


class CombinedCGModel(AbstractCGModel):
    """
    Additive combination of GRN models.
    """

    name = 'CombinedCGModel'

    def __init__(self, models):
        """
        :param models: List of models to combine (each implementing
                       the AbstractModel interface)
        """
        self.models=models

    def deps(self):
        deps = defaultdict(set)
        for m in self.models:
            d = m.deps()
            for s in d:
                deps[s].update(d[s])
        return deps

    def get_parameters(self):
        p = {}
        # Completely ignore potential overlap :?
        for model in self.models:
            p.update(model.get_parameters())
        return p

    def get_species(self):
        """
        :returns: Set of names of species in the combined model
        """
        return set.union(*[set(m.get_species()) for m in self.models])

    def get_modifier_species(self):
        """
        :returns: Set of names of species in the combined model
        """
        return set.union(*[set(m.get_modifier_species()) for m in self.models]) - self.get_species()

    def get_modified_species(self):
        pass

    def deriv(self):
        """
        Derivative function - sum of the derivatives for 
        each component model
        :param y: Current state vector
        :type y: numpy.array (or list)
        :param t: Current time
        :type t: float
        :param *args: additional arguments for derivative function
        """
        
        deriv = {}
        for m in self.models:
            d = m.deriv()
            for s in d:
                if s not in deriv:
                    deriv[s] = d[s]
                else:
                    deriv[s] = add_ast(deriv[s], d[s])
        return deriv

    def pre_step(self, db):	
        for model in self.models:
            model.pre_step(db)        

    def post_step(self, db):	
        for model in self.models:
            model.post_step(db)

    def post_iterations(self, db):	
        for model in self.models:
            model.post_iterations(db)




class AuxinHSModel(AbstractCGModel):
    def get_species(self):
        return [('auxin', 'cell')]

    def deps(self):
        return { 'auxin': [ 'auxin' ] }

    def get_modifier_species(self):
        return [('cell_type', 'cell')]

    def get_parameters(self):
        alpha_back=0.001  # Auxin production rate in all cells except QC and columella initials
	alpha_QCinit=0.01 # Auxin production rate in QC and columella initials
	beta =0.001 # Auxin degradation rate
        l = locals()
        p = dict((n,v) for n,v in l.iteritems() if n!='self')
        return p

    def deriv(self):

        ret = {}
        ret['auxin'] = ast.parse('(alpha_QCinit if (cell_type==17.0 or cell_type==10.0) else alpha_back) -beta*auxin')        
#        ret['auxin'] = ast.parse('alpha_back -beta*auxin')        
        return ret

class DIIVarParamsModel(AbstractCGModel):

    def get_species(self):
        return [('VENUS', 'cell')]

    def get_modifier_species(self):
        return [('DII_q1', 'cell'), ('DII_q2', 'cell'), ('DII_p1', 'cell'), ('auxin', 'cell'), ('DII_p2', 'cell')]

    def deps(self):
        return { 'VENUS': [ 'VENUS', 'auxin', 'DII_q1', 'DII_q2', 'DII_p1', 'DII_p2' ] }
    

    def get_parameters(self):
        p = {}
        return p

    def deriv(self):
        ret = {}
        ret['VENUS'] = ast.parse('DII_p2*(1-auxin*VENUS/(DII_q1+DII_q2*auxin+DII_p1*auxin*VENUS))', mode='eval')        
        return ret


class DIIModel(AbstractCGModel):

    def get_species(self):
        return [('VENUS', 'cell')]

    def get_modifier_species(self):
        return [('auxin', 'cell')]

    def deps(self):
        return { 'VENUS': [ 'VENUS', 'auxin' ] }
    

    def get_parameters(self):
        p1=0.06   #  parameter [Auxin-TIR1-VENUS]u/[TIR1]T
	p2=0.0053/60 # parameter delta/[VENUS]u  (in seconds).
	q1=0.91   #  parameter [TIR1]u/[TIR1]T
#	q1=0.91/10   #  parameter [TIR1]u/[TIR1]T
	q2=0.03   #  parameter [Auxin-TIR1]u/[TIR1]T

        l = locals()
        p = dict((n,v) for n,v in l.iteritems() if n!='self')
        return p

    def deriv(self):
        ret = {}
        ret['VENUS'] = ast.parse('p2*(1-auxin*VENUS/(q1+q2*auxin+p1*auxin*VENUS))', mode='eval')        
        return ret


class AuxinResponseModel(AbstractCGModel):

    def get_species(self):
        return [('IAA', 'cell'), ('mIAA', 'cell'), ('response', 'cell')]

    def get_modifier_species(self):
        return [('auxin', 'cell')]

    def deps(self):
        return { 'IAA': [('auxin', 'IAA', 'mIAA', 'response')],
                 'mIAA': [('auxin', 'IAA', 'mIAA', 'response')],
                 'response': [('auxin', 'IAA', 'mIAA', 'response')] }

    def get_parameters(self):
        am = 0.001
        amr = 0.001
        Kmr = 1.0
        bm = 0.001
        aI = 0.001
        bI = 0.001
        bIa = 0.001
        ar = 0.001
        Kar = 1.0
        KARF = 1.0
        k1 = 1.0
        k2 = 1.0
        br = 0.001
        c = 1.0
        ARF_T = 1.0
        l = locals()
        p = dict((n,v) for n,v in l.iteritems() if n!='self')
        return p

    def deriv(self):

        ret = {}
        ret['mIAA'] = ast.parse('am + amr*response/(Kmr + response) - bm*mIAA', mode='eval')
        ret['IAA'] = ast.parse('aI*mIAA - bI*IAA - bIa*auxin*IAA/(1.0+k1*auxin+k2*auxin*IAA)', mode='eval')
        ARF = ast.parse('(ARF_T - IAA)/2 - 1/(2*c) + sqrt(ARF_T*ARF_T*c*c - 2*ARF_T*IAA*c*c + 2*ARF_T*c + IAA*IAA*c*c + 2*IAA*c + 1)/(2*c)', mode='eval')
        ret['response'] = ast.parse('ar*ARF/(KARF+ARF) - br*response', mode='eval')
        VariablePropagator({'ARF':ARF.body}).visit(ret['response'])
#        ret['mIAA'] = ast.parse('1-mIAA', mode='eval')
#        ret['IAA'] = ast.parse('1-IAA', mode='eval')
#        ret['response'] = ast.parse('1-response', mode='eval')
        # Substitute for ARF within response
        
        return ret



class ConstantPropagator(ast.NodeTransformer):
    def __init__(self, constants):
        self.constants=constants

    def visit_Name(self, node):
        if node.id in self.constants:
            return ast.copy_location(ast.Num(self.constants[node.id]),node)
        else:
            return node

class VariablePropagator(ast.NodeTransformer):
    def __init__(self, vars):
        self.vars=vars

    def visit_Name(self, node):
        if node.id in self.vars:
            return self.vars[node.id]
        else:
            return node

class ParameterPropagator(ast.NodeTransformer):
    def __init__(self, params):
        self.params=params

    def visit_Name(self, node):
        if node.id in self.params:
#            if self.params[node.id].const:
#                return ast.copy_location(ast.Num(self.params[node.id].val),node)
#            else:
            return ast.copy_location(ast.Subscript(
                        value=ast.Name(id='p', ctx=ast.Load()),
                        slice=ast.Index(value=ast.Num(n=self.params[node.id]
)), ctx=node.ctx),node)
        else:
            return node


class YPropagator(ast.NodeTransformer):
    def __init__(self, y, offsets, vars, y_idx='i'):
        self.y = y
        self.offsets = offsets
        self.vars=vars
        self.y_idx = y_idx

    def visit_Name(self, node):
        if node.id in self.vars:
#            if self.vars[node.id].const:
#                return ast.copy_location(ast.Num(self.vars[node.id].val),node)
#            else:             
            """
            return ast.copy_location(ast.Subscript(
                        value=ast.Name(id='y', ctx=ast.Load()),
                        slice=ast.Index(value=ast.Num(n=self.vars[node.id].idx))
                        , ctx=node.ctx),node)
                        """
            return ast.copy_location(ast.parse(self.y+'['+self.y_idx+'+'+self.offsets+'[%d]]'%self.vars[node.id], mode='eval').body, node)
        else:
            return node


class MeanExtractor(ast.NodeTransformer):
    def __init__(self):
        self.mean_expr = []

    def visit_Call(self, node):
        if node.func.id == 'mean':
            expr = node.args[0]
            s_expr = codegen.to_source(expr)
            idx = len(self.mean_expr)
            self.mean_expr.append((s_expr, expr))
            return ast.copy_location(ast.parse('mean_%d'%idx, mode='eval').body, node)
        else:
            return node


class CGat_cwd(object):

    def deps(self):
        return { 'auxin':['auxin', 'auxin_wall', 'PIN', 'AUX', 'LAX', 'S', 'V'],
                 'auxin_wall':['auxin', 'auxin_wall', 'PIN', 'AUX', 'LAX', 'S', 'V'] }

    def get_species(self):
        """  
        Species used in model
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        return [('auxin', 'cell'), ('auxin_wall', 'wall')]

    def get_modifier_species(self):
        """  
        Species used in model but which aren't modified by it
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        return [('PIN', 'edge'), ('AUX', 'edge'), ('LAX', 'edge'), ('S', 'wall'),
                ('V', 'cell')]  

    def get_parameters(self):
        """
        Set default parameters for this model in the TissueDB
        """
        # These are defined in 'auxin_transport.py'
        # e.g. see lines 34--53

        pHapo=5.3 # apoplastic pH
        pHcyt=7.2 # cytoplasmic pH
        pK=4.8
        Voltage=-0.120  # Membrane potential (Volts)
        Temp=300 # K
        PIAAH=0.56 #0.56  # Permeability of membrane to protonated auxin (microns/sec)
	PAUX=0.56 # Permeability of membrane to anionic auxin due to AUX1 (microns/sec)
	PLAX=0.56 # Permeability of membrane to anionic auxin due to AUX1 (microns/sec)
	PPIN=0.56   # Permeability of membrane to anionic auxin due to PINs (microns/sec)
	PNPE=0.56*0.3   # Permeability of membrane to anionic auxin due to PINs (microns/sec)	
	lamb=0.14  # Cell-wall thickness (microns)
	D_cw= 32   

        # the below parameters are derived from the others - if we want
        # to be able to change the upper ones they need recalculating

	Rconst=8.31
	Fdconst=96500
       
        phi=Fdconst*Voltage/(Rconst*Temp)	
        A1=1/(1+pow(10,pHapo-pK)) 
        A2=(1-A1)*phi/(exp(phi)-1)
        A3=-(1-A1)*phi/(exp(-phi)-1)
        B1=1/(1+pow(10,pHcyt-pK))
        B2=-(1-B1)*phi/(exp(-phi)-1)
        B3=(1-B1)*phi/(exp(phi)-1)
        """
        # Make dictionary of the parameters for this model -
        """
        p = { 'pHapo': pHapo, 'pHcyt': pHcyt, 'Voltage':Voltage, 'A1':A1, 'A2':A2, 'A3':A3, 'B1':B1, 'B2':B2, 'B3':B3, 
               'PIAAH':PIAAH, 'PAUX':PAUX, 'PPIN':PPIN, 'PNPE':PNPE, 'PLAX':PLAX, 'lamb':lamb, 'D_cw':D_cw, 'Temp':Temp, 'pK':pK }
               

        return p

    def deriv_vertex(self):
        """
        a_vertex = mean(auxin_wall/S)/mean(1/S)
        flux = 2*D_cw/S*(a_vertex-auxin_wall)
        ret['auxin_wall'] = flux/S
        """
        ret = {}
        ret['auxin_wall'] = ast.parse('2*D_cw/S/S*(mean(auxin_wall/S)/mean(1/S)-auxin_wall)', mode='eval')
        return ret

    def deriv_wall(self):
        """
        J1=(B1*PIAAH+B2*PAUX*AUX+B2*PLAX*LAX+B3*PPIN*PIN+B3*PNPE)*auxin - \
                (A1*PIAAH+A2*PAUX*AUX+A2*PLAX*LAX+A3*PPIN*PIN+A3*PNPE)*auxin_wall
        ret['auxin_wall'] = -(S/V)*J
        ret['auxin'] = J/lamb
        """
        ret = {}
        ret['auxin_wall'] = ast.parse('((B1*PIAAH+B2*PAUX*AUX+B2*PLAX*LAX+B3*PPIN*PIN+B3*PNPE)*auxin - \
                (A1*PIAAH+A2*PAUX*AUX+A2*PLAX*LAX+A3*PPIN*PIN+A3*PNPE)*auxin_wall)/lamb', mode='eval')
        ret['auxin'] =ast.parse('-(S/V)*((B1*PIAAH+B2*PAUX*AUX+B2*PLAX*LAX+B3*PPIN*PIN+B3*PNPE)*auxin - \
                (A1*PIAAH+A2*PAUX*AUX+A2*PLAX*LAX+A3*PPIN*PIN+A3*PNPE)*auxin_wall)', mode='eval')
        return ret

    def blocked_walls(self,db):
        if 'casparian' in db.properties(): 
            casparian = db.get_property('casparian')
            return [wid for wid in casparian if casparian[wid]!=0]
        else:
            return []


class CGat_cwd_PINs(object):

    def deps(self):
        return { 'auxin':['auxin', 'auxin_wall', 'PIN1', 'PIN2', 'PIN3', 'PIN4', 'PIN7', 'AUX', 'LAX', 'S', 'V', 'PIN'],
                 'auxin_wall':['auxin', 'auxin_wall', 'PIN1', 'PIN2', 'PIN3', 'PIN4', 'PIN7', 'AUX', 'LAX', 'S', 'V', 'PIN'] }

    def get_species(self):
        """  
        Species used in model
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        return [('auxin', 'cell'), ('auxin_wall', 'wall')]

    def get_modifier_species(self):
        """  
        Species used in model but which aren't modified by it
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        return [('PIN1', 'edge'), ('PIN2', 'edge'), ('PIN3', 'edge'), 
                ('PIN4', 'edge'), ('PIN7', 'edge'), ('PIN', 'edge'),
                ('AUX', 'edge'), ('LAX', 'edge'), ('S', 'wall'),
                ('V', 'cell')]  

    def get_parameters(self):
        """
        Set default parameters for this model in the TissueDB
        """
        # These are defined in 'auxin_transport.py'
        # e.g. see lines 34--53

        pHapo=5.3 # apoplastic pH
        pHcyt=7.2 # cytoplasmic pH
        pK=4.8
        Voltage=-0.120  # Membrane potential (Volts)
        Temp=300 # K
        PIAAH=0.56 #0.56  # Permeability of membrane to protonated auxin (microns/sec)
	PAUX=0.56 # Permeability of membrane to anionic auxin due to AUX1 (microns/sec)
	PLAX=0.56 # Permeability of membrane to anionic auxin due to AUX1 (microns/sec)
	PPIN=0.56   # Permeability of membrane to anionic auxin due to PINs (microns/sec)
	PNPE=0.56*0.3   # Permeability of membrane to anionic auxin due to PINs (microns/sec)	
	lamb=0.14  # Cell-wall thickness (microns)
	D_cw= 32   

        # the below parameters are derived from the others - if we want
        # to be able to change the upper ones they need recalculating

	Rconst=8.31
	Fdconst=96500
       
        phi=Fdconst*Voltage/(Rconst*Temp)	
        A1=1/(1+pow(10,pHapo-pK)) 
        A2=(1-A1)*phi/(exp(phi)-1)
        A3=-(1-A1)*phi/(exp(-phi)-1)
        B1=1/(1+pow(10,pHcyt-pK))
        B2=-(1-B1)*phi/(exp(-phi)-1)
        B3=(1-B1)*phi/(exp(phi)-1)
        vPIN1 = 1.0
        vPIN2 = 1.0
        vPIN3 = 1.0
        vPIN4 = 1.0
        vPIN7 = 1.0
        KPIN1 = 1.0
        KPIN2 = 1.0
        KPIN3 = 1.0
        KPIN4 = 1.0
        KPIN7 = 1.0

        """
        # Make dictionary of the parameters for this model -
        """
        p = { 'pHapo': pHapo, 'pHcyt': pHcyt, 'Voltage':Voltage, 'A1':A1, 'A2':A2, 'A3':A3, 'B1':B1, 'B2':B2, 'B3':B3, 
              'PIAAH':PIAAH, 'PAUX':PAUX, 'PPIN':PPIN, 'PNPE':PNPE, 'PLAX':PLAX, 'lamb':lamb, 'D_cw':D_cw, 'Temp':Temp, 
              'pK':pK, 'vPIN1':vPIN1, 'vPIN2':vPIN2, 'vPIN3':vPIN3, 'vPIN4':vPIN4, 'vPIN7':vPIN7,
              'KPIN1':KPIN1, 'KPIN2':KPIN2, 'KPIN3':KPIN3, 'KPIN4':KPIN4, 'KPIN7':KPIN7,
        }
               

        return p

    def deriv_vertex(self):
        """
        a_vertex = mean(auxin_wall/S)/mean(1/S)
        flux = 2*D_cw/S*(a_vertex-auxin_wall)
        ret['auxin_wall'] = flux/S
        """
        ret = {}
        ret['auxin_wall'] = ast.parse('2*D_cw/S/S*(mean(auxin_wall/S)/mean(1/S)-auxin_wall)', mode='eval')
        return ret

    def deriv_wall(self):
        """
        J1=(B1*PIAAH+B2*PAUX*AUX+B2*PLAX*LAX+B3*PPIN*PIN+B3*PNPE)*auxin - \
                (A1*PIAAH+A2*PAUX*AUX+A2*PLAX*LAX+A3*PPIN*PIN+A3*PNPE)*auxin_wall
        ret['auxin_wall'] = -(S/V)*J
        ret['auxin'] = J/lamb
        """
        ret = {}

#        ret['auxin_wall'] = ast.parse('((B1*PIAAH+B2*PAUX*AUX+B2*PLAX*LAX+B3*PNPE)*auxin+B3*PPIN*(vPIN1*PIN1*auxin/(KPIN1+auxin) + vPIN2*PIN2*auxin/(KPIN2+auxin) + vPIN3*PIN3*auxin/(KPIN3+auxin) + vPIN4*PIN4*auxin/(KPIN4+auxin) + vPIN7*PIN7*auxin/(KPIN7+auxin) ) - (A1*PIAAH+A2*PAUX*AUX+A2*PLAX*LAX+A3*PNPE)*auxin_wall +A3*PPIN*(vPIN1*PIN1*auxin_wall + vPIN2*PIN2*auxin_wall + vPIN3*PIN3*auxin_wall + vPIN4*PIN4*auxin_wall + vPIN7*PIN7))/lamb', mode='eval') 
#        ret['auxin'] = ast.parse('-(S/V)*((B1*PIAAH+B2*PAUX*AUX+B2*PLAX*LAX+B3*PNPE)*auxin+B3*PPIN*(vPIN1*PIN1*auxin/(KPIN1+auxin) + vPIN2*PIN2*auxin/(KPIN2+auxin) + vPIN3*PIN3*auxin/(KPIN3+auxin) + vPIN4*PIN4*auxin/(KPIN4+auxin) + vPIN7*PIN7*auxin/(KPIN7+auxin) ) - (A1*PIAAH+A2*PAUX*AUX+A2*PLAX*LAX+A3*PNPE)*auxin_wall +A3*PPIN*(vPIN1*PIN1*auxin_wall + vPIN2*PIN2*auxin_wall + vPIN3*PIN3*auxin_wall + vPIN4*PIN4*auxin_wall + vPIN7*PIN7))', mode='eval') 


#        ret['auxin_wall'] = ast.parse('((B1*PIAAH+B2*PAUX*AUX+B2*PLAX*LAX+B3*PNPE)*auxin+B3*PPIN*(vPIN1*PIN1*auxin + vPIN2*PIN2*auxin + vPIN3*PIN3*auxin + vPIN4*PIN4*auxin + vPIN7*PIN7*auxin ) - ((A1*PIAAH+A2*PAUX*AUX+A2*PLAX*LAX+A3*PNPE)*auxin_wall +A3*PPIN*(vPIN1*PIN1*auxin_wall + vPIN2*PIN2*auxin_wall + vPIN3*PIN3*auxin_wall + vPIN4*PIN4*auxin_wall + vPIN7*PIN7)))/lamb', mode='eval')
#        ret['auxin'] = ast.parse('-(S/V)*((B1*PIAAH+B2*PAUX*AUX+B2*PLAX*LAX+B3*PNPE)*auxin+B3*PPIN*(vPIN1*PIN1*auxin + vPIN2*PIN2*auxin + vPIN3*PIN3*auxin + vPIN4*PIN4*auxin + vPIN7*PIN7*auxin ) - ((A1*PIAAH+A2*PAUX*AUX+A2*PLAX*LAX+A3*PNPE)*auxin_wall +A3*PPIN*(vPIN1*PIN1*auxin_wall + vPIN2*PIN2*auxin_wall + vPIN3*PIN3*auxin_wall + vPIN4*PIN4*auxin_wall + vPIN7*PIN7)))', mode='eval') 
        ret['auxin'] = ast.parse('-(S/V)*((B1*PIAAH+B2*PAUX*AUX+B2*PLAX*LAX+B3*PNPE)*auxin+B3*PPIN*(vPIN1*PIN1*auxin/(1.0+auxin/KPIN1) + vPIN2*PIN2*auxin/(1.0+auxin/KPIN2) + vPIN3*PIN3*auxin/(1.0+auxin/KPIN3) + vPIN4*PIN4*auxin/(1.0+auxin/KPIN4) + vPIN7*PIN7*auxin/(1.0+auxin/KPIN7)) - ((A1*PIAAH+A2*PAUX*AUX+A2*PLAX*LAX+A3*PNPE)*auxin_wall +A3*PPIN*(vPIN1*PIN1*auxin_wall + vPIN2*PIN2*auxin_wall + vPIN3*PIN3*auxin_wall + vPIN4*PIN4*auxin_wall + vPIN7*PIN7*auxin_wall)))', mode='eval') 
        ret['auxin_wall'] = ast.parse('((B1*PIAAH+B2*PAUX*AUX+B2*PLAX*LAX+B3*PNPE)*auxin+B3*PPIN*(vPIN1*PIN1*auxin/(1.0+auxin/KPIN1) + vPIN2*PIN2*auxin/(1.0+auxin/KPIN2) + vPIN3*PIN3*auxin/(1.0+auxin/KPIN3) + vPIN4*PIN4*auxin/(1.0+auxin/KPIN4) + vPIN7*PIN7*auxin/(1.0+auxin/KPIN7)) - ((A1*PIAAH+A2*PAUX*AUX+A2*PLAX*LAX+A3*PNPE)*auxin_wall +A3*PPIN*(vPIN1*PIN1*auxin_wall + vPIN2*PIN2*auxin_wall + vPIN3*PIN3*auxin_wall + vPIN4*PIN4*auxin_wall + vPIN7*PIN7*auxin_wall)))/lamb', mode='eval')


#        ret['auxin_wall'] = ast.parse('((B1*PIAAH+B2*PAUX*AUX+B2*PLAX*LAX+B3*PPIN*PIN+B3*PNPE)*auxin - \
#                (A1*PIAAH+A2*PAUX*AUX+A2*PLAX*LAX+A3*PPIN*PIN+A3*PNPE)*auxin_wall)/lamb', mode='eval')
#        ret['auxin'] =ast.parse('-(S/V)*((B1*PIAAH+B2*PAUX*AUX+B2*PLAX*LAX+B3*PPIN*PIN+B3*PNPE)*auxin - \
#                (A1*PIAAH+A2*PAUX*AUX+A2*PLAX*LAX+A3*PPIN*PIN+A3*PNPE)*auxin_wall)', mode='eval')
        return ret

    def blocked_walls(self,db):
        if 'casparian' in db.properties(): 
            casparian = db.get_property('casparian')
            return [wid for wid in casparian if casparian[wid]!=0]
        else:
            return []

class CodeGenModel(object):
    def __init__(self, model):
        self.model = model
        self.params = model.get_parameters()

    def gen_model(self):
        # Generate code for single-cell RHS.
        model = self.model
        params = self.params
        species = model.get_species()
        m_species = model.get_modifier_species()
        print species, m_species

        prop_params = True

        f = open('test.c', 'w')
        f.write('#include <math.h>\n#include <stdio.h>\n\n')
        f.write('void deriv(int* NEQ, double* t, double y[], double dy[])\n')
        f.write('{\n')
        f.write('    const int Nc = NEQ[1];\n')
        f.write('    const int Nf = NEQ[2];\n')
        f.write('    const double* p = &y[NEQ[0]];\n')
        f.write('    const double* ym = &y[NEQ[0]+%d];\n'%len(params))
        f.write('    const int* y_offsets = &NEQ[3];\n')
        f.write('    const int* ym_offsets = &NEQ[3+%d];\n'%(len(species)+1))
        f.write('    const int* fixed = &NEQ[3+%d];\n'%(len(species)+len(m_species)+2))
#        f.write('    printf(\"%f %d\\n\", y[0], Nc);')

        f.write('    int i=0;\n    for(i=0; i<y_offsets[%d]; ++i) dy[i]=0.0;\n'%(len(species)))

        for i, (p,v) in enumerate(params.iteritems()):
            f.write('    const double '+p+ ' = '+str(v)+';\n')
        
        f.write('    for(i=0; i<Nc; ++i) {\n')
        deriv = model.deriv()
        param_idx = dict((n,i) for i,n in enumerate(params))
        species_idx = dict((n[0],i) for i,n in enumerate(species))
        m_species_idx = dict((n[0],i) for i,n in enumerate(m_species))
        pp = ParameterPropagator(param_idx)
        yp = YPropagator('y', 'y_offsets', species_idx)
        ymp = YPropagator('ym', 'ym_offsets', m_species_idx)
        
        for j, s in enumerate(species):
            r = deriv[s[0]]
#(s, r) in deriv.iteritems():

            if prop_params:
                pp.visit(r)
            yp.visit(r)
            ymp.visit(r)
            f.write('        dy[i+y_offsets['+str(j)+']] += '+codegen.to_source(r)+';\n')
#        f.write('    if(i==0) {\n')
#        f.write('    printf(\"--- %d %d %d\\n\", i+y_offsets[0], i+y_offsets[1], i+y_offsets[2]);')
#        f.write('    printf(\"---- %f %f ?\\n\", y[0], dy[0]);')

#        f.write('    }\n')
        f.write('    }\n')
       #  f.write('    for(i=0; i<Nf; ++i) dy[fixed[i]] = 0.0;\n')

#        f.write('    printf(\"%f %f ?\\n\", y[0], dy[0]);')
        f.write('}\n')
        f.close()

        os.system("gcc -g -shared -fPIC -lm -o test.so test.c")
#        os.system("gcc -O2 -shared -fPIC -lm -o test.so test.c")
        self.deriv_ptr = dlopen('test.so', 'deriv')   


class CodeGenTransportModel(object):
    def __init__(self, model, t_model):
        self.model = model
        self.t_model = t_model
        self.params = self.get_parameters()


    def blocked_walls(self, db):
        return self.t_model.blocked_walls(db)

    def get_species(self):
        return set.union(set(self.model.get_species()), set(self.t_model.get_species()))

    def get_modifier_species(self):
        return set.union(set(self.model.get_modifier_species()), set(self.t_model.get_modifier_species())) - self.get_species()

    def get_parameters(self):
        p = self.model.get_parameters()
        p.update(self.t_model.get_parameters().iteritems())
        return p


    def gen_model(self):
        # Generate code for single-cell RHS.
        model = self.model
        t_model = self.t_model
        params = self.params
        species = self.get_species()
        m_species = self.get_modifier_species()
        
        prop_params = False

        f = open('test.c', 'w')
        f.write('#include <math.h>\n#include <stdio.h>\n\n')
        f.write('void deriv(int* NEQ, double* t, double y[], double dy[])\n')
        f.write('{\n')
        f.write('    const int Nc = NEQ[1];\n')
        f.write('    const int Nf = NEQ[2];\n')
        f.write('    const int Nv = NEQ[3];\n')
        f.write('    const int Lv = NEQ[4];\n')
        f.write('    const int Nw = NEQ[5];\n')
        f.write('    const double* p = &y[NEQ[0]];\n')
        f.write('    const double* ym = &y[NEQ[0]+%d];\n'%len(params))
        off = 6
        f.write('    const int* y_offsets = &NEQ[%d];\n'%off)
        off += len(species)+1
        f.write('    const int* ym_offsets = &NEQ[%d];\n'%off)
        off += len(m_species)+1
        f.write('    const int* fixed = &NEQ[%d];\n'%off)
        
        f.write('    const int* vertex = &NEQ[Nf+%d];\n'%off)
        f.write('    const int* walls = &NEQ[Nf+Lv+%d];\n'%off)

        for i, (p,v) in enumerate(params.iteritems()):
#            f.write('    const double '+p+ ' = '+str(v)+';\n')
            f.write('    const double '+p+ ' = p['+str(i)+'];\n')


#        f.write('    printf(\"%f %d\\n\", y[0], Nc);')

        f.write('    int i=0;\n    for(i=0; i<y_offsets[%d]; ++i) dy[i]=0.0;\n'%(len(species)))

        deriv = model.deriv()
        param_idx = dict((n,i) for i,n in enumerate(params))
        species_idx = dict((n[0],i) for i,n in enumerate(species))
        m_species_idx = dict((n[0],i) for i,n in enumerate(m_species))
        pp = ParameterPropagator(param_idx)
        yp = YPropagator('y', 'y_offsets', species_idx)
        ymp = YPropagator('ym', 'ym_offsets', m_species_idx)
        
        #for j, s in enumerate(species):
        #    if s[1] in [v[0] for v in model.get_species()]:

        for v in species:
            f.write('// '+v[0]+'  :  '+str(species_idx[v[0]])+'\n')

        for v in m_species:
            f.write('// '+v[0]+'  :  '+str(m_species_idx[v[0]])+'\n')

        f.write('    for(i=0; i<Nc; ++i) {\n')
        
        for v in model.get_species():
            
            r = deriv[v[0]]
            j = species_idx[v[0]]
#(s, r) in deriv.iteritems():
            if prop_params:
                pp.visit(r)
            yp.visit(r)
            ymp.visit(r)
            f.write('        dy[i+y_offsets['+str(j)+']] += '+codegen.to_source(r)+';\n')
#        f.write('    if(i==0) {\n')
#        f.write('    printf(\"--- %d %d %d\\n\", i+y_offsets[0], i+y_offsets[1], i+y_offsets[2]);')
#        f.write('    printf(\"---- %f %f ?\\n\", y[0], dy[0]);')

#        f.write('    }\n')
        f.write('    }\n')

        # Now handle transport stuff ....

        # Vertices

        # Extract mean expressions from rate

        d = t_model.deriv_vertex()
        m = MeanExtractor()
        for s, r in d.iteritems():
            m.visit(r)

        print m.mean_expr

        f.write('    int off = 0;\n')
        f.write('    for(i=0; i<Nv; i++) {\n')
        f.write('        int nw = vertex[off];\n')
        f.write('        int j, widx;\n')
        for i in range(len(m.mean_expr)):
            f.write('        double mean_%d = 0.0;\n'%i)
        f.write('        for(j=0; j<nw; j++) { \n')
        f.write('            widx = vertex[off+j+1];\n')

        w_yp = YPropagator('y', 'y_offsets', species_idx, 'widx')
        w_ymp = YPropagator('ym', 'ym_offsets', m_species_idx, 'widx')

        for i in range(len(m.mean_expr)):
            r = m.mean_expr[i][1]
            if prop_params:
                pp.visit(r)
            w_yp.visit(r)
            w_ymp.visit(r)
            f.write(('            mean_%d += '%i) +codegen.to_source(r)+';\n')
        f.write('        }\n')
        for i in range(len(m.mean_expr)):
            f.write('        mean_%d /= nw;\n'%i)
        f.write('        for(j=0; j<nw; j++) { \n')
        f.write('            widx = vertex[off+j+1];\n')
        print species
        for s,r in d.iteritems():
            j = species_idx[s]
            if prop_params:
                pp.visit(r)
            w_yp.visit(r)
            w_ymp.visit(r)
            f.write('            dy[widx+y_offsets['+str(j)+']] += '+codegen.to_source(r)+';\n')
        f.write('        }\n')
                
        

        f.write('        off += nw+1;\n')
        f.write('    }\n')
        
        d = t_model.deriv_wall()
        f.write('    off=0;\n')
        f.write('    for(i=0; i<Nw; i++) {\n')
        f.write('         int widx, cidx, eidx;\n')
        f.write('         widx = walls[3*i];\n')
        f.write('         cidx = walls[3*i+1];\n')
        f.write('         eidx = walls[3*i+2];\n')
 #       f.write('    printf(\"%d %d %d ?\\n\", cidx, widx, eidx);\n')
        cell_species_idx = dict((n[0],i) for i,n in enumerate(species) if n[1]=='cell')
        wall_species_idx = dict((n[0],i) for i,n in enumerate(species) if n[1]=='wall')
        edge_species_idx = dict((n[0],i) for i,n in enumerate(species) if n[1]=='edge')
 
        cell_m_species_idx = dict((n[0],i) for i,n in enumerate(m_species) if n[1]=='cell')
        wall_m_species_idx = dict((n[0],i) for i,n in enumerate(m_species) if n[1]=='wall')
        edge_m_species_idx = dict((n[0],i) for i,n in enumerate(m_species) if n[1]=='edge')

        cell_yp = YPropagator('y', 'y_offsets', cell_species_idx, 'cidx')
        cell_ymp = YPropagator('ym', 'ym_offsets', cell_m_species_idx, 'cidx')
        wall_yp = YPropagator('y', 'y_offsets', wall_species_idx, 'widx')
        wall_ymp = YPropagator('ym', 'ym_offsets', wall_m_species_idx, 'widx')
        edge_yp = YPropagator('y', 'y_offsets', edge_species_idx, 'eidx')
        edge_ymp = YPropagator('ym', 'ym_offsets', edge_m_species_idx, 'eidx')
        
        for s in d:
            r = d[s]    
#            for v in [cell_yp, cell_ymp, wall_yp, wall_ymp, edge_yp, edge_ymp, pp]:
            for v in [cell_yp, cell_ymp, wall_yp, wall_ymp, edge_yp, edge_ymp]:
                v.visit(r)
            if s in cell_species_idx:
#                f.write('    if(i==0) printf(\"%d %f ?\\n\", cidx, dy[cidx+y_offsets['+str(species_idx[s])+']]);\n')
                f.write('        dy[cidx+y_offsets['+str(species_idx[s])+']] += '+codegen.to_source(r)+';\n')
#                f.write('    if(i==0) printf(\"%d %f ??\\n\", cidx, dy[cidx+y_offsets['+str(species_idx[s])+']]);\n')
            if s in wall_species_idx:
#                f.write('    if(i==0) printf(\"%d %f w?\\n\", widx, dy[widx+y_offsets['+str(species_idx[s])+']]);\n')
                f.write('        dy[widx+y_offsets['+str(species_idx[s])+']] += '+codegen.to_source(r)+';\n')
#                f.write('    if(i==0) printf(\"%d %f w??\\n\", widx, dy[widx+y_offsets['+str(species_idx[s])+']]);\n')

            if s in edge_species_idx:
                f.write('        dy[eidx+y_offsets['+str(species_idx[s])+']] += '+codegen.to_source(r)+';\n')
        f.write('    }\n')
        f.write('    for(i=0; i<Nf; ++i) dy[fixed[i]] = 0.0;\n')

#        f.write('    printf(\"%f %f ?\\n\", y[0], dy[0]);')
        f.write('}\n')
        f.close()

        os.system("gcc -g -shared -fPIC -lm -o test.so test.c")
#        os.system("gcc -O2 -shared -fPIC -lm -o test.so test.c")
        self.deriv_ptr = dlopen('test.so', 'deriv')   


class CGGeneNetwork(object):
            
    def post_iterations(self):
        self.model.post_iterations(self.db)

    def __init__(self, db, cgmodel, updateVS=True):
        """ 
        Initialise the networks 
        :param db: Tissue database
        :type db: TissueDB
        :param model: implementation of AbstractModel
        """
        self.updateVS = updateVS

        self.db = db
        # Construct a model for each of the model classes
        self.cgmodel = cgmodel
        self.model_species = cgmodel.model.get_species()
        self.modifier_species = cgmodel.model.get_modifier_species() 

        # Set default parameters in the TissueDB parameter dictionary
        cgmodel.model.set_default_parameters(db)

        initial_values = get_parameters(db, 'initial_values')

        
        divided_props = db.get_property('divided_props')
        # Initialise all of the species needed which are not already
        # contained in the TissueDB
        for species_name, species_type  in set(self.model_species) | set(self.modifier_species):
            if species_name not in db.properties():
                add_property(db, 
                             species_name,
                             species_type,
                             initial_values.get(species_name, 0.0))
            if species_name not in divided_props and species_name not in ('V', 'S'):
                if 'auxin' in species_name:
                    divided_props[species_name] = (species_type, 'concentration', 0.0)
                else:
                    divided_props[species_name] = (species_type, 'amount', 0.0)

        print self.model_species
        

        # Set the values for those species which are fixed
        set_fixed_properties(db)


        self.model_gen = False

    def step_dt(self, dt):
        """
        Evolve the gene network for one period of length d
        :param dt: float - timestep
        """
        if self.model_gen ==False:
            self.cgmodel.gen_model()
            self.model_gen = True

        
        set_fixed_properties(self.db)
        #print 'genenetwork.step_dt'
        
        t=self.db.tissue()

        # Update the cell volumes and wall surface areas - these may
        # have been changed through tissue growth
        if self.updateVS:
            updateVS(self.db)

        self.cgmodel.model.pre_step(self.db)

        # Recalculate the wall to edge map, and the tissue maps
        wall_decomposition = get_wall_decomp(self.db)
        tissue_maps = get_tissue_maps(self.db)
     
        # For each model species, calculate the slice (indices) of
        # the y[] array which will store property values during 
        # integration
        offset=0 # current offset into the y[] array
        species_slices={} # map from species name to the slice of y[]
        species_offsets = []
        for species_name, species_type in self.model_species:
            # 'size' is how many values are needed to store a property
            # of that type
            size = len(tissue_maps[species_type])
            species_slices[species_name] = slice(offset, offset+size)
            species_offsets.append(offset)
            offset+=size
        species_offsets.append(offset)

        offset = 0
        modifier_offsets = []
        modifier_slices = {}
        for species_name, species_type in self.modifier_species:
            # 'size' is how many values are needed to store a property
            # of that type
            size = len(tissue_maps[species_type])
            modifier_offsets.append(offset)
            modifier_slices[species_name] = slice(offset, offset+size)
            offset+=size
        modifier_offsets.append(offset)

        # Extract mesh
        cfg = self.db.get_config('config')
        mesh = t.relation(cfg.mesh_id)
        # Type of each cell
        cell_type = self.db.get_property('cell_type')
        
        # Call routine to find fixed property values
        fixed_idx=self.get_fixed_idx(tissue_maps, species_slices)

        #initialise with zero
        y0=np.zeros((species_offsets[-1],))

        ### SETUP INITIAL VALUES
        # Loop over all species referred to in these models
        for species_name, species_type in self.model_species:
            # extract property dict from TissueDB
            prop=self.db.get_property(species_name)
            # obtain appropriate section of y0[]
            y0_slice=y0[species_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems() :
                y0_slice[idx]=prop[tid]
    
        ym=np.zeros((modifier_offsets[-1],))

        ### SETUP INITIAL VALUES
        # Loop over all species referred to in these models
        for species_name, species_type in self.modifier_species:
            # extract property dict from TissueDB
            prop=self.db.get_property(species_name)
            # obtain appropriate section of y0[]
            ym_slice=ym[modifier_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems() :
                ym_slice[idx]=prop[tid]

        ### SETUP PARAMETER ARRAY

#        params = self.cgmodel.model.get_parameters()
        params = self.cgmodel.params
        p = np.array([params[v] for v in params])
        
#        print p
#        print ym
        Nc = mesh.nb_wisps(2)
        y0 = np.hstack((y0,p,ym))
        neq = np.hstack(([len(self.model_species)*Nc], 
                         [Nc], 
                         [len(fixed_idx)],
                         [i*Nc for i in range(len(self.model_species)+1)],
                         [i*Nc for i in range(len(self.modifier_species)+1)],
                         fixed_idx))
        neq = neq.astype('int32')
 
        JPat = scipy.sparse.kron(np.ones((len(self.model_species), len(self.model_species))), scipy.sparse.eye(Nc,Nc))
        ###INTEGRATE OVER TIMESTEP
        print "start"
#        print y0.dtype, neq.dtype
#        print neq.flags
        JPat = JPat.tocsr()
#        print JPat, JPat.shape
        integrated = odeints_cfunc(self.cgmodel.deriv_ptr, y0, [0., dt], neq=neq, lrw=100000000, JPat=JPat, mxstep=100) 
        print "stop"

        ###SET SPECIES TO NEW VALUE (AFTER TIMESTEP)
        yend=integrated[-1,:]
        
        print yend
        print yend.shape

#        quit()

        # Update the values of the species in the tissue properties
        # from the state vector at the end of the integration
        for species_name, species_type in self.model_species: 
            prop=self.db.get_property(species_name)
            
            yend_slice=yend[species_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems() :
                prop[tid]=yend_slice[idx]


        #print 'genenetwork.step_dt done'
        self.cgmodel.model.post_step(self.db)

    def step(self):
        """
        Simulate the system for one timestep
        """

        dt=get_parameters(self.db, 'timestep')
        self.step_dt(dt)

    def steady_state(self):
        """
        Evolve the system for a long period of time (1000s)
        in an attempt to approach the steady-state - needs more work
        """
        self.step_dt(1e3)

    def get_fixed_idx(self, tissue_maps, species_slices):
        """
        Obtain a list of those elements in the ODE state vector y
        which are constant during the simulation
        (From the property 'fixed' in the tissue database.)
        This currently only works for cell-based properties
        
        The dictionary fixed maps a property name to a list
        of (expression, value) rules; for each cell, if the expression
        is satisfied, then the property is fixed
        This is a Python expression, and may depend on the cell
        id number and the values of all the properties of the tissue.

        e.g. fixed = { 'auxin':[ ('cell_type[cid]=='pericycle', 2) ] }
        would fix the auxin concentration in the pericycle.

        :param tissue_maps:
        :param species_slices:
        :returns: list of the offsets into the state vector y[] which
                  are to be held constant.

        """
        fixed = get_parameters(self.db, 'fixed')
        
        cell_map = tissue_maps['cell']
        fixed_idx = [] # list of fixed indices

        # Extract all the properties from the tissue data,
        # so these can be used in the eval statement
        all_properties=dict((name, self.db.get_property(name)) \
                                for name in self.db.properties())
        # Loop over all properties with rules
        for prop_name, rules in fixed.iteritems():
            # Ignore properties not in model
            if prop_name in species_slices:
                s=species_slices[prop_name]
                prop=self.db.get_property(prop_name)
                for (expr, value) in rules:
                    # Parse expression and convert it to python
                    # bytecode
                    code=compile(expr, '', 'eval')
                    # Loop over all cells
                    for cid, i in cell_map.iteritems():
                        # Test the expression for this cid
                        if eval(code, all_properties, { 'cid':cid }):
                            idx=s.start+i
                            fixed_idx.append(idx) 
        return fixed_idx

class CGTransportGeneNetwork(object):

    def __init__(self, db, cgmodel, updateVS=True):
        """ 
        Initialise the networks 
        :param db: Tissue database
        :type db: TissueDB
        :param model: implementation of AbstractModel
        """
        self.updateVS = updateVS
        self.db = db
        # Construct a model for each of the model classes
        self.cgmodel = cgmodel
        self.model_species = cgmodel.get_species()
        self.modifier_species = cgmodel.get_modifier_species() 

        # Set default parameters in the TissueDB parameter dictionary
#        cgmodel.set_default_parameters(db)

        initial_values = get_parameters(db, 'initial_values')

        divided_props = db.get_property('divided_props')
        # Initialise all of the species needed which are not already
        # contained in the TissueDB
        for species_name, species_type  in set(self.model_species) | set(self.modifier_species):
            if species_name not in db.properties():
                add_property(db, 
                             species_name,
                             species_type,
                             initial_values.get(species_name, 0.0))
            if species_name not in divided_props and species_name not in ('V', 'S'):
                if 'auxin' in species_name:
                    divided_props[species_name] = (species_type, 'concentration', 0.0)
                else:
                    divided_props[species_name] = (species_type, 'amount', 0.0)

        print self.model_species

        # Set the values for those species which are fixed
        set_fixed_properties(db)
        self.model_gen=False

        


    def step_dt(self, dt):
        """
        Evolve the gene network for one period of length d
        :param dt: float - timestep
        """

        if self.model_gen ==False:
            self.cgmodel.gen_model()
            self.model_gen = True

        
        set_fixed_properties(self.db)
        #print 'genenetwork.step_dt'
        
        t=self.db.tissue()

        # Update the cell volumes and wall surface areas - these may
        # have been changed through tissue growth
        if self.updateVS:
            updateVS(self.db)

        self.cgmodel.model.pre_step(self.db)

        # Recalculate the wall to edge map, and the tissue maps
        wall_decomposition = get_wall_decomp(self.db)
        tissue_maps = get_tissue_maps(self.db)
        wall = self.db.get_property('wall')

        # For each model species, calculate the slice (indices) of
        # the y[] array which will store property values during 
        # integration
        offset=0 # current offset into the y[] array
        species_slices={} # map from species name to the slice of y[]
        species_offsets = []
        for species_name, species_type in self.model_species:
            # 'size' is how many values are needed to store a property
            # of that type
            size = len(tissue_maps[species_type])
            print species_name, species_type, size
            species_slices[species_name] = slice(offset, offset+size)
            species_offsets.append(offset)
            offset+=size
        species_offsets.append(offset)

        offset = 0
        modifier_offsets = []
        modifier_slices = {}
        for species_name, species_type in self.modifier_species:
            # 'size' is how many values are needed to store a property
            # of that type
            size = len(tissue_maps[species_type])
            modifier_offsets.append(offset)
            modifier_slices[species_name] = slice(offset, offset+size)
            offset+=size
        modifier_offsets.append(offset)

        # Extract mesh
        cfg = self.db.get_config('config')
        mesh = t.relation(cfg.mesh_id)
        # Type of each cell
        cell_type = self.db.get_property('cell_type')
        
        # Call routine to find fixed property values
        fixed_idx=self.get_fixed_idx(tissue_maps, species_slices)

        #initialise with zero
        y0=np.zeros((species_offsets[-1],))

        ### SETUP INITIAL VALUES
        # Loop over all species referred to in these models
        for species_name, species_type in self.model_species:
            # extract property dict from TissueDB
            prop=self.db.get_property(species_name)
            # obtain appropriate section of y0[]
            y0_slice=y0[species_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems() :
                y0_slice[idx]=prop[tid]

    
        ym=np.zeros((modifier_offsets[-1],))

        ### SETUP MODIFIER SPECIES VALUES
        # Loop over all species referred to in these models
        for species_name, species_type in self.modifier_species:
            # extract property dict from TissueDB
            prop=self.db.get_property(species_name)
            # obtain appropriate section of y0[]
            ym_slice=ym[modifier_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems():
                ym_slice[idx]=prop[tid]

        ### SETUP PARAMETER ARRAY
        params = self.cgmodel.params
        p = np.array([params[v] for v in params])

        """
        f.write('    const int Nc = NEQ[1];\n')
        f.write('    const int Nf = NEQ[2];\n')
        f.write('    const int Lv = NEQ[3];\n')
        f.write('    const int Nw = NEQ[4];\n')
        """
        vertices = []
        eid_map = tissue_maps['edge']
        wid_map = tissue_maps['wall']
        cid_map = tissue_maps['cell']
        Nv = 0
        blocked_walls = self.cgmodel.blocked_walls(self.db)

        for pid in mesh.wisps(0):
            vl = [wid_map[wid] for wid in mesh.regions(0, pid) if wid not in blocked_walls]
            if len(vl)>1:
                Nv+=1
                vertices.append(len(vl))
                vertices.extend(vl)

        Lv = len(vertices)
        
        o_wall_array = []
        graph = get_graph(self.db)
        for eid in graph.edges():
            cidx = cid_map[graph.source(eid)]
            widx = wid_map[wall[eid]]
            eidx = eid_map[eid]
            o_wall_array.append((widx, cidx, eidx))
        
        Nw =len(o_wall_array)
        wall_array=[ i for wa in o_wall_array for i in wa ]
        print wall_array[0:17]

#        print p
#        print ym

        y_offsets = []
        for s in self.model_species:
            y_offsets.append(species_slices[s[0]].start)
        y_offsets.append(species_slices[s[0]].stop)

        ym_offsets = []
        for s in self.modifier_species:
            ym_offsets.append(modifier_slices[s[0]].start)
        ym_offsets.append(modifier_slices[s[0]].stop)

        Nc = mesh.nb_wisps(2)
        y0 = np.hstack((y0,p,ym))
        neq = np.hstack(([y_offsets[-1]], 
                         [Nc], 
                         [len(fixed_idx)],
                         [Nv],
                         [Lv],
                         [Nw],
                         y_offsets,
                         ym_offsets,
                         fixed_idx,
                         vertices,
                         wall_array))

        neq = neq.astype('int32')

        start_time = time.time()
        """
        JPat = scipy.sparse.lil_matrix((y_offsets[-1], y_offsets[-1]))
        cell_deps = self.cgmodel.model.deps()
        transport_deps = self.cgmodel.t_model.deps()
        print cell_deps
        for s in cell_deps:
            s_slice = species_slices[s]
            for t in cell_deps[s]:
                if t in [_[0] for _ in self.model_species]:
                    t_slice = species_slices[t]
                    JPat[s_slice, t_slice] = 1
        transport_species = self.cgmodel.t_model.get_species()
        wall_t_slices = [species_slices[s[0]] for s in transport_species if s[1]=='wall']
        cell_t_slices = [species_slices[s[0]] for s in transport_species if s[1]=='cell']
        edge_t_slices = [species_slices[s[0]] for s in transport_species if s[1]=='edge']
        for (widx, cidx, eidx) in o_wall_array:
            # fill this bit of the matrix
            idx = [s.start+widx for s in wall_t_slices] + [s.start+cidx for s in cell_t_slices]+[s.start+eidx for s in edge_t_slices]
            for i in idx:
                JPat[i, idx] = 1
         """


#        JPat = scipy.sparse.kron(np.ones((len(self.model_species), len(self.model_species))), scipy.sparse.eye(Nc,Nc))
        ###INTEGRATE OVER TIMESTEP
        print "start" , time.time() - start_time
#        print y0.dtype, neq.dtype
#        print neq.flags
#        JPat = JPat.tocsr()
#        print JPat, JPat.shape
        integrated = odeints_cfunc(self.cgmodel.deriv_ptr, y0, [0., dt], neq=neq, lrw=100000000, mxstep=10000, rtol=1e-8) 
        print "stop", time.time() - start_time


        ###SET SPECIES TO NEW VALUE (AFTER TIMESTEP)
        yend=integrated[-1,:]
        
        print yend
        print yend.shape

#        quit()

        # Update the values of the species in the tissue properties
        # from the state vector at the end of the integration
        for species_name, species_type in self.model_species: 
            prop=self.db.get_property(species_name)
            yend_slice=yend[species_slices[species_name]]
            print species_name, species_slices[species_name].start, species_slices[species_name].stop
            for tid, idx in tissue_maps[species_type].iteritems():
                prop[tid]=yend_slice[idx]


        #print 'genenetwork.step_dt done'
        self.cgmodel.model.post_step(self.db)

    def step(self):
        """
        Simulate the system for one timestep
        """
        dt=get_parameters(self.db, 'timestep')
        self.step_dt(dt)

    def steady_state(self):
        """
        Evolve the system for a long period of time (1000s)
        in an attempt to approach the steady-state - needs more work
        """
        self.step_dt(1e3)


    def get_fixed_idx(self, tissue_maps, species_slices):
        """
        Obtain a list of those elements in the ODE state vector y
        which are constant during the simulation
        (From the property 'fixed' in the tissue database.)
        This currently only works for cell-based properties
        
        The dictionary fixed maps a property name to a list
        of (expression, value) rules; for each cell, if the expression
        is satisfied, then the property is fixed
        This is a Python expression, and may depend on the cell
        id number and the values of all the properties of the tissue.

        e.g. fixed = { 'auxin':[ ('cell_type[cid]=='pericycle', 2) ] }
        would fix the auxin concentration in the pericycle.

        :param tissue_maps:
        :param species_slices:
        :returns: list of the offsets into the state vector y[] which
                  are to be held constant.

        """
        fixed = get_parameters(self.db, 'fixed')
        
        cell_map = tissue_maps['cell']
        fixed_idx = [] # list of fixed indices

        # Extract all the properties from the tissue data,
        # so these can be used in the eval statement
        all_properties=dict((name, self.db.get_property(name)) \
                                for name in self.db.properties())
        # Loop over all properties with rules
        for prop_name, rules in fixed.iteritems():
            # Ignore properties not in model
            if prop_name in species_slices:
                s=species_slices[prop_name]
                prop=self.db.get_property(prop_name)
                for (expr, value) in rules:
                    # Parse expression and convert it to python
                    # bytecode
                    code=compile(expr, '', 'eval')
                    # Loop over all cells
                    for cid, i in cell_map.iteritems():
                        # Test the expression for this cid
                        if eval(code, all_properties, { 'cid':cid }):
                            idx=s.start+i
                            fixed_idx.append(idx) 
        return fixed_idx

def set_fixed_properties(db):
    """
    The dictionary fixed maps a property name to a list
    of (expression, value) rules; for each cell, if the expression
    is satisfied, then the property is set to the value.
    This is a Python expression, and may depend on the cell
    id number and the values of all the properties of the tissue.
    If multiple expressions are satisfied, then all of these
    are applied, so the last in the list is used.

    e.g. fixed = { 'auxin':[ ('cell_type[cid]=='pericycle', 2) ] }
    would set the auxin concentration in the pericycle to 2.

    :param db:  tissue database
    :type db: TissueDB
    """
    # Extract mesh from TissueDB
    t = db.tissue()
    cfg = db.get_config('config')
    mesh = t.relation(cfg.mesh_id)
    # Extract all the properties from the tissue data,
    # so these can be used in the eval statement
    all_properties=dict((name, db.get_property(name)) \
                            for name in db.properties())
    # Loop over all properties with rules 
    for prop_name, rules in get_parameters(db, 'fixed').iteritems():
        # Check if property currently in tissue database
        if prop_name in db.properties():
            prop = db.get_property(prop_name)
            for (expr, value) in rules:
                # compile expression to python bytecode
                code=compile(expr, '', 'eval')
                # loop over all cells
                for cid in mesh.wisps(2):
                    # test whether expr is satisfied for this cell
                    if eval(code, all_properties, {'cid':cid}):
                        prop[cid] = value


"""
c = CodeGenModel(AuxinResponseModel())
d = CodeGenModel(CombinedCGModel([AuxinResponseModel(), AuxinHSModel()]))

c.gen_model()
"""            

        
        

