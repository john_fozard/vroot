from numpy import zeros
from vroot2D.utils.db_utilities import get_mesh, get_graph, get_parameters, \
	 set_parameters
from scipy.sparse import lil_matrix


class GrowthRegulators(object):
    """
    Model for auxin homeostasis, and E2F and PLT regulation by auxin

    Implements AbstractModel
    """
    
    name = 'GrowthRegulators'

    def pre_step(self, db):
        pass

    def post_step(self, db):
        pass

    def get_species(self):
        """
        Species used in model
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        return [('GP', 'cell'), ('DIV', 'cell')]


    def get_modifier_species(self):
        return []

    def set_default_parameters(self, db):
        """
        Set default parameters for this model in the TissueDB
        """
        p = { 'D_p': 2000.0, 'lambda_p':16.0, 'alpha_p':2.0,
              'rate': 0.1, 'kp': 1e-5 }
        set_parameters(db, self.name, p)

    def get_JPat(self, y, t, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):
        graph = get_graph(db)
        mesh = get_mesh(db)
        cell_map=tissue_maps['cell']
        GP_sl = species_slices['GP']
        #print GI_sl.start, GI_sl.step, GI_sl.stop
        GP_s = GP_sl.start
        GP_d = GP_sl.step if GP_sl.step else 1 

        M = lil_matrix((len(y), len(y)))
        for wid,(eid1,eid2) in wall_decomposition.iteritems() :
            sid = graph.source(eid1)
            tid = graph.target(eid1)
            sidx = cell_map[sid]
            tidx = cell_map[tid]
            GP_sidx = GP_s+GP_d*sidx
            GP_tidx = GP_s+GP_d*tidx

            M[GP_sidx, GP_sidx]=1
            M[GP_sidx, GP_tidx]=1
            M[GP_tidx, GP_sidx]=1
            M[GP_tidx, GP_tidx]=1


        DIV_sl = species_slices['DIV']
        DIV_s = DIV_sl.start
        DIV_d = DIV_sl.step if DIV_sl.step else 1
        for i, cid in enumerate(mesh.wisps(2)):
            idx = DIV_s+i*DIV_d
            M[idx, idx]=1
        return M
     
    def deriv(self, y, t, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):
        """
        Right hand side for the system of equations.
        :param y: current state vector
        :type y: numpy.ndarray
        :param t: current simulation time 
        :type t: float
        :param db:  tissue database
        :type db: TissueDB
        :param tissue_maps: dictionary mapping property types ('cell', 'wall',
                            'edge') to a dictionary which takes id numbers to
                            positions in slices of the y for properties of 
                            that type.
                            For example, tissue_maps['cell'] gives a dictionary
                            of the form { 101: 0, 123: 1, 121: 2, ...}
                            so the cell with cid 101 maps to the first element
                            of the slice.
                            y[species_slice['auxin']][tissue_maps['cell'][28]]
                            is the auxin concentration in cell with cid 28
        :type tissue_maps: dict
        :param species_slices: dictionary mapping property names to slices of
                               y. e.g. auxin = y[species_slice['auxin']]
        :type species_slices: dict
        :param wall_decomposition: dictionary which takes a wall id to the
                                   ids of the two corresponding edges.
              
        :param fixed_idx: indices of the elements of y[] which are to be kept
                          constant
        :type fixed_idx: list
        :returns: - numpy.array which is the right hand side of
                        the system of ODEs              
        """
        ret = zeros((len(y),))

        p = get_parameters(db, self.name)
    
        GP_sl = species_slices['GP']
        GP = y[GP_sl]
        DIV_sl = species_slices['DIV']

#        print 'GP', GP

        ret_GP=ret[GP_sl]

        ret[DIV_sl]=p['rate']

        ret_GP += - p['lambda_p']*GP


        cell_type = db.get_property('cell_type')
        alpha_p = p['alpha_p']

        graph = get_graph(db)      
        S = db.get_property('S')
        V = db.get_property('V')
        cell_map = tissue_maps['cell']
        edge_map = tissue_maps['edge']
        
        mesh = get_mesh(db)
        for cid in mesh.wisps(2):
            if cell_type[cid]==9:
#                print cid
                i = cell_map[cid]
                ret_GP[i]+=alpha_p


        D_p = p['D_p']


        for wid,(eid1,eid2) in wall_decomposition.iteritems() :
            sid = graph.source(eid1)
            tid = graph.target(eid1)
            Vs = V[sid]
            Vt = V[tid]
            Swall = S[wid]
            sidx = cell_map[sid]
            tidx = cell_map[tid]
            sGP = GP[sidx]
            tGP = GP[tidx]
            JP = Swall*D_p*(sGP-tGP)
            ret_GP[sidx]+=-JP/Vs
            ret_GP[tidx]+=JP/Vt


            
        for i in fixed_idx:
            ret[i] = 0.0

        return ret



class AuxinDelay(object):
    """
    Model for auxin homeostasis, and E2F and PLT regulation by auxin

    Implements AbstractModel
    """
    
    name = 'AuxinDelay'

    def get_species(self):
        """
        Species used in model
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        return [('auxin', 'cell'), ('response', 'cell')]

    def set_default_parameters(self, db):
        """
        Set default parameters for this model in the TissueDB
        """
        p = { 'K': 1.0, 'alpha': 0.2, 'gamma': 0.2, 'n':1, 'eps':0.01 }
        set_parameters(db, self.name, p)

    def get_JPat(self, y, t, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):
        mesh = get_mesh(db)
        cell_map=tissue_maps['cell']
        a_sl = species_slices['auxin']
        r_sl = species_slices['response']
        #print GI_sl.start, GI_sl.step, GI_sl.stop
        a_s = a_sl.start
        a_d = a_sl.step if a_sl.step else 1 
        r_s = r_sl.start
        r_d = r_sl.step if r_sl.step else 1 
        M = lil_matrix((len(y), len(y)))
        for cid in mesh.wisps(2):
            idx = cell_map[cid]
            a_idx = a_s+a_d*idx
            r_idx = r_s+r_d*idx
            M[r_idx, a_idx] = 1
            M[r_idx, r_idx] = 1

        return M
     
    def deriv(self, y, t, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):
        """
        Right hand side for the system of equations.
        :param y: current state vector
        :type y: numpy.ndarray
        :param t: current simulation time 
        :type t: float
        :param db:  tissue database
        :type db: TissueDB
        :param tissue_maps: dictionary mapping property types ('cell', 'wall',
                            'edge') to a dictionary which takes id numbers to
                            positions in slices of the y for properties of 
                            that type.
                            For example, tissue_maps['cell'] gives a dictionary
                            of the form { 101: 0, 123: 1, 121: 2, ...}
                            so the cell with cid 101 maps to the first element
                            of the slice.
                            y[species_slice['auxin']][tissue_maps['cell'][28]]
                            is the auxin concentration in cell with cid 28
        :type tissue_maps: dict
        :param species_slices: dictionary mapping property names to slices of
                               y. e.g. auxin = y[species_slice['auxin']]
        :type species_slices: dict
        :param wall_decomposition: dictionary which takes a wall id to the
                                   ids of the two corresponding edges.
              
        :param fixed_idx: indices of the elements of y[] which are to be kept
                          constant
        :type fixed_idx: list
        :returns: - numpy.array which is the right hand side of
                        the system of ODEs              
        """
        ret = zeros((len(y),))

        p = get_parameters(db, self.name)
        auxin_sl = species_slices['auxin']
        r_sl = species_slices['response']
        auxin = y[auxin_sl]
        r = y[r_sl]

        ret[r_sl]=p['alpha']/(1+(p['K']/(auxin+p['eps']))**p['n'])-p['gamma']*r

        return ret

