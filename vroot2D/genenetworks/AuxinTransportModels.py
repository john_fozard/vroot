from numpy import zeros
from vroot2D.utils.db_utilities import get_mesh, get_graph, get_parameters, set_parameters, get_wall_decomp
import numpy as np
from vroot2D.utils.celltissue_util import def_property
from scipy.sparse import lil_matrix
from math import log, sqrt, exp

class at_cwd(object):
    """
    Auxin transport model with apoplastic auxin diffusion.
    """

    name = 'at_cwd'

    def pre_step(self, db):
        pass

    def post_step(self, db):
        pass


    def get_JPat(self, y, t, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):
        graph = get_graph(db)
        cell_map=tissue_maps['cell']
        wall_map=tissue_maps['wall']
        a_sl = species_slices['auxin']
        aw_sl = species_slices['auxin_wall']


        a_s = a_sl.start
        a_d = a_sl.step if a_sl.step else 1 
        aw_s = aw_sl.start
        aw_d = aw_sl.step if aw_sl.step else 1 
        M = lil_matrix((len(y), len(y)))
        for wid,(eid1,eid2) in wall_decomposition.iteritems() :
            sid = graph.source(eid1)
            tid = graph.target(eid1)
            sidx = cell_map[sid]
            tidx = cell_map[tid]
            widx = wall_map[wid]
            a_sidx = a_s+a_d*sidx
            a_tidx = a_s+a_d*tidx
            a_widx = aw_s+aw_d*widx

            M[a_sidx, a_sidx]=1
            M[a_sidx, a_widx]=1
            M[a_widx, a_sidx]=1
            M[a_widx, a_widx]=1
            M[a_widx, a_tidx]=1
            M[a_tidx, a_widx]=1
            M[a_tidx, a_tidx]=1

        mesh = get_mesh(db)
        for wid in mesh.wisps(1):
            widx = wall_map[wid]
            a_widx = aw_s + aw_d*widx
            for wid2 in mesh.border_neighbors(1,wid):
                widx2 = wall_map[wid2]
                a_widx2 = aw_s + aw_d*widx2
                M[a_widx, a_widx2]=1
                M[a_widx2, a_widx]=1


        return M

    def get_species(self):
        """  
        Species used in model
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        return [('auxin', 'cell'), ('auxin_wall', 'wall')]

    def get_modifier_species(self):
        """  
        Species used in model but which aren't modified by it
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        return [('PIN', 'edge'), ('AUX', 'edge'), 
                ('LAX', 'edge'), ('P', 'edge')]



    def default_parameters(self):
        """
        Set default parameters for this model in the TissueDB
        """
        # These are defined in 'auxin_transport.py'
        # e.g. see lines 34--53

        pHapo=5.3 # apoplastic pH
        pHcyt=7.2 # cytoplasmic pH
        pK=4.8
        Voltage=-0.120  # Membrane potential (Volts)
        Temp=300 # K
        PIAAH=0.56  # Permeability of membrane to protonated auxin (microns/sec)
	PAUX=0.56 # Permeability of membrane to anionic auxin due to AUX1 (microns/sec)
	PLAX=0.56 #0.56 # Permeability of membrane to anionic auxin due to AUX1 (microns/sec)
	PPIN=0.56   # Permeability of membrane to anionic auxin due to PINs (microns/sec)
	PNPE=0.56*0.3   # Permeability of membrane to anionic auxin due to PINs (microns/sec)	
	lamb=0.14  # Cell-wall thickness (microns)
	D_cw= 32    
        alpha_back=0.001  # Auxin production rate in all cells except QC and columella initials
	alpha_QCinit=0.01 # Auxin production rate in QC and columella initials
	beta =0.001 # Auxin degradation rate

        # the below parameters are derived from the others - if we want
        # to be able to change the upper ones they need recalculating

        """
        phi=Fdconst*Voltage/(Rconst*Temp)	
        A1=1/(1+pow(10,pHapo-pK)) 
        A2=(1-A1)*phi/(exp(phi)-1)
        A3=-(1-A1)*phi/(exp(-phi)-1)
        B1=1/(1+pow(10,pHcyt-pK))
        B2=-(1-B1)*phi/(exp(-phi)-1)
        B3=(1-B1)*phi/(exp(phi)-1)
        """
        # Make dictionary of the parameters for this model -
        """
        p = { 'pHapo': pHapo, 'pHcyt': pHcyt, 'Voltage':Voltage, 'A1':A1, 'A2':A2, 'A3':A3, 'B1':B1, 'B2':B2, 'B3':B3, 
               'PIAAH':PIAAH, 'PAUX':PAUX, 'PPIN':PPIN, 'lamb':lamb, 'D_cw':D_cw, 'alpha_back':alpha_back,'alpha_QCinit':alpha_QCinit, 'beta':beta, 'Temp':Temp, 'pK':pK }
               
        """
        # Alternative form for parameters
        p = { 'pHapo': pHapo, 'pHcyt': pHcyt, 'Voltage':Voltage, 
               'PIAAH':PIAAH, 'PAUX':PAUX,'PLAX':PLAX, 'PPIN':PPIN, 'PNPE':PNPE, 'lamb':lamb, 'D_cw':D_cw, 'alpha_back':alpha_back,'alpha_QCinit':alpha_QCinit,'beta':beta, 'Temp':Temp, 'pK':pK }

        return p

    def set_default_parameters(self, db):
        """
        Set default parameters for this model in the TissueDB
        """
#        if self.name not in param_dict:
#            set_parameters(param_dict, self.name, self.default_parameters())
        p = self.default_parameters()
        set_parameters(db, self.name, p)

	    
    def deriv(self, y, t, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):
        """
        Right hand side for the system of equations.
        :param y: current state vector
        :type y: numpy.ndarray
        :param t: current simulation time 
        :type t: float
        :param db:  tissue database
        :type db: TissueDB
        :param tissue_maps: dictionary mapping property types ('cell', 'wall',
                            'edge') to a dictionary which takes id numbers to
                            positions in slices of the y for properties of 
                            that type.
                            For example, tissue_maps['cell'] gives a dictionary
                            of the form { 101: 0, 123: 1, 121: 2, ...}
                            so the cell with cid 101 maps to the first element
                            of the slice.
                            y[species_slice['auxin']][tissue_maps['cell'][28]]
                            is the auxin concentration in cell with cid 28
        :type tissue_maps: dict
        :param species_slices: dictionary mapping property names to slices of
                               y. e.g. auxin = y[species_slice['auxin']]
        :type species_slices: dict
        :param wall_decomposition: dictionary which takes a wall id to the
                                   ids of the two corresponding edges.
              
        :param fixed_idx: indices of the elements of y[] which are to be kept
                          constant
        :type fixed_idx: list
        :returns: - numpy.array which is the right hand side of
                        the system of ODEs              
        """

        ret = zeros((len(y), ))
        p = get_parameters(db, self.name)

        mesh = get_mesh(db)

        Auxin_sl = species_slices['auxin']
        Auxin = y[Auxin_sl]
        Auxin_wall_sl = species_slices['auxin_wall']
        Auxin_wall = y[Auxin_wall_sl]	

        graph = get_graph(db)      
        S = db.get_property('S')
        V = db.get_property('V')
        PIN = db.get_property('PIN')
        AUX = db.get_property('AUX')
	LAX = db.get_property('LAX')
	cell_type = db.get_property('cell_type')

        if 'P' in db.properties():
            P = db.get_property('P')
        else:
            P = dict((eid, 1.0) for eid in graph.edges())
        
        cell_map = tissue_maps['cell']
        edge_map = tissue_maps['edge']
	wall_map = tissue_maps['wall']

        """
        A1 = p['A1']
        A2 = p['A2']
        A3 = p['A3']
        B1 = p['B1']
        B2 = p['B2']
        B3 = p['B3']         
        """
        # If we want to vary voltage etc. A1-B3 need to be recalculated here
        pHapo=p['pHapo'] # apoplastic pH
        pHcyt=p['pHcyt'] # cytoplasmic pH
        pK=p['pK']
        Voltage=p['Voltage']  # Membrane potential (Volts)
        Temp=p['Temp'] # K

	Rconst=8.31
	Fdconst=96500

        phi=Fdconst*Voltage/(Rconst*Temp)	
        A1=1/(1+pow(10,pHapo-pK)) 
        A2=(1-A1)*phi/(exp(phi)-1)
        A3=-(1-A1)*phi/(exp(-phi)-1)
        B1=1/(1+pow(10,pHcyt-pK))
        B2=-(1-B1)*phi/(exp(-phi)-1)
        B3=(1-B1)*phi/(exp(phi)-1)

	PIAAH = p['PIAAH']
	PAUX = p['PAUX']
	PLAX = p['PLAX']
	PPIN = p['PPIN']
	PNPE = p['PNPE']
	lamb = p['lamb']
	D_cw = p['D_cw']
	alpha_back=p['alpha_back']
  	alpha_high=p['alpha_QCinit']
	beta=p['beta']

	alpha={}
	for cid in graph.vertices():
		if cell_type[cid]==17 or cell_type[cid]==10:	
			alpha[cid]=alpha_high
		else:
			alpha[cid]=alpha_back

        ret_auxin=ret[Auxin_sl]
	ret_auxin_wall=ret[Auxin_wall_sl]

        
        for wid,(eid1,eid2) in wall_decomposition.iteritems() :
            sid = graph.source(eid1)
            tid = graph.target(eid1)
            sidx = cell_map[sid]
            tidx = cell_map[tid]
            widx = wall_map[wid]
            sAux = Auxin[sidx]
            tAux = Auxin[tidx]
            wAux = Auxin_wall[widx]

            PIN1 = PIN[eid1]
            PIN2 = PIN[eid2]
            AUX1 = AUX[eid1]
            AUX2 = AUX[eid2]
	    LAX1 = LAX[eid1]
            LAX2 = LAX[eid2]
	    
            J1=(B1*PIAAH+B2*PAUX*AUX1+B2*PLAX*LAX1+B3*PPIN*PIN1+B3*PNPE)*sAux - \
                (A1*PIAAH+A2*PAUX*AUX1+A2*PLAX*LAX1+A3*PPIN*PIN1+A3*PNPE)*wAux
            J2=(B1*PIAAH+B2*PAUX*AUX2+B2*PLAX*LAX2+B3*PPIN*PIN2+B3*PNPE)*tAux - \
                (A1*PIAAH+A2*PAUX*AUX2+A2*PLAX*LAX2+A3*PPIN*PIN2+A3*PNPE)*wAux

            J1 *= P[eid1]
            J2 *= P[eid2]
            Swall = float(S[wid])

            ret_auxin[sidx] += Swall/V[sid] * (-J1) 
            ret_auxin[tidx] += Swall/V[tid] * (-J2)
            ret_auxin_wall[widx]  += (J1+J2)/lamb

        
	Auxin_vertex={}
        for pid in mesh.wisps(0):
            top = 0.0
            bottom = 0.0
            for wid in mesh.regions(0, pid):
                widx = wall_map[wid]
                top += Auxin_wall[widx]/S[wid]
                bottom += 1/S[wid]
            if bottom!=0.0:
                Auxin_vertex[pid] = top/bottom

	for wid in mesh.wisps(1):
            pid1, pid2 = mesh.borders(1, wid)
            widx = wall_map[wid]
            wAux = Auxin_wall[widx]
            flux1 = 2*D_cw/S[wid]*(Auxin_vertex[pid1]-wAux)
            flux2 = 2*D_cw/S[wid]*(Auxin_vertex[pid2]-wAux)
            ret_auxin_wall[widx] += (flux1+flux2)/S[wid]
        

	for cid in graph.vertices():
	    index_cell = cell_map[cid]
	    ret_auxin[index_cell]+=alpha[cid]-beta*Auxin[index_cell]


        for i in fixed_idx:
            ret[i] = 0.0
        return ret

class at_cwd_casp(object):
    """
    Auxin transport model with apoplastic auxin diffusion.
    """

    name = 'at_cwd'

    def pre_step(self, db):
        pass

    def post_step(self, db):
        pass


    def get_JPat(self, y, t, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):
        graph = get_graph(db)
        cell_map=tissue_maps['cell']
        wall_map=tissue_maps['wall']
        a_sl = species_slices['auxin']
        aw_sl = species_slices['auxin_wall']


        a_s = a_sl.start
        a_d = a_sl.step if a_sl.step else 1 
        aw_s = aw_sl.start
        aw_d = aw_sl.step if aw_sl.step else 1 
        M = lil_matrix((len(y), len(y)))
        for wid,(eid1,eid2) in wall_decomposition.iteritems() :
            sid = graph.source(eid1)
            tid = graph.target(eid1)
            sidx = cell_map[sid]
            tidx = cell_map[tid]
            widx = wall_map[wid]
            a_sidx = a_s+a_d*sidx
            a_tidx = a_s+a_d*tidx
            a_widx = aw_s+aw_d*widx

            M[a_sidx, a_sidx]=1
            M[a_sidx, a_widx]=1
            M[a_widx, a_sidx]=1
            M[a_widx, a_widx]=1
            M[a_widx, a_tidx]=1
            M[a_tidx, a_widx]=1
            M[a_tidx, a_tidx]=1

        mesh = get_mesh(db)
        for wid in mesh.wisps(1):
            widx = wall_map[wid]
            a_widx = aw_s + aw_d*widx
            for wid2 in mesh.border_neighbors(1,wid):
                widx2 = wall_map[wid2]
                a_widx2 = aw_s + aw_d*widx2
                M[a_widx, a_widx2]=1
                M[a_widx2, a_widx]=1


        return M

    def get_species(self):
        """  
        Species used in model
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        return [('auxin', 'cell'), ('auxin_wall', 'wall')]

    def get_modifier_species(self):
        """  
        Species used in model but which aren't modified by it
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        return [('PIN', 'edge'), ('AUX', 'edge'), ('LAX', 'edge'),
                ('casparian', 'wall'), ('P', 'edge')]

    def default_parameters(self):
        """
        Set default parameters for this model in the TissueDB
        """
        # These are defined in 'auxin_transport.py'
        # e.g. see lines 34--53

        pHapo=5.3 # apoplastic pH
        pHcyt=7.2 # cytoplasmic pH
        pK=4.8
        Voltage=-0.120  # Membrane potential (Volts)
        Temp=300 # K
        PIAAH=0.56  # Permeability of membrane to protonated auxin (microns/sec)
	PAUX=0.56 # Permeability of membrane to anionic auxin due to AUX1 (microns/sec)
	PLAX=0.56 #0.56 # Permeability of membrane to anionic auxin due to AUX1 (microns/sec)
	PPIN=0.56   # Permeability of membrane to anionic auxin due to PINs (microns/sec)
	PNPE=0.56*0.3   # Permeability of membrane to anionic auxin due to PINs (microns/sec)	
	lamb=0.14  # Cell-wall thickness (microns)
	D_cw= 32    
        alpha_back=0.001  # Auxin production rate in all cells except QC and columella initials
	alpha_QCinit=0.01 # Auxin production rate in QC and columella initials
	beta =0.002 # Auxin degradation rate

        # the below parameters are derived from the others - if we want
        # to be able to change the upper ones they need recalculating

        """
        phi=Fdconst*Voltage/(Rconst*Temp)	
        A1=1/(1+pow(10,pHapo-pK)) 
        A2=(1-A1)*phi/(exp(phi)-1)
        A3=-(1-A1)*phi/(exp(-phi)-1)
        B1=1/(1+pow(10,pHcyt-pK))
        B2=-(1-B1)*phi/(exp(-phi)-1)
        B3=(1-B1)*phi/(exp(phi)-1)
        """
        # Make dictionary of the parameters for this model -
        """
        p = { 'pHapo': pHapo, 'pHcyt': pHcyt, 'Voltage':Voltage, 'A1':A1, 'A2':A2, 'A3':A3, 'B1':B1, 'B2':B2, 'B3':B3, 
               'PIAAH':PIAAH, 'PAUX':PAUX, 'PPIN':PPIN, 'lamb':lamb, 'D_cw':D_cw, 'alpha_back':alpha_back,'alpha_QCinit':alpha_QCinit, 'beta':beta, 'Temp':Temp, 'pK':pK }
               
        """
        # Alternative form for parameters
        p = { 'pHapo': pHapo, 'pHcyt': pHcyt, 'Voltage':Voltage, 
               'PIAAH':PIAAH, 'PAUX':PAUX,'PLAX':PLAX, 'PPIN':PPIN, 'PNPE':PNPE, 'lamb':lamb, 'D_cw':D_cw, 'alpha_back':alpha_back,'alpha_QCinit':alpha_QCinit,'beta':beta, 'Temp':Temp, 'pK':pK }

        return p

    def set_default_parameters(self, db):
        """
        Set default parameters for this model in the TissueDB
        """
#        if self.name not in param_dict:
#            set_parameters(param_dict, self.name, self.default_parameters())
        p = self.default_parameters()
        set_parameters(db, self.name, p)

	    
    def deriv(self, y, t, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):
        """
        Right hand side for the system of equations.
        :param y: current state vector
        :type y: numpy.ndarray
        :param t: current simulation time 
        :type t: float
        :param db:  tissue database
        :type db: TissueDB
        :param tissue_maps: dictionary mapping property types ('cell', 'wall',
                            'edge') to a dictionary which takes id numbers to
                            positions in slices of the y for properties of 
                            that type.
                            For example, tissue_maps['cell'] gives a dictionary
                            of the form { 101: 0, 123: 1, 121: 2, ...}
                            so the cell with cid 101 maps to the first element
                            of the slice.
                            y[species_slice['auxin']][tissue_maps['cell'][28]]
                            is the auxin concentration in cell with cid 28
        :type tissue_maps: dict
        :param species_slices: dictionary mapping property names to slices of
                               y. e.g. auxin = y[species_slice['auxin']]
        :type species_slices: dict
        :param wall_decomposition: dictionary which takes a wall id to the
                                   ids of the two corresponding edges.
              
        :param fixed_idx: indices of the elements of y[] which are to be kept
                          constant
        :type fixed_idx: list
        :returns: - numpy.array which is the right hand side of
                        the system of ODEs              
        """

        ret = zeros((len(y), ))
        p = get_parameters(db, self.name)

        mesh = get_mesh(db)

        Auxin_sl = species_slices['auxin']
        Auxin = y[Auxin_sl]
        Auxin_wall_sl = species_slices['auxin_wall']
        Auxin_wall = y[Auxin_wall_sl]	

        graph = get_graph(db)      
        S = db.get_property('S')
        V = db.get_property('V')
        PIN = db.get_property('PIN')
        AUX = db.get_property('AUX')
	LAX = db.get_property('LAX')
        P = db.get_property('P')

	cell_type = db.get_property('cell_type')

        cell_map = tissue_maps['cell']
        edge_map = tissue_maps['edge']
	wall_map = tissue_maps['wall']

        casparian = db.get_property('casparian')
        blocked_walls = set( wid for wid, v in casparian.iteritems() if v>0 )
       # print "blocked walls", blocked_walls

        """
        A1 = p['A1']
        A2 = p['A2']
        A3 = p['A3']
        B1 = p['B1']
        B2 = p['B2']
        B3 = p['B3']         
        """
        # If we want to vary voltage etc. A1-B3 need to be recalculated here
        pHapo=p['pHapo'] # apoplastic pH
        pHcyt=p['pHcyt'] # cytoplasmic pH
        pK=p['pK']
        Voltage=p['Voltage']  # Membrane potential (Volts)
        Temp=p['Temp'] # K

	Rconst=8.31
	Fdconst=96500

        phi=Fdconst*Voltage/(Rconst*Temp)	
        A1=1/(1+pow(10,pHapo-pK)) 
        A2=(1-A1)*phi/(exp(phi)-1)
        A3=-(1-A1)*phi/(exp(-phi)-1)
        B1=1/(1+pow(10,pHcyt-pK))
        B2=-(1-B1)*phi/(exp(-phi)-1)
        B3=(1-B1)*phi/(exp(phi)-1)

	PIAAH = p['PIAAH']
	PAUX = p['PAUX']
	PLAX = p['PLAX']
	PPIN = p['PPIN']
	PNPE = p['PNPE']
	lamb = p['lamb']
	D_cw = p['D_cw']
	alpha_back=p['alpha_back']
  	alpha_high=p['alpha_QCinit']
	beta=p['beta']

	alpha={}
	for cid in graph.vertices():
		if cell_type[cid]==17 or cell_type[cid]==10:	
			alpha[cid]=alpha_high
		else:
			alpha[cid]=alpha_back


        ret_auxin=ret[Auxin_sl]
	ret_auxin_wall=ret[Auxin_wall_sl]

        
        for wid,(eid1,eid2) in wall_decomposition.iteritems() :
            sid = graph.source(eid1)
            tid = graph.target(eid1)
            sidx = cell_map[sid]
            tidx = cell_map[tid]
            widx = wall_map[wid]
            sAux = Auxin[sidx]
            tAux = Auxin[tidx]
            wAux = Auxin_wall[widx]

            PIN1 = PIN[eid1]
            PIN2 = PIN[eid2]
            AUX1 = AUX[eid1]
            AUX2 = AUX[eid2]
	    LAX1 = LAX[eid1]
            LAX2 = LAX[eid2]
			    
            J1=(B1*PIAAH+B2*PAUX*AUX1+B2*PLAX*LAX1+B3*PPIN*PIN1+B3*PNPE)*sAux - \
                (A1*PIAAH+A2*PAUX*AUX1+A2*PLAX*LAX1+A3*PPIN*PIN1+A3*PNPE)*wAux
            J2=(B1*PIAAH+B2*PAUX*AUX2+B2*PLAX*LAX2+B3*PPIN*PIN2+B3*PNPE)*tAux - \
                (A1*PIAAH+A2*PAUX*AUX2+A2*PLAX*LAX2+A3*PPIN*PIN2+A3*PNPE)*wAux

            Swall = float(S[wid])

            ret_auxin[sidx] += Swall/V[sid] * (-J1) 
            ret_auxin[tidx] += Swall/V[tid] * (-J2)
            ret_auxin_wall[widx]  += (J1+J2)/lamb

        
	Auxin_vertex={}
        for pid in mesh.wisps(0):
            top = 0.0
            bottom = 0.0
            for wid in mesh.regions(0, pid):
                if wid not in blocked_walls:
                    widx = wall_map[wid]
                    top += Auxin_wall[widx]/S[wid]
                    bottom += 1/S[wid]
            if bottom!=0.0:
                Auxin_vertex[pid] = top/bottom

	for wid in mesh.wisps(1):
            if wid not in blocked_walls:
                pid1, pid2 = mesh.borders(1, wid)
                widx = wall_map[wid]
                wAux = Auxin_wall[widx]
                flux1 = 2*D_cw/S[wid]*(Auxin_vertex[pid1]-wAux)
                flux2 = 2*D_cw/S[wid]*(Auxin_vertex[pid2]-wAux)
                ret_auxin_wall[widx] += (flux1+flux2)/S[wid]
        

	for cid in graph.vertices():
	    index_cell = cell_map[cid]
	    ret_auxin[index_cell]+=alpha[cid]-beta*Auxin[index_cell]


        for i in fixed_idx:
            ret[i] = 0.0
        return ret


class at_cwd_casp_pH(object):
    """
    Auxin transport model with apoplastic auxin diffusion.
    """

    name = 'at_cwd'

    def pre_step(self, db):
        pass

    def post_step(self, db):
        pass


    def get_JPat(self, y, t, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):
        graph = get_graph(db)
        cell_map=tissue_maps['cell']
        wall_map=tissue_maps['wall']
        a_sl = species_slices['auxin']
        aw_sl = species_slices['auxin_wall']


        a_s = a_sl.start
        a_d = a_sl.step if a_sl.step else 1 
        aw_s = aw_sl.start
        aw_d = aw_sl.step if aw_sl.step else 1 
        M = lil_matrix((len(y), len(y)))
        for wid,(eid1,eid2) in wall_decomposition.iteritems() :
            sid = graph.source(eid1)
            tid = graph.target(eid1)
            sidx = cell_map[sid]
            tidx = cell_map[tid]
            widx = wall_map[wid]
            a_sidx = a_s+a_d*sidx
            a_tidx = a_s+a_d*tidx
            a_widx = aw_s+aw_d*widx

            M[a_sidx, a_sidx]=1
            M[a_sidx, a_widx]=1
            M[a_widx, a_sidx]=1
            M[a_widx, a_widx]=1
            M[a_widx, a_tidx]=1
            M[a_tidx, a_widx]=1
            M[a_tidx, a_tidx]=1

        mesh = get_mesh(db)
        for wid in mesh.wisps(1):
            widx = wall_map[wid]
            a_widx = aw_s + aw_d*widx
            for wid2 in mesh.border_neighbors(1,wid):
                widx2 = wall_map[wid2]
                a_widx2 = aw_s + aw_d*widx2
                M[a_widx, a_widx2]=1
                M[a_widx2, a_widx]=1


        return M

    def get_species(self):
        """  
        Species used in model
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        return [('auxin', 'cell'), ('auxin_wall', 'wall')]

    def get_modifier_species(self):
        """  
        Species used in model but which aren't modified by it
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        return [('PIN', 'edge'), ('AUX', 'edge'), ('LAX', 'edge'),
                ('casparian', 'wall')]

    def default_parameters(self):
        """
        Set default parameters for this model in the TissueDB
        """
        # These are defined in 'auxin_transport.py'
        # e.g. see lines 34--53

        pHapo=5.3 # apoplastic pH
        pHcyt=7.2 # cytoplasmic pH
        pK=4.8
        Voltage=-0.120  # Membrane potential (Volts)
        Temp=300 # K
        PIAAH=0.56  # Permeability of membrane to protonated auxin (microns/sec)
	PAUX=0.56 # Permeability of membrane to anionic auxin due to AUX1 (microns/sec)
	PLAX=0.56 #0.56 # Permeability of membrane to anionic auxin due to AUX1 (microns/sec)
	PPIN=0.56   # Permeability of membrane to anionic auxin due to PINs (microns/sec)
	PNPE=0.56*0.3   # Permeability of membrane to anionic auxin due to PINs (microns/sec)	
	lamb=0.14  # Cell-wall thickness (microns)
	D_cw= 32    
        alpha_back=0.001  # Auxin production rate in all cells except QC and columella initials
	alpha_QCinit=0.01 # Auxin production rate in QC and columella initials
	beta =0.001 # Auxin degradation rate

        # the below parameters are derived from the others - if we want
        # to be able to change the upper ones they need recalculating

        """
        phi=Fdconst*Voltage/(Rconst*Temp)	
        A1=1/(1+pow(10,pHapo-pK)) 
        A2=(1-A1)*phi/(exp(phi)-1)
        A3=-(1-A1)*phi/(exp(-phi)-1)
        B1=1/(1+pow(10,pHcyt-pK))
        B2=-(1-B1)*phi/(exp(-phi)-1)
        B3=(1-B1)*phi/(exp(phi)-1)
        """
        # Make dictionary of the parameters for this model -
        """
        p = { 'pHapo': pHapo, 'pHcyt': pHcyt, 'Voltage':Voltage, 'A1':A1, 'A2':A2, 'A3':A3, 'B1':B1, 'B2':B2, 'B3':B3, 
               'PIAAH':PIAAH, 'PAUX':PAUX, 'PPIN':PPIN, 'lamb':lamb, 'D_cw':D_cw, 'alpha_back':alpha_back,'alpha_QCinit':alpha_QCinit, 'beta':beta, 'Temp':Temp, 'pK':pK }
               
        """
        # Alternative form for parameters
        p = { 'pHapo': pHapo, 'pHcyt': pHcyt, 'Voltage':Voltage, 
               'PIAAH':PIAAH, 'PAUX':PAUX,'PLAX':PLAX, 'PPIN':PPIN, 'PNPE':PNPE, 'lamb':lamb, 'D_cw':D_cw, 'alpha_back':alpha_back,'alpha_QCinit':alpha_QCinit,'beta':beta, 'Temp':Temp, 'pK':pK }

        return p

    def set_default_parameters(self, db):
        """
        Set default parameters for this model in the TissueDB
        """
#        if self.name not in param_dict:
#            set_parameters(param_dict, self.name, self.default_parameters())
        p = self.default_parameters()
        set_parameters(db, self.name, p)

	    
    def deriv(self, y, t, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):
        """
        Right hand side for the system of equations.
        :param y: current state vector
        :type y: numpy.ndarray
        :param t: current simulation time 
        :type t: float
        :param db:  tissue database
        :type db: TissueDB
        :param tissue_maps: dictionary mapping property types ('cell', 'wall',
                            'edge') to a dictionary which takes id numbers to
                            positions in slices of the y for properties of 
                            that type.
                            For example, tissue_maps['cell'] gives a dictionary
                            of the form { 101: 0, 123: 1, 121: 2, ...}
                            so the cell with cid 101 maps to the first element
                            of the slice.
                            y[species_slice['auxin']][tissue_maps['cell'][28]]
                            is the auxin concentration in cell with cid 28
        :type tissue_maps: dict
        :param species_slices: dictionary mapping property names to slices of
                               y. e.g. auxin = y[species_slice['auxin']]
        :type species_slices: dict
        :param wall_decomposition: dictionary which takes a wall id to the
                                   ids of the two corresponding edges.
              
        :param fixed_idx: indices of the elements of y[] which are to be kept
                          constant
        :type fixed_idx: list
        :returns: - numpy.array which is the right hand side of
                        the system of ODEs              
        """

        ret = zeros((len(y), ))
        p = get_parameters(db, self.name)

        mesh = get_mesh(db)

        Auxin_sl = species_slices['auxin']
        Auxin = y[Auxin_sl]
        Auxin_wall_sl = species_slices['auxin_wall']
        Auxin_wall = y[Auxin_wall_sl]	

        graph = get_graph(db)      
        S = db.get_property('S')
        V = db.get_property('V')
        PIN = db.get_property('PIN')
        AUX = db.get_property('AUX')
	LAX = db.get_property('LAX')
	cell_type = db.get_property('cell_type')

        cell_map = tissue_maps['cell']
        edge_map = tissue_maps['edge']
	wall_map = tissue_maps['wall']

        casparian = db.get_property('casparian')
        blocked_walls = set( wid for wid, v in casparian.iteritems() if v>0 )
       # print "blocked walls", blocked_walls

        """
        A1 = p['A1']
        A2 = p['A2']
        A3 = p['A3']
        B1 = p['B1']
        B2 = p['B2']
        B3 = p['B3']         
        """
        # If we want to vary voltage etc. A1-B3 need to be recalculated here
        pHapo=p['pHapo'] # apoplastic pH
        pHcyt=p['pHcyt'] # cytoplasmic pH
        pK=p['pK']
        Voltage=p['Voltage']  # Membrane potential (Volts)
        Temp=p['Temp'] # K

	Rconst=8.31
	Fdconst=96500

        phi=Fdconst*Voltage/(Rconst*Temp)	
        A1=1/(1+pow(10,pHapo-pK)) 
        A2=(1-A1)*phi/(exp(phi)-1)
        A3=-(1-A1)*phi/(exp(-phi)-1)
        B1=1/(1+pow(10,pHcyt-pK))
        B2=-(1-B1)*phi/(exp(-phi)-1)
        B3=(1-B1)*phi/(exp(phi)-1)

	PIAAH = p['PIAAH']
	PAUX = p['PAUX']
	PLAX = p['PLAX']
	PPIN = p['PPIN']
	PNPE = p['PNPE']
	lamb = p['lamb']
	D_cw = p['D_cw']
	alpha_back=p['alpha_back']
  	alpha_high=p['alpha_QCinit']
	beta=p['beta']

	alpha={}
	for cid in graph.vertices():
		if cell_type[cid]==17 or cell_type[cid]==10:	
			alpha[cid]=alpha_high
		else:
			alpha[cid]=alpha_back


        ret_auxin=ret[Auxin_sl]
	ret_auxin_wall=ret[Auxin_wall_sl]

        
        for wid,(eid1,eid2) in wall_decomposition.iteritems() :
            sid = graph.source(eid1)
            tid = graph.target(eid1)
            sidx = cell_map[sid]
            tidx = cell_map[tid]
            widx = wall_map[wid]
            sAux = Auxin[sidx]
            tAux = Auxin[tidx]
            wAux = Auxin_wall[widx]

            PIN1 = PIN[eid1]
            PIN2 = PIN[eid2]
            AUX1 = AUX[eid1]
            AUX2 = AUX[eid2]
	    LAX1 = LAX[eid1]
            LAX2 = LAX[eid2]
			    
            J1=(B1*PIAAH+B2*PAUX*AUX1+B2*PLAX*LAX1+B3*PPIN*PIN1+B3*PNPE)*sAux - \
                (A1*PIAAH+A2*PAUX*AUX1+A2*PLAX*LAX1+A3*PPIN*PIN1+A3*PNPE)*wAux
            J2=(B1*PIAAH+B2*PAUX*AUX2+B2*PLAX*LAX2+B3*PPIN*PIN2+B3*PNPE)*tAux - \
                (A1*PIAAH+A2*PAUX*AUX2+A2*PLAX*LAX2+A3*PPIN*PIN2+A3*PNPE)*wAux

            Swall = float(S[wid])

            ret_auxin[sidx] += Swall/V[sid] * (-J1) 
            ret_auxin[tidx] += Swall/V[tid] * (-J2)
            ret_auxin_wall[widx]  += (J1+J2)/lamb

        
	Auxin_vertex={}
        for pid in mesh.wisps(0):
            top = 0.0
            bottom = 0.0
            for wid in mesh.regions(0, pid):
                if wid not in blocked_walls:
                    widx = wall_map[wid]
                    top += Auxin_wall[widx]/S[wid]
                    bottom += 1/S[wid]
            if bottom!=0.0:
                Auxin_vertex[pid] = top/bottom

	for wid in mesh.wisps(1):
            if wid not in blocked_walls:
                pid1, pid2 = mesh.borders(1, wid)
                widx = wall_map[wid]
                wAux = Auxin_wall[widx]
                flux1 = 2*D_cw/S[wid]*(Auxin_vertex[pid1]-wAux)
                flux2 = 2*D_cw/S[wid]*(Auxin_vertex[pid2]-wAux)
                ret_auxin_wall[widx] += (flux1+flux2)/S[wid]
        

	for cid in graph.vertices():
	    index_cell = cell_map[cid]
	    ret_auxin[index_cell]+=alpha[cid]-beta*Auxin[index_cell]


        for i in fixed_idx:
            ret[i] = 0.0
        return ret

       

        
class DIIq(object):
    """
    DIIq, simulates auxin production and degradation, and auxin-regulated DII-VENUS degradation. 
    """
    name = 'DIIq'

    def pre_step(self, db):
        pass

    def post_step(self, db):
        p = get_parameters(db, self.name)
	p1=p['p1']   
	p2=p['p2']
	q1=p['q1'] 
	q2=p['q1']          
        Auxin = db.get_property('auxin')
        VENUS = db.get_property('VENUS')
        TIR1 = db.get_property('TIR1')
        AuxinTIR1 = db.get_property('AuxinTIR1')
        AuxinTIR1Venus = db.get_property('AuxinTIR1Venus')
        mesh = get_mesh(db)
        for cid in mesh.wisps(2):
            TIR1[cid]=1/(q1+q2*Auxin[cid]+p1*Auxin[cid]*VENUS[cid])
            AuxinTIR1[cid]=Auxin[cid]*TIR1[cid]
            AuxinTIR1Venus[cid]=AuxinTIR1[cid]*VENUS[cid]
        pass

    def get_species(self):
        """  
        Returns a list of the species (and types) in the model 
        :returns: list - list of property name, property type tuples
        """
        return [('auxin', 'cell'), ('VENUS', 'cell')]

    def get_modifier_species(self):
        return [('TIR1', 'cell'), ('AuxinTIR1', 'cell'), ('AuxinTIR1Venus', 'cell')]

    def get_JPat(self, y, t, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):
        graph = get_graph(db)
        cell_map=tissue_maps['cell']
        a_sl = species_slices['auxin']
        v_sl = species_slices['VENUS']

        # This is similar to 'reactions.py' lines 26--36
        # This is to work out how the elements of the 'auxin' slice
        # correspond to offsets in the y vector
        a_s = a_sl.start
        a_d = a_sl.step if a_sl.step else 1 
        v_s = v_sl.start
        v_d = v_sl.step if v_sl.step else 1 
        # Construct a sparse matrix
        M = lil_matrix((len(y), len(y)))

	# Loop over the cells this time.
        for cid in graph.vertices():
            index = cell_map[cid]
             

            # Find indices in the y vector
            a_idx = a_s+a_d*index
            v_idx = v_s+v_d*index

            # Set elements in Jacobian pattern
            M[v_idx, a_idx]=1
            M[v_idx, v_idx]=1

        return M


    def default_parameters(self):
        """
        Set default parameters for this model in the TissueDB
        """
        # Parameters governing DII-VENUS degradation (estimated in Band et al. PNAS (2012)). 
	p1=0.06   #  parameter [Auxin-TIR1-VENUS]u/[TIR1]T
	p2=0.0053/60 # parameter delta/[VENUS]u  (in seconds).
	q1=0.91   #  parameter [TIR1]u/[TIR1]T
#	q1=0.91/100.0   #  parameter [TIR1]u/[TIR1]T
	q2=0.03   #  parameter [Auxin-TIR1]u/[TIR1]T

        # Make dictionary of the parameters for this model -
        # I'm not sure which ones we'd like the user to be able to 
        # change in the interface - possibly some of the ones that
        # these parameters are derived from (e.g. the pHs).
        p = { 'p1':p1, 'p2':p2, 'q1':q1, 'q2':q2}

        return p

    def set_default_parameters(self, db):
        """
        Set default parameters for this model in the TissueDB
        """
#        if self.name not in param_dict:
#            set_parameters(param_dict, self.name, self.default_parameters()) # parameters become visible to the simulation
        p = self.default_parameters()
        set_parameters(db, self.name, p)
        

    def deriv(self, y, t, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):
        """
        Right hand side for the system of equations.
        :param y: current state vector
        :type y: numpy.ndarray
        :param t: current simulation time 
        :type t: float
        :param db:  tissue database
        :type db: TissueDB
        :param tissue_maps: dictionary mapping property types ('cell', 'wall',
                            'edge') to a dictionary which takes id numbers to
                            positions in slices of the y for properties of 
                            that type.
                            For example, tissue_maps['cell'] gives a dictionary
                            of the form { 101: 0, 123: 1, 121: 2, ...}
                            so the cell with cid 101 maps to the first element
                            of the slice.
                            y[species_slice['auxin']][tissue_maps['cell'][28]]
                            is the auxin concentration in cell with cid 28
        :type tissue_maps: dict
        :param species_slices: dictionary mapping property names to slices of
                               y. e.g. auxin = y[species_slice['auxin']]
        :type species_slices: dict
        :param wall_decomposition: dictionary which takes a wall id to the
                                   ids of the two corresponding edges.
              
        :param fixed_idx: indices of the elements of y[] which are to be kept
                          constant
        :type fixed_idx: list
        :returns: - numpy.array which is the right hand side of
                        the system of ODEs              
        """
        ret = zeros((len(y), ))
        p = get_parameters(db, self.name)

	# vertices, connections, cells, walls
        mesh = get_mesh(db) 
	
	# slices: start/step/end relating to the state vector
        Auxin_sl = species_slices['auxin']
        # auxin level in all cells [0.. #cells]; slice of numpy array
        auxin = y[Auxin_sl]
	# Same for DII-VENUS
	VENUS_sl=species_slices['VENUS']
	VENUS=y[VENUS_sl]


	# graph: topology of the cell set
        graph = get_graph(db)      
        # surface between cells 
        S = db.get_property('S')
        # volume/area of cells
        V = db.get_property('V')
        # quantities of PIN/AUX1, defined on the edges of the graph
       # PIN = db.get_property('PIN')
       # AUX = db.get_property('AUX')

	# dictionary going from cell_id in mesh to index in array e.g. auxin array
        cell_map = tissue_maps['cell']

	p1=p['p1']   
	p2=p['p2']
	q1=p['q1'] 
	q2=p['q2']  

        ret_auxin=ret[Auxin_sl]
 	ret_VENUS=ret[VENUS_sl]
        
        # Copy pasted from 'reactions.py' lines 47 -- 69
        # Changed ret[index_cell1] -> ret_auxin[index_cell1]
        # auxin_map -> cell_map
        # y[index_cell1] -> auxin[index_cell1]
        # y[index_cell2] -> auxin[index_cell2]

	for cid in graph.vertices():
		index=cell_map[cid]
		ret_VENUS[index]+=p2*(1-auxin[index]*VENUS[index]/(q1+q2*auxin[index]+p1*auxin[index]*VENUS[index]))

        # Apply boundary conditions
        
        for idx in fixed_idx: 
            ret[idx]=0.0

        return ret


class Response(object):
    name = 'Response'

    def pre_step(self, db):
        pass

    def post_step(self, db):
        pass

    def get_species(self):
        """  
        Returns a list of the species (and types) in the model 
        :returns: list - list of property name, property type tuples
        """
        return [('mIAA', 'cell'), ('IAA', 'cell'),
                ('response', 'cell'), ('auxin', 'cell')]

    def get_modifier_species(self):
        return []

    def get_JPat(self, y, t, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):
        graph = get_graph(db)
        cell_map=tissue_maps['cell']
        m_sl = species_slices['mIAA']
        I_sl = species_slices['IAA']
        r_sl = species_slices['response']
        a_sl = species_slices['auxin']

        m_s = m_sl.start
        m_d = m_sl.step if m_sl.step else 1 

        I_s = I_sl.start
        I_d = I_sl.step if I_sl.step else 1 

        r_s = r_sl.start
        r_d = r_sl.step if r_sl.step else 1 

        a_s = a_sl.start
        a_d = a_sl.step if a_sl.step else 1 

        # Construct a sparse matrix

        M = lil_matrix((len(y), len(y)))

	# Loop over the cells this time.
        for cid in graph.vertices():
            index = cell_map[cid]
             
            # Find indices in the y vector
            m_idx = m_s+m_d*index
            I_idx = I_s+I_d*index
            r_idx = r_s+r_d*index
            a_idx = a_s+a_d*index

            M[m_idx, [m_idx, I_idx, r_idx]] = 1
            M[I_idx, [m_idx, r_idx, a_idx]] = 1
            M[r_idx, [I_idx, r_idx]] = 1

        return M


    def default_parameters(self):
        am = 0.01
        amr = 0.01
        Kmr = 1.0
        bm = 0.01
        aI = 0.01
        bI = 0.01
        bIa = 0.01
        ar = 0.01
        Kar = 1.0
        KARF = 1.0
        k1 = 1.0
        k2 = 1.0
        br = 0.01
        c = 1.0
        ARF_T = 1.0
        l = locals()
        p = dict((n,v) for n,v in l.iteritems() if n!='self')
        return p

    def set_default_parameters(self, db):
        """
        Set default parameters for this model in the TissueDB
        """
#        if self.name not in param_dict:
#            set_parameters(param_dict, self.name, self.default_parameters()) # parameters become visible to the simulation
        p = self.default_parameters()
        set_parameters(db, self.name, p)
        

    def deriv(self, y, t, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):
        """
        Right hand side for the system of equations.
        :param y: current state vector
        :type y: numpy.ndarray
        :param t: current simulation time 
        :type t: float
        :param db:  tissue database
        :type db: TissueDB
        :param tissue_maps: dictionary mapping property types ('cell', 'wall',
                            'edge') to a dictionary which takes id numbers to
                            positions in slices of the y for properties of 
                            that type.
                            For example, tissue_maps['cell'] gives a dictionary
                            of the form { 101: 0, 123: 1, 121: 2, ...}
                            so the cell with cid 101 maps to the first element
                            of the slice.
                            y[species_slice['auxin']][tissue_maps['cell'][28]]
                            is the auxin concentration in cell with cid 28
        :type tissue_maps: dict
        :param species_slices: dictionary mapping property names to slices of
                               y. e.g. auxin = y[species_slice['auxin']]
        :type species_slices: dict
        :param wall_decomposition: dictionary which takes a wall id to the
                                   ids of the two corresponding edges.
              
        :param fixed_idx: indices of the elements of y[] which are to be kept
                          constant
        :type fixed_idx: list
        :returns: - numpy.array which is the right hand side of
                        the system of ODEs              
        """
        ret = zeros((len(y), ))
        p = get_parameters(db, self.name)

	# vertices, connections, cells, walls
        mesh = get_mesh(db) 
        graph = get_graph(db)
	
	# slices: start/step/end relating to the state vector
        mIAA_sl = species_slices['mIAA']
        mIAA = y[mIAA_sl]

        IAA_sl = species_slices['IAA']
        IAA = y[IAA_sl]

        r_sl = species_slices['response']
        r = y[r_sl]

        auxin_sl = species_slices['auxin']
        a = y[auxin_sl]


#        ret_mIAA = p['am'] + p['amr']*r/(p['Kmr'] + r) - p['bm']*mIAA
        ret[mIAA_sl] = p['am'] + p['amr']*r/(p['Kmr'] + r) - p['bm']*mIAA
        ret[IAA_sl] = p['aI']*mIAA - p['bI']*IAA - p['bIa']*a*IAA/(1.0+p['k1']*a+p['k2']*a*IAA)

        c = p['c']
        ARF_T = p['ARF_T']

#        ARF = ARF_T - (np.sqrt((ARF_T*c - 2*IAA*c + 1)*(ARF_T*c + 2*IAA*c + 1))/(2*c) - (ARF_T*c + 1)/(2*c))
#       ARFT/2 + sqrt((ARFT*c - 2*IAAT*c + 1)*(ARFT*c + 2*IAAT*c + 1))/(2*c) - 1/(2*c)
#       ARFT - IAAT - (ARFT*c - IAAT*c + 1)/(2*c) + (ARFT**2*c**2 - 2*ARFT*IAAT*c**2 + 2*ARFT*c + IAAT**2*c**2 + 2*IAAT*c + 1)**(1/2)/(2*c)
        ARF = (ARF_T - IAA)/2 - 1/(2*c) + np.sqrt(ARF_T*ARF_T*c*c - 2*ARF_T*IAA*c*c + 2*ARF_T*c + IAA*IAA*c*c + 2*IAA*c + 1)/(2*c)
        
#        print mIAA

        ret[r_sl] = p['ar']*ARF/(p['KARF'] + ARF) - p['br']*r
        # Apply boundary conditions
        
        for idx in fixed_idx: 
            ret[idx]=0.0

        return ret
