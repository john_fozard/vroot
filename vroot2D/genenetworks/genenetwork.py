
"""
Module to simulate systems of inter/intracellular reactions on
a tissue. Able to combine models additively.
"""

import numpy as np
from vroot2D.utils.db_utilities import get_wall_decomp, \
                         get_tissue_maps, get_parameters, \
                         add_property, get_mesh
from vroot2D.utils.db_geom import updateVS
from scipy.integrate import odeint
from odesparse import odeints

class AbstractModel(object):
    """
    Abstract object used to document Model interface -
    implement this interface rather than subclass
    Here for documentation purposes
    """
    
    name = 'AbstractModel'
    # Name of this model - used for storing parameters in TissueDB

    def get_species(self):
        """
        Species used in model
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        pass

    def set_default_parameters(self, db):
        """
        Set default parameters in TissueDB
        :param db: Tissue database
        :type db: TissueDB
        """
        pass

        
    def deriv(y, t, db, tissue_maps, species_slices, wall_decomposition, 
              fixed_idx):
        """
        Right hand side for the system of equations.
        :param y: current state vector
        :type y: numpy.ndarray
        :param t: current simulation time 
        :type t: float
        :param db:  tissue database
        :type db: TissueDB
        :param tissue_maps: dictionary mapping property types ('cell', 'wall',
                            'edge') to a dictionary which takes id numbers to
                            positions in slices of the y for properties of 
                            that type.
                            For example, tissue_maps['cell'] gives a dictionary
                            of the form { 101: 0, 123: 1, 121: 2, ...}
                            so the cell with cid 101 maps to the first element
                            of the slice.
                            y[species_slice['auxin']][tissue_maps['cell'][28]]
                            is the auxin concentration in cell with cid 28
        :type tissue_maps: dict
        :param species_slices: dictionary mapping property names to slices of
                               y. e.g. auxin = y[species_slice['auxin']]
        :type species_slices: dict
        :param wall_decomposition: dictionary which takes a wall id to the
                                   ids of the two corresponding edges.
        :type wall_decomposition: dict
        :param fixed_idx: indices of the elements of y[] which are to be kept
                          constant
        :type fixed_idx: list
        :returns: - numpy.array which is the right hand side of
                        the system of ODEs              
        """
        pass



class AbstractModelWithJPat(AbstractModel):
    """
    Abstract object used to document interface for models
    which supply Jacobian pattern.
    implement this interface rather than subclass
    Here for documentation purposes
    """
    
    def get_JPat(t, y, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):
        """
        Right hand side for the system of equations.
        :param y: current state vector
        :type y: numpy.ndarray
        :param t: current simulation time 
        :type t: float
        :param db:  tissue database
        :type db: TissueDB
        :param tissue_maps: dictionary mapping property types ('cell', 'wall',
                            'edge') to a dictionary which takes id numbers to
                            positions in slices of the y for properties of 
                            that type.
                            For example, tissue_maps['cell'] gives a dictionary
                            of the form { 101: 0, 123: 1, 121: 2, ...}
                            so the cell with cid 101 maps to the first element
                            of the slice.
                            y[species_slice['auxin']][tissue_maps['cell'][28]]
                            is the auxin concentration in cell with cid 28
        :type tissue_maps: dict
        :param species_slices: dictionary mapping property names to slices of
                               y. e.g. auxin = y[species_slice['auxin']]
        :type species_slices: dict
        :param wall_decomposition: dictionary which takes a wall id to the
                                   ids of the two corresponding edges.
              
        :type wall_decompositions: dict
        :param fixed_idx: indices of the elements of y[] which are to be kept
                          constant
        :type fixed_idx: list
        :returns: - numpy.array which is the right hand side of
                        the system of ODEs              
        """
        pass

class CombinedModel(object):
    """
    Additive combination of GRN models.
    """

    name = 'CombinedModel'

    def __init__(self, models):
        """
        :param models: List of models to combine (each implementing
                       the AbstractModel interface)
        """
        self.models=models

    def set_default_parameters(self, db):
        for model in self.models:
            model.set_default_parameters(db)

    def get_species(self):
        """
        :returns: Set of names of species in the combined model
        """
        return set.union(*[set(m.get_species()) for m in self.models])

    def get_modifier_species(self):
        """
        :returns: Set of names of species in the combined model
        """
        return set.union(*[set(m.get_modifier_species()) for m in self.models])

    def get_JPat(self, *params):
        J = self.models[0].get_JPat(*params)
        for m in self.models[1:]:
            J+=m.get_JPat(*params)
        return J
    
    def deriv(self, y, t, *args):
        """
        Derivative function - sum of the derivatives for 
        each component model
        :param y: Current state vector
        :type y: numpy.array (or list)
        :param t: Current time
        :type t: float
        :param *args: additional arguments for derivative function
        """
        return sum((m.deriv(y, t, *args) for m in self.models), \
                       np.zeros(y.shape))

    def pre_step(self, db):	
        for model in self.models:
            model.pre_step(db)        

    def post_step(self, db):	
        for model in self.models:
            model.post_step(db)

#    def post_iterations(self, db):	
#        for model in self.models:
#            model.post_iterations(db)

class GeneNetwork(object):
    """
    Class to handle integration of GRN and cell-cell transport
    """
            
    def post_iterations(self):
        self.model.post_iterations(self.db)

    def __init__(self, db, model):
        """ 
        Initialise the networks 
        :param db: Tissue database
        :type db: TissueDB
        :param model: implementation of AbstractModel
        """
        self.db = db
        # Construct a model for each of the model classes
        self.model=model
        self.model_species = model.get_species()
        self.modifier_species = model.get_modifier_species() 


        # Set default parameters in the TissueDB parameter dictionary
        model.set_default_parameters(db)

        initial_values = get_parameters(db, 'initial_values')

        divided_props = db.get_property('divided_props')
        # Initialise all of the species needed which are not already
        # contained in the TissueDB
        for species_name, species_type  in set(self.model_species) | set(self.modifier_species):
            if species_name not in db.properties():
                add_property(db, 
                             species_name,
                             species_type,
                             initial_values.get(species_name, 0.0))
            if species_name not in divided_props:
                divided_props[species_name] = (species_type, 'concentration', 0.0)

        print self.model_species

        # Set the values for those species which are fixed
        set_fixed_properties(db)

	# perform any model-related action, before iterations start

    def step_dt(self, dt):
        """
        Evolve the gene network for one period of length d
        :param dt: float - timestep
        """
        
        set_fixed_properties(self.db)
        #print 'genenetwork.step_dt'
        
        t=self.db.tissue()

        # Update the cell volumes and wall surface areas - these may
        # have been changed through tissue growth
        updateVS(self.db)

        self.model.pre_step(self.db)

        # Recalculate the wall to edge map, and the tissue maps
        wall_decomposition = get_wall_decomp(self.db)
        tissue_maps = get_tissue_maps(self.db)
     
        # For each model species, calculate the slice (indices) of
        # the y[] array which will store property values during 
        # integration
        offset=0 # current offset into the y[] array
        species_slices={} # map from species name to the slice of y[]
        for species_name, species_type in self.model_species:
            # 'size' is how many values are needed to store a property
            # of that type
            size = len(tissue_maps[species_type])
            species_slices[species_name] = slice(offset, offset+size)
            offset+=size

        # Extract mesh
        cfg = self.db.get_config('config')
        mesh = t.relation(cfg.mesh_id)
        # Type of each cell
        cell_type = self.db.get_property('cell_type')
        
        # Call routine to find fixed property values
        fixed_idx=self.get_fixed_idx(tissue_maps, species_slices)

        #initialise with zero
        y0=np.zeros((offset,))

        ### SETUP INITIAL VALUES
        # Loop over all species referred to in these models
        for species_name, species_type in self.model_species:
            # extract property dict from TissueDB
            prop=self.db.get_property(species_name)
            # obtain appropriate section of y0[]
            y0_slice=y0[species_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems() :
                y0_slice[idx]=prop[tid]

                
 
        ###INTEGRATE OVER TIMESTEP
        integrated = odeints(self.model.deriv, y0, [0., dt], \
                                (self.db, tissue_maps, species_slices, 
                                 wall_decomposition, fixed_idx), lrw=10000000) 


        ###SET SPECIES TO NEW VALUE (AFTER TIMESTEP)
        yend=integrated[-1,:]
        
        # Update the values of the species in the tissue properties
        # from the state vector at the end of the integration
        for species_name, species_type in self.model_species: 
            prop=self.db.get_property(species_name)
            yend_slice=yend[species_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems() :
                prop[tid]=yend_slice[idx]


        #print 'genenetwork.step_dt done'
        self.model.post_step(self.db)

    def step(self):
        """
        Simulate the system for one timestep
        """
        dt=get_parameters(self.db, 'timestep')
        self.step_dt(dt)

    def steady_state(self):
        """
        Evolve the system for a long period of time (1000s)
        in an attempt to approach the steady-state - needs more work
        """
        self.step_dt(1e3)



    def get_fixed_idx(self, tissue_maps, species_slices):
        """
        Obtain a list of those elements in the ODE state vector y
        which are constant during the simulation
        (From the property 'fixed' in the tissue database.)
        This currently only works for cell-based properties
        
        The dictionary fixed maps a property name to a list
        of (expression, value) rules; for each cell, if the expression
        is satisfied, then the property is fixed
        This is a Python expression, and may depend on the cell
        id number and the values of all the properties of the tissue.

        e.g. fixed = { 'auxin':[ ('cell_type[cid]=='pericycle', 2) ] }
        would fix the auxin concentration in the pericycle.

        :param tissue_maps:
        :param species_slices:
        :returns: list of the offsets into the state vector y[] which
                  are to be held constant.

        """
        fixed = get_parameters(self.db, 'fixed')
        
        cell_map = tissue_maps['cell']
        fixed_idx = [] # list of fixed indices

        # Extract all the properties from the tissue data,
        # so these can be used in the eval statement
        all_properties=dict((name, self.db.get_property(name)) \
                                for name in self.db.properties())
        # Loop over all properties with rules
        for prop_name, rules in fixed.iteritems():
            # Ignore properties not in model
            if prop_name in species_slices:
                s=species_slices[prop_name]
                prop=self.db.get_property(prop_name)
                for (expr, value) in rules:
                    # Parse expression and convert it to python
                    # bytecode
                    code=compile(expr, '', 'eval')
                    # Loop over all cells
                    for cid, i in cell_map.iteritems():
                        # Test the expression for this cid
                        if eval(code, all_properties, { 'cid':cid }):
                            idx=s.start+i
                            fixed_idx.append(idx) 

        if 'wall_fixed' in self.db.get_property('parameters'):
            wall_map = tissue_maps['wall']
            wall_fixed = get_parameters(self.db, 'wall_fixed')
            for prop_name, rules in wall_fixed.iteritems():
                if prop_name in species_slices:
                    s=species_slices[prop_name]
                    prop=self.db.get_property(prop_name)
                    for (expr, value) in rules:
                        # Parse expression and convert it to python
                        # bytecode
                        code=compile(expr, '', 'eval')
                        # Loop over all cells
                        for wid, i in wall_map.iteritems():
                        # Test the expression for this cid
                            if eval(code, all_properties, { 'wid':wid,
                                                            'mesh': get_mesh(self.db) }):
                                idx=s.start+i
                                fixed_idx.append(idx) 
            

        return fixed_idx


def set_fixed_properties(db):
    """
    The dictionary fixed maps a property name to a list
    of (expression, value) rules; for each cell, if the expression
    is satisfied, then the property is set to the value.
    This is a Python expression, and may depend on the cell
    id number and the values of all the properties of the tissue.
    If multiple expressions are satisfied, then all of these
    are applied, so the last in the list is used.

    e.g. fixed = { 'auxin':[ ('cell_type[cid]=='pericycle', 2) ] }
    would set the auxin concentration in the pericycle to 2.

    :param db:  tissue database
    :type db: TissueDB
    """
    # Extract mesh from TissueDB
    t = db.tissue()
    cfg = db.get_config('config')
    mesh = t.relation(cfg.mesh_id)
    # Extract all the properties from the tissue data,
    # so these can be used in the eval statement
    all_properties=dict((name, db.get_property(name)) \
                            for name in db.properties())
    # Loop over all properties with rules 
    for prop_name, rules in get_parameters(db, 'fixed').iteritems():
        # Check if property currently in tissue database
        if prop_name in db.properties():
            prop = db.get_property(prop_name)
            for (expr, value) in rules:
                # compile expression to python bytecode
                code=compile(expr, '', 'eval')
                # loop over all cells
                for cid in mesh.wisps(2):
                    # test whether expr is satisfied for this cell
                    if eval(code, all_properties, {'cid':cid}):
                        prop[cid] = value
                    
class GeneNetworkJPat(GeneNetwork):
    """
    Module to evolve systems of reactions where the model file
    supplies the sparsity pattern of the reaction system
    through a get_JPat method
    """
    def step_dt(self, dt):
        """
        Evolve the gene network for one timestep
        """
        
        set_fixed_properties(self.db)
        #print 'genenetwork'
        
        t=self.db.tissue()

        # Update the cell volumes and wall surface areas - these may
        # have been changed through tissue growth
        updateVS(self.db)

        # Recalculate the wall to edge map, and the tissue maps
        wall_decomposition = get_wall_decomp(self.db)
        tissue_maps = get_tissue_maps(self.db)

        self.model.pre_step(self.db)
     
        # For each model species, calculate the slice (indices) of
        # the y[] array which will store property values during 
        # integration
        offset=0 # current offset into the y[] array
        species_slices={} # map from species name to the slice of y[]
        for species_name, species_type in self.model_species:
            # 'size' is how many values are needed to store a property
            # of that type
            size = len(tissue_maps[species_type])
            species_slices[species_name] = slice(offset, offset+size)
            offset+=size

        # Extract mesh
        cfg = self.db.get_config('config')
        mesh = t.relation(cfg.mesh_id)
        # Type of each cell
        cell_type = self.db.get_property('cell_type')
        
        # Call routine to find fixed property values
        fixed_idx=self.get_fixed_idx(tissue_maps, species_slices)

        #initialise with zero
        y0=np.zeros((offset,))

        ### SETUP INITIAL VALUES
        # Loop over all species referred to in these models
        for species_name, species_type in self.model_species:
            # extract property dict from TissueDB
            prop=self.db.get_property(species_name)
            # obtain appropriate section of y0[]
            y0_slice=y0[species_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems() :
                y0_slice[idx]=prop[tid]

                
 
        ###INTEGRATE OVER TIMESTEP

        integrated = odeints(self.model.deriv, y0, [0., dt], 
                             (self.db, tissue_maps, species_slices, 
                              wall_decomposition, fixed_idx),
                             lrw=1000000, 
                             rtol = 1e-10,
                             mxstep = 4000,
                             JPat=self.model.get_JPat(y0, 0.0, self.db,
                             tissue_maps, species_slices, wall_decomposition,
                                                          fixed_idx)) 


        ###SET SPECIES TO NEW VALUE (AFTER TIMESTEP)
        yend=integrated[-1,:]

        # Update TissueDB properties from the state vector at the 
        # end of the integration
        for species_name, species_type in self.model_species: 
            prop=self.db.get_property(species_name)
            yend_slice=yend[species_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems() :
                prop[tid]=yend_slice[idx]

       # print 'genenetwork done'
        self.model.post_step(self.db)

