import numpy as np
from vroot2D.utils.db_utilities import get_mesh, get_graph, get_parameters, \
 set_parameters, refresh_property, get_wall_decomp, get_tissue_maps
from vroot2D.utils.celltissue_util import def_property
from vroot2D.utils.db_geom import updateVS
from scipy.sparse import dok_matrix, lil_matrix
from scipy.sparse.linalg import spsolve
from math import exp

class DiffusePromoterSS(object):

    name = 'DiffusePromoterSS'

    def __init__(self, db):
        self.db = db
        # Construct a model for each of the model classes
        self.model_species=self.get_species()

        initial_values = get_parameters(db, 'initial_values')
        divided_props = db.get_property('divided_props')
        # Initialise all of the species needed which are not already
        # contained in the TissueDB
        for species_name, species_type  in self.model_species:
            if species_name not in db.properties() or species_name=='PLT':
                self.db,prop = def_property(
                    db, 
                    species_name, 
                    initial_values.get(species_name, 0.0), 
                    species_type.upper(), 
                    "config",
                    "")   
            if species_name not in divided_props:
                divided_props[species_name] = (species_type, 'concentration', 0.0)

        self.set_default_parameters(db)

        # Set the values for those species which are fixed
        set_fixed_properties(db)


    def get_fixed_idx(self, tissue_maps, species_slices):
        """
        Obtain a list of those elements in the ODE state vector y
        which are constant during the simulation
        (From the property 'fixed' in the tissue database.)
        This currently only works for cell-based properties
        
        The dictionary fixed maps a property name to a list
        of (expression, value) rules; for each cell, if the expression
        is satisfied, then the property is fixed
        This is a Python expression, and may depend on the cell
        id number and the values of all the properties of the tissue.

        e.g. fixed = { 'auxin':[ ('cell_type[cid]=='pericycle', 2) ] }
        would fix the auxin concentration in the pericycle.

        :param tissue_maps:
        :param species_slices:
        :returns: list of the offsets into the state vector y[] which
                  are to be held constant.

        """
        fixed = get_parameters(self.db, 'fixed')
        
        cell_map = tissue_maps['cell']
        fixed_idx = [] # list of fixed indices

        # Extract all the properties from the tissue data,
        # so these can be used in the eval statement
        all_properties=dict((name, self.db.get_property(name)) \
                                for name in self.db.properties())
        # Loop over all properties with rules
        for prop_name, rules in fixed.iteritems():
            # Ignore properties not in model
            if prop_name in species_slices:
                s=species_slices[prop_name]
                prop=self.db.get_property(prop_name)
                for (expr, value) in rules:
                    # Parse expression and convert it to python
                    # bytecode
                    code=compile(expr, '', 'eval')
                    # Loop over all cells
                    for cid, i in cell_map.iteritems():
                        # Test the expression for this cid
                        if eval(code, all_properties, { 'cid':cid }):
                            idx=s.start+i
                            fixed_idx.append(idx) 
        return fixed_idx



    def get_species(self):
        """
        Species used in model
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        return [('GP', 'cell')]

    def set_default_parameters(self, db):
        """
        Set default parameters for this model in the TissueDB
        """
        p = { 'D_p': 2e3, 'lambda_p':16., 'alpha_p': 2. }
             
        set_parameters(db, self.name, p)




     
    def get_ss(self, y0, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):


        p = get_parameters(db, self.name)
    
        GP = y0

        cell_type = db.get_property('cell_type')
        alpha_p = p['alpha_p']

        graph = get_graph(db)      
        S = db.get_property('S')
        V = db.get_property('V')
        cell_map = tissue_maps['cell']
        edge_map = tissue_maps['edge']

        N = y0.size

        alpha = np.zeros(y0.shape)
        
        mesh = get_mesh(db)
        for cid in mesh.wisps(2):
            if cell_type[cid]==9:
#                print cid
                i = cell_map[cid]
                alpha[i]+=alpha_p

        D_p = p['D_p']

        J = lil_matrix((N,N))

        for wid,(eid1,eid2) in wall_decomposition.iteritems() :
            sid = graph.source(eid1)
            tid = graph.target(eid1)
            Vs = V[sid]
            Vt = V[tid]
            Swall = S[wid]
            sidx = cell_map[sid]
            tidx = cell_map[tid]

            k = Swall*D_p

            J[sidx, sidx]+=-k/Vs
            J[sidx, tidx]+= k/Vs
            J[tidx, sidx]+= k/Vt
            J[tidx, tidx]+=-k/Vt
        

        lambda_p = p['lambda_p']
        for i in range(N):
            J[i, i] -= lambda_p            

        for i in fixed_idx:
            J[i,:] = 0
            J[i,i] = 1
            alpha[i] = -y0[i]

        y = -spsolve(J.tocsr(), alpha)


        print 'ret_GP: ', min(y), max(y)
#        print 'ret: ', min(ret), max(ret)
        return y

    def step(self):
        """
        Evolve the gene network for one timestep
        """
        
        set_fixed_properties(self.db)
        print 'SS_start'
        
        t=self.db.tissue()

        # Update the cell volumes and wall surface areas - these may
        # have been changed through tissue growth
        updateVS(self.db)

        # Recalculate the wall to edge map, and the tissue maps
        wall_decomposition = get_wall_decomp(self.db)
        tissue_maps = get_tissue_maps(self.db)
     
        # For each model species, calculate the slice (indices) of
        # the y[] array which will store property values during 
        # integration
        offset=0 # current offset into the y[] array
        species_slices={} # map from species name to the slice of y[]
        for species_name, species_type in self.model_species:
            # 'size' is how many values are needed to store a property
            # of that type
            size = len(tissue_maps[species_type])
            species_slices[species_name] = slice(offset, offset+size)
            offset+=size

        # Extract mesh
        cfg = self.db.get_config('config')
        mesh = t.relation(cfg.mesh_id)
        # Type of each cell
        cell_type = self.db.get_property('cell_type')
        
        # Call routine to find fixed property values
        fixed_idx=self.get_fixed_idx(tissue_maps, species_slices)

        #initialise with zero
        y0=np.zeros((offset,))

        ### SETUP INITIAL VALUES
        # Loop over all species referred to in these models
        for species_name, species_type in self.model_species:
            # extract property dict from TissueDB
            prop=self.db.get_property(species_name)
            # obtain appropriate section of y0[]
            y0_slice=y0[species_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems() :
                y0_slice[idx]=prop[tid]

 
        yend=self.get_ss(y0, self.db, tissue_maps, species_slices, wall_decomposition, fixed_idx)
 
        
        for species_name, species_type in self.model_species: 
            prop=self.db.get_property(species_name)
            yend_slice=yend[species_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems() :
                prop[tid]=yend_slice[idx]

        print 'SS_end'



class DiffuseInhibitorSS(object):

    name = 'DiffuseInhibitorSS'

    def __init__(self, db):
        self.db = db
        # Construct a model for each of the model classes
        self.model_species=self.get_species()

        initial_values = get_parameters(db, 'initial_values')
        divided_props = db.get_property('divided_props')
        # Initialise all of the species needed which are not already
        # contained in the TissueDB
        for species_name, species_type  in self.model_species:
            if species_name not in db.properties() or species_name=='PLT':
                self.db,prop = def_property(
                    db, 
                    species_name, 
                    initial_values.get(species_name, 0.0), 
                    species_type.upper(), 
                    "config",
                    "")   
            if species_name not in divided_props:
                divided_props[species_name] = (species_type, 'concentration', 0.0)

        self.set_default_parameters(db)

        # Set the values for those species which are fixed
        set_fixed_properties(db)

    def step(self):
        """
        Evolve the gene network for one timestep
        """
        
        set_fixed_properties(self.db)
        print 'SS_start'
        
        t=self.db.tissue()

        # Update the cell volumes and wall surface areas - these may
        # have been changed through tissue growth
        updateVS(self.db)

        # Recalculate the wall to edge map, and the tissue maps
        wall_decomposition = get_wall_decomp(self.db)
        tissue_maps = get_tissue_maps(self.db)
     
        # For each model species, calculate the slice (indices) of
        # the y[] array which will store property values during 
        # integration
        offset=0 # current offset into the y[] array
        species_slices={} # map from species name to the slice of y[]
        for species_name, species_type in self.model_species:
            # 'size' is how many values are needed to store a property
            # of that type
            size = len(tissue_maps[species_type])
            species_slices[species_name] = slice(offset, offset+size)
            offset+=size

        # Extract mesh
        cfg = self.db.get_config('config')
        mesh = t.relation(cfg.mesh_id)
        # Type of each cell
        cell_type = self.db.get_property('cell_type')
        
        # Call routine to find fixed property values
        fixed_idx=self.get_fixed_idx(tissue_maps, species_slices)

        #initialise with zero
        y0=np.zeros((offset,))

        ### SETUP INITIAL VALUES
        # Loop over all species referred to in these models
        for species_name, species_type in self.model_species:
            # extract property dict from TissueDB
            prop=self.db.get_property(species_name)
            # obtain appropriate section of y0[]
            y0_slice=y0[species_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems() :
                y0_slice[idx]=prop[tid]

 
        yend=self.get_ss(y0, self.db, tissue_maps, species_slices, wall_decomposition, fixed_idx)
 
        
        for species_name, species_type in self.model_species: 
            prop=self.db.get_property(species_name)
            yend_slice=yend[species_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems() :
                prop[tid]=yend_slice[idx]

        print 'SS_end'



    def get_species(self):
        """
        Species used in model
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        return [('GI', 'cell')]

    def set_default_parameters(self, db):
        """
        Set default parameters for this model in the TissueDB
        """
        p = { 'D_i': 4000000.0, 'lambda_i':8000.0, 'alpha_i':1000.0 }
             
        set_parameters(db, self.name, p)




     
    def get_ss(self, y0, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):


        p = get_parameters(db, self.name)
    
        GI = y0

        cell_type = db.get_property('cell_type')
        alpha_i = p['alpha_i']

        graph = get_graph(db)      
        S = db.get_property('S')
        V = db.get_property('V')
        cell_map = tissue_maps['cell']
        edge_map = tissue_maps['edge']

        N = y0.size

        alpha = np.zeros(y0.shape)
        
        mesh = get_mesh(db)
        for cid in mesh.wisps(2):
            if cell_type[cid]==9:
#                print cid
                i = cell_map[cid]
                alpha[i]+=alpha_i

        D_i = p['D_i']

        J = lil_matrix((N,N))

        for wid,(eid1,eid2) in wall_decomposition.iteritems() :
            sid = graph.source(eid1)
            tid = graph.target(eid1)
            Vs = V[sid]
            Vt = V[tid]
            Swall = S[wid]
            sidx = cell_map[sid]
            tidx = cell_map[tid]

            k = Swall*D_i

            J[sidx, sidx]+=-k/Vs
            J[sidx, tidx]+= k/Vs
            J[tidx, sidx]+= k/Vt
            J[tidx, tidx]+=-k/Vt
        
        lambda_i = p['lambda_i']
        for i in range(N):
            J[i, i] -= lambda_i

        for i in fixed_idx:
            J[i,:] *= 0
            J[i,i] = 1
            alpha[i] = -y0[i]
            

        y = -spsolve(J.tocsr(), alpha)


#        print 'ret_GP: ', min(ret_GP), max(ret_GP)
#        print 'ret: ', min(ret), max(ret)
        return y


    def get_fixed_idx(self, tissue_maps, species_slices):
        """
        Obtain a list of those elements in the ODE state vector y
        which are constant during the simulation
        (From the property 'fixed' in the tissue database.)
        This currently only works for cell-based properties
        
        The dictionary fixed maps a property name to a list
        of (expression, value) rules; for each cell, if the expression
        is satisfied, then the property is fixed
        This is a Python expression, and may depend on the cell
        id number and the values of all the properties of the tissue.

        e.g. fixed = { 'auxin':[ ('cell_type[cid]=='pericycle', 2) ] }
        would fix the auxin concentration in the pericycle.

        :param tissue_maps:
        :param species_slices:
        :returns: list of the offsets into the state vector y[] which
                  are to be held constant.

        """
        fixed = get_parameters(self.db, 'fixed')
        
        cell_map = tissue_maps['cell']
        fixed_idx = [] # list of fixed indices

        # Extract all the properties from the tissue data,
        # so these can be used in the eval statement
        all_properties=dict((name, self.db.get_property(name)) \
                                for name in self.db.properties())
        # Loop over all properties with rules
        for prop_name, rules in fixed.iteritems():
            # Ignore properties not in model
            if prop_name in species_slices:
                s=species_slices[prop_name]
                prop=self.db.get_property(prop_name)
                for (expr, value) in rules:
                    # Parse expression and convert it to python
                    # bytecode
                    code=compile(expr, '', 'eval')
                    # Loop over all cells
                    for cid, i in cell_map.iteritems():
                        # Test the expression for this cid
                        if eval(code, all_properties, { 'cid':cid }):
                            idx=s.start+i
                            fixed_idx.append(idx) 
        return fixed_idx



def set_fixed_properties(db):
    """
    The dictionary fixed maps a property name to a list
    of (expression, value) rules; for each cell, if the expression
    is satisfied, then the property is set to the value.
    This is a Python expression, and may depend on the cell
    id number and the values of all the properties of the tissue.
    If multiple expressions are satisfied, then all of these
    are applied, so the last in the list is used.

    e.g. fixed = { 'auxin':[ ('cell_type[cid]=='pericycle', 2) ] }
    would set the auxin concentration in the pericycle to 2.

    :param db:  tissue database
    :type db: TissueDB
    """
    # Extract mesh from TissueDB
    t = db.tissue()
    cfg = db.get_config('config')
    mesh = t.relation(cfg.mesh_id)
    # Extract all the properties from the tissue data,
    # so these can be used in the eval statement
    all_properties=dict((name, db.get_property(name)) \
                            for name in db.properties())
    # Loop over all properties with rules 
    for prop_name, rules in get_parameters(db, 'fixed').iteritems():
        # Check if property currently in tissue database
        if prop_name in db.properties():
            prop = db.get_property(prop_name)
            for (expr, value) in rules:
                # compile expression to python bytecode
                code=compile(expr, '', 'eval')
                # loop over all cells
                for cid in mesh.wisps(2):
                    # test whether expr is satisfied for this cell
                    if eval(code, all_properties, {'cid':cid}):
                        prop[cid] = value
            

    


class AuxinSS(object):

    name = 'AuxinSS'

    def __init__(self, db):
        self.db = db
        # Construct a model for each of the model classes
        self.model_species=self.get_species()

        initial_values = get_parameters(db, 'initial_values')
        divided_props = db.get_property('divided_props')
        # Initialise all of the species needed which are not already
        # contained in the TissueDB
        for species_name, species_type  in self.model_species+[('auxin_edge_flux','edge'), ('auxin_cell_flux','cell')]:
            if species_name not in db.properties() or species_name=='PLT':
                self.db,prop = def_property(
                    db, 
                    species_name, 
                    initial_values.get(species_name, 0.0), 
                    species_type.upper(), 
                    "config",
                    "")   
            if species_name not in divided_props:
                divided_props[species_name] = (species_type, 'concentration', 0.0)

        self.set_default_parameters(db)

        # Set the values for those species which are fixed
        set_fixed_properties(db)


    def get_fixed_idx(self, tissue_maps, species_slices):
        """
        Obtain a list of those elements in the ODE state vector y
        which are constant during the simulation
        (From the property 'fixed' in the tissue database.)
        This currently only works for cell-based properties
        
        The dictionary fixed maps a property name to a list
        of (expression, value) rules; for each cell, if the expression
        is satisfied, then the property is fixed
        This is a Python expression, and may depend on the cell
        id number and the values of all the properties of the tissue.

        e.g. fixed = { 'auxin':[ ('cell_type[cid]=='pericycle', 2) ] }
        would fix the auxin concentration in the pericycle.

        :param tissue_maps:
        :param species_slices:
        :returns: list of the offsets into the state vector y[] which
                  are to be held constant.

        """
        fixed = get_parameters(self.db, 'fixed')
        
        cell_map = tissue_maps['cell']
        fixed_idx = [] # list of fixed indices

        # Extract all the properties from the tissue data,
        # so these can be used in the eval statement
        all_properties=dict((name, self.db.get_property(name)) \
                                for name in self.db.properties())
        # Loop over all properties with rules
        for prop_name, rules in fixed.iteritems():
            # Ignore properties not in model
            if prop_name in species_slices:
                s=species_slices[prop_name]
                prop=self.db.get_property(prop_name)
                for (expr, value) in rules:
                    # Parse expression and convert it to python
                    # bytecode
                    code=compile(expr, '', 'eval')
                    # Loop over all cells
                    for cid, i in cell_map.iteritems():
                        # Test the expression for this cid
                        if eval(code, all_properties, { 'cid':cid }):
                            idx=s.start+i
                            fixed_idx.append(idx) 
        return fixed_idx


    def get_species(self):
        """  
        Species used in model
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        return [('auxin', 'cell')]

    def default_parameters(self):
        """
        Auxin transport parameters for model as given in Band, King 2011
        Set default parameters for this model in the TissueDB
        """

        pHcyt=7.2
        pK=4.8
        Voltage=-0.120
        Temp=300
        Rconst=8.3144621
        Fdconst=96485.3365

    
        phi=Fdconst*Voltage/(Rconst*Temp)
        B1=1/(1+pow(10,pHcyt-pK))
        B2=-(1-B1)*phi/(exp(-phi)-1)
        B3=(1-B1)*phi/(exp(phi)-1)
        P_iaah = 2016
        P_AUX = 2016
        P_PIN = 1008


        p={}
        p['phi']=phi
        p['B1']=B1*P_iaah
        p['B2']=B2*P_AUX
        p['B3']=B3*P_PIN
        return p

    def set_default_parameters(self, db):
        """
        Set default parameters for this model in the TissueDB
        """
        p = { 'A1': 483.84, 'A2': 7197.12, 'A3': 33.0,
              'B1': 8.064, 'B2': 90.72, 'B3': 26548.96 }

#        p = self.default_parameters()
        set_parameters(db, self.name, p)
    

    def calculate_fluxes(self, db, wall_decomposition):

        p = get_parameters(db, self.name)
        Auxin = db.get_property('auxin')

        graph = get_graph(db)      
        S = db.get_property('S')
        V = db.get_property('V')
        PIN = db.get_property('PIN')
        AUX = db.get_property('AUX')
        edge_flux = db.get_property('auxin_edge_flux')
        cell_flux = db.get_property('auxin_cell_flux')

        A1 = p['A1']
        A2 = p['A2']
        A3 = p['A3']
        B1 = p['B1']
        B2 = p['B2']
        B3 = p['B3']         


        cell_flux.clear()
        for cid in graph.vertices():
            cell_flux[cid]=0.0

        for wid,(eid1,eid2) in wall_decomposition.iteritems() :
            sid = graph.source(eid1)
            tid = graph.target(eid1)
#            sidx = cell_map[sid]
 #           tidx = cell_map[tid]
            sAux = Auxin[sid]
            tAux = Auxin[tid]
            PIN1 = PIN[eid1]
            PIN2 = PIN[eid2]
            AUX1 = AUX[eid1]
            AUX2 = AUX[eid2]
            wAux = (sAux*(B1+B2*AUX1+B3*PIN1)+tAux*(B1+B2*AUX2+B3*PIN2)) \
                /(2*A1+A2*(AUX1+AUX2)+A3*(PIN1+PIN2))
            Swall = S[wid]
            Vs = V[sid]
            Vt = V[tid]
            a = Swall*(B1+B2*AUX1+B3*PIN1)*(A1+A2*AUX2+A3*PIN2)/ \
                (2*A1+A2*(AUX1+AUX2)+A3*(PIN1+PIN2))
            b = Swall*(B1+B2*AUX2+B3*PIN2)*(A1+A2*AUX1+A3*PIN1)/ \
                (2*A1+A2*(AUX1+AUX2)+A3*(PIN1+PIN2))
            J = a*sAux - b*tAux
            edge_flux[eid1] = a*sAux - b*tAux
            edge_flux[eid2] = -edge_flux[eid1]
            cell_flux[sid] -= J*Swall
            cell_flux[tid] += J*Swall

        

     
    def get_ss(self, y0, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):



        p = get_parameters(db, self.name)
        Auxin = y0

        graph = get_graph(db)      
        S = db.get_property('S')
        V = db.get_property('V')
        cell_map = tissue_maps['cell']
        edge_map = tissue_maps['edge']
        PIN = db.get_property('PIN')
        AUX = db.get_property('AUX')

        A1 = p['A1']
        A2 = p['A2']
        A3 = p['A3']
        B1 = p['B1']
        B2 = p['B2']
        B3 = p['B3']         

        N = y0.size
        J = lil_matrix((N,N))

        for wid,(eid1,eid2) in wall_decomposition.iteritems() :
            sid = graph.source(eid1)
            tid = graph.target(eid1)
            sidx = cell_map[sid]
            tidx = cell_map[tid]
#            sAux = Auxin[sidx]
#            tAux = Auxin[tidx]
            PIN1 = PIN[eid1]
            PIN2 = PIN[eid2]
            AUX1 = AUX[eid1]
            AUX2 = AUX[eid2]
 #           wAux = (sAux*(B1+B2*AUX1+B3*PIN1)+tAux*(B1+B2*AUX2+B3*PIN2)) \
 #               /(2*A1+A2*(AUX1+AUX2)+A3*(PIN1+PIN2))
            Swall = S[wid]
            Vs = V[sid]
            Vt = V[tid]
            a = Swall*(B1+B2*AUX1+B3*PIN1)*(A1+A2*AUX2+A3*PIN2)/ \
                (2*A1+A2*(AUX1+AUX2)+A3*(PIN1+PIN2))
            b = Swall*(B1+B2*AUX2+B3*PIN2)*(A1+A2*AUX1+A3*PIN1)/ \
                (2*A1+A2*(AUX1+AUX2)+A3*(PIN1+PIN2))

            J[sidx, sidx] += -a/Vs
            J[sidx, tidx] += b/Vs
            J[tidx, sidx] += a/Vt
            J[tidx, tidx] += -b/Vt

        alpha = np.zeros(y0.shape)

        for i in fixed_idx:
            J[i, :] *=0
            J[i, i] = 1
            
            alpha[i] = -y0[i]


        y = -spsolve(J.tocsr(), alpha)


#        print 'ret_GP: ', min(ret_GP), max(ret_GP)
#        print 'ret: ', min(ret), max(ret)
        return y


    def step(self):
        """
        Evolve the gene network for one timestep
        """
        
        set_fixed_properties(self.db)
        print 'SS_start'
        
        t=self.db.tissue()

        # Update the cell volumes and wall surface areas - these may
        # have been changed through tissue growth
        updateVS(self.db)

        # Recalculate the wall to edge map, and the tissue maps
        wall_decomposition = get_wall_decomp(self.db)
        tissue_maps = get_tissue_maps(self.db)
     
        # For each model species, calculate the slice (indices) of
        # the y[] array which will store property values during 
        # integration
        offset=0 # current offset into the y[] array
        species_slices={} # map from species name to the slice of y[]
        for species_name, species_type in self.model_species:
            # 'size' is how many values are needed to store a property
            # of that type
            size = len(tissue_maps[species_type])
            species_slices[species_name] = slice(offset, offset+size)
            offset+=size

        # Extract mesh
        cfg = self.db.get_config('config')
        mesh = t.relation(cfg.mesh_id)
        # Type of each cell
        cell_type = self.db.get_property('cell_type')
        
        # Call routine to find fixed property values
        fixed_idx=self.get_fixed_idx(tissue_maps, species_slices)

        #initialise with zero
        y0=np.zeros((offset,))

        ### SETUP INITIAL VALUES
        # Loop over all species referred to in these models
        for species_name, species_type in self.model_species:
            # extract property dict from TissueDB
            prop=self.db.get_property(species_name)
            # obtain appropriate section of y0[]
            y0_slice=y0[species_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems() :
                y0_slice[idx]=prop[tid]

 
        yend=self.get_ss(y0, self.db, tissue_maps, species_slices, wall_decomposition, fixed_idx)
 
        
        for species_name, species_type in self.model_species: 
            prop=self.db.get_property(species_name)
            yend_slice=yend[species_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems() :
                prop[tid]=yend_slice[idx]

        self.get_ss(y0, self.db, tissue_maps, species_slices, wall_decomposition, fixed_idx)
        self.calculate_fluxes(self.db, wall_decomposition)

        print 'SS_end'



class acwdSS(object):

    name = 'acwdSS'

    def __init__(self, db, const_J=False):
        self.db = db
        # Construct a model for each of the model classes
        self.model_species=self.get_species()
        

        initial_values = get_parameters(db, 'initial_values')
        divided_props = db.get_property('divided_props')
        # Initialise all of the species needed which are not already
        # contained in the TissueDB
        for species_name, species_type  in self.model_species:
            if species_name not in db.properties() or species_name=='PLT':
                self.db,prop = def_property(
                    db, 
                    species_name, 
                    initial_values.get(species_name, 0.0), 
                    species_type.upper(), 
                    "config",
                    "")   
            if species_name not in divided_props:
                divided_props[species_name] = (species_type, 'concentration', 0.0)

            


        self.set_default_parameters(db)

        # Set the values for those species which are fixed
        set_fixed_properties(db)


    def get_fixed_idx(self, tissue_maps, species_slices):
        """
        Obtain a list of those elements in the ODE state vector y
        which are constant during the simulation
        (From the property 'fixed' in the tissue database.)
        This currently only works for cell-based properties
        
        The dictionary fixed maps a property name to a list
        of (expression, value) rules; for each cell, if the expression
        is satisfied, then the property is fixed
        This is a Python expression, and may depend on the cell
        id number and the values of all the properties of the tissue.

        e.g. fixed = { 'auxin':[ ('cell_type[cid]=='pericycle', 2) ] }
        would fix the auxin concentration in the pericycle.

        :param tissue_maps:
        :param species_slices:
        :returns: list of the offsets into the state vector y[] which
                  are to be held constant.

        """
        fixed = get_parameters(self.db, 'fixed')
        
        cell_map = tissue_maps['cell']
        fixed_idx = [] # list of fixed indices

        # Extract all the properties from the tissue data,
        # so these can be used in the eval statement
        all_properties=dict((name, self.db.get_property(name)) \
                                for name in self.db.properties())
        # Loop over all properties with rules
        for prop_name, rules in fixed.iteritems():
            # Ignore properties not in model
            if prop_name in species_slices:
                s=species_slices[prop_name]
                prop=self.db.get_property(prop_name)
                for (expr, value) in rules:
                    # Parse expression and convert it to python
                    # bytecode
                    code=compile(expr, '', 'eval')
                    # Loop over all cells
                    for cid, i in cell_map.iteritems():
                        # Test the expression for this cid
                        if eval(code, all_properties, { 'cid':cid }):
                            idx=s.start+i
                            fixed_idx.append(idx) 

        return fixed_idx


    def get_species(self):
        """  
        Species used in model
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        return [('auxin', 'cell'), ('auxin_wall', 'wall')]


    def set_default_parameters(self, db):
        """
        Set d efault parameters for this model in the TissueDB
        """

        pHapo=5.3 # apoplastic pH
        pHcyt=7.2 # cytoplasmic pH 
        pK=4.8
        Voltage=-0.120  # Membrane potential (Volts)
        Temp=300 # K
        PIAAH=0.56#0.56  # Permeability of membrane to protonated auxin (microns/sec)
	PAUX=0.56 # 0.56 # Permeability of membrane to anionic auxin due to AUX1 (microns/sec)
	PLAX=0.56 #.56 # Permeability of membrane to anionic auxin due to AUX1 (microns/sec)
	PPIN=0.56   # Permeability of membrane to anionic auxin due to PINs (microns/sec)
	PNPE=0.56*0.3   # Permeability of membrane to anionic auxin due to PINs (microns/sec)	
	lamb=0.14  # Cell-wall thickness (microns)
	D_cw = 32    
        alpha_back= 0.001  # Auxin production rate in all cells except QC and columella initials
	alpha_QCinit=0.01 # Auxin production rate in QC and columella initials
        
	beta =0.001 # Auxin degradation rate

        p = { 'pHapo': pHapo, 'pHcyt': pHcyt, 'Voltage':Voltage, 
               'PIAAH':PIAAH, 'PAUX':PAUX,'PLAX':PLAX, 'PPIN':PPIN, 'PNPE':PNPE, 'lamb':lamb, 'D_cw':D_cw, 'alpha_back':alpha_back,'alpha_QCinit':alpha_QCinit,'beta':beta, 'Temp':Temp, 'pK':pK }

        set_parameters(db, self.name, p)
        return p

     
    def get_ss(self, y0, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):

        p = get_parameters(db, self.name)
        graph = get_graph(db)      
        S = db.get_property('S')
        V = db.get_property('V')
        cell_type = db.get_property('cell_type')
        cell_map = tissue_maps['cell']
        edge_map = tissue_maps['edge']
        wall_map = tissue_maps['wall']
        PIN = db.get_property('PIN')
        AUX = db.get_property('AUX')
        LAX = db.get_property('LAX')

#        print 'J LAX', [(i,v) for (i,v) in LAX.iteritems() if abs(v)>1e-6]

        pHapo=p['pHapo'] # apoplastic pH
        pHcyt=p['pHcyt'] # cytoplasmic pH
        pK=p['pK']
        Voltage=p['Voltage']  # Membrane potential (Volts)
        Temp=p['Temp'] # K

        Rconst=8.31
        Fdconst=96500

        phi=Fdconst*Voltage/(Rconst*Temp)	
        A1=1/(1+pow(10,pHapo-pK)) 
        A2=(1-A1)*phi/(exp(phi)-1)
        A3=-(1-A1)*phi/(exp(-phi)-1)
        B1=1/(1+pow(10,pHcyt-pK))
        B2=-(1-B1)*phi/(exp(-phi)-1)
        B3=(1-B1)*phi/(exp(phi)-1)

        PIAAH = p['PIAAH']
        PAUX = p['PAUX']
        PLAX = p['PLAX']
        PPIN = p['PPIN']
        PNPE = p['PNPE']
        P = db.get_property('P')

        lamb = p['lamb']
        D_cw = p['D_cw']
        alpha_back=p['alpha_back']
        alpha_high=p['alpha_QCinit']
        beta=p['beta']

        alpha={}
        for cid in graph.vertices():
                if cell_type[cid]==17 or cell_type[cid]==10:	
                        alpha[cid]=alpha_high
                else:
                        alpha[cid]=alpha_back


        N = y0.size
        J = dok_matrix((N,N))

        a_sl = species_slices['auxin']
        aw_sl = species_slices['auxin_wall']
        a_s = a_sl.start
        a_d = a_sl.step if a_sl.step else 1 
        aw_s = aw_sl.start
        aw_d = aw_sl.step if aw_sl.step else 1

        mesh = get_mesh(db)


        for wid,(eid1,eid2) in wall_decomposition.iteritems() :
            sid = graph.source(eid1)
            tid = graph.target(eid1)
            sidx = cell_map[sid]
            tidx = cell_map[tid]
            widx = wall_map[wid]
            a_sidx = a_s+a_d*sidx
            a_tidx = a_s+a_d*tidx
            a_widx = aw_s+aw_d*widx


            P1 = P[eid1]
            P2 = P[eid2]

            PIN1 = PIN[eid1]
            PIN2 = PIN[eid2]

            AUX1 = AUX[eid1]
            AUX2 = AUX[eid2]

            LAX1 = LAX[eid1]
            LAX2 = LAX[eid2]

            Swall = float(S[wid])
            Vs = V[sid]
            Vt = V[tid]

            b1 = B1*PIAAH+B2*PAUX*AUX1+B2*PLAX*LAX1+B3*PPIN*PIN1+B3*PNPE
            a1 = A1*PIAAH+A2*PAUX*AUX1+A2*PLAX*LAX1+A3*PPIN*PIN1+A3*PNPE

            b2 = B1*PIAAH+B2*PAUX*AUX2+B2*PLAX*LAX2+B3*PPIN*PIN2+B3*PNPE
            a2 = A1*PIAAH+A2*PAUX*AUX2+A2*PLAX*LAX2+A3*PPIN*PIN2+A3*PNPE

            J[a_sidx, a_sidx] += -b1*Swall/Vs*P1
            J[a_sidx, a_widx] += a1*Swall/Vs*P1
            J[a_widx, a_sidx] += b1/lamb*P1
            J[a_widx, a_widx] += -a1/lamb*P1

            J[a_tidx, a_tidx] += -b2*Swall/Vt*P2
            J[a_tidx, a_widx] += a2*Swall/Vt*P2
            J[a_widx, a_tidx] += b2/lamb*P2
            J[a_widx, a_widx] += -a2/lamb*P2

        if 'casparian' in db.properties():
            casparian = db.get_property('casparian')
            print 'casparian', list(wid for wid,v in casparian.iteritems() if v!=0)
        else:
            casparian = {}



        S_v = {}
        for pid in mesh.wisps(0):
            bottom = 0.0
            for wid in mesh.regions(0, pid):
                if casparian.get(wid,0)==0:
                    bottom += 1/S[wid]
            if bottom!=0.0:
                S_v[pid] = 1.0/bottom


        for wid1 in mesh.wisps(1):
            if casparian.get(wid1,0)==0:
                Sw1 = S[wid1]
                k = 2*D_cw/Sw1/Sw1
                widx1 = wall_map[wid1]
                a_widx1 = aw_s+aw_d*widx1
                Np = 0
                for pid in mesh.borders(1, wid1):
                    N = len(set(wid for wid in mesh.regions(0, pid) if casparian.get(wid,0)==0))
                    if N>1:
                        for wid2 in mesh.regions(0, pid):
                            if wid2 != wid1 and casparian.get(wid2, 0)==0:
                                Np+=1
                                Sw2 = S[wid2]
                                widx2 = wall_map[wid2]
                                a_widx2 = aw_s+aw_d*widx2
                                J[a_widx1, a_widx2] += k*S_v[pid]/Sw2
                        J[a_widx1, a_widx1] += k*(S_v[pid]/Sw1-1.0)

                if Np==0 and mesh.nb_regions(1,wid1)==1:
                    J[a_widx1, a_widx1] = 1.0
            elif mesh.nb_regions(1,wid)==1:
                widx1 = wall_map[wid1]
                a_widx1 = aw_s+aw_d*widx1
                J[a_widx1, a_widx1] = 1.0

        """
        for wid1 in mesh.wisps(1):   
            if mesh.nb_regions(1, wid1)==1:
                widx1 = wall_map[wid1]
                a_widx1 = aw_s+aw_d*widx1
                J[a_widx1, a_widx1] = 1.0
        """

        r = np.zeros(y0.shape)

        for cid in mesh.wisps(2):
            cidx = cell_map[cid]            
            a_cidx = a_s+a_d*cidx
            J[a_cidx, a_cidx] -= beta
            r[a_cidx] = alpha[cid]

        for i in fixed_idx:
            J[i, :] = 0
            J[i, i] = 1
            r[i] = -y0[i]


#        for i in range(N):
#            print i, J[i, i]
#        A = J.tocsr()
#        print 'A'
#        print a_s, a_d, aw_s, aw_d, len(cell_map), len(wall_map), len(wall_decomposition)
        self.J = J.tocsr()
#
          
                
    
        y = -spsolve(self.J, r)

        print 'err -----',  np.max(np.abs(J.dot(y)+r))

        """
        for i in fixed_idx:
            print y[i], y0[i]

        print 'y, diff: ', max(y), min(y), max(np.abs(y-y0))
        
        
        z = self.deriv(y0, 0, db, tissue_maps, species_slices, wall_decomposition, fixed_idx)
        print np.max(np.abs(z))
        delta = 0.000001
        print 'fixed', fixed_idx

        for i in range(500):
            if i in fixed_idx:
                continue
            y1 = np.array(y0)
            h = delta*y0[i]+1e-12
            y1[i]+=h
            est = (self.deriv(y1, 0, db, tissue_maps, species_slices, wall_decomposition, fixed_idx)-z)/h
#            print J[:,i].nonzero()
            for j in J[:,i].nonzero()[0]:
                if abs(J[j,i]-est.T[j])>1e-6:
                    print i, j, J[j,i], J[j,i]-est.T[j]
        """

        return y


    def step(self):
        """
        Evolve the gene network for one timestep
        """
        
        set_fixed_properties(self.db)
        
        t=self.db.tissue()

        # Update the cell volumes and wall surface areas - these may
        # have been changed through tissue growth
        updateVS(self.db)

        # Recalculate the wall to edge map, and the tissue maps
        wall_decomposition = get_wall_decomp(self.db)
        tissue_maps = get_tissue_maps(self.db)
     
        # For each model species, calculate the slice (indices) of
        # the y[] array which will store property values during 
        # integration
        offset=0 # current offset into the y[] array
        species_slices={} # map from species name to the slice of y[]
        for species_name, species_type in self.model_species:
            # 'size' is how many values are needed to store a property
            # of that type
            size = len(tissue_maps[species_type])
            species_slices[species_name] = slice(offset, offset+size)
            offset+=size

        # Extract mesh
        cfg = self.db.get_config('config')
        mesh = t.relation(cfg.mesh_id)
        # Type of each cell
        cell_type = self.db.get_property('cell_type')
        
        # Call routine to find fixed property values
        fixed_idx=self.get_fixed_idx(tissue_maps, species_slices)

        #initialise with zero
        y0=np.zeros((offset,))

        ### SETUP INITIAL VALUES
        # Loop over all species referred to in these models
        for species_name, species_type in self.model_species:
            # extract property dict from TissueDB
            prop=self.db.get_property(species_name)
            # obtain appropriate section of y0[]
            y0_slice=y0[species_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems() :
                y0_slice[idx]=prop[tid]

 
        yend=self.get_ss(y0, self.db, tissue_maps, species_slices, wall_decomposition, fixed_idx)
 
        
        for species_name, species_type in self.model_species: 
            prop=self.db.get_property(species_name)
            yend_slice=yend[species_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems() :
                prop[tid]=yend_slice[idx]



class acwdSSpH(object):

    name = 'acwdSSpH'

    def __init__(self, db):
        self.db = db
        # Construct a model for each of the model classes
        self.model_species=self.get_species()

        initial_values = get_parameters(db, 'initial_values')
        divided_props = db.get_property('divided_props')
        # Initialise all of the species needed which are not already
        # contained in the TissueDB
        for species_name, species_type  in self.model_species:
            if species_name not in db.properties() or species_name=='PLT':
                self.db,prop = def_property(
                    db, 
                    species_name, 
                    initial_values.get(species_name, 0.0), 
                    species_type.upper(), 
                    "config",
                    "")   
            if species_name not in divided_props:
                divided_props[species_name] = (species_type, 'concentration', 0.0)

        for species_name, species_type in [('pHapo','wall'), ('pHcyt','cell')]:
            if species_name not in db.properties() or species_name=='PLT':
                self.db,prop = def_property(
                    db, 
                    species_name, 
                    initial_values.get(species_name, 0.0), 
                    species_type.upper(), 
                    "config",
                    "")   
            if species_name not in divided_props:
                divided_props[species_name] = (species_type, 'attribute', 0.0)


        self.set_default_parameters(db)

        # Set the values for those species which are fixed
        set_fixed_properties(db)


    def get_fixed_idx(self, tissue_maps, species_slices):
        """
        Obtain a list of those elements in the ODE state vector y
        which are constant during the simulation
        (From the property 'fixed' in the tissue database.)
        This currently only works for cell-based properties
        
        The dictionary fixed maps a property name to a list
        of (expression, value) rules; for each cell, if the expression
        is satisfied, then the property is fixed
        This is a Python expression, and may depend on the cell
        id number and the values of all the properties of the tissue.

        e.g. fixed = { 'auxin':[ ('cell_type[cid]=='pericycle', 2) ] }
        would fix the auxin concentration in the pericycle.

        :param tissue_maps:
        :param species_slices:
        :returns: list of the offsets into the state vector y[] which
                  are to be held constant.

        """
        fixed = get_parameters(self.db, 'fixed')
        
        cell_map = tissue_maps['cell']
        fixed_idx = [] # list of fixed indices

        # Extract all the properties from the tissue data,
        # so these can be used in the eval statement
        all_properties=dict((name, self.db.get_property(name)) \
                                for name in self.db.properties())
        # Loop over all properties with rules
        for prop_name, rules in fixed.iteritems():
            # Ignore properties not in model
            if prop_name in species_slices:
                s=species_slices[prop_name]
                prop=self.db.get_property(prop_name)
                for (expr, value) in rules:
                    # Parse expression and convert it to python
                    # bytecode
                    code=compile(expr, '', 'eval')
                    # Loop over all cells
                    for cid, i in cell_map.iteritems():
                        # Test the expression for this cid
                        if eval(code, all_properties, { 'cid':cid }):
                            idx=s.start+i
                            fixed_idx.append(idx) 
        return fixed_idx


    def get_species(self):
        """  
        Species used in model
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        return [('auxin', 'cell'), ('auxin_wall', 'wall')]


    def set_default_parameters(self, db):
        """
        Set d efault parameters for this model in the TissueDB
        """

        pHapo=5.3 # apoplastic pH
        pHcyt=7.2 # cytoplasmic pH 
        pK=4.8
        Voltage=-0.120  # Membrane potential (Volts)
        Temp=300 # K
        PIAAH=0.56#0.56  # Permeability of membrane to protonated auxin (microns/sec)
	PAUX=0.56 # 0.56 # Permeability of membrane to anionic auxin due to AUX1 (microns/sec)
	PLAX=0.56 #.56 # Permeability of membrane to anionic auxin due to AUX1 (microns/sec)
	PPIN=0.56   # Permeability of membrane to anionic auxin due to PINs (microns/sec)
	PNPE=0.56*0.3   # Permeability of membrane to anionic auxin due to PINs (microns/sec)	
	lamb=0.14  # Cell-wall thickness (microns)
	D_cw = 32    
        alpha_back= 0.00  # Auxin production rate in all cells except QC and columella initials
	alpha_QCinit=0.01 # Auxin production rate in QC and columella initials
        
	beta =0.001 # Auxin degradation rate

        p = { 'pHapo': pHapo, 'pHcyt': pHcyt, 'Voltage':Voltage, 
               'PIAAH':PIAAH, 'PAUX':PAUX,'PLAX':PLAX, 'PPIN':PPIN, 'PNPE':PNPE, 'lamb':lamb, 'D_cw':D_cw, 'alpha_back':alpha_back,'alpha_QCinit':alpha_QCinit,'beta':beta, 'Temp':Temp, 'pK':pK }

        set_parameters(db, self.name, p)
        return p

     
    def get_ss(self, y0, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):

    

        p = get_parameters(db, self.name)

        graph = get_graph(db)      
        S = db.get_property('S')
        V = db.get_property('V')
        cell_type = db.get_property('cell_type')
        cell_map = tissue_maps['cell']
        edge_map = tissue_maps['edge']
        wall_map = tissue_maps['wall']
        PIN = db.get_property('PIN')
        AUX = db.get_property('AUX')
        LAX = db.get_property('LAX')

        pHapo = db.get_property('pHapo')
        pHcyt = db.get_property('pHcyt')

#        print 'J LAX', [(i,v) for (i,v) in LAX.iteritems() if abs(v)>1e-6]

#        pHapo=p['pHapo'] # apoplastic pH
#        pHcyt=p['pHcyt'] # cytoplasmic pH
        pK=p['pK']
        Voltage=p['Voltage']  # Membrane potential (Volts)
        Temp=p['Temp'] # K

	Rconst=8.31
	Fdconst=96500

        phi=Fdconst*Voltage/(Rconst*Temp)	

	PIAAH = p['PIAAH']
	PAUX = p['PAUX']
	PLAX = p['PLAX']
	PPIN = p['PPIN']
	PNPE = p['PNPE']
        P = db.get_property('P')

	lamb = p['lamb']
	D_cw = p['D_cw']
	alpha_back=p['alpha_back']
  	alpha_high=p['alpha_QCinit']
	beta=p['beta']

	alpha={}
	for cid in graph.vertices():
		if cell_type[cid]==17 or cell_type[cid]==10:	
			alpha[cid]=alpha_high
		else:
			alpha[cid]=alpha_back


        N = y0.size
        J = lil_matrix((N,N))

        a_sl = species_slices['auxin']
        aw_sl = species_slices['auxin_wall']
        a_s = a_sl.start
        a_d = a_sl.step if a_sl.step else 1 
        aw_s = aw_sl.start
        aw_d = aw_sl.step if aw_sl.step else 1

        mesh = get_mesh(db)

        
        for wid,(eid1,eid2) in wall_decomposition.iteritems() :
            sid = graph.source(eid1)
            tid = graph.target(eid1)
            sidx = cell_map[sid]
            tidx = cell_map[tid]
            widx = wall_map[wid]
            a_sidx = a_s+a_d*sidx
            a_tidx = a_s+a_d*tidx
            a_widx = aw_s+aw_d*widx


            P1 = P[eid1]
            P2 = P[eid2]

            PIN1 = PIN[eid1]
            PIN2 = PIN[eid2]

            AUX1 = AUX[eid1]
            AUX2 = AUX[eid2]

            LAX1 = LAX[eid1]
            LAX2 = LAX[eid2]
                    
            Swall = float(S[wid])
            Vs = V[sid]
            Vt = V[tid]

            A1=1/(1+pow(10,pHapo[wid]-pK)) 
            A2=(1-A1)*phi/(exp(phi)-1)
            A3=-(1-A1)*phi/(exp(-phi)-1)
            B1=1/(1+pow(10,pHcyt[sid]-pK))
            B2=-(1-B1)*phi/(exp(-phi)-1)
            B3=(1-B1)*phi/(exp(phi)-1)


            b1 = B1*PIAAH+B2*PAUX*AUX1+B2*PLAX*LAX1+B3*PPIN*PIN1+B3*PNPE
            a1 = A1*PIAAH+A2*PAUX*AUX1+A2*PLAX*LAX1+A3*PPIN*PIN1+A3*PNPE

            B1=1/(1+pow(10,pHcyt[tid]-pK))
            B2=-(1-B1)*phi/(exp(-phi)-1)
            B3=(1-B1)*phi/(exp(phi)-1)


            b2 = B1*PIAAH+B2*PAUX*AUX2+B2*PLAX*LAX2+B3*PPIN*PIN2+B3*PNPE
            a2 = A1*PIAAH+A2*PAUX*AUX2+A2*PLAX*LAX2+A3*PPIN*PIN2+A3*PNPE

            J[a_sidx, a_sidx] += -b1*Swall/Vs*P1
            J[a_sidx, a_widx] += a1*Swall/Vs*P1
            J[a_widx, a_sidx] += b1/lamb*P1
            J[a_widx, a_widx] += -a1/lamb*P1

            J[a_tidx, a_tidx] += -b2*Swall/Vt*P2
            J[a_tidx, a_widx] += a2*Swall/Vt*P2
            J[a_widx, a_tidx] += b2/lamb*P2
            J[a_widx, a_widx] += -a2/lamb*P2

        if 'casparian' in db.properties():
            casparian = db.get_property('casparian')
            print 'casparian', list(wid for wid,v in casparian.iteritems() if v!=0)
        else:
            casparian = {}

        
            
        S_v = {}
        for pid in mesh.wisps(0):
            bottom = 0.0
            for wid in mesh.regions(0, pid):
                if casparian.get(wid,0)==0:
                    bottom += 1/S[wid]
            if bottom!=0.0:
                S_v[pid] = 1.0/bottom
       

        for wid1 in mesh.wisps(1):
            if casparian.get(wid1,0)==0:
                Sw1 = S[wid1]
                k = 2*D_cw/Sw1/Sw1
                widx1 = wall_map[wid1]
                a_widx1 = aw_s+aw_d*widx1
                Np = 0
                for pid in mesh.borders(1, wid1):
                    N = len(set(wid for wid in mesh.regions(0, pid) if casparian.get(wid,0)==0))
                    if N>1:
                        for wid2 in mesh.regions(0, pid):
                            if wid2 != wid1 and casparian.get(wid2, 0)==0:
                                Np+=1
                                Sw2 = S[wid2]
                                widx2 = wall_map[wid2]
                                a_widx2 = aw_s+aw_d*widx2
                                J[a_widx1, a_widx2] += k*S_v[pid]/Sw2
                        J[a_widx1, a_widx1] += k*(S_v[pid]/Sw1-1.0)

                if Np==0 and mesh.nb_regions(1,wid1)==1:
                    J[a_widx1, a_widx1] = 1.0
            elif mesh.nb_regions(1,wid)==1:
                widx1 = wall_map[wid1]
                a_widx1 = aw_s+aw_d*widx1
                J[a_widx1, a_widx1] = 1.0

        """
        for wid1 in mesh.wisps(1):   
            if mesh.nb_regions(1, wid1)==1:
                widx1 = wall_map[wid1]
                a_widx1 = aw_s+aw_d*widx1
                J[a_widx1, a_widx1] = 1.0
        """
       
        r = np.zeros(y0.shape)

        for cid in mesh.wisps(2):
            cidx = cell_map[cid]            
            a_cidx = a_s+a_d*cidx
            J[a_cidx, a_cidx] -= beta
            r[a_cidx] = alpha[cid]

 
        for i in fixed_idx:
            J[i, :] *=0
            J[i, i] = 1
            
            r[i] = -y0[i]

#        for i in range(N):
#            print i, J[i, i]
#        A = J.tocsr()
#        print 'A'
#        print a_s, a_d, aw_s, aw_d, len(cell_map), len(wall_map), len(wall_decomposition)

        y = -spsolve(J.tocsr(), r)

        print 'err -----',  np.max(np.abs(J.dot(y)+r))

        """
        for i in fixed_idx:
            print y[i], y0[i]

        print 'y, diff: ', max(y), min(y), max(np.abs(y-y0))
        
        
        z = self.deriv(y0, 0, db, tissue_maps, species_slices, wall_decomposition, fixed_idx)
        print np.max(np.abs(z))
        delta = 0.000001
        print 'fixed', fixed_idx

        for i in range(500):
            if i in fixed_idx:
                continue
            y1 = np.array(y0)
            h = delta*y0[i]+1e-12
            y1[i]+=h
            est = (self.deriv(y1, 0, db, tissue_maps, species_slices, wall_decomposition, fixed_idx)-z)/h
#            print J[:,i].nonzero()
            for j in J[:,i].nonzero()[0]:
                if abs(J[j,i]-est.T[j])>1e-6:
                    print i, j, J[j,i], J[j,i]-est.T[j]
        """

        return y


    def step(self):
        """
        Evolve the gene network for one timestep
        """
        
        set_fixed_properties(self.db)
        
        t=self.db.tissue()

        # Update the cell volumes and wall surface areas - these may
        # have been changed through tissue growth
        updateVS(self.db)

        # Recalculate the wall to edge map, and the tissue maps
        wall_decomposition = get_wall_decomp(self.db)
        tissue_maps = get_tissue_maps(self.db)
     
        # For each model species, calculate the slice (indices) of
        # the y[] array which will store property values during 
        # integration
        offset=0 # current offset into the y[] array
        species_slices={} # map from species name to the slice of y[]
        for species_name, species_type in self.model_species:
            # 'size' is how many values are needed to store a property
            # of that type
            size = len(tissue_maps[species_type])
            species_slices[species_name] = slice(offset, offset+size)
            offset+=size

        # Extract mesh
        cfg = self.db.get_config('config')
        mesh = t.relation(cfg.mesh_id)
        # Type of each cell
        cell_type = self.db.get_property('cell_type')
        
        # Call routine to find fixed property values
        fixed_idx=self.get_fixed_idx(tissue_maps, species_slices)

        #initialise with zero
        y0=np.zeros((offset,))

        ### SETUP INITIAL VALUES
        # Loop over all species referred to in these models
        for species_name, species_type in self.model_species:
            # extract property dict from TissueDB
            prop=self.db.get_property(species_name)
            # obtain appropriate section of y0[]
            y0_slice=y0[species_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems() :
                y0_slice[idx]=prop[tid]

 
        yend=self.get_ss(y0, self.db, tissue_maps, species_slices, wall_decomposition, fixed_idx)
 
        
        for species_name, species_type in self.model_species: 
            prop=self.db.get_property(species_name)
            yend_slice=yend[species_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems() :
                prop[tid]=yend_slice[idx]



class AuxinWallsSS(object):

    name = 'AuxinWallsSS'

    def __init__(self, db):
        self.db = db
        # Construct a model for each of the model classes
        self.model_species=self.get_species()

        initial_values = get_parameters(db, 'initial_values')
        divided_props = db.get_property('divided_props')
        # Initialise all of the species needed which are not already
        # contained in the TissueDB
        for species_name, species_type  in self.model_species:
            if species_name not in db.properties() or species_name=='PLT':
                self.db,prop = def_property(
                    db, 
                    species_name, 
                    initial_values.get(species_name, 0.0), 
                    species_type.upper(), 
                    "config",
                    "")   
            if species_name not in divided_props:
                divided_props[species_name] = (species_type, 'concentration', 0.0)

        self.set_default_parameters(db)

        # Set the values for those species which are fixed
        set_fixed_properties(db)


    def get_fixed_idx(self, tissue_maps, species_slices):
        """
        Obtain a list of those elements in the ODE state vector y
        which are constant during the simulation
        (From the property 'fixed' in the tissue database.)
        This currently only works for cell-based properties
        
        The dictionary fixed maps a property name to a list
        of (expression, value) rules; for each cell, if the expression
        is satisfied, then the property is fixed
        This is a Python expression, and may depend on the cell
        id number and the values of all the properties of the tissue.

        e.g. fixed = { 'auxin':[ ('cell_type[cid]=='pericycle', 2) ] }
        would fix the auxin concentration in the pericycle.

        :param tissue_maps:
        :param species_slices:
        :returns: list of the offsets into the state vector y[] which
                  are to be held constant.

        """
        fixed = get_parameters(self.db, 'fixed')
        
        cell_map = tissue_maps['cell']
        fixed_idx = [] # list of fixed indices

        # Extract all the properties from the tissue data,
        # so these can be used in the eval statement
        all_properties=dict((name, self.db.get_property(name)) \
                                for name in self.db.properties())
        # Loop over all properties with rules
        for prop_name, rules in fixed.iteritems():
            # Ignore properties not in model
            if prop_name in species_slices:
                s=species_slices[prop_name]
                prop=self.db.get_property(prop_name)
                for (expr, value) in rules:
                    # Parse expression and convert it to python
                    # bytecode
                    code=compile(expr, '', 'eval')
                    # Loop over all cells
                    for cid, i in cell_map.iteritems():
                        # Test the expression for this cid
                        if eval(code, all_properties, { 'cid':cid }):
                            idx=s.start+i
                            fixed_idx.append(idx) 
        return fixed_idx


    def get_species(self):
        """  
        Species used in model
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        return [('auxin', 'cell'), ('auxin_wall', 'wall')]


    def set_default_parameters(self, db):
        """
        Set default parameters for this model in the TissueDB
        """
        p={}
	p['A1']=0.24
	p['A2']=3.57
	p['A3']=0.034
	p['B1']=0.004
	p['B2']=0.045
	p['B3']=4.68
	p['P_iaah']=2016 #3600 x 0.56 um s-1 = 2016 um hr-1
	p['P_AUX']=2016  #3600 x 0.56 um s-1 = 2016 um hr-1
	p['P_LAX']=2016  #3600 x 0.56 um s-1 = 2016 um hr-1
	p['P_PIN']=1008  #3600 x 0.28 um s-1 = 1008 um hr-1 

	p['lamb'] = 0.2
	p['D_cw'] = 1
        set_parameters(db, self.name, p)
    

     
    def get_ss(self, y0, db, tissue_maps, species_slices, wall_decomposition, fixed_idx):

        p = get_parameters(db, self.name)

        graph = get_graph(db)      
        S = db.get_property('S')
        V = db.get_property('V')
        cell_map = tissue_maps['cell']
        edge_map = tissue_maps['edge']
        wall_map = tissue_maps['wall']
        PIN = db.get_property('PIN')
        AUX = db.get_property('AUX')
        A1 = p['A1']
        A2 = p['A2']
        A3 = p['A3']
        B1 = p['B1']
        B2 = p['B2']
        B3 = p['B3']         
	P_iaah = p['P_iaah']
	P_AUX = p['P_AUX']
	P_LAX = p['P_LAX']
	P_PIN = p['P_PIN']
	lamb = p['lamb']
	D_cw = p['D_cw']


        N = y0.size
        J = lil_matrix((N,N))

        a_sl = species_slices['auxin']
        aw_sl = species_slices['auxin_wall']
        a_s = a_sl.start
        a_d = a_sl.step if a_sl.step else 1 
        aw_s = aw_sl.start
        aw_d = aw_sl.step if aw_sl.step else 1
 
        for wid,(eid1,eid2) in wall_decomposition.iteritems() :
            sid = graph.source(eid1)
            tid = graph.target(eid1)
            sidx = cell_map[sid]
            tidx = cell_map[tid]
            widx = wall_map[wid]
            a_sidx = a_s+a_d*sidx
            a_tidx = a_s+a_d*tidx
            a_widx = aw_s+aw_d*widx

            PIN1 = PIN[eid1]
            PIN2 = PIN[eid2]
            AUX1 = AUX[eid1]
            AUX2 = AUX[eid2]
                    
            Swall = S[wid]
            Vs = V[sid]
            Vt = V[tid]

            b1 = B1*P_iaah+B2*P_AUX*AUX1+B3*P_PIN*PIN1
            a1 = A1*P_iaah+A2*P_AUX*AUX1+A3*P_PIN*PIN1

            b2 = B1*P_iaah+B2*P_AUX*AUX2+B3*P_PIN*PIN2
            a2 = A1*P_iaah+A2*P_AUX*AUX2+A3*P_PIN*PIN2


            J[a_sidx, a_sidx] += -b1*Swall/Vs
            J[a_sidx, a_widx] += a1*Swall/Vs
            J[a_widx, a_sidx] += b1/lamb
            J[a_widx, a_widx] += -a1/lamb

            J[a_tidx, a_tidx] += -b2*Swall/Vt
            J[a_tidx, a_widx] += a2*Swall/Vt
            J[a_widx, a_tidx] += b2/lamb
            J[a_widx, a_widx] += -a2/lamb
#            print widx, J[a_widx, a_widx]

        mesh = get_mesh(db)
        S_v = {}
        for pid in mesh.wisps(0):
            bottom = 0.0
            for wid in mesh.regions(0, pid):
                bottom += 1/S[wid]
            if bottom!=0.0:
                S_v[pid] = 1.0/bottom

        for wid1 in mesh.wisps(1):
            Sw1 = S[wid1]
            k = 2*D_cw/Sw1/Sw1
            widx1 = wall_map[wid1]
            a_widx1 = aw_s+aw_d*widx1
            Np = 0
            for pid in mesh.borders(1, wid1):
                N = mesh.nb_regions(0, pid)
                if N>1:
                    Np+=1
                    for wid2 in mesh.regions(0, pid):
                        if wid2 != wid1:
                            Sw2 = S[wid2]
                            widx2 = wall_map[wid2]
                            a_widx2 = aw_s+aw_d*widx2
                            J[a_widx1, a_widx2] += k*S_v[pid]/Sw2
                    J[a_widx1, a_widx1] += k*(S_v[pid]/Sw1-1.0)
            if Np == 0:
   #             print wid1, a_widx1, J[a_widx1, a_widx1], list(mesh.regions(1, wid1)), wall_decomposition.get(wid1, None)
   #         if len(list(mesh.regions(1, wid1)))<2:
                J[a_widx1, a_widx1] += 1.0
                    


        alpha = np.zeros(y0.shape)

        for i in fixed_idx:
            J[i, :] *=0
            J[i, i] = 1
            
            alpha[i] = -y0[i]

#        for i in range(N):
#            print i, J[i, i]
#        A = J.tocsr()
#        print 'A'
#        print a_s, a_d, aw_s, aw_d, len(cell_map), len(wall_map), len(wall_decomposition)

        y = -spsolve(J.tocsr(), alpha)


#        print 'ret_GP: ', min(ret_GP), max(ret_GP)
#        print 'ret: ', min(ret), max(ret)
        return y


    def step(self):
        """
        Evolve the gene network for one timestep
        """
        
        set_fixed_properties(self.db)
        print 'SS_start'
        
        t=self.db.tissue()

        # Update the cell volumes and wall surface areas - these may
        # have been changed through tissue growth
        updateVS(self.db)

        # Recalculate the wall to edge map, and the tissue maps
        wall_decomposition = get_wall_decomp(self.db)
        tissue_maps = get_tissue_maps(self.db)
     
        # For each model species, calculate the slice (indices) of
        # the y[] array which will store property values during 
        # integration
        offset=0 # current offset into the y[] array
        species_slices={} # map from species name to the slice of y[]
        for species_name, species_type in self.model_species:
            # 'size' is how many values are needed to store a property
            # of that type
            size = len(tissue_maps[species_type])
            species_slices[species_name] = slice(offset, offset+size)
            offset+=size

        # Extract mesh
        cfg = self.db.get_config('config')
        mesh = t.relation(cfg.mesh_id)
        # Type of each cell
        cell_type = self.db.get_property('cell_type')
        
        # Call routine to find fixed property values
        fixed_idx=self.get_fixed_idx(tissue_maps, species_slices)

        #initialise with zero
        y0=np.zeros((offset,))

        ### SETUP INITIAL VALUES
        # Loop over all species referred to in these models
        for species_name, species_type in self.model_species:
            # extract property dict from TissueDB
            prop=self.db.get_property(species_name)
            # obtain appropriate section of y0[]
            y0_slice=y0[species_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems() :
                y0_slice[idx]=prop[tid]

 
        yend=self.get_ss(y0, self.db, tissue_maps, species_slices, wall_decomposition, fixed_idx)
 
        
        for species_name, species_type in self.model_species: 
            prop=self.db.get_property(species_name)
            yend_slice=yend[species_slices[species_name]]
            for tid, idx in tissue_maps[species_type].iteritems() :
                prop[tid]=yend_slice[idx]

        print 'SS_end'
