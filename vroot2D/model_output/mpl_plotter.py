

"""
Module to plot tissue using matplotlib rather than OpenAlea
"""

from openalea.celltissue import TissueDB
from openalea.container import ordered_pids
from matplotlib.collections import PolyCollection, CircleCollection
from matplotlib.patches import Rectangle
from matplotlib.text import Text
from random import random
from matplotlib.lines import Line2D
from math import log
import scipy.linalg as la

from openalea.tissueshape import centroid

import matplotlib
#matplotlib.use('TkAgg')

import matplotlib.pylab as plt
import numpy as np


from vroot2D.utils.db_utilities import get_mesh, get_graph, get_wall_decomp
from vroot2D.utils.db_geom import ordered_wids_pids



from mpl_plot_midline import MPLPlotMidline

from vroot2D.growth.growth_midline import ml_to_xy


def plot_col_PINs(db, ax):
    mesh = get_mesh(db)
    graph = get_graph(db)
    pos = db.get_property('position')
    cell_type = db.get_property('cell_type')
    PIN = db.get_property('PIN')
    wall = db.get_property('wall')
    w_d = get_wall_decomp(db)
    e = 0.1
    for cid, ct in cell_type.iteritems():
        if ct in [11, 12, 13]: # columella cells
            c = centroid(mesh, pos, 2, cid)
            for eid in graph.out_edges(cid):
                if PIN[eid]!=0:
                    wid = wall[eid]
                    pid1, pid2 = mesh.borders(1, wid)
                    p0 = (1-e)*pos[pid1]+e*c
                    p1 = (1-e)*pos[pid2]+e*c
                    plt.gca().add_line(Line2D((p0[0],p1[0]),(p0[1],p1[1]), color='g', lw=2)) 
                
    

class MPLPlotter(object):

    def __init__(self, dbs, prop_name, plot_midline=False,  
                 limits=None, fs=(8*4,6*4), format='eps', 
                 colours=None, fn_base='/tmp/out', offset_y=200,
                 with_inset=False, with_scale=True,
                 cm_name='Blues', set_aspect=True, log_prop=False, 
                 edge_prop=None, thresh_prop = True, colorbar=False,
                 grav_arrow=None):
        self.dbs = dbs
        self.fs = fs
        self.prop_name = prop_name
        self.count = 0
        self.limits = limits
        self.format = format
        self.colours = colours
        self.fn_base = fn_base
        self.offset_y = offset_y
        self.cm_name = cm_name
        self.set_aspect = set_aspect
        self.plot_midline = plot_midline
        self.log_prop = log_prop
        self.edge_prop = edge_prop
        self.with_scale = with_scale
        if plot_midline:
            from mpl_plot_midline import MPLPlotMidline
            self.midline_plotter = MPLPlotMidline(self.dbs[0])
        self.with_inset = with_inset
        self.thresh_prop = thresh_prop
        self.colorbar = colorbar
        self.grav_arrow = grav_arrow

    def redraw(self):
        plt.figure(figsize=self.fs, dpi=100)

        #plt.clf()
        cm = plt.get_cmap(self.cm_name)
        a = plt.gcf().add_axes([0, 0, 1, 1], frameon=False)
        for i, db in enumerate(self.dbs):
            offset = np.array((0, -self.offset_y*i))
            mesh = get_mesh(db)
            pos = db.get_property('position')
            cell_type = db.get_property('cell_type')
            polys = []
            p_cols = []
            data = []
            prop = db.get_property(self.prop_name)
            print 'PROP: ', self.prop_name, ' ', max(prop.itervalues()), min(prop.itervalues())
            if self.thresh_prop:
                t = np.percentile([v for v in prop.itervalues()], 95)
                prop = dict((cid, min(v,t)) for cid, v in prop.iteritems())
            
            for cid in mesh.wisps(2):
                pids = ordered_pids(mesh, cid)
                polys.append([pos[pid]+offset for pid in pids])
#                data.append(min(prop[cid],2))
                if self.log_prop:
                    data.append(log(max(prop[cid],1e-10)))
                else:
                    data.append(prop[cid])
                if self.colours:
                    if type(self.colours[i])==dict:
                        p_cols.append(self.colours[i][cell_type[cid]])
                    else:
                        p_cols.append(self.colours[i])
            if self.colours:
                
                col=PolyCollection(polys, linewidths=(0.2,), antialiaseds=True, facecolors=p_cols)
            else:
                col=PolyCollection(polys, linewidths=(0.2,), antialiaseds=True, cmap = cm)
                col.set_array(np.asarray(data))

            a.add_collection(col)#, autolim=True)
            if self.colorbar:
                plt.colorbar(col)

        if self.edge_prop:
            edge_prop=db.get_property(self.edge_prop)
            delta = 2.0
            graph = get_graph(db)
            wall = db.get_property('wall')
            pat = []
            pat_data = []
            for cid in mesh.wisps(2):
                wids, pids=ordered_wids_pids(mesh, pos, cid)
                pts=[np.array(pos[pid]) for pid in pids]
                i_pos = {}
                N = len(pids)
                for i in range(N):
                    t2 = pts[(i+1)%N]-pts[i]
                    t1 = pts[i]-pts[i-1]
                    t1=t1/la.norm(t1)
                    t2=t2/la.norm(t2)
                    t = (t1+t2)/(1+np.dot(t1,t2))
                    i_pos[pids[i]] = pts[i] - delta*np.array((t[1],-t[0]))
                for eid in graph.out_edges(cid):
                    wid = wall[eid]
                    pid1, pid2 = mesh.borders(1,wid)
                    p1 = pos[pid1]
                    p2 = pos[pid2]
                    i_p1 = i_pos[pid1]
                    i_p2 = i_pos[pid2]
                
                    poly=(p1,p2,i_p2,i_p1)
                    pat.append(poly)
                    pat_data.append(edge_prop[eid])
            col=PolyCollection(pat, linewidths=(0.2,), antialiaseds=True, cmap=plt.cm.bwr)
            col.set_array(-np.asarray(pat_data))
            R = np.max(np.abs(pat_data))
            a.add_collection(col)
            col.set_clim(-R,R)
            plt.colorbar(col)

        if self.plot_midline:
            self.midline_plotter.redraw()
            
#        plot_col_PINs(db, a)
        plt.ylim(self.limits[1])
        plt.xlim(self.limits[0])
        plt.xticks([])
        plt.yticks([])
        if self.set_aspect:
            a.set_aspect('equal', adjustable='datalim')

        if self.with_inset:
            kappa = db.get_property('kappa')
            l = db.get_property('l')
            x0 = db.get_property('x0')
            theta0 = db.get_property('theta0')
            tip = ml_to_xy((len(l)-1, 0, 0), kappa, l, x0, theta0)
            
            a2 = plt.gcf().add_axes([0.0, 0.0, 0.4, 0.4], frameon=True, alpha=0.2)
            col=PolyCollection(polys, linewidths=(0.2,), antialiaseds=True, cmap = cm)
            col.set_array(np.asarray(data))
            a2.add_collection(col)
            a2.set_xticks([])
            a2.set_yticks([])
           
            plt.xlim([tip[0]-133, tip[0]+133])
            plt.ylim([tip[1]-100, tip[1]+100])
            plot_col_PINs(db, a2)
            a2.patch.set_alpha(0.5)

        if self.grav_arrow is not None:
            a.arrow(self.limits[0][1]-90, self.limits[1][1]-90, 
                    self.grav_arrow[0]*70,
                    self.grav_arrow[1]*70,
                    head_width=10, head_length=10, fc='k', ec='k')
     
        if self.with_scale:
            a.add_line(Line2D((self.limits[0][1]-120,self.limits[0][1]-20),
                          (self.limits[1][1]-50, self.limits[1][1]-50),
                          linewidth=5, color='k'))
        plt.savefig(self.fn_base+str(self.count)+'.png')
#        plt.draw()
        self.count += 1
        plt.close()

try:
    from PIL import Image
except ImportError:
    import Image

class MPLOverdraw(object):

    def __init__(self, dbs, prop_name, plot_midline=False,  
                 limits=None, img_seq=None, img_seq_offset=0,
                 colours=None, fn_base='/tmp/out', offset_y=200,
                 cm_name='jet', log_prop=False, transform=None):
   
        self.img_seq = img_seq
        self.img_seq_offset = img_seq_offset
        image = Image.open(img_seq%img_seq_offset)
        self.dpi=100.
        self.dbs = dbs
        self.prop_name = prop_name
        self.fig = plt.figure(figsize=((image.size[0])/self.dpi,float(image.size[1])/self.dpi), dpi=self.dpi)
        self.count = 0
        self.limits = limits
        self.colours = colours
        self.fn_base = fn_base
        self.offset_y = offset_y
        self.cm_name = cm_name
        self.plot_midline = plot_midline
        self.log_prop = log_prop
        self.transform = transform
        if plot_midline:
            from mpl_plot_midline import MPLPlotMidline
            self.midline_plotter = MPLPlotMidline(self.dbs[0])
     

    def redraw(self):
        image = Image.open(self.img_seq%(self.img_seq_offset+self.count))
        self.fig.clf()
        cm = plt.get_cmap(self.cm_name)
        a = self.fig.add_axes([0, 0, 1, 1], frameon=False)
        plt.imshow(image, cmap=plt.cm.Greys)
        for i, db in enumerate(self.dbs):
            offset = np.array((0, -self.offset_y*i))
            mesh = get_mesh(db)
            pos = db.get_property('position')
            cell_type = db.get_property('cell_type')
            polys = []
            p_cols = []
            data = []
            prop = db.get_property(self.prop_name)
            print 'PROP: ', self.prop_name, ' ', max(prop.itervalues()), min(prop.itervalues())
            for cid in mesh.wisps(2):
                pids = ordered_pids(mesh, cid)
                if self.transform:
                    points = map(self.transform, map(pos.get,pids))
                polys.append(points)#[pos[pid]+offset for pid in pids])
#                data.append(min(prop[cid],2))
                if self.log_prop:
                    data.append(log(max(prop[cid],1e-10)))
                else:
                    data.append(min(prop[cid],2))
                if self.colours:
                    if type(self.colours[i])==dict:
                        p_cols.append(self.colours[i][cell_type[cid]])
                    else:
                        p_cols.append(self.colours[i])
            if self.colours:
                
                col=PolyCollection(polys, linewidths=(0.2,), antialiaseds=True, facecolors=p_cols)
            else:
                col=PolyCollection(polys, linewidths=(0.2,), antialiaseds=True, cmap = cm)
                col.set_array(np.asarray(data))

            a.add_collection(col)#, autolim=True)

        if self.plot_midline:
            self.midline_plotter.redraw()
            
#        plot_col_PINs(db, a)
        plt.ylim([0,image.size[1]])
        plt.xlim([0,image.size[0]])
        plt.xticks([])
        plt.yticks([])
        plt.savefig(self.fn_base+str(self.count)+'.png', dpi=self.dpi)
#        plt.draw()
        self.count += 1
        plt.close()
