
"""
Module to plot tissue using matplotlib rather than OpenAlea
"""

from openalea.celltissue import TissueDB
from openalea.container import ordered_pids
from matplotlib.collections import PolyCollection, CircleCollection, LineCollection
from matplotlib.patches import Rectangle
from matplotlib.text import Text
from random import random

from math import log

import matplotlib
import matplotlib.pylab as plt
from matplotlib.lines import Line2D
import numpy as np

from random import random

from vroot2D.utils.db_utilities import get_mesh, get_graph, get_wall_decomp
from vroot2D.utils.db_geom import ordered_wids_pids, centroid

class MPLWallProp(object):
    def __init__(self, db, wall_prop_name,
                 limits=None, fs=(7.2,5.68), format='png', 
                 fn_base='/tmp/both', set_aspect=False,  
                 cmap_name=None, colorbar=False, thresh_prop=True):
        self.db = db
        self.wall_prop_name = wall_prop_name # Flux property - (wall,point)
        self.limits = limits
        self.format = format
        self.fn_base = fn_base
        self.cmap_name = cmap_name
        self.colorbar = colorbar
        self.set_aspect = set_aspect
        self.thresh_prop = thresh_prop
        self.fig = plt.figure(figsize=fs, dpi=100)
        self.count = 0


    def redraw(self):
        plt.clf()
        a = plt.gcf().add_axes([0, 0, 1, 1], frameon=False)
        db = self.db
        mesh = get_mesh(db)
        pos = db.get_property('position')

        cell_polys = []
        wall_prop = db.get_property(self.wall_prop_name)

        print 'PROP:', max(wall_prop.itervalues()), min(wall_prop.itervalues())

        for cid in mesh.wisps(2):
            pids = ordered_pids(mesh, cid)
            cell_polys.append([pos[pid] for pid in pids])

        lines = []
        data = []
        for wid, v in wall_prop.iteritems():
            pid1, pid2 = mesh.borders(1, wid)
            lines.append((pos[pid1], pos[pid2]))
            data.append(v)
            
            
        col = PolyCollection(cell_polys, linewidths=(0.2,), antialiaseds=True, facecolors=(1,1,1))
        

        col2 = LineCollection(lines, lw=4, antialiaseds=True)
        col2.set_array(np.asarray(data))

        # col=PolyCollection(polys, linewidths=(0.2,), antialiaseds=True, cmap = cm)
        #    col.set_array(np.asarray(data))


        a.add_collection(col)
        a.add_collection(col2)

        if self.colorbar:
            plt.colorbar(col2)

        plt.ylim(self.limits[1])
        plt.xlim(self.limits[0])
        plt.xticks([])
        plt.yticks([])
        if self.set_aspect:
            a.autoscale_view()
            a.set_aspect('equal', adjustable='datalim')
        plt.savefig(self.fn_base+'%03d'%self.count+'.'+self.format)
        self.count += 1
