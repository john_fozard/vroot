
"""
Module to plot tissue using matplotlib rather than OpenAlea
"""

from openalea.celltissue import TissueDB
from openalea.container import ordered_pids
from matplotlib.collections import PolyCollection, CircleCollection
from matplotlib.patches import Rectangle
from matplotlib.text import Text
from random import random

from math import log

import matplotlib
import matplotlib.pylab as plt
from matplotlib.lines import Line2D
import numpy as np

from random import random

from vroot2D.utils.db_utilities import get_mesh, get_graph, get_wall_decomp
from vroot2D.utils.db_geom import ordered_wids_pids, centroid

class MPLWallFluxes(object):
    def __init__(self, db, flux_prop_name,
                 limits=None, fs=(7.2,5.68), format='png', 
                 fn_base='/tmp/both', set_aspect=False,  
                 cmap_name=None, colorbar=False, thresh_prop=True):
        self.db = db
        self.flux_prop_name = flux_prop_name # Flux property - (wall,point)
        self.limits = limits
        self.format = format
        self.fn_base = fn_base
        self.cmap_name = cmap_name
        self.colorbar = colorbar
        self.set_aspect = set_aspect
        self.thresh_prop = thresh_prop
        self.fig = plt.figure(figsize=fs, dpi=100)
        self.count = 0


    def redraw(self):
        plt.clf()
        a = plt.gcf().add_axes([0, 0, 1, 1], frameon=False)
        db = self.db
        mesh = get_mesh(db)
        pos = db.get_property('position')

        cell_polys = []
        arrow_polys = []
        arrow_data = []
        flux_prop = db.get_property(self.flux_prop_name)

        print 'PROP:', max(flux_prop.itervalues()), min(flux_prop.itervalues())

        for cid in mesh.wisps(2):
            pids = ordered_pids(mesh, cid)
            cell_polys.append([pos[pid] for pid in pids])


        for (wid, pid), v in flux_prop.iteritems():
            o_pid = (set(mesh.borders(1, wid)) - set([pid])).pop()
            p1 = pos[pid]
            p2 = pos[o_pid]
            d = p2 - p1
            h = np.array((d[1], -d[0]))
            if v>0:
                poly = [p1+0.1*d, p1 + 0.4*d +0.1*h, p1 + 0.4*d - 0.1*h]
            else:
                poly = [p1+0.4*d, p1 + 0.1*d +0.1*h, p1 + 0.1*d - 0.1*h]
                v = -v
            arrow_polys.append(poly)
            arrow_data.append(v)
            
        col = PolyCollection(cell_polys, linewidths=(0.2,), antialiaseds=True, facecolors=(1,1,1))
        

        col2 = PolyCollection(arrow_polys, linewidths=0, antialiaseds=True)
        col2.set_array(np.asarray(arrow_data))

        # col=PolyCollection(polys, linewidths=(0.2,), antialiaseds=True, cmap = cm)
        #    col.set_array(np.asarray(data))


        a.add_collection(col)
        a.add_collection(col2)

        if self.colorbar:
            plt.colorbar(col2)

        plt.ylim(self.limits[1])
        plt.xlim(self.limits[0])
        plt.xticks([])
        plt.yticks([])
        if self.set_aspect:
            a.autoscale_view()
            a.set_aspect('equal', adjustable='datalim')
        plt.savefig(self.fn_base+'%03d'%self.count+'.'+self.format)
        self.count += 1
