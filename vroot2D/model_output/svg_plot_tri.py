

import numpy as np
import itertools
from math import sin, cos, exp, sqrt, asin, atan2
import scipy.linalg as la
from operator import itemgetter
import re

from openalea.celltissue import TissueDB
from vroot2D.utils.db_utilities import get_mesh 
from vroot2D.utils.db_geom import ordered_wids_pids
from vroot2D.growth.growth_midline import rot, breakpoints

from lxml import etree
import copy, os

from subprocess import call, Popen, PIPE

TEXTEXT_NS = u"http://www.iki.fi/pav/software/textext/"
SVG_NS = u"http://www.w3.org/2000/svg"
XLINK_NS = u"http://www.w3.org/1999/xlink"
ID_PREFIX = "textext-"

NSS = {
    u'textext': TEXTEXT_NS,
    u'svg': SVG_NS,
    u'xlink': XLINK_NS,
}


def fix_xml_namespace(node):
    svg = '{%s}' % SVG_NS
        
    if node.tag.startswith(svg):
        node.tag = node.tag[len(svg):]
        
    for key in node.attrib.keys():
        if key.startswith(svg):
            new_key = key[len(svg):]
            node.attrib[new_key] = node.attrib[key]
            del node.attrib[key]
        
    for c in node:
        fix_xml_namespace(c)

it = 0
def latex_to_svg(latex_string):
    global it
    doc = '\\documentclass{article}\\begin{document}\\pagestyle{empty}'+latex_string+'\\end{document}'
    f = open('/tmp/article%d.tex'%it, 'w')
    f.write(doc)
    f.close()
    call(['pdflatex', '-output-directory=/tmp', '/tmp/article%d.tex'%it])
    os.system('pdfcrop /tmp/article%d.pdf /tmp/crop%d.pdf'%(it,it))
    call(['pdf2svg', '/tmp/crop%d.pdf'%it, '/tmp/article%d.svg'%it])
    tree = etree.parse('/tmp/article%d.svg'%it)
    it +=1
    root = tree.getroot()

    fix_xml_namespace(root)
    href_map = {}

    ahash = hash(latex_string)
    # Map items to new ids
    for i, el in enumerate(root.xpath('//*[attribute::id]')):
        cur_id = el.attrib['id']
        new_id = "%s%s-%d" % (ID_PREFIX, ahash, i)
        href_map['#' + cur_id] = "#" + new_id
        el.attrib['id'] = new_id

    # Replace hrefs
    url_re = re.compile('^url\((.*)\)$')

    for el in root.xpath('//*[attribute::xlink:href]', namespaces=NSS):
        print el
        href = el.attrib['{%s}href'%XLINK_NS]
        el.attrib['{%s}href'%XLINK_NS] = href_map.get(href, href)

    for el in root.xpath('//*[attribute::svg:clip-path]', namespaces=NSS):
        value = el.attrib['clip-path']
        m = url_re.match(value)
        if m:
            el.attrib['clip-path'] = \
                'url(%s)' % href_map.get(m.group(1), m.group(1))

    for el in root.xpath('//*[attribute::style]', namespaces=NSS):
        style = el.attrib['style']
        sl = style.split(';')
        bad = []
        for s in sl:
            if s.split(':')[0].strip()=='fill':
                bad.append(s)
        el.set('style', ';'.join(s for s in sl if s not in bad))

        # Bundle everything in a single group
    master_group = etree.SubElement(root, 'g')
    for c in root:
        if c is master_group: continue
        master_group.append(c)

    return copy.copy(master_group)
    
celltype_colours = { 0:'cadetblue', 1:'blue', 2:'green', 3:'purple',
                     4:'gray', 5:'orange', 6:'chocolate',
                     7:'linen', 8:'white', 9:'azure'}

    


class SVGPlotter(object):
    def __init__(self, db):
        self.db = db
        self.count = 0
        self.plot_midline = True
        
    def redraw(self):
        db = self.db
        svg = etree.Element("svg")    
        mesh = get_mesh(db)
        pos = db.get_property('position')
        cell_type = db.get_property('cell_type')
        ct_groups = []
        cell_triangles = db.get_property('cell_triangles')
        drawing = etree.SubElement(svg, 'g', transform='scale(1,-1)')
        tissue = etree.SubElement(drawing, 'g')

        for ct in celltype_colours:
            ct_groups.append(etree.SubElement(tissue, 'g', style='stroke: black; fill: '+celltype_colours[ct]))
        bg = etree.SubElement(tissue, 'g', style='stroke: black; stroke-width: 0.3')
        wall_verts = set()
        for cid in list(mesh.wisps(2)):
            wids, pids = ordered_wids_pids(mesh, pos, cid)
            wall_verts.update(pids)
            pts = [(pos[pid][0], pos[pid][1]) for pid in pids]
            path = 'M %f,%f '%tuple(pts[0]) + ' '.join('%f,%f'%tuple(x) for x in pts[1:]) +' Z'
            etree.SubElement(bg, 'path', d=path, fill='none')
            eps = 0.1
            for tri in cell_triangles[cid]:
                p = [pos[pid] for pid in tri]
                c = (p[0]+p[1]+p[2])/3.0
                p = [(1-eps)*x + eps*c for x in p]
                pts = [(x[0], x[1]) for x in p]
                
                path = 'M %f,%f '%tuple(pts[0]) + ' '.join('%f,%f'%tuple(x) for x in pts[1:]) +' Z'
                etree.SubElement(ct_groups[cell_type[cid]], 'path', d=path, stroke='none')
        verts = etree.SubElement(tissue, 'g', style='fill: black')
        int_verts = etree.SubElement(tissue, 'g', style='fill: darkred')
        for pid in mesh.wisps(0):
            if pid in wall_verts:
                etree.SubElement(verts, 'circle', cx=str(pos[pid][0]), cy=str(pos[pid][1]), r = str(0.45))
            else:
                etree.SubElement(int_verts, 'circle', cx=str(pos[pid][0]), cy=str(pos[pid][1]), r = str(0.45))
            
        if self.plot_midline:
            overlay = etree.SubElement(drawing, 'g')
            kappa = db.get_property('kappa')
            l = db.get_property('l')
            x0 = db.get_property('x0')
            theta0 = db.get_property('theta0')
            bp, angles, tangents = breakpoints(kappa, l, x0, theta0)
            H = 60

            upper_line = etree.SubElement(overlay, 'g', style='stroke: red; fill: none;')
            lower_line = etree.SubElement(overlay, 'g', style='stroke: blue; fill: none;')
            mid_line = etree.SubElement(overlay, 'g', style='stroke: black; fill: none;')
            for x0, x1, t0, t1, k in zip(bp[:-1], bp[1:],
                                         tangents[:-1], tangents[1:],
                                         kappa):
                n0 = rot(t0)
                n1 = rot(t1)
                xu0 = x0 + H*n0
                xl0 = x0 - H*n0
                xu1 = x1 + H*n1
                xl1 = x1 - H*n1
                pm = 'M %f,%f '%(x0[0], x0[1])
                pu = 'M %f,%f '%(xu0[0], xu0[1])
                pl = 'M %f,%f '%(xl0[0], xl0[1])
                if abs(k)<1e-15:
                    pm += '%f,%f'%(x1[0],x1[1])
                    pu += '%f,%f'%(xu1[0],xu1[1])
                    pl += '%f,%f'%(xl1[0],xl1[1])
                else:
                    pm += 'A %f,%f %f %d,%d %f,%f '%(1/abs(k), 1/abs(k), 0, 0, 1 if k>0 else 0, x1[0], x1[1]) 
                    if k>0:
                        pu += 'A %f,%f %f %d,%d %f,%f '%(1/abs(k)-H, 1/abs(k)-H, 0, 0, 1 if k>0 else 0, xu1[0], xu1[1]) 
                        pl += 'A %f,%f %f %d,%d %f,%f '%(1/abs(k)+H, 1/abs(k)+H, 0, 0, 1 if k>0 else 0, xl1[0], xl1[1]) 
                    else:
                        pu += 'A %f,%f %f %d,%d %f,%f '%(1/abs(k)+H, 1/abs(k)+H, 0, 0, 1 if k>0 else 0, xu1[0], xu1[1]) 
                        pl += 'A %f,%f %f %d,%d %f,%f '%(1/abs(k)-H, 1/abs(k)-H, 0, 0, 1 if k>0 else 0, xl1[0], xl1[1]) 
                etree.SubElement(mid_line, 'path', d=pm)
                etree.SubElement(upper_line, 'path', d=pu)
                etree.SubElement(lower_line, 'path', d=pl)
            cross_line = etree.SubElement(overlay, 'g', style='stroke: black; fill: none;')
            for x, t in zip(bp, tangents):
                xu = x + H*rot(t)
                xl = x - H*rot(t)
                etree.SubElement(cross_line, 'path', 
                                 d='M %f,%f %f,%f'%(xu[0], xu[1], xl[0], xl[1]))

        etree.ElementTree(svg).write('/tmp/midline%03d.svg'%self.count)
        self.count += 1




