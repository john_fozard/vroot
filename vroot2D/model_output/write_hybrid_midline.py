

from vroot2D.utils.geom import pt_in_polygon, pt_in_tri, det
from math import sqrt, atan2, sin, cos
from vroot2D.utils.db_utilities import get_mesh
import numpy as np
import matplotlib.pylab as plt
from vroot2D.utils.db_geom import ordered_wids_pids


class WriteHybridMidline(object):
     def __init__(self, db, fn=None):
        """
        Initialise midline-related properties in the TissueDB
        :param db: Tissue database 
        :type db: TissueDB
        """        

        self.db = db
#        params = get_parameters(db, 'mm_parameters')
        
        if fn:
            self.of=open(fn, 'w')
        else:
            self.of=None
           
     def step(self):
          db = self.db
          kappa = db.get_property('kappa')
          lengths = db.get_property('l')
          if self.of:
               for k, l in zip(kappa, lengths):
                    self.of.write("{} {}\n".format(k,l))
               self.of.write("\n")


