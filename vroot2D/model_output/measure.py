

from vroot2D.utils.geom import pt_in_polygon, pt_in_tri, det
from math import sqrt, atan2, sin, cos
from vroot2D.utils.db_utilities import get_mesh
import numpy as np
import scipy.linalg as la
from vroot2D.utils.db_geom import ordered_wids_pids
from openalea.tissueshape import centroid

class MeasureFibres(object):
     def __init__(self, db, fn=None, midline_pids=None):
        """
        Initialise midline-related properties in the TissueDB
        :param db: Tissue database 
        :type db: TissueDB
        """        
        self.db = db
        if fn:
            self.of=open(fn, 'w')
        else:
            self.of=None
           
     def step(self):
         pos = self.db.get_property('position')
#
         t = self.db.get_property('time')
         cell_triangles = self.db.get_property('cell_triangles')
         A0 = self.db.get_property('A0')
         A1 = self.db.get_property('A1')
         tri_ref_pos = self.db.get_property('tri_ref_pos') 

         theta0 = 0.0
         theta1 = 0.0
         
         for (cid, tri), (X0, Y0, X1, Y1, X2, Y2) in tri_ref_pos.iteritems():
              
              F0 = np.array(((X1-X0, Y1-Y0), (X2-X0, Y2-Y0)))
              x0 = pos[tri[0]]
              x1 = pos[tri[1]]
              x2 = pos[tri[2]]
              F1 = np.vstack((x1-x0,x2-x0))
              F = np.dot(F1.transpose(), la.inv(F0).transpose())
#                    print F
              a0 = np.dot(F,A0[(cid,tri)])
#              a0 /= sqrt(a0[0]**2+a0[1]**2)
              theta0 +=atan2(a0[0], a0[1])
              a1 = np.dot(F,A1[(cid,tri)])
#              a1 /= sqrt(a1[0]**2+a1[1]**2)
              theta1 +=atan2(a1[0], a1[1])
         theta0/=len(tri_ref_pos)
         theta1/=len(tri_ref_pos)
         print 'fibre_angles :', theta0, theta1
         if self.of:
             self.of.write('%g %g %g\n'%(t, theta0, theta1))


class MeasurePropX(object):
     def __init__(self, db, prop_name, fn=None):
          self.db = db
          self.prop = db.get_property(prop_name)
          if fn:
               self.of=open(fn, 'w')
          else:
               self.of=None

     def step(self):
         pos = self.db.get_property('position')
         t = self.db.get_property('time')
         mesh = get_mesh(self.db)
         for cid in mesh.wisps(2):
              c = centroid(mesh, pos, 2, cid)
              if self.of:
                   self.of.write('%g %g\n'%(c[0], self.prop[cid]))
         self.of.write('\n\n')

         


