
"""
Module to plot tissue using matplotlib rather than OpenAlea
"""

from openalea.celltissue import TissueDB
from openalea.container import ordered_pids
from matplotlib.collections import PolyCollection, CircleCollection
from matplotlib.patches import Rectangle
from matplotlib.text import Text
from random import random

from math import log



import matplotlib
import matplotlib.pylab as plt
from matplotlib.lines import Line2D
import numpy as np

from random import random

from vroot2D.utils.db_utilities import get_mesh, get_graph, get_wall_decomp
from vroot2D.utils.db_geom import ordered_wids_pids, centroid

#plt.ion()
def plot_col_PINs(db, ax):
    mesh = get_mesh(db)
    graph = get_graph(db)
    pos = db.get_property('position')
    cell_type = db.get_property('cell_type')
    PIN = db.get_property('PIN')
    wall = db.get_property('wall')
    w_d = get_wall_decomp(db)
    e = 0.1
    for cid, ct in cell_type.iteritems():
       if ct in (11,12,13,14):
            c = centroid(mesh, pos, 2, cid)
            for eid in graph.out_edges(cid):
                if PIN.get(eid, 0)!=0:
#                    print 'PIN', eid, PIN[eid]
                    wid = wall[eid]
                    pid1, pid2 = mesh.borders(1, wid)
                    p0 = (1-e)*pos[pid1]+e*c
                    p1 = (1-e)*pos[pid2]+e*c
                    plt.gca().add_line(Line2D((p0[0],p1[0]),(p0[1],p1[1]), color='g', lw=2))

def plot_stats(db, ax):
    mesh = get_mesh(db)
    pos = db.get_property('position')
    cell_type = db.get_property('cell_type')
    stats = db.get_property('statoliths')
    er = db.get_property('er')
    radii = []
    centres = []
    for cid, d in stats.iteritems():
        for r, c in zip(d[0].tolist(), d[1].tolist()):
            circ = plt.Circle(c, radius=r, color='r')
            plt.gca().add_patch(circ)


def plot_arrows(db, arrows, ax):
    pass

def bounding_box(pts):
    min_v = np.array((1e20, 1e20))
    max_v = np.array((1e20, 1e20))
    for v in pts:
        min_v = np.minimum(v, min_v)
        max_v = np.maximum(v, max_v)
    return min_v, max_v

def plot_auxin_fluxes(db, ax):
    mesh = get_mesh(db)
    pos = db.get_property('position')
    flux = db.get_property('auxin_flux')
    graph = get_graph(db)
    edge_groups = gen_edge_groups(db)
    

def plot_auxin_flux_field(db, ax):
    mesh = get_mesh(db)
    pos = db.get_property('position')
    wall = db.get_property('wall')
    flux = db.get_property('auxin_flux')
    graph = get_graph(db)
    all_fluxes = []
    def rot(v):
        return np.array((-v[1], v[0]))

    for cid in mesh.wisps(2):
        wids, pids = ordered_wids_pids(mesh, pos, cid)
        for wid, (pid1, pid2) in zip(wids, pairs(pids)):
            v0 = pos[pid0]
            v1 = pos[pid1]
            normal = rot(v1 - v0)
            normal = normal / la.norm(normal)
            mp = 0.5*(pos[pid0] + pos[pid1])
            all_fluxes.append((mp, auxin_flux*normal))
    bb = bounding_box([x[0] for x in all_fluxes])
    NB = 10
    box_fluxes = np.array((NB, NB, 2))
    for mp, flux in all_fluxes:
        i = int(mp[0] - bb[0][0])/(bb[1][0] - bb[0][0])
        i = max(0, min(i, NB-1))
        j = int(mp[1] - bb[0][1])/(bb[1][1] - bb[0][1])
        j = max(0, min(j, NB-1))
        box_fluxes[i][j] += flux

    arrows = [ np.array(bb[0] + (bb[1]-bb[0])/NB*np.array((


        

class MPLSimple(object):

    def __init__(self, dbs, prop_name, plot_midline=False, with_PINs=True, 
                 limits=None, fs=(7.2,5.68), format='png', 
                 colours=None, fn_base='/tmp/both', offset_y=200,
                 colorbar=True, plot_auxin_fluxes=True,
                 cm_name='Pastel1', set_aspect=False, log_prop=False, cmap=None, thresh_prop=True, statoliths=False):
        self.dbs = dbs
        self.prop_name = prop_name
        self.fig = plt.figure(figsize=fs, dpi=100)
        self.count = 0
        self.limits = limits
        self.format = format
        self.colours = colours
        self.fn_base = fn_base
        self.offset_y = offset_y
        self.cm_name = cm_name
        self.cmap = cmap
        self.colorbar=colorbar
        self.set_aspect = set_aspect
        self.plot_midline = plot_midline
        self.log_prop = log_prop
        self.with_PINs = with_PINs
        self.thresh_prop = thresh_prop
        self.plot_auxin_fluxes = plot_auxin_fluxes
        if plot_midline:
            from mpl_plot_midline import MPLPlotMidline
            self.midline_plotter = MPLPlotMidline(self.dbs[0])
        self.statoliths = statoliths

    def redraw(self):
        plt.clf()
        if self.cmap:
            cm = self.cmap
        else:
            cm = plt.get_cmap(self.cm_name)
        a = plt.gcf().add_axes([0, 0, 1, 1], frameon=False)
        db = self.dbs[0]
        mesh = get_mesh(db)
        pos = db.get_property('position')
        cell_type = db.get_property('cell_type')
        polys = []
        p_cols = []
        data = []
        prop = db.get_property(self.prop_name)
        auxin = db.get_property('auxin')
#        print [(cid,prop[cid], auxin[cid]) for cid in mesh.wisps(2)]
#        quit()
        print 'PROP:', max(prop.itervalues()), min(prop.itervalues())
        for cid in mesh.wisps(2):
            pids = ordered_pids(mesh, cid)
            polys.append([pos[pid] for pid in pids])

            if self.log_prop:
                data.append(log(max(prop[cid],1e-10)))
            else:
                data.append(prop[cid])

            if self.colours:
                if type(self.colours[i])==dict:
                    p_cols.append(self.colours[i][cell_type[cid]])
                else:
                    p_cols.append(self.colours[i])

        if self.thresh_prop:
            t = np.percentile(data, 95)
            data = [min(v, t) for v in data]

        if self.colours:
            
            col=PolyCollection(polys, linewidths=(0.2,), antialiaseds=True, facecolors=p_cols)
        else:
            col=PolyCollection(polys, linewidths=(0.2,), antialiaseds=True, cmap = cm)
            col.set_array(np.asarray(data))

        if self.colorbar:
            plt.colorbar(col)

        a.add_collection(col)

        if self.with_PINs:
            plot_col_PINs(db, a)

        if self.statoliths:
            plot_stats(db, a)

        if self.plot_midline:
            self.midline_plotter.redraw()
            
#        a.add_line(Line2D((self.limits[0][1]-120,self.limits[0][1]-20),
#                          (self.limits[1][0]+20, self.limits[1][0]+20),
#                          linewidth=5, color='k'))
        plt.ylim(self.limits[1])
        plt.xlim(self.limits[0])
        plt.xticks([])
        plt.yticks([])
        if self.set_aspect:
            a.autoscale_view()
            a.set_aspect('equal', adjustable='datalim')
        plt.savefig(self.fn_base+'%03d'%self.count+'.'+self.format)
#        plt.draw()
        self.count += 1
