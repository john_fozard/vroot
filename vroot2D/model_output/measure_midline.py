

from vroot2D.utils.geom import pt_in_polygon, pt_in_tri, det
from math import sqrt, atan2, sin, cos
from vroot2D.utils.db_utilities import get_mesh
import numpy as np
import matplotlib.pylab as plt
from vroot2D.utils.db_geom import ordered_wids_pids

def length(a,b):
    dx=b[0]-a[0]
    dy=b[1]-a[1]
    return sqrt(dx*dx+dy*dy)

def angle(a,b, flip_x=False):
    dx=b[0]-a[0]
    dy=b[1]-a[1]
    if not flip_x:
        return atan2(dy,dx)
    else:
        return -atan2(dy, -dx)

def midline_tip(p, flip_x=False):
    l_tot = 0.0
    N = len(p)
    for i in range(N-1):
        x0 = p[i]
        x1 = p[i+1]
        l = length(x0, x1)
        l_tot += l
    x0=p[-2]
    x1=p[-1]
    theta=angle(x0, x1, flip_x)
    return l_tot, theta

def midline_regr(p, v):
    values = []
    s = []
    l_tot = 0.0
    N = len(p)
    for i in range(N-1):
        x0 = p[i]
        x1 = p[i+1]
        v0 = v[i]
        v1 = v[i+1]
        l = length(x0, x1)
        values.append(np.dot(x1-x0,v1-v0)/np.dot(x1-x0,x1-x0))
        s.append(l_tot+l/2)
        l_tot += l
    return s, values

def midline_angular(p, v):
    values = []
    s = []
    l_tot = 0.0
    N = len(p)
    for i in range(N-1):
        x0 = p[i]
        x1 = p[i+1]
        v0 = v[i]
        v1 = v[i+1]
        l = length(x0, x1)
        values.append(det(x1-x0,v1-v0)/l/l)
        s.append(l_tot + l/2)
        l_tot += l
    return s, values

def midline_dcgr(p,v):
    w = []
    N = len(p)
    lengths = []
    for i in range(N-1):
        x0 = p[i]
        x1 = p[i+1]
        v0 = v[i]
        v1 = v[i+1]
        l = length(x0, x1)
        w.append(det(x1-x0,v1-v0)/l/l)
        lengths.append(l)
    print 'w, lengths: ', w, lengths
    print 'dw', [w[i+1]-w[i] for i in range(N-2)]
    l_tot=0
    d = []
    for i in range(1,N-1):
        x = p[i]
        d.append((w[i]-w[i-1])/(0.5*(lengths[i-1]+lengths[i])))
         
    return lengths[:-1], d

def midline_kappa(p, v):
    kappa = []
    N = len(p)
    lengths = []
    for i in range(N-1):
        lengths.append(length(p[i], p[i+1]))
    for i in range(N-2):
        d0 = p[i+1] - p[i]
        d1 = p[i+2] - p[i+1]
        a = atan2(det(d0, d1), np.dot(d0,d1))
        kappa.append(a/(0.5*(lengths[i]+lengths[i+1])))
    return lengths[:-1], kappa


def midline_kdot(p, v):
    sr, regr = midline_regr(p, v)
    sd, dcgr = midline_dcgr(p, v)
    sk, kappa = midline_kappa(p, v)
    k_dot = np.array(dcgr) - np.array(kappa)*(
        np.array(regr[1:])+np.array(regr[:-1]))/2.0
    return sk, k_dot
    

def bary_inverse(p, x0, x1, x2):
    lambda0 = ((x1[1]-x2[1])*(p[0]-x2[0])+(x2[0]-x1[0])*(p[1]-x2[1]))/ \
        ((x1[1]-x2[1])*(x0[0]-x2[0])+(x2[0]-x1[0])*(x0[1]-x2[1]))
    lambda1 = ((x2[1]-x0[1])*(p[0]-x2[0])+(x0[0]-x2[0])*(p[1]-x2[1]))/ \
        ((x1[1]-x2[1])*(x0[0]-x2[0])+(x2[0]-x1[0])*(x0[1]-x2[1]))
    lambda2 = 1.0 - lambda0 - lambda1
    return lambda0, lambda1, lambda2

    

def find_containing_cell(db, x):
    mesh = get_mesh(db)
    pos = db.get_property('position')
    for cid in mesh.wisps(2):
        wids, pids = ordered_wids_pids(mesh, pos, cid)
        if pt_in_polygon([pos[pid] for pid in pids], x):
            return cid
    return None

def find_containing_tri(db, cid, x):
    pos = db.get_property('position')
    cell_triangles = db.get_property('cell_triangles')
    for t in cell_triangles[cid]:
        if pt_in_tri([pos[pid] for pid in t], x):
            return t
    

class MeasureMidline(object):
     def __init__(self, db, fn=None, midline_pids=None, flip_x=False):
        """
        Initialise midline-related properties in the TissueDB
        :param db: Tissue database 
        :type db: TissueDB
        """        

        self.db = db
        self.flip_x = flip_x
#        params = get_parameters(db, 'mm_parameters')
        
        if not midline_pids:
            midline_pids = []
            pos = db.get_property('position')
            x = [p[0] for p in pos.itervalues()]
            y = [p[1] for p in pos.itervalues()]
            ymid = 0.5*(max(y)+min(y))
            x0 = np.array([min(x), ymid])
            x1 = np.array([max(x), ymid])
            # Now find points near the midline
            NM = 10
            s = np.linspace(0,1,NM)
            eps = 1e-6
            xmid = [x0*s+x1*(1-s) for s in np.linspace(eps, 1-eps, NM)]            
            print 'xmid:', xmid
            # Find the triangle containing each of these points
            for p in xmid:
                cid = find_containing_cell(db, p)
                if cid:
                    t = find_containing_tri(db, cid, p)
                    if t:
                        b = bary_inverse(p, pos[t[0]], pos[t[1]], pos[t[2]])
                        midline_pids.append((t,b))
            p = [ sum((pos[pid]*w for pid,w in zip(*pl)), np.array((0.0,0.0)))
              for pl in midline_pids ] 
            l, theta = midline_tip(p, flip_x = self.flip_x)
            print 'init_length: ', l, theta
        db.set_property('midline_pids', midline_pids)
        db.set_description('midline_pids', '')
        if fn:
            self.of=open(fn, 'w', 0)
        else:
            self.of=None
           
     def step(self):
         pos = self.db.get_property('position')
         vel = self.db.get_property('velocity')
         midline_pids = self.db.get_property('midline_pids')
         t = self.db.get_property('time')
  
         p = [ sum((pos[pid]*w for pid,w in zip(*pl)), np.array((0.0,0.0)))
              for pl in midline_pids ] 
#        print 'xmid_new: ', p
#
 #       print vel
         v = [ sum((vel[pid]*w for pid,w in zip(*pl)), np.array((0.0,0.0))) 
              for pl in midline_pids ] 
         print 'v_new:', v


         l, theta = midline_tip(p, flip_x = self.flip_x)

         print 'tip: ', l, theta
         if self.of:
             self.of.write('%f %f %f\n'%(t, l,theta))


     def write_regr(self, f):
         pos = self.db.get_property('position')
         vel = self.db.get_property('velocity')
         t = self.db.get_property('time')
  
         p = [ sum((pos[pid]*w for pid,w in zip(*pl)), np.array((0.0,0.0)))
              for pl in midline_pids ] 
#        print 'xmid_new: ', p
#
 #       print vel
         v = [ sum((vel[pid]*w for pid,w in zip(*pl)), np.array((0.0,0.0))) 
              for pl in midline_pids ] 

         ltot, theta = midline_tip(p)
         l, g = midline_regr(p, v)
         for s, gamma in zip(l,g):
             f.write("%f %f\n"%(ltot-s,gamma))
             
