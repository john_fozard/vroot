

import numpy as np
import itertools
from math import sin, cos, exp, sqrt, asin, atan2, log
import scipy.linalg as la
from operator import itemgetter
import re

from openalea.celltissue import TissueDB
from openalea.container import ordered_pids
#from openalea.tissueshape import centroid
from vroot2D.utils.db_utilities import get_mesh, get_graph
from vroot2D.utils.db_geom import ordered_wids_pids, centroid
from vroot2D.growth.growth_midline import rot, breakpoints

from lxml import etree
import copy, os

from subprocess import call, Popen, PIPE

TEXTEXT_NS = u"http://www.iki.fi/pav/software/textext/"
SVG_NS = u"http://www.w3.org/2000/svg"
XLINK_NS = u"http://www.w3.org/1999/xlink"
ID_PREFIX = "textext-"

NSS = {
    u'textext': TEXTEXT_NS,
    u'svg': SVG_NS,
    u'xlink': XLINK_NS,
}

def sig_figs(v):
    return str(int(v))


def fix_xml_namespace(node):
    svg = '{%s}' % SVG_NS
        
    if node.tag.startswith(svg):
        node.tag = node.tag[len(svg):]
        
    for key in node.attrib.keys():
        if key.startswith(svg):
            new_key = key[len(svg):]
            node.attrib[new_key] = node.attrib[key]
            del node.attrib[key]
        
    for c in node:
        fix_xml_namespace(c)

it = 0
def latex_to_svg(latex_string):
    global it
    doc = '\\documentclass{article}\\begin{document}\\pagestyle{empty}'+latex_string+'\\end{document}'
    f = open('/tmp/article%d.tex'%it, 'w')
    f.write(doc)
    f.close()
    call(['pdflatex', '-output-directory=/tmp', '/tmp/article%d.tex'%it])
    os.system('pdfcrop /tmp/article%d.pdf /tmp/crop%d.pdf'%(it,it))
    call(['pdf2svg', '/tmp/crop%d.pdf'%it, '/tmp/article%d.svg'%it])
    tree = etree.parse('/tmp/article%d.svg'%it)
    it +=1
    root = tree.getroot()

    fix_xml_namespace(root)
    href_map = {}

    ahash = hash(latex_string)
    # Map items to new ids
    for i, el in enumerate(root.xpath('//*[attribute::id]')):
        cur_id = el.attrib['id']
        new_id = "%s%s-%d" % (ID_PREFIX, ahash, i)
        href_map['#' + cur_id] = "#" + new_id
        el.attrib['id'] = new_id

    # Replace hrefs
    url_re = re.compile('^url\((.*)\)$')

    for el in root.xpath('//*[attribute::xlink:href]', namespaces=NSS):
        print el
        href = el.attrib['{%s}href'%XLINK_NS]
        el.attrib['{%s}href'%XLINK_NS] = href_map.get(href, href)

    for el in root.xpath('//*[attribute::svg:clip-path]', namespaces=NSS):
        value = el.attrib['clip-path']
        m = url_re.match(value)
        if m:
            el.attrib['clip-path'] = \
                'url(%s)' % href_map.get(m.group(1), m.group(1))

    for el in root.xpath('//*[attribute::style]', namespaces=NSS):
        style = el.attrib['style']
        sl = style.split(';')
        bad = []
        for s in sl:
            if s.split(':')[0].strip()=='fill':
                bad.append(s)
        el.set('style', ';'.join(s for s in sl if s not in bad))

        # Bundle everything in a single group
    master_group = etree.SubElement(root, 'g')
    for c in root:
        if c is master_group: continue
        master_group.append(c)

    return copy.copy(master_group)
    
celltype_colors = { 0:'cadetblue', 1:'blue', 2:'green', 3:'purple',
                     4:'gray', 5:'orange', 6:'chocolate',
                     7:'linen', 8:'white', 9:'azure' }



class Cmap(object):
    def __init__(self, data_range=[0,1], colours=[(0.0, (1.0, 1.0, 1.0)), (1.0, (0.0, 0.0, 1.0))]):
        self.range = data_range
        self.colours = colours

    def __call__(self, value):
        y = (value - self.range[0])/(self.range[1]-self.range[0])
        y = min(1.0, max(0.0,y))
        for (x0,c0), (x1,c1) in zip(self.colours[:-1], self.colours[1:]):
            if x0 <= y <=x1:

                t = (y-x0)/(x1-x0)
                return tuple([int(255*((1-t)*v0+t*v1)) for v0,v1 in zip(c0,c1)])

red_white_blue = Cmap(data_range=[-1,1], colours = [(0.0, (1.0, 0.0, 0.0)),
                                                  (0.5, (1.0, 1.0, 1.0)),
                                                  (1.0, (0.0, 0.0, 1.0))])


class SVGPlotter(object):
    def __init__(self, db, fn_base='/tmp/organ',
                 color='celltype', plot_midline=False, plot_tris=False,
                 ct_colors=celltype_colors, plot_fibres=False,
                 fibre_len=10, prop=None, log_prop=False, sub_graph=False,
                 sub_graph_prop=None, sub_graph_function=None, tri_style=None, plot_PINAUX=False,
                 plot_col_PINs=False, title=None,
                 disc_prop=None, disc_colors=None, cm=None, thresh_prop=False,
                 prop_min=None, prop_max=None):
        self.db = db
        self.title = title
        self.count = 0
        self.plot_midline = plot_midline
        self.fn_base = fn_base
        self.color = color
        self.ct_colors = ct_colors
        self.plot_fibres = plot_fibres
        self.fibre_len = fibre_len
        self.prop = prop
        self.log_prop = log_prop
        self.sub_graph = sub_graph
        self.sub_graph_prop = sub_graph_prop
        self.plot_tris = plot_tris
        self.tri_style = tri_style
        self.plot_col_PINs = plot_col_PINs
        if prop:
            self.color = 'prop'
        self.colourbar = True
        self.plot_PINAUX = plot_PINAUX
        self.disc_prop = disc_prop
        self.disc_colors = disc_colors
        self.cm = cm
        self.thresh_prop = thresh_prop
        self.sub_graph_function = sub_graph_function
        self.prop_min = prop_min
        self.prop_max = prop_max

        
    def draw(self, filename):
        print 'DRAW', self.prop
        db = self.db
        svg = etree.Element("svg", id='svg2', xmlns='http://www.w3.org/2000/svg', version='1.0')    
#        etree.SubElement(svg, "rect", width="100%", height="100%", fill="white")

        mesh = get_mesh(db)
        pos = db.get_property('position')
        cell_type = db.get_property('cell_type')
        ct_groups = {}

        min_x = min(p[0] for p in pos.itervalues())
        min_y = min(p[1] for p in pos.itervalues())
        max_x = max(p[0] for p in pos.itervalues())
        max_y = max(p[1] for p in pos.itervalues())

        length = max_x-min_x
        width = max_y-min_y
        
        #drawing = etree.SubElement(svg, 'g', transform='scale(1,-1)')
        drawing = etree.SubElement(svg, 'g', transform='matrix(1,0,0,-1,20,%f)'%(20+max_y))
        tissue = etree.SubElement(drawing, 'g')
        if self.color == 'celltype':
            for ct in set(cell_type.itervalues()):
                ct_groups[ct] =etree.SubElement(tissue, 'g', style='stroke: black; fill: '+self.ct_colors[ct%10]+'; stroke-width: 0.1px')
            for cid in list(mesh.wisps(2)):
                wids, pids = ordered_wids_pids(mesh, pos, cid)
                pts = [(pos[pid][0], pos[pid][1]) for pid in pids]
                path = 'M %f,%f '%tuple(pts[0]) + ' '.join('%f,%f'%tuple(x) for x in pts[1:]) +' Z'
                etree.SubElement(ct_groups[cell_type[cid]], 'path', d=path)
        elif self.disc_prop:
            print 'DISCPROP'
            disc_groups = {}
            disc_prop = self.db.get_property(self.disc_prop)
            for val in disc_prop.itervalues():
                disc_groups[val] = etree.SubElement(tissue, 'g', style='stroke: black; fill: '+self.disc_colors[val]+'; stroke-width: 0.1px')
            for cid in list(mesh.wisps(2)):
                wids, pids = ordered_wids_pids(mesh, pos, cid)
                pts = [(pos[pid][0], pos[pid][1]) for pid in pids]
                path = 'M %f,%f '%tuple(pts[0]) + ' '.join('%f,%f'%tuple(x) for x in pts[1:]) +' Z'
                etree.SubElement(disc_groups[disc_prop[cid]], 'path', d=path)            
        elif self.prop:
            print 'PROP'
            cells = etree.SubElement(tissue, 'g', style='stroke: black; stroke-width: 0.1px')
            prop = self.db.get_property(self.prop)
            if self.log_prop:
                transform = lambda x: log(x)
            else:
                transform = lambda x: x


            if self.thresh_prop:
                t = np.percentile([transform(v) for v in prop.itervalues()], 95)
                prop = dict((cid, min(v,t)) for cid, v in prop.iteritems())

            if self.prop_max is None:
                prop_max = max(transform(v) for v in prop.itervalues())
            else:
                prop_max = transform(self.prop_max)
            if self.prop_min is None:
                prop_min = min(transform(v) for v in prop.itervalues())
            else:
                prop_min = transform(self.prop_min)

            if not self.cm:
                cm = Cmap([prop_min, prop_max])
            else:
                cm = Cmap([prop_min, prop_max], colours=self.cm)
                cm.range=[prop_min, prop_max]
            for cid in list(mesh.wisps(2)):
                wids, pids = ordered_wids_pids(mesh, pos, cid)
                pts = [(pos[pid][0], pos[pid][1]) for pid in pids]
                col = cm(transform(prop[cid]))
                path = 'M %f,%f '%tuple(pts[0]) + ' '.join('%f,%f'%tuple(x) for x in pts[1:]) +' Z'
                etree.SubElement(cells, 'path', d=path, fill='rgb(%d,%d,%d)'%col)
            if self.colourbar:
                x_s = max_x - min_x
                y_s = max_y - min_y
                width=20.0
                colourbar = etree.SubElement(drawing, 'g')
                N_col = 100
                for i in range(N_col):
                    col = cm(prop_min+(prop_max-prop_min)*(i+0.5)/N_col)
                    etree.SubElement(colourbar, 'rect', x=str(1.1*max_x), y =str(min_y+float(i)/N_col*y_s), width=str(width), height=str(float(y_s)/N_col), style = 'stroke: none; fill: rgb(%d,%d,%d)'%col).set('shape-rendering', 'crispEdges')
                etree.SubElement(colourbar, 'rect', x=str(1.1*max_x), y =str(min_y), width=str(width), height=str(y_s), style='fill: none; stroke: black;')
                l1=etree.SubElement(colourbar, 'text', transform='translate(%f,%f) scale(1,-1)'%(1.1*max_x+1.1*width, min_y))
                if self.prop_max is None:
                    prop_max = max(v for v in prop.itervalues())
                else:
                    prop_max = self.prop_max
                if self.prop_min is None:
                    prop_min = min(v for v in prop.itervalues())
                else:
                    prop_min = self.prop_min
                if self.log_prop:
                    if log(prop_min)<0:
                        print 'LOG_MIN'
                        scaling = int(log(prop_min)/log(10))
                        l1.text='%.1f x 10'%(prop_min/10**scaling)
                        ls1 = etree.SubElement(l1, 'tspan')
                        ls1.set('baseline-shift','super')
                        ls1.text='%d'%(scaling)
                    else:
                        l1.text='%.2f'%prop_min
                else:
                    l1.text=sig_figs(prop_min)


                l1.set('font-size', '24')
                l1=etree.SubElement(colourbar, 'text', transform='translate(%f,%f) scale(1,-1)'%(1.1*max_x+1.1*width, min_y+y_s))
                if self.log_prop:
                    l1.text='%f'%prop_max
                else:
                    l1.text=sig_figs(prop_max) #'%.2f'%prop_max
                l1.set('font-size', '24')

            if self.sub_graph:
                sg_prop = db.get_property(self.sub_graph_prop)
                sg_prop_min = min(v for v in sg_prop.itervalues())
                sg_prop_max = max(v for v in sg_prop.itervalues())
                sg = etree.SubElement(drawing, 'g')                
                etree.SubElement(sg, 'path', d='M %d,%d %d,%d %d,%d'%(min_x, min_y-10, min_x, min_y-60, max_x, min_y-60), style='stroke: black; fill: none; stroke-width: 2')
                for cid in mesh.wisps(2):
                    xc = centroid(mesh, pos, 2, cid)
                    y = min_y-60+50*(sg_prop[cid]-sg_prop_min)/(sg_prop_max-sg_prop_min)
                    etree.SubElement(sg, 'circle', cx=str(xc[0]), cy=str(y), r='2', style='stroke: none; fill: black')
                l1=etree.SubElement(sg, 'text', transform='translate(%f,%f) scale(1,-1)'%(min_x-2, min_y-60))
                l1.text='%.2f'%sg_prop_min
                l1.set('font-size', '24')
                l1.set('text-anchor', 'end')
                l1=etree.SubElement(sg, 'text', transform='translate(%f,%f) scale(1,-1)'%(min_x-2, min_y-10))
                l1.text='%.2f'%sg_prop_max
                l1.set('font-size', '24')
                l1.set('text-anchor', 'end')
            if self.sub_graph_function:
                Ng = 1000
                sg = etree.SubElement(drawing, 'g')                
                etree.SubElement(sg, 'path', d='M %d,%d %d,%d %d,%d'%(min_x, min_y-10, min_x, min_y-60, max_x, min_y-60), style='stroke: black; fill: none; stroke-width: 2')
                # find max / min of function
                l = db.get_property('l')
                l_tot = sum(l)
                x_min = min(p[0] for p in pos.itervalues())
                x_max = max(p[0] for p in pos.itervalues())
                x_vals = np.linspace(x_min, x_max, Ng)
                sg_vals = [ self.sub_graph_function(x_max-x) for x in x_vals ]
                sg_max = max(sg_vals)
                sg_min = min(sg_vals)
                for x, v in zip(x_vals, sg_vals):
                    y = min_y-60+50*(v - sg_min)/(sg_max - sg_min)
                    etree.SubElement(sg, 'circle', cx=str(x), cy=str(y), r='2', style='stroke: none; fill: black')
                l1=etree.SubElement(sg, 'text', transform='translate(%f,%f) scale(1,-1)'%(min_x-2, min_y-60))
                l1.text='%.2f'%sg_min
                l1.set('font-size', '24')
                l1.set('text-anchor', 'end')
                l1=etree.SubElement(sg, 'text', transform='translate(%f,%f) scale(1,-1)'%(min_x-2, min_y-10))
                l1.text='%.2f'%sg_max
                l1.set('font-size', '24')
                l1.set('text-anchor', 'end')


        else:
            cells = etree.SubElement(tissue, 'g', style='stroke: black; fill: '+self.color)
            for cid in list(mesh.wisps(2)):
                wids, pids = ordered_wids_pids(mesh, pos, cid)
                pts = [(pos[pid][0], pos[pid][1]) for pid in pids]
                path = 'M %f,%f '%tuple(pts[0]) + ' '.join('%f,%f'%tuple(x) for x in pts[1:]) +' Z'
                etree.SubElement(cells, 'path', d=path)

        if self.plot_tris:
#            print 'PLOT TRIS'
            tris = etree.SubElement(tissue, 'g')
#            tri_ref_pos = db.get_property('tri_ref_pos')
            cell_triangles = db.get_property('cell_triangles')
            if self.tri_style:
                ts = self.tri_style
                if 'fill' not in ts:
                    ts += '; fill: none'
            else:
                ts = 'fill: none; stroke: black'
            for cid, tl in cell_triangles.iteritems():
                for tri in tl:
                    etree.SubElement(tris, 'path', d='M '+' '.join('%f, %f'%tuple(pos[p]) for p in tri)+' z', style=ts)
            wall_overlay = etree.SubElement(tissue, 'g')
            for cid in list(mesh.wisps(2)):
                wids, pids = ordered_wids_pids(mesh, pos, cid)
                pts = [(pos[pid][0], pos[pid][1]) for pid in pids]
                path = 'M %f,%f '%tuple(pts[0]) + ' '.join('%f,%f'%tuple(x) for x in pts[1:]) +' Z'
                etree.SubElement(wall_overlay, 'path', d=path, style='stroke: black; fill: none')



        if self.plot_PINAUX:
            lAUX = etree.SubElement(tissue, 'g', style='stroke:orange; stroke-width:2')
            lPIN = etree.SubElement(tissue, 'g', style='stroke:green; stroke-width:2')
            AUX = db.get_property('AUX')
            PIN = db.get_property('PIN')
            wall = db.get_property('wall')
            e = 0.1
            graph = get_graph(db)
            for cid in mesh.wisps(2):
                c = centroid(mesh, pos, 2, cid)
                for eid in graph.out_edges(cid):
                    if AUX.get(eid, 0)!=0:
                        wid = wall[eid]
                        pid1, pid2 = mesh.borders(1, wid)
                        p0 = (1-e)*pos[pid1]+e*c
                        p1 = (1-e)*pos[pid2]+e*c
                        etree.SubElement(lAUX, 'line', x1=str(p0[0]), y1=str(p0[1]), x2=str(p1[0]), y2=str(p1[1]))
                    if PIN.get(eid, 0)!=0:
                        wid = wall[eid]
                        pid1, pid2 = mesh.borders(1, wid)
                        p0 = (1-e)*pos[pid1]+e*c
                        p1 = (1-e)*pos[pid2]+e*c
                        etree.SubElement(lPIN, 'line', x1=str(p0[0]), y1=str(p0[1]), x2=str(p1[0]), y2=str(p1[1]))

        if self.plot_col_PINs:
            lPIN = etree.SubElement(tissue, 'g', style='stroke:green')
            PIN = db.get_property('PIN')
            wall = db.get_property('wall')
            cell_type = db.get_property('cell_type')
            e = 0.1
            graph = get_graph(db)
            for cid in mesh.wisps(2):
              if cell_type[cid] in (11,12,13):
                c = centroid(mesh, pos, 2, cid)
                for eid in graph.out_edges(cid):
                    if PIN.get(eid, 0)!=0:
                        wid = wall[eid]
                        pid1, pid2 = mesh.borders(1, wid)
                        p0 = (1-e)*pos[pid1]+e*c
                        p1 = (1-e)*pos[pid2]+e*c
                        etree.SubElement(lPIN, 'line', x1=str(p0[0]), y1=str(p0[1]), x2=str(p1[0]), y2=str(p1[1]))



        if self.plot_fibres:
            fibres0 = etree.SubElement(tissue, 'g', style='stroke:green')
            fibres1 = etree.SubElement(tissue, 'g', style='stroke:red')
            cell_triangles = db.get_property('cell_triangles')
            A0 = db.get_property('A0')
            A1 = db.get_property('A1')
            tri_ref_pos = db.get_property('tri_ref_pos')
            L = self.fibre_len
            for cid in mesh.wisps(2):
                pids = ordered_pids(mesh, cid)
                a0_ave = np.array((0.0,0.0))
                a1_ave = np.array((0.0,0.0))
                for tri in cell_triangles[cid]:
                    X0, Y0, X1, Y1, X2, Y2 = tri_ref_pos[(cid, tri)]
                    F0 = np.array(((X1-X0, Y1-Y0), (X2-X0, Y2-Y0)))
                    x0 = pos[tri[0]]
                    x1 = pos[tri[1]]
                    x2 = pos[tri[2]]
                    F1 = np.vstack((x1-x0,x2-x0))
                    F = np.dot(F1.transpose(), la.inv(F0).transpose())
                    a0 = np.dot(F,A0[(cid,tri)])
                    a0 /= sqrt(a0[0]**2+a0[1]**2)
                    a1 = np.dot(F,A1[(cid,tri)])
                    a1 /= sqrt(a1[0]**2+a1[1]**2)
                    a0_ave += a0
                    a1_ave += a1
                a0_ave /= len(cell_triangles[cid])
                a1_ave /= len(cell_triangles[cid])
                xc = centroid(mesh, pos, 2, cid)
                p0 = xc-a0_ave*L
                p1 = xc+a0_ave*L
                etree.SubElement(fibres0, 'line', x1=str(p0[0]), y1=str(p0[1]), x2=str(p1[0]), y2=str(p1[1]))
                p0 = xc-a1_ave*L
                p1 = xc+a1_ave*L
                etree.SubElement(fibres1, 'line', x1=str(p0[0]), y1=str(p0[1]), x2=str(p1[0]), y2=str(p1[1]))




        if self.plot_midline:
            overlay = etree.SubElement(drawing, 'g')
            kappa = db.get_property('kappa')
            l = db.get_property('l')
            x0 = db.get_property('x0')
            theta0 = db.get_property('theta0')
            bp, angles, tangents = breakpoints(kappa, l, x0, theta0)
            H = 60

            upper_line = etree.SubElement(overlay, 'g', style='stroke: red; fill: none; stroke-width: 3')
            lower_line = etree.SubElement(overlay, 'g', style='stroke: blue; fill: none; stroke-width: 3')
            mid_line = etree.SubElement(overlay, 'g', style='stroke: black; fill: none; stroke-width: 3')
            for x0, x1, t0, t1, k in zip(bp[:-1], bp[1:],
                                         tangents[:-1], tangents[1:],
                                         kappa):
                n0 = rot(t0)
                n1 = rot(t1)
                xu0 = x0 + H*n0
                xl0 = x0 - H*n0
                xu1 = x1 + H*n1
                xl1 = x1 - H*n1
                pm = 'M %f,%f '%(x0[0], x0[1])
                pu = 'M %f,%f '%(xu0[0], xu0[1])
                pl = 'M %f,%f '%(xl0[0], xl0[1])
                if abs(k)<1e-15:
                    pm += '%f,%f'%(x1[0],x1[1])
                    pu += '%f,%f'%(xu1[0],xu1[1])
                    pl += '%f,%f'%(xl1[0],xl1[1])
                else:
                    pm += 'A %f,%f %f %d,%d %f,%f '%(1/abs(k), 1/abs(k), 0, 0, 1 if k>0 else 0, x1[0], x1[1]) 
                    if k>0:
                        pu += 'A %f,%f %f %d,%d %f,%f '%(1/abs(k)-H, 1/abs(k)-H, 0, 0, 1 if k>0 else 0, xu1[0], xu1[1]) 
                        pl += 'A %f,%f %f %d,%d %f,%f '%(1/abs(k)+H, 1/abs(k)+H, 0, 0, 1 if k>0 else 0, xl1[0], xl1[1]) 
                    else:
                        pu += 'A %f,%f %f %d,%d %f,%f '%(1/abs(k)+H, 1/abs(k)+H, 0, 0, 1 if k>0 else 0, xu1[0], xu1[1]) 
                        pl += 'A %f,%f %f %d,%d %f,%f '%(1/abs(k)-H, 1/abs(k)-H, 0, 0, 1 if k>0 else 0, xl1[0], xl1[1]) 
                etree.SubElement(mid_line, 'path', d=pm)
                etree.SubElement(upper_line, 'path', d=pu)
                etree.SubElement(lower_line, 'path', d=pl)
            cross_line = etree.SubElement(overlay, 'g', style='stroke: black; fill: none; stroke-width: 3;')
            for x, t in zip(bp, tangents):
                xu = x + H*rot(t)
                xl = x - H*rot(t)
                etree.SubElement(cross_line, 'path', 
                                 d='M %f,%f %f,%f'%(xu[0], xu[1], xl[0], xl[1]))


        if not (self.sub_graph or self.sub_graph_function):
            etree.SubElement(drawing, 'rect', x=str(max_x-20-100), y = str(min_y-40), width=str(100), height=str(10), stroke='none', fill='black')
        else:
            etree.SubElement(drawing, 'rect', x=str(max_x-20-100), y = str(min_y-75), width=str(100), height=str(10), stroke='none', fill='black')
        
        if self.title:
            t = etree.SubElement(svg, 'text', x="10", y="10") 
            t.text=self.title
            t.set('font-size', '12')



        etree.ElementTree(svg).write(filename, xml_declaration=True, encoding='utf-8')

    def redraw(self):
        self.draw(self.fn_base+'%03d.svg'%self.count)
        self.count += 1


