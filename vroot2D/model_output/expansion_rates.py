

from vroot2D.utils.geom import area
from math import sqrt, atan2, sin, cos
from vroot2D.utils.db_utilities import get_mesh, add_property
from vroot2D.utils.utils import pairs

import numpy as np
import scipy.linalg as la
from vroot2D.utils.db_geom import ordered_wids_pids


class MeasureExpansion(object):
    def __init__(self, db, fn=None, midline_pids=None):
        """
        Initialise midline-related properties in the TissueDB
        :param db: Tissue database 
        :type db: TissueDB
        """        
        self.db = db
        divided_props = db.get_property('divided_props')
        if 'expansion' not in db.properties():
             add_property(db, 'expansion', 'cell', 0.0)
             divided_props['expansion'] = ('cell', 'concentration', 0.0)
     
    def step(self):
        mesh = get_mesh(self.db)
        pos = self.db.get_property('position')
        vel = self.db.get_property('velocity')
        expansion = self.db.get_property('expansion')
        for cid in mesh.wisps(2):
            o_wids, o_pids = ordered_wids_pids(mesh, pos, cid)
            A = area([pos[pid] for pid in o_pids])
            rate = 0.0
            for pp in pairs(o_pids):
                 x0 = pos[pp[0]]
                 x1 = pos[pp[1]]
                 v0 = vel[pp[0]]
                 v1 = vel[pp[1]]
                 rate += 0.5*(v0[0]*x1[1]+x0[0]*v1[1]-v1[0]*x0[1]-x1[0]*v0[1])
#                 rate += (v1[0]-v0[0])/(x1[0]-x0[0])
            expansion[cid]=rate/A
                 
              
              
              
