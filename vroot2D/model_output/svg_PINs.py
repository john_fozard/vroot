

import numpy as np
import itertools
from math import sin, cos, exp, sqrt, asin, atan2, log
import scipy.linalg as la
from operator import itemgetter
import re

from openalea.celltissue import TissueDB
from openalea.container import ordered_pids
#from openalea.tissueshape import centroid
from vroot2D.utils.db_utilities import get_mesh, get_graph
from vroot2D.utils.db_geom import ordered_wids_pids, centroid
from vroot2D.growth.growth_midline import rot, breakpoints

from lxml import etree
import copy, os

from subprocess import call, Popen, PIPE

TEXTEXT_NS = u"http://www.iki.fi/pav/software/textext/"
SVG_NS = u"http://www.w3.org/2000/svg"
XLINK_NS = u"http://www.w3.org/1999/xlink"
ID_PREFIX = "textext-"

NSS = {
    u'textext': TEXTEXT_NS,
    u'svg': SVG_NS,
    u'xlink': XLINK_NS,
}




def fix_xml_namespace(node):
    svg = '{%s}' % SVG_NS
        
    if node.tag.startswith(svg):
        node.tag = node.tag[len(svg):]
        
    for key in node.attrib.keys():
        if key.startswith(svg):
            new_key = key[len(svg):]
            node.attrib[new_key] = node.attrib[key]
            del node.attrib[key]
        
    for c in node:
        fix_xml_namespace(c)

it = 0
def latex_to_svg(latex_string):
    global it
    doc = '\\documentclass{article}\\begin{document}\\pagestyle{empty}'+latex_string+'\\end{document}'
    f = open('/tmp/article%d.tex'%it, 'w')
    f.write(doc)
    f.close()
    call(['pdflatex', '-output-directory=/tmp', '/tmp/article%d.tex'%it])
    os.system('pdfcrop /tmp/article%d.pdf /tmp/crop%d.pdf'%(it,it))
    call(['pdf2svg', '/tmp/crop%d.pdf'%it, '/tmp/article%d.svg'%it])
    tree = etree.parse('/tmp/article%d.svg'%it)
    it +=1
    root = tree.getroot()

    fix_xml_namespace(root)
    href_map = {}

    ahash = hash(latex_string)
    # Map items to new ids
    for i, el in enumerate(root.xpath('//*[attribute::id]')):
        cur_id = el.attrib['id']
        new_id = "%s%s-%d" % (ID_PREFIX, ahash, i)
        href_map['#' + cur_id] = "#" + new_id
        el.attrib['id'] = new_id

    # Replace hrefs
    url_re = re.compile('^url\((.*)\)$')

    for el in root.xpath('//*[attribute::xlink:href]', namespaces=NSS):
        print el
        href = el.attrib['{%s}href'%XLINK_NS]
        el.attrib['{%s}href'%XLINK_NS] = href_map.get(href, href)

    for el in root.xpath('//*[attribute::svg:clip-path]', namespaces=NSS):
        value = el.attrib['clip-path']
        m = url_re.match(value)
        if m:
            el.attrib['clip-path'] = \
                'url(%s)' % href_map.get(m.group(1), m.group(1))

    for el in root.xpath('//*[attribute::style]', namespaces=NSS):
        style = el.attrib['style']
        sl = style.split(';')
        bad = []
        for s in sl:
            if s.split(':')[0].strip()=='fill':
                bad.append(s)
        el.set('style', ';'.join(s for s in sl if s not in bad))

        # Bundle everything in a single group
    master_group = etree.SubElement(root, 'g')
    for c in root:
        if c is master_group: continue
        master_group.append(c)

    return copy.copy(master_group)
    
celltype_colors = { 0:'cadetblue', 1:'blue', 2:'green', 3:'purple',
                     4:'gray', 5:'orange', 6:'chocolate',
                     7:'linen', 8:'white', 9:'azure' }



class Cmap(object):
    def __init__(self, data_range=[0,1], colours=[(0.0, (1.0, 1.0, 1.0)), (1.0, (0.0, 0.0, 1.0))]):
        self.range = data_range
        self.colours = colours

    def __call__(self, value):
        y = (value - self.range[0])/(self.range[1]-self.range[0])
        y = min(1.0, max(0.0,y))
        for (x0,c0), (x1,c1) in zip(self.colours[:-1], self.colours[1:]):
            if x0 <= y <=x1:

                t = (y-x0)/(x1-x0)
                return tuple([int(255*((1-t)*v0+t*v1)) for v0,v1 in zip(c0,c1)])

red_white_blue = Cmap(data_range=[-1,1], colours = [(0.0, (1.0, 0.0, 0.0)),
                                                  (0.5, (1.0, 1.0, 1.0)),
                                                  (1.0, (0.0, 0.0, 1.0))])


class SVGPINPlotter(object):
    def __init__(self, db, title=None, fn_base='/tmp/organ',
                 prop_name=None, prop_colour='green'):
        self.db = db
        self.title = title
        self.count = 0
        self.fn_base = fn_base
        self.prop_name = prop_name
        self.prop_colour = prop_colour



    def draw(self, filename):
        print 'DRAW', self.prop_name
        db = self.db
        svg = etree.Element("svg", id='svg2', xmlns='http://www.w3.org/2000/svg', version='1.0')    
        etree.SubElement(svg, "rect", width="100%", height="100%", fill="white")

        mesh = get_mesh(db)
        pos = db.get_property('position')
        cell_type = db.get_property('cell_type')
        ct_groups = {}

        min_x = min(p[0] for p in pos.itervalues())
        min_y = min(p[1] for p in pos.itervalues())
        max_x = max(p[0] for p in pos.itervalues())
        max_y = max(p[1] for p in pos.itervalues())

        length = max_x-min_x
        width = max_y-min_y
        
        #drawing = etree.SubElement(svg, 'g', transform='scale(1,-1)')
        drawing = etree.SubElement(svg, 'g', transform='matrix(1,0,0,-1,20,%f)'%(20+max_y))
        tissue = etree.SubElement(drawing, 'g')
        cells = etree.SubElement(tissue, 'g', style='stroke: black; fill: none;')
        for cid in list(mesh.wisps(2)):
            wids, pids = ordered_wids_pids(mesh, pos, cid)
            pts = [(pos[pid][0], pos[pid][1]) for pid in pids]
            path = 'M %f,%f '%tuple(pts[0]) + ' '.join('%f,%f'%tuple(x) for x in pts[1:]) +' Z'
            etree.SubElement(cells, 'path', d=path)



        lprop = etree.SubElement(tissue, 'g', style='stroke:'+self.prop_colour+'; stroke-width:2')
        prop = db.get_property(self.prop_name)
        wall = db.get_property('wall')
        e = 0.1
        graph = get_graph(db)
        for cid in mesh.wisps(2):
            c = centroid(mesh, pos, 2, cid)
            for eid in graph.out_edges(cid):
                if prop.get(eid, 0)!=0:
                    wid = wall[eid]
                    pid1, pid2 = mesh.borders(1, wid)
                    p0 = (1-e)*pos[pid1]+e*c
                    p1 = (1-e)*pos[pid2]+e*c
                    etree.SubElement(lprop, 'line', x1=str(p0[0]), y1=str(p0[1]), x2=str(p1[0]), y2=str(p1[1]))
        
        etree.SubElement(drawing, 'rect', x=str(max_x-20-100), y = str(min_y-40), width=str(100), height=str(10), stroke='none', fill='black')
        
        if self.title:
            t = etree.SubElement(svg, 'text', x="10", y="10") 
            t.text=self.title
            t.set('font-size', '12')

        etree.ElementTree(svg).write(filename, xml_declaration=True, encoding='utf-8')

    def redraw(self):
        self.draw(self.fn_base+'%03d.svg'%self.count)
        self.count += 1


