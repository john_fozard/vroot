

"""
Module to plot tissue using matplotlib rather than OpenAlea
"""

from openalea.celltissue import TissueDB
from openalea.container import ordered_pids
from matplotlib.collections import PolyCollection, CircleCollection, LineCollection
from matplotlib.patches import Rectangle
from matplotlib.text import Text
from random import random
from matplotlib.lines import Line2D
from math import log, sqrt


from openalea.tissueshape import centroid

import matplotlib
#matplotlib.use('TkAgg')

import matplotlib.pylab as plt
import numpy as np
import scipy.linalg as la

from random import random

from vroot2D.utils.db_utilities import get_mesh, get_graph, get_wall_decomp
from vroot2D.utils.db_geom import ordered_wids_pids

plt.ion()

class MPLSimple(object):

    def __init__(self, dbs, prop_name, plot_midline=True, with_inset=True, 
                 tip_main=False, limits=None, fs=(8,6), format='eps',
                 fibre_len=15,
                 colours=None, fn_base='/tmp/both', offset_y=200):
        self.dbs = dbs
        self.prop_name = prop_name
        self.fig = plt.figure(figsize=fs, dpi=100)
        self.count = 0
        self.limits = limits
        self.format = format
        self.colours = colours
        self.fn_base = fn_base
        self.offset_y = offset_y
        self.fibre_len = fibre_len

    def redraw(self):
        plt.clf()
        cm = plt.get_cmap('Pastel1')
        a = plt.gcf().add_axes([0, 0, 1, 1], frameon=False)
        for i, db in enumerate(self.dbs):
            offset = np.array((0, -self.offset_y*i))
            mesh = get_mesh(db)
            pos = db.get_property('position')
            polys = []
            data = []
            p_cols = []
            prop = db.get_property(self.prop_name)
            cell_type = db.get_property('cell_type')
            print 'PROP:', max(prop.itervalues()), min(prop.itervalues())
            for cid in mesh.wisps(2):
                pids = ordered_pids(mesh, cid)
                polys.append([pos[pid]+offset for pid in pids])
                data.append(min(prop[cid],2))
                if self.colours:
                    if type(self.colours[i])==dict:
                        p_cols.append(self.colours[i][cell_type[cid]])
                    else:
                        p_cols.append(self.colours[i])
            L = self.fibre_len
            l0 = []
            l1 = []
            cell_triangles = db.get_property('cell_triangles')
            A0 = db.get_property('A0')
            A1 = db.get_property('A1')
            tri_ref_pos = db.get_property('tri_ref_pos')
            for cid in mesh.wisps(2):
                pids = ordered_pids(mesh, cid)
#                print pids, type(pids)
#                print offset
#                print [pos[pid] for pid in pids]
                polys.append([pos[pid]+offset for pid in pids])
                data.append(min(prop[cid],2))
                a0_ave = np.array((0.0,0.0))
                a1_ave = np.array((0.0,0.0))
                for tri in cell_triangles[cid]:
                    X0, Y0, X1, Y1, X2, Y2 = tri_ref_pos[(cid, tri)]
                    F0 = np.array(((X1-X0, Y1-Y0), (X2-X0, Y2-Y0)))
                    x0 = pos[tri[0]]
                    x1 = pos[tri[1]]
                    x2 = pos[tri[2]]
                    F1 = np.vstack((x1-x0,x2-x0))
                    F = np.dot(F1.transpose(), la.inv(F0).transpose())
#                    print F
                    a0 = np.dot(F,A0[(cid,tri)])
                    a0 /= sqrt(a0[0]**2+a0[1]**2)
                    a1 = np.dot(F,A1[(cid,tri)])
                    a1 /= sqrt(a1[0]**2+a1[1]**2)
                    a0_ave += a0
                    a1_ave += a1
                a0_ave /= len(cell_triangles[cid])
                a1_ave /= len(cell_triangles[cid])
                xc = centroid(mesh, pos, 2, cid)+offset
                l0.append((xc-a0_ave*L,xc+a0_ave*L))
                l1.append((xc-a1_ave*L,xc+a1_ave*L))
              
            if self.colours:
                col=PolyCollection(polys, linewidths=(0.2,), antialiaseds=True, facecolors=p_cols)
            else:
                col=PolyCollection(polys, linewidths=(0.2,), antialiaseds=True, cmap = cm)
                col.set_array(np.asarray(data))
            a.add_collection(col)#, autolim=True)
            col0 = LineCollection(l0, linewidths=(1.,), color='r')
            col1 = LineCollection(l1, linewidths=(1.,), color='g')
            a.add_collection(col0)
            a.add_collection(col1)
            
        a.add_line(Line2D((self.limits[0][1]-120,self.limits[0][1]-20),
                          (self.limits[1][0]+20, self.limits[1][0]+20),
                          linewidth=5, color='k'))
        plt.ylim(self.limits[1])
        plt.xlim(self.limits[0])
        plt.xticks([])
        plt.yticks([])
        a.set_aspect('equal', adjustable='datalim')

        plt.savefig(self.fn_base+str(self.count)+'.'+self.format)
#        plt.draw()
        self.count += 1
