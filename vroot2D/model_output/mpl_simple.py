
"""
Module to plot tissue using matplotlib rather than OpenAlea
"""

from openalea.celltissue import TissueDB
from openalea.container import ordered_pids
from matplotlib.collections import PolyCollection, CircleCollection
from matplotlib.patches import Rectangle
from matplotlib.text import Text
from random import random

from math import log


from openalea.tissueshape import centroid

import matplotlib
matplotlib.use("Agg")


import matplotlib.pylab as plt
from matplotlib.lines import Line2D
import numpy as np


from random import random

from vroot2D.utils.db_utilities import get_mesh, get_graph, get_wall_decomp
from vroot2D.utils.db_geom import ordered_wids_pids

#plt.ion()

class MPLSimple(object):

    def __init__(self, dbs, prop_name, plot_midline=False, with_inset=True, 
                 tip_main=False, limits=None, fs=(8,6), format='eps', 
                 colours=None, fn_base='/tmp/both', offset_y=200,
                 cm_name='jet', set_aspect=True, log_prop=False, edge_prop=None, colorbar=False):
        self.dbs = dbs
        self.prop_name = prop_name
        self.fig = plt.figure(figsize=fs, dpi=100)
        self.count = 0
        self.limits = limits
        self.format = format
        self.colours = colours
        self.fn_base = fn_base
        self.offset_y = offset_y
        self.cm_name = cm_name
        self.set_aspect = set_aspect
        self.plot_midline = plot_midline
        self.log_prop = log_prop
        self.edge_prop = edge_prop
        self.colorbar = colorbar
        if plot_midline:
            from mpl_plot_midline import MPLPlotMidline
            self.midline_plotter = [ MPLPlotMidline(db) for db in self.dbs ]

    def redraw(self):
        plt.clf()
        cm = plt.get_cmap(self.cm_name)
        a = plt.gcf().add_axes([0, 0, 1, 1], frameon=False)
        for i, db in enumerate(self.dbs):
            offset = np.array((0, -self.offset_y*i))
            mesh = get_mesh(db)
            pos = db.get_property('position')
            cell_type = db.get_property('cell_type')
            polys = []
            p_cols = []
            data = []
            prop = db.get_property(self.prop_name)
            print 'PROP:', max(prop.itervalues()), min(prop.itervalues())
            for cid in mesh.wisps(2):
                pids = ordered_pids(mesh, cid)
                polys.append([pos[pid]+offset for pid in pids])
#                data.append(min(prop[cid],2))
                if self.log_prop:
                    data.append(log(max(prop[cid],1e-10)))
                else:
                    #data.append(min(prop[cid],2))
                    data.append(prop[cid])
                if self.colours:
                    if type(self.colours[i])==dict:
                        p_cols.append(self.colours[i][cell_type[cid]])
                    else:
                        p_cols.append(self.colours[i])
            if self.colours:
                
                col=PolyCollection(polys, linewidths=(0.2,), antialiaseds=True, facecolors=p_cols)
            else:
                col=PolyCollection(polys, linewidths=(0.2,), antialiaseds=True, cmap = cm)
                col.set_array(np.asarray(data))

            a.add_collection(col)#, autolim=True)


        if self.colorbar:
            plt.colorbar(col)

        if self.plot_midline:
            for i, mp in enumerate(self.midline_plotter):
                if i==1:
                    offset = np.array((0, -self.offset_y*i))
                    mp.redraw(offset=offset)
            
        a.add_line(Line2D((self.limits[0][1]-120,self.limits[0][1]-20),
                          (self.limits[1][0]+20, self.limits[1][0]+20),
                          linewidth=5, color='k'))
        plt.ylim(self.limits[1])
        plt.xlim(self.limits[0])
        plt.xticks([])
        plt.yticks([])
        if self.set_aspect:
            a.set_aspect('equal', adjustable='datalim')
        plt.savefig(self.fn_base+'%03d'%self.count+'.'+self.format)
#        plt.draw()
        self.count += 1
