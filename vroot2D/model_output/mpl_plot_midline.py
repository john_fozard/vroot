

"""
Module to plot tissue using matplotlib rather than OpenAlea
"""



from matplotlib.lines import Line2D
from vroot2D.growth.growth_midline import ml_to_xy

import matplotlib.pylab as plt
import numpy as np
from operator import itemgetter

from vroot2D.utils.db_utilities import get_mesh
from openalea.tissueshape import centroid


class MPLPlotMidline(object):

    def __init__(self, db):
        self.db = db
        self.h = 70
        

    def redraw(self, offset=None):
        if offset is None:
            offset = np.array((0,0))

        db = self.db
        kappa = db.get_property('kappa')
        l = db.get_property('l')
        x0 = db.get_property('x0')
        theta0 = db.get_property('theta0')
        Ns = 10
        N = len(l)
        h = self.h
        midline = []
        for i in range(len(l)):
            for s in range(Ns):
                midline.append(ml_to_xy((i, float(s)/Ns, 0), kappa, l, x0, theta0))
        midline.append(ml_to_xy((i, 1, 0), kappa, l, x0, theta0))
        topline = []
        for i in range(len(l)):
            for s in range(Ns):
                topline.append(ml_to_xy((i, float(s)/Ns, h), kappa, l, x0, theta0))
        topline.append(ml_to_xy((i, 1, h), kappa, l, x0, theta0))
        botline = []
        for i in range(len(l)):
            for s in range(Ns):
                botline.append(ml_to_xy((i, float(s)/Ns, -h), kappa, l, x0, theta0))
        botline.append(ml_to_xy((i, 1, -h), kappa, l, x0, theta0))
        plt.hold(True)
        x=[m[0]+offset[0] for m in midline]
        y=[m[1]+offset[1] for m in midline]
        plt.plot(x,y,'y-', lw=0.5)

        x=[m[0]+offset[0] for m in botline]
        y=[m[1]+offset[1] for m in botline]
        plt.plot(x,y,'b-', lw=0.5)

        x=[m[0]+offset[0] for m in topline]
        y=[m[1]+offset[1] for m in topline]
        plt.plot(x,y,'r-', lw=0.5)

        for i in range(0, Ns*N+1, Ns):
            plt.gca().add_line(Line2D((topline[i][0]+offset[0], botline[i][0]+offset[0]), (topline[i][1]+offset[1], botline[i][1]+offset[1]), lw=0.5))

    #    plt.draw()
