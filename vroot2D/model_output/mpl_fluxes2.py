
"""
Module to plot tissue using matplotlib rather than OpenAlea
"""

from openalea.celltissue import TissueDB
from openalea.container import ordered_pids
from matplotlib.collections import PolyCollection, CircleCollection
from matplotlib.patches import Rectangle
from matplotlib.text import Text
from random import random
from collections import defaultdict

from math import log, atan2, sin, cos, sqrt

def rot(v):
    """
    rotate a 2d vector (Vector2) by 90 degrees anticlockwise
    :param v: input vector
    :type v: Vector2/list/tuple
    :returns: Vector2 rotated v
    """
    return np.array((-v[1],v[0]))


import matplotlib
import matplotlib.pylab as plt
from matplotlib.lines import Line2D
import numpy as np
import scipy.linalg as la

from random import random

from vroot2D.utils.db_utilities import get_mesh, get_graph, get_wall_decomp
from vroot2D.utils.db_geom import ordered_wids_pids, centroid

def bounding_box(pts):
    min_v = np.array((1e20, 1e20))
    max_v = np.array((1e20, 1e20))
    for v in pts:
        min_v = np.minimum(v, min_v)
        max_v = np.maximum(v, max_v)
    return min_v, max_v

        

class MPLFluxes(object):

    def __init__(self, db, prop_name, 
                 limits=None, fs=(7.2,5.68), format='png', 
                 fn_base='/tmp/both', offset_y=200,
                 colorbar=True, 
                 cm_name='Pastel1', set_aspect=False, log_prop=False, cmap=None, thresh_prop=True):
        self.db = db
        self.prop_name = prop_name
        self.fig = plt.figure(figsize=fs, dpi=100)
        self.count = 0
        self.limits = limits
        self.format = format
        self.fn_base = fn_base
        self.offset_y = offset_y
        self.cm_name = cm_name
        self.cmap = cmap
        self.colorbar=colorbar
        self.set_aspect = set_aspect
        self.log_prop = log_prop
        self.thresh_prop = thresh_prop

    def redraw(self):
        plt.clf()
        if self.cmap:
            cm = self.cmap
        else:
            cm = plt.get_cmap(self.cm_name)
        a = plt.gcf().add_axes([0, 0, 1, 1], frameon=False)
        db = self.db
        mesh = get_mesh(db)
        pos = db.get_property('position')
        wall = db.get_property('wall') 
        polys = []
        p_cols = []
        data = []
        prop = db.get_property(self.prop_name)
        print 'PROP:', max(prop.itervalues()), min(prop.itervalues())
        for cid in mesh.wisps(2):
            pids = ordered_pids(mesh, cid)
            polys.append([pos[pid] for pid in pids])

        col=PolyCollection(polys, linewidths=(1.0,), antialiaseds=True, facecolors=(1,1,1))
        a.add_collection(col)

        centroids = dict((cid, centroid(mesh, pos, 2, cid)) for cid in mesh.wisps(2))
        wall_groups = defaultdict(list)
        prop = db.get_property(self.prop_name)
        J = {}
        wall_decomp = get_wall_decomp(db)
        
        graph = get_graph(db)

        arrow_polys = []
        arrow_data = []
        scale = 3
        for wid, (eid1, eid2) in wall_decomp.iteritems():
            cid1 = graph.source(eid1)
            cid2 = graph.target(eid1)
            v = 0.5*(prop[eid1]-prop[eid2])

            pid0, pid1 = mesh.borders(1, wid)
            p0 = pos[pid0]
            p1 = pos[pid1]
 
            c = 0.5*(p0+p1)

            t = p1 - p0
            t = t/la.norm(t)

            n = rot(t)
            if np.dot(n, centroids[cid2] - centroids[cid1])<0:
                n = -n

            if v>0:
                poly = [c+n*scale, c+0.4*t*scale, c-0.4*t*scale]
            else:
                poly = [c-n*scale, c+0.4*t*scale, c-0.4*t*scale]
                v = -v
            arrow_polys.append(poly)
            arrow_data.append(v)      
  
        col2 = PolyCollection(arrow_polys, linewidths=0, antialiaseds=True)
        col2.set_array(np.asarray(arrow_data))
        a.add_collection(col2)

        if self.colorbar:
            plt.colorbar(col2)

        plt.ylim(self.limits[1])
        plt.xlim(self.limits[0])
        plt.xticks([])
        plt.yticks([])
        if self.set_aspect:
            a.autoscale_view()
            a.set_aspect('equal', adjustable='datalim')
        plt.savefig(self.fn_base+'%03d'%self.count+'.'+self.format)
        self.count += 1




class MPLCWFluxes(object):

    def __init__(self, db, prop_name, 
                 limits=None, fs=(7.2,5.68), format='png', 
                 fn_base='/tmp/both', offset_y=200,
                 colorbar=True, 
                 cm_name='Pastel1', set_aspect=False, log_prop=False, cmap=None, thresh_prop=True):
        self.db = db
        self.prop_name = prop_name
        self.fig = plt.figure(figsize=fs, dpi=100)
        self.count = 0
        self.limits = limits
        self.format = format
        self.fn_base = fn_base
        self.offset_y = offset_y
        self.cm_name = cm_name
        self.cmap = cmap
        self.colorbar=colorbar
        self.set_aspect = set_aspect
        self.log_prop = log_prop
        self.thresh_prop = thresh_prop

    def redraw(self):
        plt.clf()
        if self.cmap:
            cm = self.cmap
        else:
            cm = plt.get_cmap(self.cm_name)
        a = plt.gcf().add_axes([0, 0, 1, 1], frameon=False)
        db = self.db
        mesh = get_mesh(db)
        pos = db.get_property('position')
        wall = db.get_property('wall') 
        polys = []
        p_cols = []
        data = []
        prop = db.get_property(self.prop_name)
        print 'PROP:', max(prop.itervalues()), min(prop.itervalues())
        for cid in mesh.wisps(2):
            pids = ordered_pids(mesh, cid)
            polys.append([pos[pid] for pid in pids])

        col=PolyCollection(polys, linewidths=(1.0,), antialiaseds=True, facecolors=(1,1,1))
        a.add_collection(col)

        centroids = dict((cid, centroid(mesh, pos, 2, cid)) for cid in mesh.wisps(2))
        wall_groups = defaultdict(list)
        prop = db.get_property(self.prop_name)
        J = {}
        wall_decomp = get_wall_decomp(db)
        
        graph = get_graph(db)

        arrow_polys = []
        arrow_data = []
        scale = 3


        for cid in mesh.wisps(2):
            for eid in graph.out_edges(cid):
                v = prop[eid]
                wid = wall[eid]

                pid0, pid1 = mesh.borders(1, wid)
                p0 = pos[pid0]
                p1 = pos[pid1]
 
                c = 0.5*(p0+p1)

                t = p1 - p0
                t = t/la.norm(t)

                n = rot(t)
                if np.dot(n, c - centroids[cid])<0:
                    n = -n

                    c = c - 0.3*t*scale
                if v<0:
                    v = -v
                    poly = [c-n*scale, c+0.4*t*scale, c-0.4*t*scale]
                else:
                    poly = [c, c+0.4*t*scale-n*scale, c-0.4*t*scale-n*scale]
                arrow_polys.append(poly)
                arrow_data.append(v)      
  
        col2 = PolyCollection(arrow_polys, linewidths=0, antialiaseds=True)
        col2.set_array(np.asarray(arrow_data))
        a.add_collection(col2)

        if self.colorbar:
            plt.colorbar(col2)

        plt.ylim(self.limits[1])
        plt.xlim(self.limits[0])
        plt.xticks([])
        plt.yticks([])
        if self.set_aspect:
            a.autoscale_view()
            a.set_aspect('equal', adjustable='datalim')
        plt.savefig(self.fn_base+'%03d'%self.count+'.'+self.format)
        self.count += 1
