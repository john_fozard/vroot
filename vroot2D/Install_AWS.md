# Installing on Ubuntu Server 14.04 LTS (AWS)

Here we display a complete list of instructions to 
install and run the simulations on a fresh install of Ubuntu Server
14.04 LTS. This is of particular interest if it is desired to
run the simulations on a virtual machine (e.g. VirtualBox),
or on an Amazon Web Services instance. 

We supply a script file which performs all these steps.

###Installing basic packages

```sudo apt-get update
sudo apt-get upgrade
sudo apt-get install g++ build-essential python-dev python-numpy python-scipy scons git python-matplotlib
```
Also need to install numerical libraries from Ubuntu

```sudo apt-get install libboost-python-dev libsundials-serial-dev libsuitesparse-dev liopenblas-dev
```

Install meshpy and some libraries for gmsh
```sudo apt-get install python-pip
sudo pip install meshpy
```

Install libraries needed for gmsh (Ubuntu Server only)

```sudo apt-get install libglu1-mesa libsm6
```
sudo apt-get clean

Download source from plant-images git repository
```git clone https://code.plant-images.org/vroot2D
```

Install Eigen C++ numerical library
```cd vroot2D
cd src
cd cpp
wget "bitbucket.org/eigen/eigen/get/3.2.5.tar.gz"
tar zxvf 3.2.5.tar.gz 
mv eigen-eigen-bdd17ee3b1b3/ eigen
cd ../..
```

Install gmsh mesh generator (needs to be a more recent version than in the Ubuntu repositories)
```mkdir Downloads
cd Downloads/
wget "http://geuz.org/gmsh/bin/Linux/gmsh-2.9.3-Linux64.tgz"
tar zxvf gmsh-2.9.3-Linux64.tgz 
ls
rm gmsh-2.9.3-Linux64.tgz 
ls
cd ..
```

### Add temporary swap file (AWS only)
```sudo dd if=/dev/zero of=/swapfile bs=1M count=1024
sudo mkswap /swapfile
sudo swapon /swapfile
```

### Compilation
```cd vroot2D
scons .
```

### Remove swap file (AWS only)
```sudo swapoff /swapfile
sudo rm /swapfile
```
### Fix triangle mesh generation to include neighbours
```cd /usr/local/lib/python2.7/dist-packages/meshpy
sudo sed -i "s/pzj/npzj/" triangle.py
```



