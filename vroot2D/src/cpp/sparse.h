// $Author : JAF$
// $Id : $


#ifndef SPARSE_H
#define SPARSE_H

#include <iostream>
#include <vector>

class spmat
{
 public:

  int                   m, n, nnz;
  int max;
  int                   *i, *j;
  double                *a;
  spmat(int pm=0, int pn=0, int pmax=0)
    : m(pm), n(pn), nnz(0), max(pmax)
    { i=new int[pmax]; j=new int[pmax]; a=new double[pmax]; };
  ~spmat()
    { delete[] i; delete[] j; delete[] a; };
  void push(const int pi, const int pj, const double pa)
    { 	i[nnz]=pi; 
	j[nnz]=pj;
	a[nnz]=pa; 
	++nnz; 
    }
  double& val(const int pn)
    { return a[pn]; };
  void rw()
    { nnz=0; };
  virtual void resize(int pm, int pn, int pmax)
    {  delete[] i; delete[] j; delete[] a; m=pm; n=pn; max=pmax;
    i=new int[pmax]; j=new int[pmax]; a=new double[pmax]; };
};

class spmat_umf: public spmat
{
 private:
  void *numeric, *symbolic;
  int *Map;
 public:
  int *Ap, *Ai;
  double *Ax;
  spmat_umf(int pm=0, int pn=0, int pmax=0) :
    spmat(pm,pn,pmax), numeric(0), symbolic(0)
    {
      Map=new int[max]; Ap=new int[n+1]; Ai=new int[max]; Ax=new double[max];
    };

  ~spmat_umf()
    {
      if(numeric) lu_fnum(); if(symbolic) lu_fsymb();
      delete[] Map; delete[] Ap; delete[] Ai; delete[] Ax;
    };
  void resize(int pm, int pn, int pmax)
    {
      delete[] Map; delete[] Ap; delete[] Ai; delete[] Ax;
      spmat::resize(pm, pn, pmax);
      Map=new int[max]; Ap=new int[n+1];
      Ai = new int[max]; Ax=new double[max];
    };
  spmat_umf& lu_conv();
  spmat_umf& lu_convm();
  spmat_umf& lu_symb();
  spmat_umf& lu_fsymb();
  spmat_umf& lu_num();
  spmat_umf& lu_fnum();
  void lu_solve(double*, double*);
};

class cscmat
{

 private:
  void *numeric, *symbolic;
 public:

  int  m, n;
  std::vector<int> Ap, Ai;
  std::vector<double> Ax;
  std::vector<int> igrp, jgrp;
  

  cscmat(int pm=0, int pn=0, int nnz_est=0) :
  numeric(0), symbolic(0), m(pm), n(pn)
    {
      Ap.resize(n+1); Ai.reserve(nnz_est); Ax.reserve(nnz_est);
      igrp.resize(n+1); jgrp.resize(n);
    };

  ~cscmat()
    {
      if(numeric) lu_fnum(); if(symbolic) lu_fsymb();
    };

  void rw()
  {
    Ai.clear(); Ax.clear();
  }

  void resize(int pm, int pn, int nnz_est=0)
    {
      m=pm; n=pn;
      Ap.resize(n+1); Ai.reserve(nnz_est); Ax.reserve(nnz_est);
      igrp.resize(n+1); 
      jgrp.resize(n);
    };
  
  void lu_symb();
  void lu_fsymb();
  int  lu_num();
  void lu_fnum();
  void lu_solve(double*, double*);

  int group();

};


class coomat
{
 public:
  int  m, n;
  std::vector<int> Ai, Aj;
  std::vector<double> Ax;
  coomat(int pm=0, int pn=0, int nnz_est=0) : m(pm), n(pn)
    {
      Ai.resize(nnz_est); Aj.reserve(nnz_est); Ax.reserve(nnz_est);
    }  
  void push(const int pi, const int pj, const double px)
  {
    Ai.push_back(pi); Aj.push_back(pj); Ax.push_back(px);
  }
  void rw()
  {
    Ai.clear(); Aj.clear(); Ax.clear();
  }
  void to_csc(cscmat& C);
};

#endif // SPARSE_H
