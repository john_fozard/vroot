

/*! \file module.cpp
 *  C++ Python extension for mechanical simulations */

// A define to allow large numbers of arguments in boost::python wrapped
// functions
#define BOOST_PYTHON_MAX_ARITY 20
//#define EIGEN_DONT_PARALLELIZE

//#include <google/profiler.h>

#include <Python.h>
#include <boost/python.hpp>
#include <numpy/arrayobject.h>

#include <iostream>
#include <cstdio>
#include <cmath>

// Includes for the differential-algebraic solver IDA from the
// SUNDIALS suite of codes
#include <ida/ida.h>
#include <ida/ida_spgmr.h>
// This include file is for the IDA dense linear algebra and Jacobian
// calculation by finite differences.
//#include <ida/ida_dense.h>
#include <nvector/nvector_serial.h>
#include <sundials/sundials_types.h>
#include <sundials/sundials_math.h>

// My (not very robust, and really simple) sparse matrix helper library
#include "sparse.h"

// Std OpenMP library
#include <omp.h>

// Import Eigen3 library
#include <eigen/Eigen/Core>
#include <eigen/Eigen/LU>

//! Typedef for simple 2d vector class
typedef Eigen::Vector2d Vec2;

//! Unused RAIL object for release of the Python GIL in multithreaded sections
class ScopedGILRelease {
public:
        inline ScopedGILRelease() { m_thread_state = PyEval_SaveThread(); }
        inline ~ScopedGILRelease() { PyEval_RestoreThread(m_thread_state); m_thread_state = NULL; }
private:
        PyThreadState* m_thread_state;
};




/*! \brief Rotate a vec2 by 90 degrees CCW
 * \param v a vector
 * \return v rotated by 90 degrees */
static inline Vec2 rot(const Vec2& v)
{
  return Vec2(v[1],-v[0]);
}


class dmat : public Eigen::Array<double, 6, 1>
{
public:
  typedef Eigen::Array<double, 6, 1> Base;
  dmat() : Base() {}

  dmat(const double& v) : Base(dmat::Ones()*v) {}

  dmat(const dmat& other) : Base(other) {}



  // This constructor allows you to construct MyVectorType from Eigen expressions                                                                   
  template<typename OtherDerived>
  dmat(const Eigen::ArrayBase<OtherDerived>& other)
    : Base(other)
  { }
  // This method allows you to assign Eigen expressions to MyVectorType                                                                             
  template<typename OtherDerived>
  dmat& operator= (const Eigen::ArrayBase <OtherDerived>& other)
  {
    this->Base::operator=(other);
    return *this;
  }
};


class dmat4 : public Eigen::Array<double, 4, 1>
{
public:
  typedef Eigen::Array<double, 4, 1> Base;
  dmat4() : Base() {}

  dmat4(const double& v) : Base(dmat4::Ones()*v) {}

  dmat4(const dmat4& other) : Base(other) {}



  // This constructor allows you to construct MyVectorType from Eigen expressions                                                                   
  template<typename OtherDerived>
  dmat4(const Eigen::ArrayBase<OtherDerived>& other)
    : Base(other)
  { }
  // This method allows you to assign Eigen expressions to MyVectorType                                                                             
  template<typename OtherDerived>
  dmat4& operator= (const Eigen::ArrayBase <OtherDerived>& other)
  {
    this->Base::operator=(other);
    return *this;
  }
};



Eigen::Matrix<dmat, 2, 1> rotD(const Eigen::Matrix<dmat, 2, 1>& v)
{
  Eigen::Matrix<dmat, 2, 1> r;
  r[0]=v[1];
  r[1]=-v[0];
return r;
}

/*! \brief dot(a,rot(b))
 * \param a first vector
 * \param b second vector
 * \return a[0]*b[1]-a[1]*b[0]; */
double det(const Vec2&a, const Vec2&b)
{
  return a[0]*b[1]-a[1]*b[0];
}

double max_diff(const cscmat& mat1, const cscmat& mat2)
{
  double max_diff = 0.0;
  for(int j=0; j!=mat1.n; ++j) {
    for(int k1=mat1.Ap[j]; k1!=mat1.Ap[j+1];++k1)
      {
	double cdiff = std::abs(mat1.Ax[k1]);
	for(int k2=mat2.Ap[j]; k2!=mat2.Ap[j+1];++k2)
	  if(mat1.Ai[k1]==mat2.Ai[k2])
	    cdiff = std::abs(mat1.Ax[k1]-mat2.Ax[k2]);
	if(cdiff>max_diff)
	  max_diff = cdiff;
	if(cdiff==std::abs(mat1.Ax[k1]) && std::abs(cdiff)>1e-15)
	  std::cout << "missing" << mat1.Ai[k1] << " " << j << " " << mat1.Ax[k1] << " " << max_diff << "\n";
      }
  }
  return max_diff;
}

//! \brief Print cscmat to stream
std::ostream& operator<<(std::ostream& os, const cscmat& mat)
{
  os << mat.m << " " << mat.n << "\n";
  os << mat.Ap.size() << " " << mat.Ai.size() << " " << mat.Ax.size() << "\n";
  // Loop over elements, column by column
  for(int j=0; j!=mat.n; ++j) {
    for(int k=mat.Ap[j]; k!=mat.Ap[j+1];++k)
      // Omit elements which are small
      if(std::fabs(mat.Ax[k])>1e-15)
	os << "(" << mat.Ai[k] << "," << j << ") : " << mat.Ax[k] << "\n";
  }
  return(os);
}

//! \brief Print cscmat to stream
std::ostream& operator<<(std::ostream& os, const coomat& mat)
{
  os << mat.m << " " << mat.n << "\n";
  os << mat.Ai.size() << " " << mat.Aj.size() << " " << mat.Ax.size() << "\n";
  // Loop over elements, column by column
  for(int k=0; k!=mat.Ai.size(); ++k) {
      // Omit elements which are small
      //   if(std::fabs(mat.Ax[k])>1e-15)
	os << "(" << mat.Ai[k] << "," << mat.Aj[k] << ") : " << mat.Ax[k] << "\n";
  }
  return(os);
}


//! \brief Print spmat_umf to stream 
std::ostream& operator<<(std::ostream& os, const spmat_umf& mat)
{
  os << mat.m << " " << mat.n << "\n";
  // Loop over elements, column by column
  for(int j=0; j!=mat.n; ++j) {
    for(int k=mat.Ap[j]; k!=mat.Ap[j+1];++k)
      // Omit elements which are small
      if(std::fabs(mat.Ax[k])>1e-15)
	os << "(" << mat.Ai[k] << "," << j << ") : " << mat.Ax[k] << "\n";
  }
  return(os);
}


//! \brief Structure to store simulation state
struct sim_st { 
  double mu_eps;
  //! Number of cells
  int Nc;
  //! Number of edges
  int Ne; 
  //! Number of triangles
  int Nt;
  //! Number of verticies
  int Nv; 
  //! Number of vertices with drag forces
  int Nf;

  //! Pointer to array 
  int *cell_idx;
  //! Pointer to array of cell attributes 
  //! (turgor pressure)
  double *cell_attribs; 
  //! Pointer to array of start and end vertex indices for each edge
  int (*edge_idx)[2]; 
  //! Pointer to array of cell attributes 
  //! (lambda, gamma, phiinv)
  double (*edge_attribs)[5];
  int (*tri_idx)[3];
  double (*tri_attribs)[10];
  //! Pointer to array of indices of vertex coordinates with drag
  //! x = [x_0, y_0, x_1, y_1, ... ]
  int *fixed;
  //! Current jacobian matrix
  cscmat* J;
  bool visc;
  //! Per-vertex drag
  double drag;
};


/*! \brief Calculation of Jacobian matrix sparsity pattern
 * \param J Jacobian matrix (modified)
 * \param ss Simulation state vector */
void JPat(cscmat& J, const sim_st& ss)
{

  int n = 2*ss.Nv+4*ss.Nt+ss.Ne;
  coomat cJ(n, n);

  for(int i=0; i<n; ++i)
    cJ.push(i, i, 1.0);
  
  // Loop over cells
  for(int i=0; i<ss.Nc; ++i)
    {
      // Loop over edges in cell
      for(int j=ss.cell_idx[i]; j<ss.cell_idx[i+1]; ++j)
	{

	  // Cell edges.
	  // Start position, end position and edge natural length
	  // may all depend on each other
	  const int i0=ss.edge_idx[j][0];
	  const int i1=ss.edge_idx[j][1];

	  cJ.push(2*i0,  2*i0,   1.0);
	  cJ.push(2*i0,  2*i0+1, 1.0);
	  cJ.push(2*i0,  2*i1,   1.0);
	  cJ.push(2*i0,  2*i1+1, 1.0);
	  cJ.push(2*i0,  2*ss.Nv+4*ss.Nt+j, 1.0);

	  cJ.push(2*i0+1,  2*i0,   1.0);
	  cJ.push(2*i0+1,  2*i0+1, 1.0);
	  cJ.push(2*i0+1,  2*i1,   1.0);
	  cJ.push(2*i0+1,  2*i1+1, 1.0);
	  cJ.push(2*i0+1,  2*ss.Nv+4*ss.Nt+j, 1.0);

	  cJ.push(2*i1,  2*i0,   1.0);
	  cJ.push(2*i1,  2*i0+1, 1.0);
	  cJ.push(2*i1,  2*i1,   1.0);
	  cJ.push(2*i1,  2*i1+1, 1.0);
	  cJ.push(2*i1,  2*ss.Nv+4*ss.Nt+j, 1.0);

	  cJ.push(2*i1+1,  2*i0,   1.0);
	  cJ.push(2*i1+1,  2*i0+1, 1.0);
	  cJ.push(2*i1+1,  2*i1,   1.0);
	  cJ.push(2*i1+1,  2*i1+1, 1.0);
	  cJ.push(2*i1+1,  2*ss.Nv+4*ss.Nt+j, 1.0);

	  cJ.push(2*ss.Nv+4*ss.Nt+j,  2*i0,   1.0);
	  cJ.push(2*ss.Nv+4*ss.Nt+j,  2*i0+1, 1.0);
	  cJ.push(2*ss.Nv+4*ss.Nt+j,  2*i1,   1.0);
	  cJ.push(2*ss.Nv+4*ss.Nt+j,  2*i1+1, 1.0);
	  cJ.push(2*ss.Nv+4*ss.Nt+j,  2*ss.Nv+4*ss.Nt+j, 1.0);

	}
    }

  for(int i=0; i<ss.Nt; ++i)
    {
      // Triangles
      // The forces on each corner may depend on the position of
      // all the other corners
      const int i0 = ss.tri_idx[i][0];
      const int i1 = ss.tri_idx[i][1];
      const int i2 = ss.tri_idx[i][2];
      const int iT = 2*ss.Nv + 4*i;
      const int idx[] = { 2*i0, 2*i0+1, 2*i1, 2*i1+1, 
			2*i2, 2*i2+1, 
			iT, iT+1, iT+2, iT+3 };
      for(int k=0; k<10; k++)
	for(int l=0; l<10; l++) 
	  cJ.push(idx[k], idx[l], 1.0);
    }

  cJ.to_csc(J);
}



/*! \brief Residual of differential equation system
 * \param x Simulation state vector (vertex positions + edge natural lengths)
 * \param v Simulation velocity vector (v = dx/dt)
 * \param mu_eps Damping parameter
 * \param ss Simulation state structure */
Eigen::VectorXd res_tri(const Eigen::VectorXd& x, const Eigen::VectorXd& v, double mu_eps, const sim_st& ss)
{
  // Residual vector
  Eigen::VectorXd r(2*ss.Nv+4*ss.Nt+ss.Ne);
  r.setZero();
  // Loop over all cells in cell_idx
  for(int i=0; i<ss.Nc; ++i)
  {
      // Cell pressure
      const double p=ss.cell_attribs[i];
      // Loop over the edges for this cell
      for(int j=ss.cell_idx[i]; j<ss.cell_idx[i+1]; ++j)
	{
	  // Cell edge forces
	  // Indices of start and end vertex
	  const int i0=ss.edge_idx[j][0];
	  const int i1=ss.edge_idx[j][1];
	  // Vector along edge
	  const Vec2 d=x.segment<2>(2*i1)-x.segment<2>(2*i0);
	  // Length of edge
	  const double l=d.norm();
	  // Pressure force
	  const Vec2 p_force=0.5*p*rot(d);
	  // Calculate the spring forces
	  // Spring strength, lambda
	  const double lambda=ss.edge_attribs[j][0];
	  // Spring natural length
	  const double l0=1.0; //x[2*ss.Nv+j];

	  // Elastic tension in spring
	  const Vec2 t_force=lambda*(l-l0)*d/(l*l0);
	  // Edge viscous drag forces
	  const double phi_inv= ss.edge_attribs[j][2];
	  // Edge yield stress
	  const double tau = ss.edge_attribs[j][3];
	  // Edge yield shear rate
	  const double eps0 = ss.edge_attribs[j][4];


	  // Velocity difference between end-points
	  const Vec2 dv=v.segment<2>(2*i1)-v.segment<2>(2*i0);
	  // Drag forces from Lockhart
	  // Zero yield stress, assumed that wall is
	  // always under tension

	  // Strain rate in wall
	  const double eps = d.dot(dv)/(l*l);
	  
	  
	  // CHANGE WALL VISC HERE
	  //const Vec2 d_force=phi_inv/(l*l*l)*d.dot(dv)*d;

	  const double eta = abs(eps/eps0)>1e-50 ? tau/eps0*(1-exp(-eps0/eps))+phi_inv : tau/eps0 + phi_inv;
	  const Vec2 d_force = 0.0*eta*eps*d/l;


	  // Assign forces to start and end vertices in residual
	  // pressure + elastic tension + viscous tension
	  r.segment<2>(2*i0)+=p_force; //+t_force+d_force;
	  r.segment<2>(2*i1)+=p_force; //-t_force-d_force;
	  // Edge spring length relaxation
	  const double gamma=ss.edge_attribs[j][1];
	  // Relaxation of spring natural lengths
	  //	  r[2*ss.Nv+j]=gamma*(l-l0)-v[2*ss.Nv+j];
	  r[2*ss.Nv+4*ss.Nt+j]=-v[2*ss.Nv+4*ss.Nt+j];


	}
    }
  
  for(int i=0; i<ss.Nt; ++i)
  {
    
      const int i0=ss.tri_idx[i][0];
      const int i1=ss.tri_idx[i][1];
      const int i2=ss.tri_idx[i][2];
      const int iF0=2*ss.Nv+4*i;
      const Eigen::Map<const Eigen::Matrix2d> F0(x.segment<4>(iF0).data());
      const Eigen::Map<const Eigen::Matrix2d> vF0(v.segment<4>(iF0).data());
      Eigen::Map<Eigen::Matrix2d> rF0(r.segment<4>(iF0).data());

      const double lambda=ss.tri_attribs[i][0];
      const double mu1=ss.tri_attribs[i][1];
      const double mu2=ss.tri_attribs[i][2];
      const double gamma=ss.tri_attribs[i][3];
      const double tau = ss.tri_attribs[i][4];
      const double eps0 = ss.tri_attribs[i][5];
      Eigen::Map<Eigen::Vector2d> A0(&ss.tri_attribs[i][6]);
      Eigen::Map<Eigen::Vector2d> A1(&ss.tri_attribs[i][8]);

      // Elastic forces owing to each triangle

      const Eigen::VectorBlock<const Eigen::VectorXd, 2> x0= x.segment<2>(2*i0); 
      const Eigen::VectorBlock<const Eigen::VectorXd, 2> x1= x.segment<2>(2*i1); 
      const Eigen::VectorBlock<const Eigen::VectorXd, 2> x2= x.segment<2>(2*i2); 
      const Eigen::VectorBlock<const Eigen::VectorXd, 2> v0= v.segment<2>(2*i0); 
      const Eigen::VectorBlock<const Eigen::VectorXd, 2> v1= v.segment<2>(2*i1); 
      const Eigen::VectorBlock<const Eigen::VectorXd, 2> v2= v.segment<2>(2*i2); 

      //      std::cout << std::endl;
      // std::cout << F0 << std::endl;
      // std::cout << std::endl;
   
      Eigen::Matrix2d F0_inv;
      F0_inv=F0.inverse();
      Eigen::Matrix2d F1;
      F1 << x1 - x0, x2-x0;
      Eigen::Matrix2d F1_inv;
      F1_inv=F1.inverse();

      Eigen::Matrix2d F;
      F = F1*F0_inv;
      
      Vec2 a0 = F1 * F0_inv * A0;
      a0.normalize();
      Vec2 a1 = F1 * F0_inv * A1;
      a1.normalize();


      //      Eigen::Matrix2d E;

      
      // Calculate the invariants of the rate of strain matrix;

      //      const double I1 = E.diagonal().sum();
      //      const double I2 = E.determinant();
      //    const double eps = E.squaredNorm();
      // const double eps = E.norm();

      Eigen::Matrix2d E;
      E=0.5*(F.transpose()*F-Eigen::Matrix2d::Identity());


      Eigen::Matrix2d aEa0 = a0*a0.transpose();
      const double asr0 = a0.dot(E*a0);
      Eigen::Matrix2d aEa1 = a1*a1.transpose();
      const double asr1 = a1.dot(E*a1);


      Eigen::Matrix<double, 2, 2> T;
      T=F*(lambda*E.trace()*Eigen::Matrix2d::Identity()+2.0*lambda*E + mu2*asr0*aEa0 + mu2*asr1*aEa1);
      //T = F*lambda*E;

      //      std::cout << E << std::endl;
      //      Eigen::Matrix2d T;

      /*
      // Key is to let the mus depend on I2.
      // Modified Bingham model
      // T. C. Papanastasiou, Flows of materials with yield, J. of Rheology, 31 (1987), pp. 384–404.
      // via A. M. Robertson Lecture Notes on Non-Newtonian Fluids

      const double eta = mu1 + tau_Y*(1-exp(-n*S))/S;
      
      */

      //      const double eta = abs(eps/eps0)>1e-50 ? mu1 + tau/eps0*(1-exp(-eps0/eps)) : mu1 + tau/eps0;
      


      // T.noalias()=2*eta*E + mu2*(a0*a0.transpose()*asr0+a1*a1.transpose()*asr1) + mu3*(aEa0+aEa0.transpose()+aEa1+aEa1.transpose());

      r.segment<2>(2*i0)+=0.5*T*rot(x2-x1);
      r.segment<2>(2*i1)+=0.5*T*rot(x0-x2);
      r.segment<2>(2*i2)+=0.5*T*rot(x1-x0);

      rF0 += vF0 - gamma*E*F0;

    }
  for(int i=0; i<ss.Nv; ++i)
    {
      // Velocity-dependent drag forces 
      r.segment<2>(2*i)+=-ss.mu_eps*v.segment<2>(2*i);
    }
  for(int i=0; i<ss.Nf; ++i)
    {
      // Fixed vertex components
      r.segment<2>(2*ss.fixed[i])=v.segment<2>(2*ss.fixed[i]);
    }

  //  std::cout << r << std::endl;



  return r;

}



/*! \brief Numerical calculation of Jacobian matrix
 *
 * Calculates J = \theta_x \partial r / \partial x +
 *                \theta_v \partial r / \partial v
 * by finite differences, where r(x,v) is the residual function
 *
 *
 * The columns of the Jacobian matrix are arranged into groups
 *
 * e.g. with the sparsity pattern
 *
 *       / 1 0 0 0 0 \ 
 *       | 1 1 0 0 0 |
 *       | 0 0 1 0 1 |
 *       | 0 0 1 0 0 |
 *       \ 0 0 0 1 0 /
 *
 * the column groups are { {0,2,3}, {1,4} }, as these columns can be 
 * calculated by finite differences simultaneously.
 * These groupings are stored in J.igrp[Ng+1] and J.jgrp[n] 
 * where the column indices for the m-th group are given by
 * J.jgrp[k] for  J.igrp[m]<=k<J.igrp[m+1]
 *
 * \param J  Jacobian matrix
 * \param x0 Simulation state vector (vertex positions + edge natural lengths
 * \param v0 Simulation velocity vector (v = dx/dt)
 * \param theta_x Coefficient of dr/dx
 * \param theta_v Coefficient of dr/dv
 * \param mu_eps Damping parameter
 * \param ss Simulation state structure */
void J_num(cscmat& J, const Eigen::VectorXd& x0, const Eigen::VectorXd& v0, double theta_x, double theta_v, const sim_st& ss)
{
  const double aeps=1e-6;
  const double reps=1e-6;

  int Ng;
  // Count number of column groups
  for(Ng=0; J.igrp[Ng]!=J.n; ++Ng)
      ;
  
  //   This calculation is parallelized using OpenMP
  //   set the environment variable  OMP_NUM_THREADS to 1 
  //   to force single-threaded computation
  //   export OMP_NUM_THREADS=1
  //   or compile without openmp options

  // This starts the parallel section of code (thread per processor) 
    #pragma omp parallel 
    {
    Eigen::VectorXd r2(J.m), r1(J.m);
    Eigen::VectorXd x(J.n), v(J.n);
  
    // Loop over each column group (distributed over the threads)
    #pragma omp for
    for(int g=0; g<Ng; ++g)
      //for(int g=0; J.igrp[g]!=J.n; ++g)
    {
      x=x0;
      v=v0;
      // Calculate location of first point (increase those members of
      // x which are within the column group by a "small" amount)
      for(int k=J.igrp[g]; k!=J.igrp[g+1]; ++k)
	{
	  x[J.jgrp[k]]=x0[J.jgrp[k]]+std::max(aeps,reps*fabs(x0[J.jgrp[k]]));
	}
      // Calculate residual at first point
      r1=res_tri(x, v, ss.mu_eps, ss);
      // Calculate location of second point (decrease those members of
      // x which are within the column group by a "small" amount)
      for(int k=J.igrp[g]; k!=J.igrp[g+1]; ++k)
	{
	  x[J.jgrp[k]]=x0[J.jgrp[k]]-std::max(aeps,reps*fabs(x0[J.jgrp[k]]));
	
	}
      // Calculate residual at second point
      r2=res_tri(x, v, ss.mu_eps, ss);
      // Calculate contribution to jacobian by centred finite differences
      for(int k=J.igrp[g]; k!=J.igrp[g+1]; ++k) 
	for(int l=J.Ap[J.jgrp[k]];l!=J.Ap[J.jgrp[k]+1];++l) 
	  J.Ax[l]=0.5*(r1[J.Ai[l]]-r2[J.Ai[l]])*
	    theta_x/std::max(aeps,reps*fabs(x0[J.jgrp[k]]));
    }

    // Now calculate dependence on velocity variables
    #pragma omp for
    //  for(int g=0; J.igrp[g]!=J.n; ++g)
    for(int g=0; g<Ng; ++g)
        {
      v=v0;
      x=x0;
      // Calculate velocity of first point (increase those members of
      // v which are within the column group by a "small" amount)
      for(int k=J.igrp[g]; k!=J.igrp[g+1]; ++k)
	{
	  v[J.jgrp[k]]=v0[J.jgrp[k]]+std::max(aeps,reps*fabs(v0[J.jgrp[k]]));
	}
      // Calculate residual at first point      
      r1=res_tri(x, v, ss.mu_eps, ss);
      // Calculate velocity of second point (decrease those members of
      // v which are within the column group by a "small" amount)
      for(int k=J.igrp[g]; k!=J.igrp[g+1]; ++k)
	{
	  v[J.jgrp[k]]=v0[J.jgrp[k]]-std::max(aeps,reps*fabs(v0[J.jgrp[k]]));
	}
      // Calculate residual at second point
      r2=res_tri(x, v, ss.mu_eps, ss);
      // Calculate contribution to jacobian by centred finite differences
      for(int k=J.igrp[g]; k!=J.igrp[g+1]; ++k) 
	for(int l=J.Ap[J.jgrp[k]];l!=J.Ap[J.jgrp[k]+1];++l) 
	  J.Ax[l]+=0.5*(r1[J.Ai[l]]-r2[J.Ai[l]])*
	    theta_v/std::max(aeps,reps*fabs(v0[J.jgrp[k]]));
    }
   
}
       
}



/*! \brief Norm for error tolerance (in NR convergence) for BE
 * (TODO) Explain this norm
 * \param w Vector
 * \param y Current simulation state vector
 * \param rtol Relative error tolerance
 * \param atol Absolute error tolerance
 */

inline double nrm(const Eigen::VectorXd& w, const Eigen::VectorXd& y, const double rtol, const double atol)
{
  return (w.cwiseAbs().cwiseQuotient(rtol*y.cwiseAbs()+atol*Eigen::VectorXd::Ones(y.size()))).maxCoeff();
}



/*! \brief Calculate initial values for IDA using BE
 *  \param yval  simulation state vector
 *  \param ypval rate of change of simulation state vector (output)
 *  \param ss    simulation state structure
 *  \param dt0   initial timestep
 */
void init_vals(double *yval, double* ypval, const sim_st& ss, double dt0)
{
  const int n=2*ss.Nv+4*ss.Nt+ss.Ne;
  cscmat J(n, n);
  JPat(J, ss);
  J.group();

  int niter;
  const int niter_max=20;
  bool iter_fail;

  double dt=0.001;
  double dist;
  const double rtol=0.0;
  const double atol=1e-3;
  const double kappa=0.1;


  Eigen::VectorXd y0(n), y1(n), z0(n), z1(n), r(n), delta(n);

  
  y0=Eigen::Map<Eigen::VectorXd>(yval, n); 
  //  z0=Eigen::Map<Eigen::VectorXd>(ypval, n);
  z0.setZero();

  //  std::cout << "y0: " << y0.transpose() << std::endl;


  r=res_tri(y0, z0, ss.mu_eps, ss);

  std::cout << "r_init0 : " << r.cwiseAbs().maxCoeff() <<"\n";


  //  z0 = r;

  J_num(J, y0, z0, 0.0, -1.0, ss);
  J.lu_symb();
  J.lu_num();
  J.lu_solve(z1.data(), r.data());

  z0 = z1;
  r=res_tri(y0, z0, ss.mu_eps, ss);

  std::cout << "r_init1 : " << r.cwiseAbs().maxCoeff() <<"\n";


  J_num(J, y0, z0, 0.0, -1.0, ss);
  J.lu_num();
  J.lu_solve(z1.data(), r.data());
  
  z0 += z1;

  //  std::cout << r << std::endl;
  
  /*

  for(int k=0; k<3; ++k)
    {
      iter_fail=false;
      while(1)
	{
	  niter++;
	  y1=y0+dt*z1;
	  
	  J_num(J, y1, z1, -dt, -1.0, ss);
	  J.lu_symb();
	  J.lu_num();
	  J.lu_solve(delta.data(), r.data());
      
	  dist=nrm(delta, y0, rtol, atol);
	  
	  std::cout << niter << " " << r.cwiseAbs().maxCoeff() << " " << delta.cwiseAbs().maxCoeff() << " " << dist << " " << kappa << " " << nrm(r, y0, rtol, atol) << " " << dt << "\n";

      //  if(dist<kappa)
      //break;
	  if(nrm(r, y0, rtol, atol)<1e-8)
	break;

	  if(niter>niter_max)
	    {
	      std::cout << "INIT FAIL\n";
	      iter_fail=true;
	      break;
	    }
	  z1+=0.5*delta;	 
	  //z1+=delta;	 
	}
      if(iter_fail)
	{
	  z1.setZero();
	  dt*=0.2;
	}
      else
	break;
    }
  */
  //  Eigen::Map<Eigen::VectorXd>(yval, n)=y0;
  Eigen::Map<Eigen::VectorXd>(ypval, n)=z0;
}

/*! \brief Print stats for IDA integration (from IDA examples)
 *  \param mem Pointer to IDA data structure */
static void PrintFinalStats(void *mem)
{
  int retval;
  long int nst, nni, nje, nre, nreLS, netf, ncfn, nge;

  retval = IDAGetNumSteps(mem, &nst);
  retval = IDAGetNumResEvals(mem, &nre);
  //  retval = IDADenseGetNumJacEvals(mem, &nje);
  retval = IDAGetNumNonlinSolvIters(mem, &nni);
  retval = IDAGetNumErrTestFails(mem, &netf);
  retval = IDAGetNumNonlinSolvConvFails(mem, &ncfn);
  //  retval = IDADenseGetNumResEvals(mem, &nreLS);
  retval = IDAGetNumGEvals(mem, &nge);
  
  printf("\nFinal Run Statistics: \n\n");
  printf("Number of steps                    = %ld\n", nst);
  printf("Number of residual evaluations     = %ld\n", nre);
  //  printf("Number of Jacobian evaluations     = %ld\n", nje);
  printf("Number of nonlinear iterations     = %ld\n", nni);
  printf("Number of error test failures      = %ld\n", netf);
  printf("Number of nonlinear conv. failures = %ld\n", ncfn);
  printf("Number of root fn. evaluations     = %ld\n", nge);
}



extern "C" {

/*! \brief C wrapper for IDA callback for residual function
 * Function signature prescribed by IDA
 * \param tres    time
 * \param yy      state of simulation
 * \param yp      d(yy)/dt
 * \param resval  residuals of differential equations (modified)
 * \param rdata   pointer to sim_st object containing simulation state */
int resrob(realtype tres, N_Vector yy, N_Vector yp, N_Vector resval, void *rdata)
{
  sim_st *ss;
  ss=static_cast<sim_st*>(rdata);
  int n=2*ss->Nv+4*ss->Nt+ss->Ne;
  Eigen::Map<Eigen::VectorXd>(NV_DATA_S(resval),n)=res_tri(Eigen::Map<Eigen::VectorXd>(NV_DATA_S(yy),n) ,Eigen::Map<Eigen::VectorXd>(NV_DATA_S(yp),n), ss->mu_eps, *(ss));
  return 0;

}

/*! C wrapper for IDA callback for Jacobian calculation
 * C wrapper for IDA callback for Jacobian calculation
 * Note that the function signature is prescribed by IDA, and that the
 * \param tt        time
 * \param yy        state of simulation
 * \param yp        d(yy)/dt
 * \param rr        residual 
 * \param cj        calculate d(res)/d(yy) + cj d(res)/d(yp)
 * \param user_data pointer to sim_st object containing simulation state
 * \param tmp1      temporary storage
 * \param tmp2      temporary storage
 * \param tmp3      temporary storage */
int psetup(realtype tt, N_Vector yy, N_Vector yp, N_Vector rr, 	 
	   realtype cj, void *user_data, N_Vector tmp1, N_Vector tmp2, 	 
	   N_Vector tmp3)
{
  // Basically, this is just a wrapper, called by IDA,
  // which calls the Jacobian calculation function  J_num
  sim_st* ss=static_cast<sim_st*>(user_data);
  const int n=2*ss->Nv+4*ss->Nt+ss->Ne;
  J_num(*(ss->J), Eigen::Map<Eigen::VectorXd>(NV_DATA_S(yy),n), Eigen::Map<Eigen::VectorXd>(NV_DATA_S(yp),n), 1.0, cj, *(ss));
  ss->J->lu_symb();
  return ss->J->lu_num();
}


/*! \brief C wrapper for IDA callback for Jacobian linear system solve
 * C wrapper for IDA callback for Jacobian calculation
 * Note that the function signature is prescribed by IDA
 * \param tt        time
 * \param yy        state of simulation
 * \param yp        d(yy)/dt
 * \param rr        residual
 * \param rvec      right-hand side
 * \param zvec      solution (output)
 * \param cj
 * \param delta
 * \param user_data pointer to sim_st object containing simulation state
 * \param tmp      temporary storage */
int psolve(realtype tt, N_Vector yy, N_Vector yp, N_Vector rr, 	 
	   N_Vector rvec, N_Vector zvec, realtype cj, realtype delta, 	 
	   void *user_data, N_Vector tmp)
{
  sim_st* ss=static_cast<sim_st*>(user_data);
  ss->J->lu_solve(NV_DATA_S(zvec), NV_DATA_S(rvec));
  return 0;
}
 	


}


/*! \brief Numerical simulation of mechanics using IDA
 * Simulation using IDA
 * See the IDA documentation for more details.
 * \param tend          Length of period to integrate over
 * \param dt0           Initial timestep to attempt
 * \param init          If true, do not use initial rate of change vector xp
 * \param cell_idx      Cell edge index array
 * \param cell_attribs  Cell attribute array
 * \param edge_idx      Edge vertex index array
 * \param edge_attribs  Edge attribute array
 * \param edge_vars     Edge variable array
 * \param quad_idx      Quad vertex index array
 * \param quad_attribs  Quad attribute array
 * \param angle_idx     Angular sping vertex index array
 * \param angle_attribs Angular sping vertex attribute array
 * \param fixed         Fixed vertex coordinate index array
 * \param verts         Vertex location array
 * \param xp            Rate of change of state variable array
 */ 

void evolve_ida(double tend, double dt0, double mu_eps, int Nc, int cell_idx[], double cell_attribs[], int Ne, int edge_idx[][2], double edge_attribs[][5], double edge_vars[], int Nt, int tri_idx[][3], double tri_attribs[][10], double tri_vars[][4], int Nf, int fixed[], int Nv, double verts[][2], double xp[], bool init)
{


  const int n=2*Nv+4*Nt+Ne;

  /* Initialise simulation state structure from arguments */
  sim_st ss;
  ss.mu_eps=mu_eps;
  ss.Nc=Nc;
  ss.cell_idx=cell_idx;
  ss.cell_attribs=cell_attribs; 
  ss.Ne=Ne; 
  ss.edge_idx=edge_idx; 
  ss.edge_attribs=edge_attribs;
  ss.Nt=Nt;
  ss.tri_idx=tri_idx;
  ss.tri_attribs=tri_attribs;
  ss.Nv=Nv;
  ss.Nf=Nf;
  ss.fixed=fixed;
  
  /* Calculate Jacobian sparsity and group columns */
  ss.J=new cscmat(n,n);
  JPat(*ss.J, ss);
  (ss.J)->group();
  
  void *mem;


  N_Vector yy, yp;
  realtype rtol, atol, *yval, *ypval;
  realtype t0, tout, tret;
  int retval;

  t0=0.0;

  mem = NULL;
  yy = yp = NULL;
  yval = ypval = NULL;

  tout=tend;
  tret=0.0;

  /* Allocate N-vectors. */
  yy = N_VNew_Serial(n);
  yp = N_VNew_Serial(n);
 
  /* Create and initialize  y */
  yval  = NV_DATA_S(yy);
  for(int i=0; i<Nv; ++i)
    {
      yval[2*i]=verts[i][0];
      yval[2*i+1]=verts[i][1];
    }
  for(int i=0; i<Nt; ++i)
    for(int j=0; j<4; ++j)
      yval[2*Nv+4*i+j]=tri_vars[i][j]; 
  for(int i=0; i<Ne; ++i)
    {
      yval[2*Nv+4*Nt+i]=edge_vars[i];
    }




  /* Create and initialize  y'  */
  /* if init is true, BE is used to calculate y', 
     otherwise the value passed to routine (from 
     a previous step) is used */
  ypval = NV_DATA_S(yp);
  if(1) {
  for(int i=0; i<n; ++i)
    {
      ypval[i]=0.0;
    }
  init_vals(yval, ypval, ss, dt0);
  } else {
  for(int i=0; i<2*Nv; ++i)
    {
      ypval[i]=xp[i];
    }
  }


  //  std::cout << ypval[0] << std::endl;


  /* A check to see that the residual at the start of the step is small */
  Eigen::VectorXd r(n);
  r=res_tri(Eigen::Map<Eigen::VectorXd>(yval,n),Eigen::Map<Eigen::VectorXd>(ypval,n),ss.mu_eps, ss);
  std::cout << "r_start: " << r.cwiseAbs().maxCoeff() <<"\n";
  // std::cout << std::endl;
  // std::cout << r <<std::endl;

  /* Set absolute and relative tolerances - these
     probably need to be not too small as excessive 
     accuracy is expensive, but also not too large so that
     the solution drifts off the manifold */
  rtol = RCONST(1.0e-8);
  atol = RCONST(1.0e-8);
 
 
  /* Call IDACreate and IDAMalloc to initialize IDA memory */
  mem = IDACreate();
  IDASetUserData(mem, static_cast<void*>(&ss)); 
  IDAInit(mem, resrob, t0, yy, yp);
  IDASStolerances(mem, rtol, atol);
  IDASetMaxNumSteps(mem, 10000); 
  /* Initialise dense solver */
  //IDADense(mem, n);
  /* Initialse GMRES solver */
  IDASpgmr(mem, 20);
  /* Set preconditioner */
  IDASpilsSetPreconditioner(mem, psetup, psolve);

  /* Advance solution using solver */

  retval=IDASolve(mem, tout, &tret, yy, yp, IDA_NORMAL);


  printf("IDASolve %d\n", retval);

  /* Update numpy arrays from IDA vectors */
  for(int i=0; i<Nv; ++i)
    {
      verts[i][0]=yval[2*i];
      verts[i][1]=yval[2*i+1];
    }
  for(int i=0; i<Nt; ++i)
    for(int j=0; j<4; ++j)
      tri_vars[i][j]=yval[2*Nv+4*i+j];
  for(int i=0; i<Ne; ++i)
    {
      edge_vars[i]=yval[2*Nv+4*Nt+i];
    }
  for(int i=0; i<2*Nv; ++i)
    {
      xp[i]=ypval[i];
    }
  /* Print summary of integration */
  PrintFinalStats(mem);

  /* Print residual at end of time step */
  r=res_tri(Eigen::Map<Eigen::VectorXd>(yval,n),Eigen::Map<Eigen::VectorXd>(ypval,n),ss.mu_eps, ss);
  std::cout << "r_end: " << r.cwiseAbs().maxCoeff() <<"\n";

  /* Free memory */
  IDAFree(&mem);
  N_VDestroy_Serial(yy);
  N_VDestroy_Serial(yp);

  //  delete ss.J;

}


void evolve_ida_python(boost::python::list& uniforms, boost::python::list& arrays)
{
  //  std::cerr << "evolvebepython" << std::endl;  
  double mu_eps = boost::python::extract<double>(uniforms[0]);
  double tend = boost::python::extract<double>(uniforms[1]);
  double dt0 = boost::python::extract<double>(uniforms[2]);
  int init = boost::python::extract<int>(uniforms[3]);
  boost::python::object cells = arrays[0];
  boost::python::object cell_attribs = arrays[1];
  boost::python::object cell_vars = arrays[2];
  boost::python::object edges = arrays[3];
  boost::python::object edge_attribs = arrays[4];
  boost::python::object edge_vars = arrays[5];
  boost::python::object tris = arrays[6];
  boost::python::object tri_attribs = arrays[7];
  boost::python::object tri_vars = arrays[8];
  boost::python::object vertex_attribs = arrays[9];
  boost::python::object vertex_vars = arrays[10];
  boost::python::object fixed = arrays[11];
  boost::python::object xp = arrays[12];
  evolve_ida(tend, dt0, mu_eps,
	    PyArray_DIM(cells.ptr(), 0)-1, 
	    static_cast<int*>(PyArray_DATA(cells.ptr())), 
	    static_cast<double(*)>(PyArray_DATA(cell_attribs.ptr())), 
	    PyArray_DIM(edges.ptr(),0), 
	    static_cast<int (*)[2]>(PyArray_DATA(edges.ptr())), 
	    static_cast<double(*)[5]>(PyArray_DATA(edge_attribs.ptr())),
	    static_cast<double*>(PyArray_DATA(edge_vars.ptr())),
	    PyArray_DIM(tris.ptr(),0), 
	    static_cast<int (*)[3]>(PyArray_DATA(tris.ptr())), 
	    static_cast<double(*)[10]>(PyArray_DATA(tri_attribs.ptr())),
	    static_cast<double(*)[4]>(PyArray_DATA(tri_vars.ptr())),
	    PyArray_DIM(fixed.ptr(),0),
	    static_cast<int*>(PyArray_DATA(fixed.ptr())),
	    PyArray_DIM(vertex_vars.ptr(),0),
	    static_cast<double (*)[2]>(PyArray_DATA(vertex_vars.ptr())),
	    static_cast<double*>(PyArray_DATA(xp.ptr())),
	                init); 
}




// boost::python stuff to expose functions to Python 
using namespace boost::python;
BOOST_PYTHON_MODULE(module_tri_growth)
{
  import_array();
  def("evolve_ida", evolve_ida_python);
}
