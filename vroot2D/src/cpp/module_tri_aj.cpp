

/*! \file module.cpp
 *  C++ Python extension for mechanical simulations */

// A define to allow large numbers of arguments in boost::python wrapped
// functions
#define BOOST_PYTHON_MAX_ARITY 20
//#define EIGEN_DONT_PARALLELIZE

//#include <google/profiler.h>

#include <Python.h>
#include <boost/python.hpp>
#include <numpy/arrayobject.h>

#include <iostream>
#include <cstdio>
#include <cmath>

// Includes for the differential-algebraic solver IDA from the
// SUNDIALS suite of codes
#include <ida/ida.h>
#include <ida/ida_spgmr.h>
// This include file is for the IDA dense linear algebra and Jacobian
// calculation by finite differences.
//#include <ida/ida_dense.h>
#include <nvector/nvector_serial.h>
#include <sundials/sundials_types.h>
#include <sundials/sundials_math.h>

// My (not very robust, and really simple) sparse matrix helper library
#include "sparse.h"

// Std OpenMP library
#include <omp.h>

// Import Eigen3 library
#include <eigen/Eigen/Core>
#include <eigen/Eigen/LU>

//! Typedef for simple 2d vector class
typedef Eigen::Vector2d Vec2;

//! Unused RAIL object for release of the Python GIL in multithreaded sections
class ScopedGILRelease {
public:
        inline ScopedGILRelease() { m_thread_state = PyEval_SaveThread(); }
        inline ~ScopedGILRelease() { PyEval_RestoreThread(m_thread_state); m_thread_state = NULL; }
private:
        PyThreadState* m_thread_state;
};




/*! \brief Rotate a vec2 by 90 degrees CCW
 * \param v a vector
 * \return v rotated by 90 degrees */
static inline Vec2 rot(const Vec2& v)
{
  return Vec2(v[1],-v[0]);
}


class dmat : public Eigen::Array<double, 6, 1>
{
public:
  typedef Eigen::Array<double, 6, 1> Base;
  dmat() : Base() {}

  dmat(const double& v) : Base(dmat::Ones()*v) {}

  dmat(const dmat& other) : Base(other) {}



  // This constructor allows you to construct MyVectorType from Eigen expressions                                                                   
  template<typename OtherDerived>
  dmat(const Eigen::ArrayBase<OtherDerived>& other)
    : Base(other)
  { }
  // This method allows you to assign Eigen expressions to MyVectorType                                                                             
  template<typename OtherDerived>
  dmat& operator= (const Eigen::ArrayBase <OtherDerived>& other)
  {
    this->Base::operator=(other);
    return *this;
  }
};


class dmat4 : public Eigen::Array<double, 4, 1>
{
public:
  typedef Eigen::Array<double, 4, 1> Base;
  dmat4() : Base() {}

  dmat4(const double& v) : Base(dmat4::Ones()*v) {}

  dmat4(const dmat4& other) : Base(other) {}



  // This constructor allows you to construct MyVectorType from Eigen expressions                                                                   
  template<typename OtherDerived>
  dmat4(const Eigen::ArrayBase<OtherDerived>& other)
    : Base(other)
  { }
  // This method allows you to assign Eigen expressions to MyVectorType                                                                             
  template<typename OtherDerived>
  dmat4& operator= (const Eigen::ArrayBase <OtherDerived>& other)
  {
    this->Base::operator=(other);
    return *this;
  }
};



Eigen::Matrix<dmat, 2, 1> rotD(const Eigen::Matrix<dmat, 2, 1>& v)
{
  Eigen::Matrix<dmat, 2, 1> r;
  r[0]=v[1];
  r[1]=-v[0];
return r;
}

/*! \brief dot(a,rot(b))
 * \param a first vector
 * \param b second vector
 * \return a[0]*b[1]-a[1]*b[0]; */
double det(const Vec2&a, const Vec2&b)
{
  return a[0]*b[1]-a[1]*b[0];
}

double max_diff(const cscmat& mat1, const cscmat& mat2)
{
  double max_diff = 0.0;
  for(int j=0; j!=mat1.n; ++j) {
    for(int k1=mat1.Ap[j]; k1!=mat1.Ap[j+1];++k1)
      {
	double cdiff = std::abs(mat1.Ax[k1]);
	for(int k2=mat2.Ap[j]; k2!=mat2.Ap[j+1];++k2)
	  if(mat1.Ai[k1]==mat2.Ai[k2])
	    cdiff = std::abs(mat1.Ax[k1]-mat2.Ax[k2]);
	if(cdiff>max_diff)
	  max_diff = cdiff;
	if(cdiff==std::abs(mat1.Ax[k1]) && std::abs(cdiff)>1e-15)
	  std::cout << "missing" << mat1.Ai[k1] << " " << j << " " << mat1.Ax[k1] << " " << max_diff << "\n";
      }
  }
  return max_diff;
}

//! \brief Print cscmat to stream
std::ostream& operator<<(std::ostream& os, const cscmat& mat)
{
  os << mat.m << " " << mat.n << "\n";
  os << mat.Ap.size() << " " << mat.Ai.size() << " " << mat.Ax.size() << "\n";
  // Loop over elements, column by column
  for(int j=0; j!=mat.n; ++j) {
    for(int k=mat.Ap[j]; k!=mat.Ap[j+1];++k)
      // Omit elements which are small
      if(std::fabs(mat.Ax[k])>1e-15)
	os << "(" << mat.Ai[k] << "," << j << ") : " << mat.Ax[k] << "\n";
  }
  return(os);
}

//! \brief Print cscmat to stream
std::ostream& operator<<(std::ostream& os, const coomat& mat)
{
  os << mat.m << " " << mat.n << "\n";
  os << mat.Ai.size() << " " << mat.Aj.size() << " " << mat.Ax.size() << "\n";
  // Loop over elements, column by column
  for(int k=0; k!=mat.Ai.size(); ++k) {
      // Omit elements which are small
      //   if(std::fabs(mat.Ax[k])>1e-15)
	os << "(" << mat.Ai[k] << "," << mat.Aj[k] << ") : " << mat.Ax[k] << "\n";
  }
  return(os);
}


//! \brief Print spmat_umf to stream 
std::ostream& operator<<(std::ostream& os, const spmat_umf& mat)
{
  os << mat.m << " " << mat.n << "\n";
  // Loop over elements, column by column
  for(int j=0; j!=mat.n; ++j) {
    for(int k=mat.Ap[j]; k!=mat.Ap[j+1];++k)
      // Omit elements which are small
      if(std::fabs(mat.Ax[k])>1e-15)
	os << "(" << mat.Ai[k] << "," << j << ") : " << mat.Ax[k] << "\n";
  }
  return(os);
}


//! \brief Structure to store simulation state
struct sim_st { 
  double mu_eps;
  //! Number of cells
  int Nc;
  //! Number of edges
  int Ne; 
  //! Number of triangles
  int Nt;
  //! Number of verticies
  int Nv; 
  //! Number of vertices with drag forces
  int Nf;

  //! Pointer to array 
  int *cell_idx;
  //! Pointer to array of cell attributes 
  //! (turgor pressure)
  double *cell_attribs; 
  //! Pointer to array of start and end vertex indices for each edge
  int (*edge_idx)[2]; 
  //! Pointer to array of cell attributes 
  //! (lambda, gamma, phiinv)
  double (*edge_attribs)[5];
  int (*tri_idx)[3];
  double (*tri_attribs)[16];
  //! Pointer to array of indices of vertex coordinates with drag
  //! x = [x_0, y_0, x_1, y_1, ... ]
  int *fixed;
  //! Current jacobian matrix
  cscmat* J;
  bool visc;
  //! Per-vertex drag
  double drag;
};



/*! \brief Calculation of Jacobian matrix sparsity pattern
 * \param J Jacobian matrix (modified)
 * \param ss Simulation state vector */
void JPat(cscmat& J, const sim_st& ss)
{
  coomat cJ(2*ss.Nv+ss.Ne, 2*ss.Nv+ss.Ne);


 

  // Loop over cells
  for(int i=0; i<ss.Nc; ++i)
    {
      // Loop over edges in cell
      for(int j=ss.cell_idx[i]; j<ss.cell_idx[i+1]; ++j)
	{

	  // Cell edges.
	  // Start position, end position and edge natural length
	  // may all depend on each other
	  const int i0=ss.edge_idx[j][0];
	  const int i1=ss.edge_idx[j][1];

	  cJ.push(2*i0,  2*i0,   1.0);
	  cJ.push(2*i0,  2*i0+1, 1.0);
	  cJ.push(2*i0,  2*i1,   1.0);
	  cJ.push(2*i0,  2*i1+1, 1.0);
	  cJ.push(2*i0,  2*ss.Nv+j, 1.0);

	  cJ.push(2*i0+1,  2*i0,   1.0);
	  cJ.push(2*i0+1,  2*i0+1, 1.0);
	  cJ.push(2*i0+1,  2*i1,   1.0);
	  cJ.push(2*i0+1,  2*i1+1, 1.0);
	  cJ.push(2*i0+1,  2*ss.Nv+j, 1.0);

	  cJ.push(2*i1,  2*i0,   1.0);
	  cJ.push(2*i1,  2*i0+1, 1.0);
	  cJ.push(2*i1,  2*i1,   1.0);
	  cJ.push(2*i1,  2*i1+1, 1.0);
	  cJ.push(2*i1,  2*ss.Nv+j, 1.0);

	  cJ.push(2*i1+1,  2*i0,   1.0);
	  cJ.push(2*i1+1,  2*i0+1, 1.0);
	  cJ.push(2*i1+1,  2*i1,   1.0);
	  cJ.push(2*i1+1,  2*i1+1, 1.0);
	  cJ.push(2*i1+1,  2*ss.Nv+j, 1.0);

	  cJ.push(2*ss.Nv+j,  2*i0,   1.0);
	  cJ.push(2*ss.Nv+j,  2*i0+1, 1.0);
	  cJ.push(2*ss.Nv+j,  2*i1,   1.0);
	  cJ.push(2*ss.Nv+j,  2*i1+1, 1.0);
	  cJ.push(2*ss.Nv+j,  2*ss.Nv+j, 1.0);

	}
    }
    // Loop over all quadrilaterals
  for(int i=0; i<ss.Nt; ++i)
    {
      // Triangles
      // The forces on each corner may depend on the position of
      // all the other corners
      const int i0=ss.tri_idx[i][0];
      const int i1=ss.tri_idx[i][1];
      const int i2=ss.tri_idx[i][2];

      cJ.push(2*i0,    2*i0,    1.0);
      cJ.push(2*i0,    2*i0+1,  1.0);
      cJ.push(2*i0,    2*i1,    1.0);
      cJ.push(2*i0,    2*i1+1,  1.0);
      cJ.push(2*i0,    2*i2,    1.0);
      cJ.push(2*i0,    2*i2+1,  1.0);

      cJ.push(2*i0+1,  2*i0,    1.0);
      cJ.push(2*i0+1,  2*i0+1,  1.0);
      cJ.push(2*i0+1,  2*i1,    1.0);
      cJ.push(2*i0+1,  2*i1+1,  1.0);
      cJ.push(2*i0+1,  2*i2,    1.0);
      cJ.push(2*i0+1,  2*i2+1,  1.0);

      cJ.push(2*i1,    2*i0,    1.0);
      cJ.push(2*i1,    2*i0+1,  1.0);
      cJ.push(2*i1,    2*i1,    1.0);
      cJ.push(2*i1,    2*i1+1,  1.0);
      cJ.push(2*i1,    2*i2,    1.0);
      cJ.push(2*i1,    2*i2+1,  1.0);

      cJ.push(2*i1+1,  2*i0,    1.0);
      cJ.push(2*i1+1,  2*i0+1,  1.0);
      cJ.push(2*i1+1,  2*i1,    1.0);
      cJ.push(2*i1+1,  2*i1+1,  1.0);
      cJ.push(2*i1+1,  2*i2,    1.0);
      cJ.push(2*i1+1,  2*i2+1,  1.0);

      cJ.push(2*i2,    2*i0,    1.0);
      cJ.push(2*i2,    2*i0+1,  1.0);
      cJ.push(2*i2,    2*i1,    1.0);
      cJ.push(2*i2,    2*i1+1,  1.0);
      cJ.push(2*i2,    2*i2,    1.0);
      cJ.push(2*i2,    2*i2+1,  1.0);

      cJ.push(2*i2+1,  2*i0,    1.0);
      cJ.push(2*i2+1,  2*i0+1,  1.0);
      cJ.push(2*i2+1,  2*i1,    1.0);
      cJ.push(2*i2+1,  2*i1+1,  1.0);
      cJ.push(2*i2+1,  2*i2,    1.0);
      cJ.push(2*i2+1,  2*i2+1,  1.0);

   }
  cJ.to_csc(J);
}

/*! \brief Residual of differential equation system
 * \param x Simulation state vector (vertex positions + edge natural lengths)
 * \param v Simulation velocity vector (v = dx/dt)
 * \param mu_eps Damping parameter
 * \param ss Simulation state structure */
Eigen::VectorXd res_tri(const Eigen::VectorXd& x, const Eigen::VectorXd& v, double mu_eps, const sim_st& ss)
{
  // Residual vector
  Eigen::VectorXd r(2*ss.Nv+ss.Ne);
  r.setZero();
  // Loop over all cells in cell_idx
  for(int i=0; i<ss.Nc; ++i)
  {
      // Cell pressure
      const double p=ss.cell_attribs[i];
      // Loop over the edges for this cell
      for(int j=ss.cell_idx[i]; j<ss.cell_idx[i+1]; ++j)
	{
	  // Cell edge forces
	  // Indices of start and end vertex
	  const int i0=ss.edge_idx[j][0];
	  const int i1=ss.edge_idx[j][1];
	  // Vector along edge
	  const Vec2 d=x.segment<2>(2*i1)-x.segment<2>(2*i0);
	  // Length of edge
	  const double l=d.norm();
	  // Pressure force
	  const Vec2 p_force=0.5*p*rot(d);
	  // Calculate the spring forces
	  // Spring strength, lambda
	  const double lambda=ss.edge_attribs[j][0];
	  // Spring natural length
	  const double l0=x[2*ss.Nv+j];

	  // Elastic tension in spring
	  const Vec2 t_force=lambda*(l-l0)*d/(l*l0);
	  // Edge viscous drag forces
	  const double phi_inv= ss.edge_attribs[j][2];
	  // Edge yield stress
	  const double tau = ss.edge_attribs[j][3];
	  // Edge yield shear rate
	  const double eps0 = ss.edge_attribs[j][4];


	  // Velocity difference between end-points
	  const Vec2 dv=v.segment<2>(2*i1)-v.segment<2>(2*i0);
	  // Drag forces from Lockhart
	  // Zero yield stress, assumed that wall is
	  // always under tension

	  // Strain rate in wall
	  const double eps = d.dot(dv)/(l*l);
	  
	  
	  // CHANGE WALL VISC HERE
	  //const Vec2 d_force=phi_inv/(l*l*l)*d.dot(dv)*d;

	  const double eta = abs(eps/eps0)>1e-50 ? tau/eps0*(1-exp(-eps0/eps))+phi_inv : tau/eps0 + phi_inv;
	  const Vec2 d_force=eta*eps*d/l;


	  // Assign forces eigen matrix normto start and end vertices in residual
	  // pressure + elastic tension + viscous tension
	  r.segment<2>(2*i0)+=p_force+t_force+d_force;
	  r.segment<2>(2*i1)+=p_force-t_force-d_force;
	  // Edge spring length relaxation
	  const double gamma=ss.edge_attribs[j][1];
	  // Relaxation of spring natural lengths
	  //	  r[2*ss.Nv+j]=gamma*(l-l0)-v[2*ss.Nv+j];
	  r[2*ss.Nv+j]=-v[2*ss.Nv+j];


	}
    }
  
  for(int i=0; i<ss.Nt; ++i)
  {
    
      const int i0=ss.tri_idx[i][0];
      const int i1=ss.tri_idx[i][1];
      const int i2=ss.tri_idx[i][2];

      const double lambda=ss.tri_attribs[i][0];
      const double mu1=ss.tri_attribs[i][1];
      const double mu2=ss.tri_attribs[i][2];
      const double mu3=ss.tri_attribs[i][3];
      const double tau = ss.tri_attribs[i][4];
      const double eps0 = ss.tri_attribs[i][5];
      Eigen::Map<Eigen::Vector2d> A0(&ss.tri_attribs[i][6]);
      Eigen::Map<Eigen::Vector2d> A1(&ss.tri_attribs[i][8]);
      Eigen::Map<Eigen::Vector2d> X0(&ss.tri_attribs[i][10]);
      Eigen::Map<Eigen::Vector2d> X1(&ss.tri_attribs[i][12]);
      Eigen::Map<Eigen::Vector2d> X2(&ss.tri_attribs[i][14]);
      // Elastic forces owing to each triangle

      const Eigen::VectorBlock<const Eigen::VectorXd, 2> x0= x.segment<2>(2*i0); 
      const Eigen::VectorBlock<const Eigen::VectorXd, 2> x1= x.segment<2>(2*i1); 
      const Eigen::VectorBlock<const Eigen::VectorXd, 2> x2= x.segment<2>(2*i2); 
      const Eigen::VectorBlock<const Eigen::VectorXd, 2> v0= v.segment<2>(2*i0); 
      const Eigen::VectorBlock<const Eigen::VectorXd, 2> v1= v.segment<2>(2*i1); 
      const Eigen::VectorBlock<const Eigen::VectorXd, 2> v2= v.segment<2>(2*i2); 
   
      Eigen::Matrix2d F0;
      F0 << X1-X0, X2-X0;
      Eigen::Matrix2d F0_inv;
      F0_inv=F0.inverse();
      Eigen::Matrix2d F1;
      F1 << x1 - x0, x2-x0;
      Eigen::Matrix2d F1_inv;
      F1_inv=F1.inverse();
      

      Vec2 a0 = F1 * F0_inv * A0;
      a0.normalize();
      Vec2 a1 = F1 * F0_inv * A1;
      a1.normalize();

      Eigen::Matrix<double, 2, 2> grad;
      grad << v1 - v0, v2 - v0;
      grad=grad*F1_inv;

      Eigen::Matrix2d E;
      E=0.5*(grad+grad.transpose());

      
      // Calculate the invariants of the rate of strain matrix;

      //      const double I1 = E.diagonal().sum();
      //      const double I2 = E.determinant();
      //    const double eps = E.squaredNorm();
      const double eps = E.norm();

      /*
      Eigen::Matrix2d E;
      E=0.5*(F.transpose()*F-Eigen::Matrix2d::Identity());
      Eigen::Matrix<double, 2, 2> T;
      T=F*(lambda*E.trace()*Eigen::Matrix2d::Identity()+2.0*lambda*E);
      */


      Eigen::Matrix2d aEa0 = a0*(E*a0).transpose();
      const double asr0 = a0.dot(E*a0);
      Eigen::Matrix2d aEa1 = a1*(E*a1).transpose();
      const double asr1 = a1.dot(E*a1);
      Eigen::Matrix2d T;

      /*
      // Key is to let the mus depend on I2.
      // Modified Bingham model
      // T. C. Papanastasiou, Flows of materials with yield, J. of Rheology, 31 (1987), pp. 384–404.
      // via A. M. Robertson Lecture Notes on Non-Newtonian Fluids

      const double eta = mu1 + tau_Y*(1-exp(-n*S))/S;
      
      */

      const double eta = abs(eps/eps0)>1e-50 ? mu1 + tau/eps0*(1-exp(-eps0/eps)) : mu1 + tau/eps0;
      


      T.noalias()=2*eta*E + mu2*(a0*a0.transpose()*asr0+a1*a1.transpose()*asr1) + mu3*(aEa0+aEa0.transpose()+aEa1+aEa1.transpose());
      
      r.segment<2>(2*i0)+=0.5*T*rot(x2-x1);
      r.segment<2>(2*i1)+=0.5*T*rot(x0-x2);
      r.segment<2>(2*i2)+=0.5*T*rot(x1-x0);
    }
  for(int i=0; i<ss.Nv; ++i)
    {
      // Velocity-dependent drag forces 
      r.segment<2>(2*i)+=-ss.mu_eps*v.segment<2>(2*i);
    }
  for(int i=0; i<ss.Nf; ++i)
    {
      // Fixed vertex components
      r.segment<2>(2*ss.fixed[i])=v.segment<2>(2*ss.fixed[i]);
    }

  return r;

}



/*! \brief Numerical calculation of Jacobian matrix
 *
 * Calculates J = \theta_x \partial r / \partial x +
 *                \theta_v \partial r / \partial v
 * by finite differences, where r(x,v) is the residual function
 *
 *
 * The columns of the Jacobian matrix are arranged into groups
 *
 * e.g. with the sparsity pattern
 *
 *       / 1 0 0 0 0 \ 
 *       | 1 1 0 0 0 |
 *       | 0 0 1 0 1 |
 *       | 0 0 1 0 0 |
 *       \ 0 0 0 1 0 /
 *
 * the column groups are { {0,2,3}, {1,4} }, as these columns can be 
 * calculated by finite differences simultaneously.
 * These groupings are stored in J.igrp[Ng+1] and J.jgrp[n] 
 * where the column indices for the m-th group are given by
 * J.jgrp[k] for  J.igrp[m]<=k<J.igrp[m+1]
 *
 * \param J  Jacobian matrix
 * \param x0 Simulation state vector (vertex positions + edge natural lengths
 * \param v0 Simulation velocity vector (v = dx/dt)
 * \param theta_x Coefficient of dr/dx
 * \param theta_v Coefficient of dr/dv
 * \param mu_eps Damping parameter
 * \param ss Simulation state structure */
void J_num(cscmat& J, const Eigen::VectorXd& x0, const Eigen::VectorXd& v0, double theta_x, double theta_v, const sim_st& ss)
{
  const double aeps=1e-8;
  const double reps=1e-7;

  int Ng;
  // Count number of column groups
  for(Ng=0; J.igrp[Ng]!=J.n; ++Ng)
      ;
  
  //   This calculation is parallelized using OpenMP
  //   set the environment variable  OMP_NUM_THREADS to 1 
  //   to force single-threaded computation
  //   export OMP_NUM_THREADS=1
  //   or compile without openmp options

  // This starts the parallel section of code (thread per processor) 
    #pragma omp parallel 
    {
    Eigen::VectorXd r2(J.m), r1(J.m);
    Eigen::VectorXd x(J.n), v(J.n);
  
    // Loop over each column group (distributed over the threads)
    #pragma omp for
    for(int g=0; g<Ng; ++g)
      //for(int g=0; J.igrp[g]!=J.n; ++g)
    {
      x=x0;
      v=v0;
      // Calculate location of first point (increase those members of
      // x which are within the column group by a "small" amount)
      for(int k=J.igrp[g]; k!=J.igrp[g+1]; ++k)
	{
	  x[J.jgrp[k]]=x0[J.jgrp[k]]+std::max(aeps,reps*fabs(x0[J.jgrp[k]]));
	}
      // Calculate residual at first point
      r1=res_tri(x, v, ss.mu_eps, ss);
      // Calculate location of second point (decrease those members of
      // x which are within the column group by a "small" amount)
      for(int k=J.igrp[g]; k!=J.igrp[g+1]; ++k)
	{
	  x[J.jgrp[k]]=x0[J.jgrp[k]]-std::max(aeps,reps*fabs(x0[J.jgrp[k]]));
	
	}
      // Calculate residual at second point
      r2=res_tri(x, v, ss.mu_eps, ss);
      // Calculate contribution to jacobian by centred finite differences
      for(int k=J.igrp[g]; k!=J.igrp[g+1]; ++k) 
	for(int l=J.Ap[J.jgrp[k]];l!=J.Ap[J.jgrp[k]+1];++l) 
	  J.Ax[l]=0.5*(r1[J.Ai[l]]-r2[J.Ai[l]])*
	    theta_x/std::max(aeps,reps*fabs(x0[J.jgrp[k]]));
    }

    // Now calculate dependence on velocity variables
    #pragma omp for
    //  for(int g=0; J.igrp[g]!=J.n; ++g)
    for(int g=0; g<Ng; ++g)
        {
      v=v0;
      x=x0;
      // Calculate velocity of first point (increase those members of
      // v which are within the column group by a "small" amount)
      for(int k=J.igrp[g]; k!=J.igrp[g+1]; ++k)
	{
	  v[J.jgrp[k]]=v0[J.jgrp[k]]+std::max(aeps,reps*fabs(v0[J.jgrp[k]]));
	}
      // Calculate residual at first point      
      r1=res_tri(x, v, ss.mu_eps, ss);
      // Calculate velocity of second point (decrease those members of
      // v which are within the column group by a "small" amount)
      for(int k=J.igrp[g]; k!=J.igrp[g+1]; ++k)
	{
	  v[J.jgrp[k]]=v0[J.jgrp[k]]-std::max(aeps,reps*fabs(v0[J.jgrp[k]]));
	}
      // Calculate residual at second point
      r2=res_tri(x, v, ss.mu_eps, ss);
      // Calculate contribution to jacobian by centred finite differences
      for(int k=J.igrp[g]; k!=J.igrp[g+1]; ++k) 
	for(int l=J.Ap[J.jgrp[k]];l!=J.Ap[J.jgrp[k]+1];++l) 
	  J.Ax[l]+=0.5*(r1[J.Ai[l]]-r2[J.Ai[l]])*
	    theta_v/std::max(aeps,reps*fabs(v0[J.jgrp[k]]));
    }
   
}
       
}


void J_exact(cscmat& J, const Eigen::VectorXd& x, const Eigen::VectorXd& v, double theta_x, double theta_v, const sim_st& ss)
{

  coomat cJ(2*ss.Nv+ss.Ne, 2*ss.Nv+ss.Ne);
  /* Analytical calculation of Jacobian matrix */
  // Residual vector
  for(int i=0; i<ss.Nc; ++i)
    {
      // Cell pressure
      const double p=ss.cell_attribs[i];
      // Loop over the edges for this cell
      for(int j=ss.cell_idx[i]; j<ss.cell_idx[i+1]; ++j)
	{
	  // Cell edge forces
	  // Indices of start and end vertex
	  const int i0=ss.edge_idx[j][0];
	  const int i1=ss.edge_idx[j][1];
	  // Vector along edge


	  //	  std::cout << " i01 :" << i0 << " " << i1 << "\n";
	  const Vec2 d=x.segment<2>(2*i1)-x.segment<2>(2*i0);
	  // Length of edge
	  const double l=d.norm();
	  // Pressure force
	  const Vec2 p_force=0.5*p*rot(d);
	  
	  Eigen::Matrix<dmat4, 2, 1> hp_force;
	  hp_force(0) << 0, -0.5*p, 0, 0.5*p;
	  hp_force(1) << 0.5*p, 0, -0.5*p, 0;
	  
	  // Calculate the spring forces
	  // Spring strength, lambda
	  const double lambda=ss.edge_attribs[j][0];
	  // Spring natural length
	  const double l0=x[2*ss.Nv+j];

	  // Elastic tension in spring

	  Eigen::Matrix<dmat4, 2, 1> ht_force;
	  double y = lambda*(1.0/l0-1.0/l);
	  
	  ht_force(0) << -y-lambda/pow(l,3)*pow(d(0),2), 
	    -lambda/pow(l,3)*d(0)*d(1),
	    y+lambda/pow(l,3)*pow(d(0),2), 
	    lambda/pow(l,3)*d(0)*d(1);
	  ht_force(1) << -lambda/pow(l,3)*(d(0)*d(1)),
	    -y-lambda/pow(l,3)*pow(d(1),2),
	    lambda/pow(l,3)*(d(0)*d(1)),
	    y+lambda/pow(l,3)*pow(d(1),2);


	  Vec2 jt_force = -lambda/l0/l0*d;

	  //	  std::cout << "d " << d.transpose() << "\n";

	  //	  std::cout << "lambda " << lambda << "\n";
	  //	  std::cout << "l, l0 " << l << " " << l0 << "\n";

	  //	  std::cout << "ht " << ht_force(0).transpose() << " \n" << ht_force(1).transpose() << std::endl;	    
	  //	  std::cout<< d(0) << " " << d(1) << " " << l << " " << y <<std::endl;

	  // Edge viscous drag forces
	  const double phi_inv=ss.edge_attribs[j][2];
	  // Edge yield stress
	  const double tau = ss.edge_attribs[j][3];
	  // Edge yield shear rate
	  const double eps0 = ss.edge_attribs[j][4];


	  // Velocity difference between end-points
	  const Vec2 dv=v.segment<2>(2*i1)-v.segment<2>(2*i0);
	  // Drag forces from Lockhart
	  // Zero yield stress, assumed that wall is
	  // always under tension

	  // Strain rate in wall
	  const double eps = d.dot(dv)/(l*l);
	  

	  dmat4 deps;
	  deps << -(d(0))/(l*l), -(d(1))/(l*l), 
	    (d(0))/(l*l), (d(1))/(l*l);


      	  // CHANGE WALL VISC HERE
	  //const Vec2 d_force=phi_inv/(l*l*l)*d.dot(dv)*d;
	  //	  const Vec2 d_force=eta*eps*d/l;

	  const double eta = abs(eps/eps0)>1e-50 ? tau/eps0*(1-exp(-eps0/eps))+phi_inv : tau/eps0 + phi_inv;
	  const double eta_p = abs(eps/eps0)>1e-50 ? -tau/eps/eps*exp(-eps0/eps) : 0;
	  
	  Eigen::Matrix<dmat4, 2, 1> dd_force = ((eta/l+eta_p*eps/l)*d).cast<dmat4>()*deps;

	  //	  std::cout << "d :" << d.transpose() << "\n";
	  //	  std::cout << "dv :" << dv.transpose() << "\n";
	  //	  std::cout << "l :" << l << "\n";


	  dmat4 heps;
	  heps << -dv(0)/l/l+2*eps/l/l*d(0), -dv(1)/l/l+2*eps/l/l*d(1), 
	    dv(0)/l/l-2*eps/l/l*d(0), dv(1)/l/l-2*eps/l/l*d(1);

	  //	  std::cout << "eps :" << eps << std::endl;
	  //	  std::cout << "heps :" << heps.transpose() << std::endl;

	  dmat4 hl;
	  hl << -(d(0))/l, -(d(1))/l, (d(0))/l, (d(1))/l;

	  //	  std::cout << "hl :" << heps.transpose() << std::endl;

	  Eigen::Matrix<dmat4, 2, 1> hd;
	  hd(0) << -1, 0, 1, 0;
	  hd(1) << 0, -1, 0, 1;
	  
	  //	  std::cout << "hd : " << hd(0).transpose() << "\n";
	  //	  std::cout << "     " << hd(1).transpose() << "\n";
	  
	  Eigen::Matrix<dmat4, 2, 1> hd_force = ((eta/l+eta_p*eps/l)*d).cast<dmat4>()*heps
	    + ((eta*eps)/l*dmat4::Ones())*hd-(eta*eps*d/l/l).cast<dmat4>()*hl;

	  
	  Eigen::Matrix<dmat4, 2, 1> hr0 = hp_force+ht_force+hd_force;
	  Eigen::Matrix<dmat4, 2, 1> hr1 = hp_force-ht_force-hd_force;
	  Eigen::Matrix<dmat4, 2, 1> dr0 = dd_force;
	  Eigen::Matrix<dmat4, 2, 1> dr1 = -dd_force;
	  
	  Vec2 jr0 = jt_force;
	  Vec2 jr1 = -jt_force;




	  cJ.push(2*i0,   2*i0,   theta_x*hr0(0)(0)+theta_v*dr0(0)(0));
	  cJ.push(2*i0,   2*i0+1, theta_x*hr0(0)(1)+theta_v*dr0(0)(1));
	  cJ.push(2*i0,   2*i1,   theta_x*hr0(0)(2)+theta_v*dr0(0)(2));
	  cJ.push(2*i0,   2*i1+1, theta_x*hr0(0)(3)+theta_v*dr0(0)(3));
	  cJ.push(2*i0,   2*ss.Nv+j, theta_x*jr0(0));
	  cJ.push(2*i0+1, 2*i0,   theta_x*hr0(1)(0)+theta_v*dr0(1)(0));
	  cJ.push(2*i0+1, 2*i0+1, theta_x*hr0(1)(1)+theta_v*dr0(1)(1));
	  cJ.push(2*i0+1, 2*i1,   theta_x*hr0(1)(2)+theta_v*dr0(1)(2));
	  cJ.push(2*i0+1, 2*i1+1, theta_x*hr0(1)(3)+theta_v*dr0(1)(3));
	  cJ.push(2*i0+1, 2*ss.Nv+j, theta_x*jr0(1));
	  cJ.push(2*i1,   2*i0,   theta_x*hr1(0)(0)+theta_v*dr1(0)(0));
	  cJ.push(2*i1,   2*i0+1, theta_x*hr1(0)(1)+theta_v*dr1(0)(1));
	  cJ.push(2*i1,   2*i1,   theta_x*hr1(0)(2)+theta_v*dr1(0)(2));
	  cJ.push(2*i1,   2*i1+1, theta_x*hr1(0)(3)+theta_v*dr1(0)(3));
	  cJ.push(2*i1,   2*ss.Nv+j, theta_x*jr1(0));
	  cJ.push(2*i1+1, 2*i0,   theta_x*hr1(1)(0)+theta_v*dr1(1)(0));
	  cJ.push(2*i1+1, 2*i0+1, theta_x*hr1(1)(1)+theta_v*dr1(1)(1));
	  cJ.push(2*i1+1, 2*i1,   theta_x*hr1(1)(2)+theta_v*dr1(1)(2));
	  cJ.push(2*i1+1, 2*i1+1, theta_x*hr1(1)(3)+theta_v*dr1(1)(3));
	  cJ.push(2*i1+1, 2*ss.Nv+j, theta_x*jr1(1));


	  // Assign forces eigen matrix normto start and end vertices in residual
	  // pressure + elastic tension + viscous tension
	  //	  r.segment<2>(2*i0)+=p_force+t_force+d_force;
	  //      r.segment<2>(2*i1)+=p_force-t_force-d_force;
	  // Edge spring length relaxation
	  const double gamma=ss.edge_attribs[j][1];
	  // Relaxation of spring natural lengths
	  //      r[2*ss.Nv+j]=gamma*(l-l0)-v[2*ss.Nv+j];

	  cJ.push(2*ss.Nv+j, 2*ss.Nv+j, -theta_v);

	}
    }


  for(int i=0; i<ss.Nt; ++i)
  {

      const int i0=ss.tri_idx[i][0];
      const int i1=ss.tri_idx[i][1];
      const int i2=ss.tri_idx[i][2];

      //      std::cout << "tri" << i0 << ", "<< i1 << ", "<<i2 << std::endl;

      const double lambda=ss.tri_attribs[i][0];
      const double mu1=ss.tri_attribs[i][1];
      const double mu2=ss.tri_attribs[i][2];
      const double mu3=ss.tri_attribs[i][3];
      const double tau=ss.tri_attribs[i][4];
      const double eps0 = ss.tri_attribs[i][5];
      Eigen::Map<Eigen::Vector2d> A0(&ss.tri_attribs[i][6]);
      Eigen::Map<Eigen::Vector2d> A1(&ss.tri_attribs[i][8]);
      Eigen::Map<Eigen::Vector2d> X0(&ss.tri_attribs[i][10]);
      Eigen::Map<Eigen::Vector2d> X1(&ss.tri_attribs[i][12]);
      Eigen::Map<Eigen::Vector2d> X2(&ss.tri_attribs[i][14]);
      // Elastic forces owing to each triangle

      const Eigen::VectorBlock<const Eigen::VectorXd, 2> x0= x.segment<2>(2*i0); 
      const Eigen::VectorBlock<const Eigen::VectorXd, 2> x1= x.segment<2>(2*i1); 
      const Eigen::VectorBlock<const Eigen::VectorXd, 2> x2= x.segment<2>(2*i2); 
      const Eigen::VectorBlock<const Eigen::VectorXd, 2> v0= v.segment<2>(2*i0); 
      const Eigen::VectorBlock<const Eigen::VectorXd, 2> v1= v.segment<2>(2*i1); 
      const Eigen::VectorBlock<const Eigen::VectorXd, 2> v2= v.segment<2>(2*i2); 
   
      Eigen::Matrix2d F0;
      F0 << X1-X0, X2-X0;
      Eigen::Matrix2d F0_inv;
      F0_inv=F0.inverse();
      
      Eigen::Matrix2d F1;
      F1 << x1 - x0, x2-x0;

      



      Eigen::Matrix<dmat, 2, 2> hF1;
      hF1(0,0) << -1, 0, 1, 0, 0, 0;
      hF1(0,1) << -1, 0, 0, 0, 1, 0;
      hF1(1,0) << 0, -1, 0, 1, 0, 0;
      hF1(1,1) << 0, -1, 0, 0, 0, 1;



      Eigen::Matrix2d F1_inv;
      F1_inv=F1.inverse();

      //      std::cout << "x012: " << x0.transpose() << " " << x1.transpose() << " " << x2.transpose() << "\n";
      
      Eigen::Matrix<dmat, 2, 2> hF1_inv;

      double J = F1(0,0)*F1(1,1)-F1(0,1)*F1(1,0);
      dmat hJ = hF1(0,0)*F1(1,1)+F1(0,0)*hF1(1,1)-
	        hF1(0,1)*F1(1,0)-F1(0,1)*hF1(1,0);
      hF1_inv(0,0) = 1/J*(hF1(1,1)-hJ/J*F1(1,1));
      hF1_inv(0,1) = 1/J*(-hF1(0,1)+hJ/J*F1(0,1));
      hF1_inv(1,0) = 1/J*(-hF1(1,0)+hJ/J*F1(1,0));
      hF1_inv(1,1) = 1/J*(hF1(0,0)-hJ/J*F1(0,0));

      //      std::cout << "J " << J << " " << hJ.transpose() << "\n";

      /*
      std :: cout << "hF1_inv" << "\n";
      std::cout << hF1_inv(0,0).transpose() <<"\n";
      std::cout <<  hF1_inv(0,1).transpose() << "\n";
      std::cout << hF1_inv(1,0).transpose() << "\n";
      std::cout << hF1_inv(1,1).transpose() << std::endl;
      */

      Vec2 a0 = F1 * F0_inv * A0;
      Eigen::Matrix<dmat, 2, 1> ha0 = hF1*((F0_inv*A0).cast<dmat>()*dmat::Ones());

      //std::cout << "F0_inv*A0" << (F0_inv*A0).transpose() << std::endl;

      // std::cout << "ha0" << ha0(0) << " " << ha0(1) << std::endl;

      //      

      //std::cout << "ha0 " << ha0(0) << " " << ha0(1) << std::endl;

      double la0 = a0.norm();
      //      Eigen::Matrix<dmat, 2, 1> ua0 = a0.cast<dmat>()*dmat::Ones();
      //      ha0 = ha0/la0+(ua0(0)*ha0(0)+ua0(1)*ha0(1))*ua0/(la0*la0*la0);
      ha0 = ha0/la0-(a0(0)*ha0(0)+a0(1)*ha0(1))*a0.cast<dmat>()/(la0*la0*la0);
      //ha0 = ha0/la0+a0.cast<dmat>().dot(ha0).array()*a0.cast<dmat>()/(la0*la0*la0);

      

      //      std::cout << "la0 " << la0 << std::endl;
      //std::cout << "a0 " << a0 << std::endl;
      //      std::cout << "ua0 " << ua0(0) << " " << ua0(1) << std::endl;
      //std::cout << "ha0_norm " << ha0(0) << " " << ha0(1) << std::endl;

      a0.normalize();

      Vec2 a1 = F1 * F0_inv * A1;
      Eigen::Matrix<dmat, 2, 1> ha1 = hF1* (F0_inv*A1).cast<dmat>();

      double la1 = a1.norm();
      //      Eigen::Matrix<dmat, 2, 1> ua1 = a1.cast<dmat>();
      ha1 = ha1/la1-(a1(0)*ha1(0)+a1(1)*ha1(1))*a1.cast<dmat>()/(la1*la1*la1);
      a1.normalize();


      Eigen::Matrix<double, 2, 2> grad;
      grad << v1 - v0, v2 - v0;
      grad=grad*F1_inv;

      Eigen::Matrix<dmat, 2, 2> hgrad;
      Eigen::Matrix<double, 2, 2> grad2;
      grad2 << v1 - v0, v2 - v0;
      hgrad = grad2.cast<dmat>()*hF1_inv;


      Eigen::Matrix<dmat, 2, 2> dgrad;

      dgrad(0,0) << -1, 0, 1, 0, 0, 0;
      dgrad(0,1) << -1, 0, 0, 0, 1, 0;
      dgrad(1,0) << 0, -1, 0, 1, 0, 0;
      dgrad(1,1) << 0, -1, 0, 0, 0, 1;
  
      dgrad = dgrad*F1_inv.cast<dmat>();

      //      std::cout << "dgrad" << dgrad(0,0) << " " << dgrad(0,1) << " " << dgrad(1,0) << " " << dgrad(1,1) << std::endl;

      Eigen::Matrix2d E;
      E=0.5*(grad+grad.transpose());

      Eigen::Matrix<dmat, 2, 2> dE;
      dE = 0.5*dmat::Ones()*(dgrad+dgrad.transpose());
      
      Eigen::Matrix<dmat, 2, 2> hE;
      hE = 0.5*dmat::Ones()*(hgrad+hgrad.transpose());

      // Calculate the invariants of the rate of strain matrix;

      //      const double I1 = E.diagonal().sum();
      //      const double I2 = E.determinant();
      const double eps = E.norm();
      const double eps2 = sqrt(E(0,0)*E(0,0) + E(0,1)*E(0,1) + E(1,0)*E(1,0) + E(1,1)*E(1,1));
      dmat deps = (E(0,0)*dE(0,0)+E(0,1)*dE(0,1)+E(1,0)*dE(1,0)+E(1,1)*dE(1,1))/eps2;
      dmat heps = (E(0,0)*hE(0,0)+E(0,1)*hE(0,1)+E(1,0)*hE(1,0)+E(1,1)*hE(1,1))/eps2;



      
      //Eigen::Matrix2d E;
      //E=0.5*(F.transpose()*F-Eigen::Matrix2d::Identity());
      //Eigen::Matrix<double, 2, 2> T;
      //T=F*(lambda*E.trace()*Eigen::Matrix2d::Identity()+2.0*lambda*E);



      Eigen::Matrix2d aEa0 = a0*(E*a0).transpose();

      const double asr0 = a0.dot(E*a0);
      Eigen::Matrix<dmat, 2, 1> ca0 = a0.cast<dmat>()*dmat::Ones();
      Eigen::Matrix<dmat, 2, 1> dprod0 = dE*ca0;
      dmat dasr0 = ca0(0)*dprod0(0)+ca0(1)*dprod0(1);
      Eigen::Matrix<dmat, 2, 2> daEa0 = ca0*dprod0.transpose();
      Eigen::Matrix<dmat, 2, 1> hprod0 = hE*ca0+E.cast<dmat>()*ha0;
      Vec2 prod0 = E*a0;
      dmat hasr0 = ca0(0)*hprod0(0)+ca0(1)*hprod0(1) + ha0(0)*prod0(0) + ha0(1)*prod0(1);
      Eigen::Matrix<dmat, 2, 2> haEa0 = ca0*hprod0.transpose() + ha0*prod0.transpose().cast<dmat>();

      //      std::cout << "haEa0" << haEa0(0,0).transpose() << "\n";
      //      std::cout << "haEa0" << haEa0(0,1).transpose() << "\n";
      //      std::cout << "haEa0" << haEa0(1,0).transpose() << "\n";
      //      std::cout << "haEa0" << haEa0(1,1).transpose() << "\n";


      Eigen::Matrix2d aEa1 = a1*(E*a1).transpose();
      const double asr1 = a1.dot(E*a1);
      Eigen::Matrix<dmat, 2, 1> ca1 = a1.cast<dmat>()*dmat::Ones();
      Eigen::Matrix<dmat, 2, 1> dprod1 = dE*ca1;
      dmat dasr1 = ca1(0)*dprod1(0)+ca1(1)*dprod1(1);
      Eigen::Matrix<dmat, 2, 2> daEa1 = ca1*dprod1.transpose();
      Eigen::Matrix<dmat, 2, 1> hprod1 = hE*ca1+E.cast<dmat>()*ha1;
      Vec2 prod1 = E*a1;
      dmat hasr1 = ca1(0)*hprod1(0)+ca1(1)*hprod1(1) + ha1(0)*prod1(0) + ha1(1)*prod1(1);
      Eigen::Matrix<dmat, 2, 2> haEa1 = ca1*hprod1.transpose() + ha1*prod1.transpose().cast<dmat>();

      Eigen::Matrix2d T;


      // Key is to let the mus depend on I2.
      // Modified Bingham model
      // T. C. Papanastasiou, Flows of materials with yield, J. of Rheology, 31 (1987), pp. 384–404.
      // via A. M. Robertson Lecture Notes on Non-Newtonian Fluids

      //      const double eta = mu1 + tau_Y*(1-exp(-n*S))/S;
      
      

      const double eta = abs(eps/eps0)>1e-6 ? mu1 + tau/eps0*(1-exp(-eps0/eps)) : mu1 + tau/eps0;

      dmat deta;
      if(abs(eps/eps0)>1e-6)
	deta =  -(dmat::Ones()*tau/eps/eps*exp(-eps0/eps))*deps;
      else
	deta.setZero();

      dmat heta;
      if(abs(eps/eps0)>1e-6)
	heta = -(dmat::Ones()*tau/eps/eps*exp(-eps0/eps))*heps;
      else
	heta.setZero();

      //        std::cout << "heta" << heta << std::endl;
      //        std::cout << "eta" << eta << std::endl;

      T.noalias()=2*eta*E + mu2*(a0*a0.transpose()*asr0+a1*a1.transpose()*asr1) + mu3*(aEa0+aEa0.transpose()+aEa1+aEa1.transpose());

      Eigen::Matrix<dmat, 2, 2> dT =  (2*eta*dmat::Ones())*dE + 2*deta*E.cast<dmat>() + mu2*dmat::Ones()*((a0*a0.transpose()).cast<dmat>()*dasr0+(a1*a1.transpose()).cast<dmat>()*dasr1) + mu3*dmat::Ones()*(daEa0+daEa0.transpose()+daEa1+daEa1.transpose());


      Eigen::Matrix<dmat, 2, 2> hT =  (2*eta*dmat::Ones())*hE + 2*heta*E.cast<dmat>() +
	mu2*dmat::Ones()*((a0*a0.transpose()).cast<dmat>()*hasr0+(ca0*ha0.transpose()+ha0*ca0.transpose())*(asr0*dmat::Ones()) +
			  (a1*a1.transpose()).cast<dmat>()*hasr1+(ca1*ha1.transpose()+ha1*ca1.transpose())*(asr1*dmat::Ones()))
	+ mu3*dmat::Ones()*(haEa0+haEa0.transpose()+haEa1+haEa1.transpose());


      //      Eigen::Matrix<dmat, 2, 2> hT =  (2*eta*dmat::Ones())*hE;

      //   std::cout << "ha0" << ha0(0) << " " << ha0(1) << std::endl;
      //   std::cout << "heta" << heta << std::endl;
      //   std::cout << "hasr0" << hasr0 << std::endl;
      //   std::cout << "haEa0" << haEa0(0) << " " << haEa0(1) << std::endl;


      //      std::cout << "hE" << hE(0,0) << " " << hE(0,1) << " " << hE(1,0) << " " << hE(1,1) << std::endl;


      //      std::cout << "hT" << hT(0,0) << " " << hT(0,1) << " " << hT(1,0) << " " << hT(1,1) << std::endl;

      //      std::cout << "dT" << dT(0,0) << " " << dT(0,1) << " " << dT(1,0) << " " << dT(1,1) << std::endl;
      

      Eigen::Matrix<dmat, 2, 1> dr0 = (0.5*dmat::Ones())*dT*(rot(x2-x1).cast<dmat>());
      Eigen::Matrix<dmat, 2, 1> dr1 = (0.5*dmat::Ones())*dT*(rot(x0-x2).cast<dmat>());
      Eigen::Matrix<dmat, 2, 1> dr2 = (0.5*dmat::Ones())*dT*(rot(x1-x0).cast<dmat>());

   
      Eigen::Matrix<dmat, 2, 1> hx0;
      hx0(0) << 1, 0, 0, 0, 0, 0;
      hx0(1) << 0, 1, 0, 0, 0, 0;
      Eigen::Matrix<dmat, 2, 1> hx1;
      hx1(0) << 0, 0, 1, 0, 0, 0;
      hx1(1) << 0, 0, 0, 1, 0, 0;
      Eigen::Matrix<dmat, 2, 1> hx2;
      hx2(0) << 0, 0, 0, 0, 1, 0;
      hx2(1) << 0, 0, 0, 0, 0, 1;



      Eigen::Matrix<dmat, 2, 1> hr0 = 0.5*dmat::Ones()*(hT*(rot(x2-x1).cast<dmat>())+T.cast<dmat>()*rotD(hx2-hx1));

      Eigen::Matrix<dmat, 2, 1> hr1 = 0.5*dmat::Ones()*(hT*(rot(x0-x2).cast<dmat>())+T.cast<dmat>()*rotD(hx0-hx2));
      Eigen::Matrix<dmat, 2, 1> hr2 = 0.5*dmat::Ones()*(hT*(rot(x1-x0).cast<dmat>())+T.cast<dmat>()*rotD(hx1-hx0));

      //      std::cout << "hr0" << hr0(0) << " " << hr0(1) << std::endl;
      //      std::cout << "hr1" << hr1(0) << " " << hr1(1) << std::endl;
      //      std::cout << "hr2" << hr2(0) << " " << hr2(1) << std::endl;
      
	  cJ.push(2*i0,   2*i0,   theta_x*hr0(0)(0)+theta_v*dr0(0)(0));
	  cJ.push(2*i0,   2*i0+1, theta_x*hr0(0)(1)+theta_v*dr0(0)(1));
	  cJ.push(2*i0,   2*i1,   theta_x*hr0(0)(2)+theta_v*dr0(0)(2));
	  cJ.push(2*i0,   2*i1+1, theta_x*hr0(0)(3)+theta_v*dr0(0)(3));
	  cJ.push(2*i0,   2*i2,   theta_x*hr0(0)(4)+theta_v*dr0(0)(4));
	  cJ.push(2*i0,   2*i2+1, theta_x*hr0(0)(5)+theta_v*dr0(0)(5));

	  cJ.push(2*i0+1, 2*i0,   theta_x*hr0(1)(0)+theta_v*dr0(1)(0));
	  cJ.push(2*i0+1, 2*i0+1, theta_x*hr0(1)(1)+theta_v*dr0(1)(1));
	  cJ.push(2*i0+1, 2*i1,   theta_x*hr0(1)(2)+theta_v*dr0(1)(2));
	  cJ.push(2*i0+1, 2*i1+1, theta_x*hr0(1)(3)+theta_v*dr0(1)(3));
	  cJ.push(2*i0+1, 2*i2,   theta_x*hr0(1)(4)+theta_v*dr0(1)(4));
	  cJ.push(2*i0+1, 2*i2+1, theta_x*hr0(1)(5)+theta_v*dr0(1)(5));

	  cJ.push(2*i1,   2*i0,   theta_x*hr1(0)(0)+theta_v*dr1(0)(0));
	  cJ.push(2*i1,   2*i0+1, theta_x*hr1(0)(1)+theta_v*dr1(0)(1));
	  cJ.push(2*i1,   2*i1,   theta_x*hr1(0)(2)+theta_v*dr1(0)(2));
	  cJ.push(2*i1,   2*i1+1, theta_x*hr1(0)(3)+theta_v*dr1(0)(3));
	  cJ.push(2*i1,   2*i2,   theta_x*hr1(0)(4)+theta_v*dr1(0)(4));
	  cJ.push(2*i1,   2*i2+1, theta_x*hr1(0)(5)+theta_v*dr1(0)(5));

	  cJ.push(2*i1+1, 2*i0,   theta_x*hr1(1)(0)+theta_v*dr1(1)(0));
	  cJ.push(2*i1+1, 2*i0+1, theta_x*hr1(1)(1)+theta_v*dr1(1)(1));
	  cJ.push(2*i1+1, 2*i1,   theta_x*hr1(1)(2)+theta_v*dr1(1)(2));
	  cJ.push(2*i1+1, 2*i1+1, theta_x*hr1(1)(3)+theta_v*dr1(1)(3));
	  cJ.push(2*i1+1, 2*i2,   theta_x*hr1(1)(4)+theta_v*dr1(1)(4));
	  cJ.push(2*i1+1, 2*i2+1, theta_x*hr1(1)(5)+theta_v*dr1(1)(5));


	  cJ.push(2*i2 ,  2*i0,   theta_x*hr2(0)(0)+theta_v*dr2(0)(0));
	  cJ.push(2*i2,   2*i0+1, theta_x*hr2(0)(1)+theta_v*dr2(0)(1));
	  cJ.push(2*i2,   2*i1,   theta_x*hr2(0)(2)+theta_v*dr2(0)(2));
	  cJ.push(2*i2,   2*i1+1, theta_x*hr2(0)(3)+theta_v*dr2(0)(3));
	  cJ.push(2*i2,   2*i2,   theta_x*hr2(0)(4)+theta_v*dr2(0)(4));
	  cJ.push(2*i2,   2*i2+1, theta_x*hr2(0)(5)+theta_v*dr2(0)(5));

	  cJ.push(2*i2+1, 2*i0,   theta_x*hr2(1)(0)+theta_v*dr2(1)(0));
	  cJ.push(2*i2+1, 2*i0+1, theta_x*hr2(1)(1)+theta_v*dr2(1)(1));
	  cJ.push(2*i2+1, 2*i1,   theta_x*hr2(1)(2)+theta_v*dr2(1)(2));
	  cJ.push(2*i2+1, 2*i1+1, theta_x*hr2(1)(3)+theta_v*dr2(1)(3));
	  cJ.push(2*i2+1, 2*i2,   theta_x*hr2(1)(4)+theta_v*dr2(1)(4));
	  cJ.push(2*i2+1, 2*i2+1, theta_x*hr2(1)(5)+theta_v*dr2(1)(5));


    }

  for(int i=0; i<ss.Nv; ++i)
    {
      // Velocity-dependent drag forces 
      cJ.push(2*i,   2*i,   -theta_v*ss.mu_eps);
      cJ.push(2*i+1, 2*i+1, -theta_v*ss.mu_eps);
    }
  // Zero out elements that are fixed
  for(int i=0; i<ss.Nf; ++i)
    {
      // Fixed vertex components
      for(int k=0; k<cJ.Aj.size(); k++)
	{
	  if(cJ.Ai[k]==2*ss.fixed[i] || cJ.Ai[k]==2*ss.fixed[i]+1)
	    cJ.Ax[k]=0.0;
	}
      cJ.push(2*ss.fixed[i], 2*ss.fixed[i], theta_v);
      cJ.push(2*ss.fixed[i]+1, 2*ss.fixed[i]+1, theta_v);
    }

  //  std::cout << cJ << "\n";
  J.rw();

  cJ.to_csc(J);




}


/*! \brief Norm for error tolerance (in NR convergence) for BE
 * (TODO) Explain this norm
 * \param w Vector
 * \param y Current simulation state vector
 * \param rtol Relative error tolerance
 * \param atol Absolute error tolerance
 */

inline double nrm(const Eigen::VectorXd& w, const Eigen::VectorXd& y, const double rtol, const double atol)
{
  return (w.cwiseAbs().cwiseQuotient(rtol*y.cwiseAbs()+atol*Eigen::VectorXd::Ones(y.size()))).maxCoeff();
}

/*! \brief Evolve mechanical state of simulation using backwards Euler
 * Simulation using Backwards Euler
 * \param tend          Length of period to integrate over
 * \param dt0           Initial timestep to attempt
 * \param init          If true, do not use initial rate of change vector xp
 * \param cell_idx      Cell edge index array
 * \param cell_attribs  Cell attribute array
 * \param edge_idx      Edge vertex index array
 * \param edge_attribs  Edge attribute array
 * \param edge_vars     Edge variable array
 * \param tri_idx      Tri vertex index array
 * \param tri_attribs  Tri attribute array
 * \param fixed         Fixed vertex coordinate index array
 * \param verts         Vertex location array
 * \param xp            Rate of change of state variable array
 */ 
void evolve_be(double tend, double dt0, double mu_eps, int Nc, int cell_idx[], double cell_attribs[], int Ne, int edge_idx[][2], double edge_attribs[][5], double edge_vars[],  int Nt, int tri_idx[][3], double tri_attribs[][16], int Nf, int fixed[], int Nv, double verts[][2], double xp[], bool init)
{

  //  ProfilerStart("test.prof");
  //  std::cerr << "evolvebe" << std::endl;
  //  std::cout << "Nt: " << Nt << std::endl;

  // Absolute error tolerance
  const double atol=1e-4;
  // Relative error tolerance
  const double rtol=0.0;
  // Maximum number of iterations in the NR to try before
  // step is reattempted with smaller timestep
  const int niter_max=100;
  // Tolerance for convergence of NR iteration
  const double kappa=0.1;
 
  const int Nr=150;
  // Total number of unknowns
  const int n=2*Nv+Ne;

  // Initialise simulation state structure from arguments 
  // See definition of struct sim_st for details
  sim_st ss;
  ss.mu_eps=mu_eps;
  ss.Nc=Nc;
  ss.cell_idx=cell_idx;
  ss.cell_attribs=cell_attribs; 
  ss.Ne=Ne; 
  ss.edge_idx=edge_idx; 
  ss.edge_attribs=edge_attribs;
  ss.Nt=Nt;
  ss.tri_idx=tri_idx;
  ss.tri_attribs=tri_attribs;
  ss.Nv=Nv;
  ss.fixed=fixed;
  ss.Nf=Nf;
  ss.visc=init;

  // Calculate Jacobian pattern, and group columns for numerical
  // evaluation of J using finite differences
  //  cscmat J2(2*Nv+Ne, 2*Nv+Ne);
  cscmat J(2*Nv+Ne, 2*Nv+Ne);
  //  JPat(J2, ss);
  //  J2.group();
  // std::cerr << "calc_pat" << std::endl;
  // std::cerr << "array_size" << 400*Nv << std::endl;

  spmat_umf M(2*Nv, 2*Nv, 400*Nv);
  // std::cerr << "alloc M" << std::endl;

  // Structure to store statistics about integration
  // Note that this is 
  struct
  {
    int steps;
    int jacs;
    int iters;
    int itfails;
    int jacfails;
    int accyfails;
    int extendstep;
  } stats;
  // Zero this structure
  stats.steps=0;
  stats.jacs=0;
  stats.iters=0;
  stats.itfails=0;
  stats.jacfails=0;
  stats.accyfails=0;
  stats.extendstep=0;
  

  // New timestep length
  double dt_new;

  // Time from start of timestep
  double t=0;
  // Current timestep
  double dt=dt0;

  // Iteration counter for NR
  int      niter;
  // 
  double   dist, err;

  // Flag as to whether first step; could be used to treat this differently
  bool first_step=true;

  Eigen::VectorXd y0(n), y1(n), z0(n), z1(n), r(n), delta(n), f(n), v(n);
  y0.segment(0,2*Nv)=Eigen::Matrix<double, 1, Eigen::Dynamic>::Map(verts[0],2*Nv);
  y0.segment(2*Nv,Ne)=Eigen::Matrix<double, 1, Eigen::Dynamic>::Map(edge_vars,Ne);
  if(init)
    z0.setZero();
  else
    z0=Eigen::Map<Eigen::VectorXd>(xp,n);

  //  res_tri(y0,z0,ss.mu_eps,ss);

  std::cout << "J num" << std::endl;
  //  J_num(J2, y0, z0, 0.0, -1.0, ss);
 



  //  J_num(J2, y0, z0, 0.0, -1.0, ss);  
  J_exact(J, y0, z0, 0.0, -1.0, ss);
  std::cerr << "J.lu_symb" << std::endl;


  J.lu_symb();
  if(J.lu_num()==1)
    std::cout << "*\n*\n*\n*\nInit J singular\n*\n*\n*\n*\n";

  // Flag for whether the jacobian needs updating; either for change in
  // stepsize or convergence failure of NR method (note that currently code
  // uses full NR method
  bool needs_jac=true;
  // Whether the jacobian J was calculated at a previous time-step
  // (for modified NR, not used currently)
  bool jac_current=false;
  // Flag set if NR iteration failed
  bool iter_fail;
  // Loop until the end of the calculation period
  while(t<tend) 
    {
      std::cout << "t " << t << " dt " << dt << std::endl;
      /*
      if(needs_jac)
	{
	  // Calculate a new Jacobian matrix by finite differences
	  stats.jacs++;
	  J_num(J, y0, z0, -dt, -1.0, ss);
	  needs_jac=false;
	  jac_current=true;
	  if(J.lu_num()==1)
	  {
	    std::cout << J << "\n";
	  }
      	}
      */
      niter=0;
      iter_fail=false;
      z1=z0;
      while(1)
	{
	  niter++;
	  stats.iters++;
	  y1=y0+dt*z1;

	  r=res_tri(y1,z1,ss.mu_eps,ss);

	  /*
	  if(niter==2) {
	     J_exact(J, y1, z1, -1.0, -0.0, ss);

	     J_num(J2, y1, z1, -1.0, -0.0, ss);
	     std::cout << "Jx*" << ss.Nv << std::endl;
	  //  J_num(J2, y0, z0, 0.0, -1.0, ss);
	     double e1 = max_diff(J,J2);
	     std::cout<<"*";
	     double e2 = max_diff(J2,J);

	     std::cout << e1 << " " << e2 << std::endl;


	     J_exact(J, y1, z1, -0.0, -1.0, ss);

	     J_num(J2, y1, z1, -0.0, -1.0, ss);
	  	  std::cout << "Ju*" << std::endl;
	  //  J_num(J2, y0, z0, 0.0, -1.0, ss);
	     e1 = max_diff(J,J2);
	     std::cout<<"*";
	     e2 = max_diff(J2,J);

	     std::cout << e1 << " " << e2 << std::endl;
	  

	  }
	  */



	  J_exact(J, y1, z1, -dt, -1.0, ss);

	  //	  J_num(J2, y1, z1, -dt, -1.0, ss);	  

	  needs_jac=false;
	  jac_current=true;
	  stats.jacs++;
	  J.lu_num();
	  J.lu_solve(delta.data(), r.data());
	  std::cout << delta[0] << " z1: " << z1[0]+delta[0] << "\n";
	 
	  dist=nrm(delta, y0, rtol, atol);

	  std::cout << niter << " " << r.cwiseAbs().maxCoeff() << " " << delta.cwiseAbs().maxCoeff() << " " << dist << " " << kappa << " " << nrm(r, y0, rtol, atol) << "\n";

	  if(dist>1e14 or std::isnan(dist))
	    {
	      iter_fail=true;
	      break;
	    }

	  if(dist<kappa)
	    break;

	  z1+=delta;	 
	  
	  if(niter>niter_max)
	    {
	      iter_fail=true;
	      break;
	    }
	}
      std::cout << niter << " " << r.cwiseAbs().maxCoeff() << " " << delta.cwiseAbs().maxCoeff() << " "<< delta.cwiseAbs().sum() << " " << dist << " " << kappa << "\n";
      dt_new=dt;
      if(iter_fail)
	{
      	  if(jac_current)
	    {
	      stats.itfails++;
	      dt_new=0.5*dt;
	      z0=z0;
	      dt=dt_new;
	      needs_jac=true;
	      continue;
	    } 
	  else
	    {
	      stats.jacfails++;
	      needs_jac=true;
	      continue;
	    }
	}
      if(niter<niter_max/10 && dt<dt0)
	{
	  dt_new=1.3*dt;
	  needs_jac=true;
	}
      if(first_step) first_step=false;
      t+=dt;
      stats.steps++;
      
      y0=y1;
      z0=z1;

      dt=dt_new;
      jac_current=false;
    }


  // Print information about the integration over this time-step
  std::cout << "jacs: " << stats.jacs << " steps: "<< stats.steps << std::endl;
  std::cout << "itfails: " << stats.itfails << " jacfails: "<< stats.jacfails <<  std::endl;
  std::cout << "accyfails: " << stats.accyfails << " stepextendss: " << stats.extendstep << std::endl;

  // Update numpy matrix containing vertex positions
  Eigen::Matrix<double, 1, Eigen::Dynamic >::Map(verts[0],2*Nv)=y0.segment(0,2*Nv);
  // Update numpy matrix containing edge natural lengths
  Eigen::Matrix<double, 1, Eigen::Dynamic >::Map(edge_vars,Ne)=y0.segment(2*Nv,Ne);
  // Update numpy matrix containing current velocity
  // (To help restart integration, particularly if IDA is used for
  //    following timestep
  Eigen::Map<Eigen::VectorXd>(xp,n)=z0;
  //  ProfilerStop();
}



/*! \brief Calculate initial values for IDA using BE
 *  \param yval  simulation state vector
 *  \param ypval rate of change of simulation state vector (output)
 *  \param ss    simulation state structure
 *  \param dt0   initial timestep
 */
void init_vals(double *yval, double* ypval, const sim_st& ss, double dt0)
{
  const int n=2*ss.Nv+ss.Ne;
  cscmat J(n, n);
  JPat(J, ss);
  J.group();

  int niter;
  const int niter_max=100;
  bool iter_fail;

  double dt=dt0;
  double dist;
  const double rtol=0.0;
  const double atol=1e-3;
  const double kappa=0.1;


  Eigen::VectorXd y0(n), y1(n), z0(n), z1(n), r(n), delta(n);
  
  y0=Eigen::Map<Eigen::VectorXd>(yval, n); 
  //  z0=Eigen::Map<Eigen::VectorXd>(ypval, n);
  z0.setZero();

  niter=0;
  z1=z0;

  for(int k=0; k<3; ++k)
    {
      iter_fail=false;
      while(1)
	{
	  niter++;
	  y1=y0+dt*z1;
	  
	  r=res_tri(y1, z1, ss.mu_eps, ss);
	  J_num(J, y1, z1, -dt, -1.0, ss);
	  J.lu_symb();
	  J.lu_num();
	  J.lu_solve(delta.data(), r.data());
      
	  dist=nrm(delta, y0, rtol, atol);
	  
	  std::cout << niter << " " << r.cwiseAbs().maxCoeff() << " " << delta.cwiseAbs().maxCoeff() << " " << dist << " " << kappa << " " << nrm(r, y0, rtol, atol) << "\n";

      //  if(dist<kappa)
      //break;
	  if(nrm(r, y0, rtol, atol)<1e-8)
	break;

	  if(niter>niter_max)
	    {
	      std::cout << "INIT FAIL\n";
	      iter_fail=true;
	      break;
	    }
	  z1+=0.5*delta;	 
	}
      if(iter_fail)
	{
	  z1.setZero();
	  dt*=0.2;
	}
      else
	break;
    }

  Eigen::Map<Eigen::VectorXd>(yval, n)=y1;
  Eigen::Map<Eigen::VectorXd>(ypval, n)=z1;
}

/*! \brief Print stats for IDA integration (from IDA examples)
 *  \param mem Pointer to IDA data structure */
static void PrintFinalStats(void *mem)
{
  int retval;
  long int nst, nni, nje, nre, nreLS, netf, ncfn, nge;

  retval = IDAGetNumSteps(mem, &nst);
  retval = IDAGetNumResEvals(mem, &nre);
  //  retval = IDADenseGetNumJacEvals(mem, &nje);
  retval = IDAGetNumNonlinSolvIters(mem, &nni);
  retval = IDAGetNumErrTestFails(mem, &netf);
  retval = IDAGetNumNonlinSolvConvFails(mem, &ncfn);
  //  retval = IDADenseGetNumResEvals(mem, &nreLS);
  retval = IDAGetNumGEvals(mem, &nge);
  
  printf("\nFinal Run Statistics: \n\n");
  printf("Number of steps                    = %ld\n", nst);
  printf("Number of residual evaluations     = %ld\n", nre);
  //  printf("Number of Jacobian evaluations     = %ld\n", nje);
  printf("Number of nonlinear iterations     = %ld\n", nni);
  printf("Number of error test failures      = %ld\n", netf);
  printf("Number of nonlinear conv. failures = %ld\n", ncfn);
  printf("Number of root fn. evaluations     = %ld\n", nge);
}



extern "C" {

/*! \brief C wrapper for IDA callback for residual function
 * Function signature prescribed by IDA
 * \param tres    time
 * \param yy      state of simulation
 * \param yp      d(yy)/dt
 * \param resval  residuals of differential equations (modified)
 * \param rdata   pointer to sim_st object containing simulation state */
int resrob(realtype tres, N_Vector yy, N_Vector yp, N_Vector resval, void *rdata)
{
  sim_st *ss;
  ss=static_cast<sim_st*>(rdata);
  int n=2*ss->Nv+ss->Ne;
  Eigen::Map<Eigen::VectorXd>(NV_DATA_S(resval),n)=res_tri(Eigen::Map<Eigen::VectorXd>(NV_DATA_S(yy),n) ,Eigen::Map<Eigen::VectorXd>(NV_DATA_S(yp),n), ss->mu_eps, *(ss));
  return 0;

}

/*! C wrapper for IDA callback for Jacobian calculation
 * C wrapper for IDA callback for Jacobian calculation
 * Note that the function signature is prescribed by IDA, and that the
 * \param tt        time
 * \param yy        state of simulation
 * \param yp        d(yy)/dt
 * \param rr        residual 
 * \param cj        calculate d(res)/d(yy) + cj d(res)/d(yp)
 * \param user_data pointer to sim_st object containing simulation state
 * \param tmp1      temporary storage
 * \param tmp2      temporary storage
 * \param tmp3      temporary storage */
int psetup(realtype tt, N_Vector yy, N_Vector yp, N_Vector rr, 	 
	   realtype cj, void *user_data, N_Vector tmp1, N_Vector tmp2, 	 
	   N_Vector tmp3)
{
  // Basically, this is just a wrapper, called by IDA,
  // which calls the Jacobian calculation function  J_num
  sim_st* ss=static_cast<sim_st*>(user_data);
  const int n=2*ss->Nv+ss->Ne;
  J_exact(*(ss->J), Eigen::Map<Eigen::VectorXd>(NV_DATA_S(yy),n), Eigen::Map<Eigen::VectorXd>(NV_DATA_S(yp),n), 1.0, cj, *(ss));
  ss->J->lu_symb();
  return ss->J->lu_num();
}


/*! \brief C wrapper for IDA callback for Jacobian linear system solve
 * C wrapper for IDA callback for Jacobian calculation
 * Note that the function signature is prescribed by IDA
 * \param tt        time
 * \param yy        state of simulation
 * \param yp        d(yy)/dt
 * \param rr        residual
 * \param rvec      right-hand side
 * \param zvec      solution (output)
 * \param cj
 * \param delta
 * \param user_data pointer to sim_st object containing simulation state
 * \param tmp      temporary storage */
int psolve(realtype tt, N_Vector yy, N_Vector yp, N_Vector rr, 	 
	   N_Vector rvec, N_Vector zvec, realtype cj, realtype delta, 	 
	   void *user_data, N_Vector tmp)
{
  sim_st* ss=static_cast<sim_st*>(user_data);
  ss->J->lu_solve(NV_DATA_S(zvec), NV_DATA_S(rvec));
  return 0;
}
 	


}


/*! \brief Numerical simulation of mechanics using IDA
 * Simulation using IDA
 * See the IDA documentation for more details.
 * \param tend          Length of period to integrate over
 * \param dt0           Initial timestep to attempt
 * \param init          If true, do not use initial rate of change vector xp
 * \param cell_idx      Cell edge index array
 * \param cell_attribs  Cell attribute array
 * \param edge_idx      Edge vertex index array
 * \param edge_attribs  Edge attribute array
 * \param edge_vars     Edge variable array
 * \param quad_idx      Quad vertex index array
 * \param quad_attribs  Quad attribute array
 * \param angle_idx     Angular sping vertex index array
 * \param angle_attribs Angular sping vertex attribute array
 * \param fixed         Fixed vertex coordinate index array
 * \param verts         Vertex location array
 * \param xp            Rate of change of state variable array
 */ 

void evolve_ida(double tend, double dt0, double mu_eps, int Nc, int cell_idx[], double cell_attribs[], int Ne, int edge_idx[][2], double edge_attribs[][5], double edge_vars[], int Nt, int tri_idx[][3], double tri_attribs[][16], int Nf, int fixed[], int Nv, double verts[][2], double xp[], bool init)
{


  const int n=2*Nv+Ne;

  /* Initialise simulation state structure from arguments */
  sim_st ss;
  ss.mu_eps=mu_eps;
  ss.Nc=Nc;
  ss.cell_idx=cell_idx;
  ss.cell_attribs=cell_attribs; 
  ss.Ne=Ne; 
  ss.edge_idx=edge_idx; 
  ss.edge_attribs=edge_attribs;
  ss.Nt=Nt;
  ss.tri_idx=tri_idx;
  ss.tri_attribs=tri_attribs;
  ss.Nv=Nv;
  ss.Nf=Nf;
  ss.fixed=fixed;
  
  /* Calculate Jacobian sparsity and group columns */
  ss.J=new cscmat(n,n);
  //  JPat(*ss.J, ss);
  //  (ss.J)->group();
  void *mem;


  N_Vector yy, yp;
  realtype rtol, atol, *yval, *ypval;
  realtype t0, tout, tret;
  int retval;

  t0=0.0;

  mem = NULL;
  yy = yp = NULL;
  yval = ypval = NULL;

  tout=tend;
  tret=0.0;

  /* Allocate N-vectors. */
  yy = N_VNew_Serial(n);
  yp = N_VNew_Serial(n);
 
  /* Create and initialize  y */
  yval  = NV_DATA_S(yy);
  for(int i=0; i<Nv; ++i)
    {
      yval[2*i]=verts[i][0];
      yval[2*i+1]=verts[i][1];
    }
  for(int i=0; i<Ne; ++i)
    {
      yval[2*Nv+i]=edge_vars[i];
    }

  /* Create and initialize  y'  */
  /* if init is true, BE is used to calculate y', 
     otherwise the value passed to routine (from 
     a previous step) is used */
  ypval = NV_DATA_S(yp);
  if(init) {
  for(int i=0; i<2*Nv+Ne; ++i)
    {
      ypval[i]=0.0;
    }
  init_vals(yval, ypval, ss, dt0);
  } else {
  for(int i=0; i<2*Nv+Ne; ++i)
    {
      ypval[i]=xp[i];
    }
  }


  //  std::cout << ypval[0] << std::endl;


  /* A check to see that the residual at the start of the step is small */
  Eigen::VectorXd r(n);
  r=res_tri(Eigen::Map<Eigen::VectorXd>(yval,n),Eigen::Map<Eigen::VectorXd>(ypval,n),ss.mu_eps, ss);
  std::cout << "r_start: " << r.cwiseAbs().maxCoeff() <<"\n";


  /* Set absolute and relative tolerances - these
     probably need to be not too small as excessive 
     accuracy is expensive, but also not too large so that
     the solution drifts off the manifold */
  rtol = RCONST(1.0e-8);
  atol = RCONST(1.0e-8);
 
 
  /* Call IDACreate and IDAMalloc to initialize IDA memory */
  mem = IDACreate();
  IDASetUserData(mem, static_cast<void*>(&ss)); 
  IDAInit(mem, resrob, t0, yy, yp);
  IDASStolerances(mem, rtol, atol);
  IDASetMaxNumSteps(mem, 10000); 
  /* Initialise dense solver */
  //IDADense(mem, n);
  /* Initialse GMRES solver */
  IDASpgmr(mem, 20);
  /* Set preconditioner */
  IDASpilsSetPreconditioner(mem, psetup, psolve);

  /* Advance solution using solver */

  retval=IDASolve(mem, tout, &tret, yy, yp, IDA_NORMAL);


  printf("IDASolve %d\n", retval);

  /* Update numpy arrays from IDA vectors */
  for(int i=0; i<Nv; ++i)
    {
      verts[i][0]=yval[2*i];
      verts[i][1]=yval[2*i+1];
    }
  for(int i=0; i<Ne; ++i)
    {
      edge_vars[i]=yval[2*Nv+i];
    }

  for(int i=0; i<n; ++i)
    {
      xp[i]=ypval[i];
    }

  /* Print summary of integration */
  PrintFinalStats(mem);

  /* Print residual at end of time step */
  r=res_tri(Eigen::Map<Eigen::VectorXd>(yval,n),Eigen::Map<Eigen::VectorXd>(ypval,n),ss.mu_eps, ss);
  std::cout << "r_end: " << r.cwiseAbs().maxCoeff() <<"\n";

  /* Free memory */
  IDAFree(&mem);
  N_VDestroy_Serial(yy);
  N_VDestroy_Serial(yp);

  delete ss.J;

}




/*! \brief Python wrapper for simulation using Backwards Euler
 * Python wrapper for simulation using Backwards Euler
 *   All numpy arrays must be of the type shown, contiguous and column major
 *   (e.g. the 3x3 matrix a is stored as a11, a21, s31, a12, a22, a32, 
 *   a13, a23, a33 ; this is the normal storage order for numpy arrays)
 *   See numpy.ascontiguousarray() and numpy.asfortranarray()
 *   Note that C/C++ arrays are stored in transpose (row-major) order
 *

 *
 * \param uniforms Python list of floats
 * - mu_eps Per-vertex drag
 * - tend  Length of period to integrate over
 * - dt0   Initial timestep to attempt
 * - init  If true, do not use initial rate of change vector xp
 *
 * \param arrays Python list
 * - cells Numpy array (Nc+1 elements, one-dimensional, dtype=int32) 
 *   indicating which elements in the edges array correspond to which cells:
 *   cell i being { edge[j] for  cell[i] <= j <= cell[i+1]-1 } 
 * - cell_attribs  Numpy array (Nc elements, one-dimensional, 
 *   dtype=float64) of cell attributes (turgor pressures)
 * - cell_vars Numpy array of cell variables (not used)
 * - edges Numpy array (2 by Ne, dtype=int32) each column being 
 *   the indices of the vertices at the start and end of each edge
 * - edge_attribs Numpy array (3 by Ne, dtype=float64);
 *   each column c being the attributes of an edge
 *     c[0] - lambda, spring constant
 *     c[1] - gamma,  spring natural length relaxation rate
 *     c[2] - phiinv, edge viscosity / inverse of extensibility
 * - edge_vars Numpy array (Ne elements, one-dimensional, 
 *   dtype=float64) of edge spring natural lengths. Updated at the end
 *   point of the integration.
 * - tris Numpy array (3 by Nq, dtype=int32); each column being
 *   the indices of the vertices at the four corners of the each 
 *   triangular element, in counter-clockwise order.
 * - tri_attribs Numpy array (7 by Nq, dtype=float64);
 *   each column t being the properties of one triangular element:
 *     t[0]          - lambda, elastic constant
 *     t[1] to t[6] - (X[0],Y[0],X[1],Y[1],X[2],Y[2])
 *                     locations of triangular vertices in the initial
 *                     (reference) configuration.
 * - tri_vars Numpy array of variables on triangles (not used)
 * - fixed Numpy array (one-dimensional, Nf in length, dtype=int32) 
 *   indicating the vertex coordinates on which drag is applied 
 *   (to prevent solid-body rotation and translation of the whole tissue). 
 *   Values correspond to indices in verts, when verts is converted to a 
 *   one-dimensional array [x0,y0,x1,y1,..]
 * - xp Numpy array (one-dimensional, 2*Nv+Ne in length, dtype=float64)
 *   of the rate of change of the state vector. Used as first guess for z
 *   if init is False. Modified on output to be z for the last simulation 
 *   step. */

void evolve_be_python(boost::python::list& uniforms, boost::python::list& arrays)
{
  //  std::cerr << "evolvebepython" << std::endl;  
  double mu_eps = boost::python::extract<double>(uniforms[0]);
  double tend = boost::python::extract<double>(uniforms[1]);
  double dt0 = boost::python::extract<double>(uniforms[2]);
  int init = boost::python::extract<int>(uniforms[3]);
  boost::python::object cells = arrays[0];
  boost::python::object cell_attribs = arrays[1];
  boost::python::object cell_vars = arrays[2];
  boost::python::object edges = arrays[3];
  boost::python::object edge_attribs = arrays[4];
  boost::python::object edge_vars = arrays[5];
  boost::python::object tris = arrays[6];
  boost::python::object tri_attribs = arrays[7];
  boost::python::object tri_vars = arrays[8];
  boost::python::object vertex_attribs = arrays[9];
  boost::python::object vertex_vars = arrays[10];
  boost::python::object fixed = arrays[11];
  boost::python::object xp = arrays[12];
  evolve_be(tend, dt0, mu_eps,
	    PyArray_DIM(cells.ptr(), 0)-1, 
	    static_cast<int*>(PyArray_DATA(cells.ptr())), 
	    static_cast<double(*)>(PyArray_DATA(cell_attribs.ptr())), 
	    PyArray_DIM(edges.ptr(),0), 
	    static_cast<int (*)[2]>(PyArray_DATA(edges.ptr())), 
	    static_cast<double(*)[5]>(PyArray_DATA(edge_attribs.ptr())),
	    static_cast<double*>(PyArray_DATA(edge_vars.ptr())),
	    PyArray_DIM(tris.ptr(),0), 
	    static_cast<int (*)[3]>(PyArray_DATA(tris.ptr())), 
	    static_cast<double(*)[16]>(PyArray_DATA(tri_attribs.ptr())),
	    PyArray_DIM(fixed.ptr(),0),
	    static_cast<int*>(PyArray_DATA(fixed.ptr())),
	    PyArray_DIM(vertex_vars.ptr(),0),
	    static_cast<double (*)[2]>(PyArray_DATA(vertex_vars.ptr())),
	    static_cast<double*>(PyArray_DATA(xp.ptr())),
	                init); 
}


void evolve_ida_python(boost::python::list& uniforms, boost::python::list& arrays)
{
  //  std::cerr << "evolvebepython" << std::endl;  
  double mu_eps = boost::python::extract<double>(uniforms[0]);
  double tend = boost::python::extract<double>(uniforms[1]);
  double dt0 = boost::python::extract<double>(uniforms[2]);
  int init = boost::python::extract<int>(uniforms[3]);
  boost::python::object cells = arrays[0];
  boost::python::object cell_attribs = arrays[1];
  boost::python::object cell_vars = arrays[2];
  boost::python::object edges = arrays[3];
  boost::python::object edge_attribs = arrays[4];
  boost::python::object edge_vars = arrays[5];
  boost::python::object tris = arrays[6];
  boost::python::object tri_attribs = arrays[7];
  boost::python::object tri_vars = arrays[8];
  boost::python::object vertex_attribs = arrays[9];
  boost::python::object vertex_vars = arrays[10];
  boost::python::object fixed = arrays[11];
  boost::python::object xp = arrays[12];
  evolve_ida(tend, dt0, mu_eps,
	    PyArray_DIM(cells.ptr(), 0)-1, 
	    static_cast<int*>(PyArray_DATA(cells.ptr())), 
	    static_cast<double(*)>(PyArray_DATA(cell_attribs.ptr())), 
	    PyArray_DIM(edges.ptr(),0), 
	    static_cast<int (*)[2]>(PyArray_DATA(edges.ptr())), 
	    static_cast<double(*)[5]>(PyArray_DATA(edge_attribs.ptr())),
	    static_cast<double*>(PyArray_DATA(edge_vars.ptr())),
	    PyArray_DIM(tris.ptr(),0), 
	    static_cast<int (*)[3]>(PyArray_DATA(tris.ptr())), 
	    static_cast<double(*)[16]>(PyArray_DATA(tri_attribs.ptr())),
	    PyArray_DIM(fixed.ptr(),0),
	    static_cast<int*>(PyArray_DATA(fixed.ptr())),
	    PyArray_DIM(vertex_vars.ptr(),0),
	    static_cast<double (*)[2]>(PyArray_DATA(vertex_vars.ptr())),
	    static_cast<double*>(PyArray_DATA(xp.ptr())),
	                init); 
}




// boost::python stuff to expose functions to Python 
using namespace boost::python;
BOOST_PYTHON_MODULE(module_tri_aj)
{
  import_array();
  def("evolve_be", evolve_be_python);
  def("evolve_ida", evolve_ida_python);
}
