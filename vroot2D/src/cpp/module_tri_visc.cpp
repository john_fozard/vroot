

/*! \file module.cpp
 *  C++ Python extension for mechanical simulations */

// A define to allow large numbers of arguments in boost::python wrapped
// functions
#define BOOST_PYTHON_MAX_ARITY 20
//#define EIGEN_DONT_PARALLELIZE

#include <Python.h>
#include <boost/python.hpp>
#include <numpy/arrayobject.h>

#include <iostream>
#include <cstdio>
#include <cmath>

// Includes for the differential-algebraic solver IDA from the
// SUNDIALS suite of codes
#include <ida/ida.h>
#include <ida/ida_spgmr.h>
// This include file is for the IDA dense linear algebra and Jacobian
// calculation by finite differences.
//#include <ida/ida_dense.h>
#include <nvector/nvector_serial.h>
#include <sundials/sundials_types.h>
#include <sundials/sundials_math.h>

// My (not very robust, and really simple) sparse matrix helper library
#include "sparse.h"

// Std OpenMP library
#include <omp.h>

// Import Eigen3 library
#include <eigen/Eigen/Core>
#include <eigen/Eigen/LU>

//! Typedef for simple 2d vector class
typedef Eigen::Vector2d Vec2;

//! Unused RAIL object for release of the Python GIL in multithreaded sections
class ScopedGILRelease {
public:
        inline ScopedGILRelease() { m_thread_state = PyEval_SaveThread(); }
        inline ~ScopedGILRelease() { PyEval_RestoreThread(m_thread_state); m_thread_state = NULL; }
private:
        PyThreadState* m_thread_state;
};

/*! \brief Rotate a vec2 by 90 degrees CCW
 * \param v a vector
 * \return v rotated by 90 degrees */
static inline Vec2 rot(const Vec2& v)
{
  return Vec2(v[1],-v[0]);
}

/*! \brief dot(a,rot(b))
 * \param a first vector
 * \param b second vector
 * \return a[0]*b[1]-a[1]*b[0]; */
double det(const Vec2&a, const Vec2&b)
{
  return a[0]*b[1]-a[1]*b[0];
}

//! \brief Print cscmat to stream
std::ostream& operator<<(std::ostream& os, const cscmat& mat)
{
  os << mat.m << " " << mat.n << "\n";
  os << mat.Ap.size() << " " << mat.Ai.size() << " " << mat.Ax.size() << "\n";
  // Loop over elements, column by column
  for(int j=0; j!=mat.n; ++j) {
    for(int k=mat.Ap[j]; k!=mat.Ap[j+1];++k)
      // Omit elements which are small
      if(std::fabs(mat.Ax[k])>1e-15)
	os << "(" << mat.Ai[k] << "," << j << ") : " << mat.Ax[k] << "\n";
  }
  return(os);
}

//! \brief Print spmat_umf to stream 
std::ostream& operator<<(std::ostream& os, const spmat_umf& mat)
{
  os << mat.m << " " << mat.n << "\n";
  // Loop over elements, column by column
  for(int j=0; j!=mat.n; ++j) {
    for(int k=mat.Ap[j]; k!=mat.Ap[j+1];++k)
      // Omit elements which are small
      if(std::fabs(mat.Ax[k])>1e-15)
	os << "(" << mat.Ai[k] << "," << j << ") : " << mat.Ax[k] << "\n";
  }
  return(os);
}


//! \brief Structure to store simulation state
struct sim_st { 
  double mu_eps;
  //! Number of cells
  int Nc;
  //! Number of edges
  int Ne; 
  //! Number of triangles
  int Nt;
  //! Number of verticies
  int Nv; 
  //! Number of vertices with drag forces
  int Nf;

  //! Pointer to array 
  int *cell_idx;
  //! Pointer to array of cell attributes 
  //! (turgor pressure)
  double *cell_attribs; 
  //! Pointer to array of start and end vertex indices for each edge
  int (*edge_idx)[2]; 
  //! Pointer to array of cell attributes 
  //! (lambda, gamma, phiinv)
  double (*edge_attribs)[3];
  int (*tri_idx)[3];
  double (*tri_attribs)[12];
  //! Pointer to array of indices of vertex coordinates with drag
  //! x = [x_0, y_0, x_1, y_1, ... ]
  int *fixed;
  //! Current jacobian matrix
  cscmat* J;
  bool visc;
  //! Per-vertex drag
  double drag;
};


/*! \brief Calculation of Jacobian matrix sparsity pattern
 * \param J Jacobian matrix (modified)
 * \param ss Simulation state vector */
void JPat(cscmat& J, const sim_st& ss)
{
  coomat cJ(2*ss.Nv+ss.Ne, 2*ss.Nv+ss.Ne);

  for(int i=0; i<2*ss.Nv+ss.Ne; ++i)
    cJ.push(i, i, 1.0);

  // Loop over cells
  for(int i=0; i<ss.Nc; ++i)
    {
      // Loop over edges in cell
      for(int j=ss.cell_idx[i]; j<ss.cell_idx[i+1]; ++j)
	{

	  // Cell edges.
	  // Start position, end position and edge natural length
	  // may all depend on each other
	  const int i0=ss.edge_idx[j][0];
	  const int i1=ss.edge_idx[j][1];

	  cJ.push(2*i0,  2*i0,   1.0);
	  cJ.push(2*i0,  2*i0+1, 1.0);
	  cJ.push(2*i0,  2*i1,   1.0);
	  cJ.push(2*i0,  2*i1+1, 1.0);
	  cJ.push(2*i0,  2*ss.Nv+j, 1.0);

	  cJ.push(2*i0+1,  2*i0,   1.0);
	  cJ.push(2*i0+1,  2*i0+1, 1.0);
	  cJ.push(2*i0+1,  2*i1,   1.0);
	  cJ.push(2*i0+1,  2*i1+1, 1.0);
	  cJ.push(2*i0+1,  2*ss.Nv+j, 1.0);

	  cJ.push(2*i1,  2*i0,   1.0);
	  cJ.push(2*i1,  2*i0+1, 1.0);
	  cJ.push(2*i1,  2*i1,   1.0);
	  cJ.push(2*i1,  2*i1+1, 1.0);
	  cJ.push(2*i1,  2*ss.Nv+j, 1.0);

	  cJ.push(2*i1+1,  2*i0,   1.0);
	  cJ.push(2*i1+1,  2*i0+1, 1.0);
	  cJ.push(2*i1+1,  2*i1,   1.0);
	  cJ.push(2*i1+1,  2*i1+1, 1.0);
	  cJ.push(2*i1+1,  2*ss.Nv+j, 1.0);

	  cJ.push(2*ss.Nv+j,  2*i0,   1.0);
	  cJ.push(2*ss.Nv+j,  2*i0+1, 1.0);
	  cJ.push(2*ss.Nv+j,  2*i1,   1.0);
	  cJ.push(2*ss.Nv+j,  2*i1+1, 1.0);
	  cJ.push(2*ss.Nv+j,  2*ss.Nv+j, 1.0);

	}
    }
    // Loop over all quadrilaterals
  for(int i=0; i<ss.Nt; ++i)
    {
      // Triangles
      // The forces on each corner may depend on the position of
      // all the other corners
      const int i0=ss.tri_idx[i][0];
      const int i1=ss.tri_idx[i][1];
      const int i2=ss.tri_idx[i][2];

      cJ.push(2*i0,    2*i0,    1.0);
      cJ.push(2*i0,    2*i0+1,  1.0);
      cJ.push(2*i0,    2*i1,    1.0);
      cJ.push(2*i0,    2*i1+1,  1.0);
      cJ.push(2*i0,    2*i2,    1.0);
      cJ.push(2*i0,    2*i2+1,  1.0);

      cJ.push(2*i0+1,  2*i0,    1.0);
      cJ.push(2*i0+1,  2*i0+1,  1.0);
      cJ.push(2*i0+1,  2*i1,    1.0);
      cJ.push(2*i0+1,  2*i1+1,  1.0);
      cJ.push(2*i0+1,  2*i2,    1.0);
      cJ.push(2*i0+1,  2*i2+1,  1.0);

      cJ.push(2*i1,    2*i0,    1.0);
      cJ.push(2*i1,    2*i0+1,  1.0);
      cJ.push(2*i1,    2*i1,    1.0);
      cJ.push(2*i1,    2*i1+1,  1.0);
      cJ.push(2*i1,    2*i2,    1.0);
      cJ.push(2*i1,    2*i2+1,  1.0);

      cJ.push(2*i1+1,  2*i0,    1.0);
      cJ.push(2*i1+1,  2*i0+1,  1.0);
      cJ.push(2*i1+1,  2*i1,    1.0);
      cJ.push(2*i1+1,  2*i1+1,  1.0);
      cJ.push(2*i1+1,  2*i2,    1.0);
      cJ.push(2*i1+1,  2*i2+1,  1.0);

      cJ.push(2*i2,    2*i0,    1.0);
      cJ.push(2*i2,    2*i0+1,  1.0);
      cJ.push(2*i2,    2*i1,    1.0);
      cJ.push(2*i2,    2*i1+1,  1.0);
      cJ.push(2*i2,    2*i2,    1.0);
      cJ.push(2*i2,    2*i2+1,  1.0);

      cJ.push(2*i2+1,  2*i0,    1.0);
      cJ.push(2*i2+1,  2*i0+1,  1.0);
      cJ.push(2*i2+1,  2*i1,    1.0);
      cJ.push(2*i2+1,  2*i1+1,  1.0);
      cJ.push(2*i2+1,  2*i2,    1.0);
      cJ.push(2*i2+1,  2*i2+1,  1.0);

   }
  cJ.to_csc(J);
}

/*! \brief Residual of differential equation system
 * \param x Simulation state vector (vertex positions + edge natural lengths)
 * \param v Simulation velocity vector (v = dx/dt)
 * \param mu_eps Damping parameter
 * \param ss Simulation state structure */
Eigen::VectorXd res_tri(const Eigen::VectorXd& x, const Eigen::VectorXd& v, double mu_eps, const sim_st& ss)
{
  // Residual vector
  Eigen::VectorXd r(2*ss.Nv+ss.Ne);
  r.setZero();
  // Loop over all cells in cell_idx
  for(int i=0; i<ss.Nc; ++i)
    {
      // Cell pressure
      const double p=ss.cell_attribs[i];
      // Loop over the edges for this cell
      for(int j=ss.cell_idx[i]; j<ss.cell_idx[i+1]; ++j)
	{
	  // Cell edge forces
	  // Indices of start and end vertex
	  const int i0=ss.edge_idx[j][0];
	  const int i1=ss.edge_idx[j][1];
	  // Vector along edge
	  const Vec2 d=x.segment<2>(2*i1)-x.segment<2>(2*i0);
	  // Length of edge
	  const double l=d.norm();
	  // Pressure force
	  const Vec2 p_force=0.5*p*rot(d);
	  // Calculate the spring forces
	  // Spring strength, lambda
	  const double lambda=ss.edge_attribs[j][0];
	  // Spring natural length
	  const double l0=x[2*ss.Nv+j];

	  // Elastic tension in spring
	  const Vec2 t_force=lambda*(l-l0)*d/(l*l0);
	  // Edge viscous drag forces
	  const double phi_inv=ss.edge_attribs[j][2];
	  // Velocity difference between end-points
	  const Vec2 dv=v.segment<2>(2*i1)-v.segment<2>(2*i0);
	  // Drag forces from Lockhart
	  // Zero yield stress, assumed that wall is
	  // always under tension
	  const Vec2 d_force=phi_inv/(l*l*l)*d.dot(dv)*d;
	  // Assign forces to start and end vertices in residual
	  // pressure + elastic tension + viscous tension
	  r.segment<2>(2*i0)+=p_force+t_force+d_force;
	  r.segment<2>(2*i1)+=p_force-t_force-d_force;
	  // Edge spring length relaxation
	  const double gamma=ss.edge_attribs[j][1];
	  // Relaxation of spring natural lengths
	  r[2*ss.Nv+j]=gamma*(l-l0)-v[2*ss.Nv+j];
	}
    }
  for(int i=0; i<ss.Nt; ++i)
    {

      const int i0=ss.tri_idx[i][0];
      const int i1=ss.tri_idx[i][1];
      const int i2=ss.tri_idx[i][2];

      const double lambda=ss.tri_attribs[i][0];
      const double mu1=ss.tri_attribs[i][1];
      const double mu2=ss.tri_attribs[i][2];
      const double mu3=ss.tri_attribs[i][3];
      Eigen::Map<Eigen::Vector2d> A(&ss.tri_attribs[i][4]);
      Eigen::Map<Eigen::Vector2d> X0(&ss.tri_attribs[i][6]);
      Eigen::Map<Eigen::Vector2d> X1(&ss.tri_attribs[i][8]);
      Eigen::Map<Eigen::Vector2d> X2(&ss.tri_attribs[i][10]);
      // Elastic forces owing to each triangle

      const Eigen::VectorBlock<const Eigen::VectorXd, 2> x0= x.segment<2>(2*i0); 
      const Eigen::VectorBlock<const Eigen::VectorXd, 2> x1= x.segment<2>(2*i1); 
      const Eigen::VectorBlock<const Eigen::VectorXd, 2> x2= x.segment<2>(2*i2); 
      const Eigen::VectorBlock<const Eigen::VectorXd, 2> v0= v.segment<2>(2*i0); 
      const Eigen::VectorBlock<const Eigen::VectorXd, 2> v1= v.segment<2>(2*i1); 
      const Eigen::VectorBlock<const Eigen::VectorXd, 2> v2= v.segment<2>(2*i2); 
   
      Eigen::Matrix2d F0;
      F0 << X1-X0, X2-X0;
      Eigen::Matrix2d F0_inv;
      F0_inv=F0.inverse();
      Eigen::Matrix2d F1;
      F1 << x1 - x0, x2-x0;
      Eigen::Matrix2d F1_inv;
      F1_inv=F1.inverse();

      Vec2 a = F1 * F0_inv * A;
      a.normalize();

      Eigen::Matrix<double, 2, 2> grad;
      grad << v1 - v0, v2 - v0;
      grad=grad*F1_inv;

      Eigen::Matrix2d E;
      E=0.5*(grad+grad.transpose());
      Eigen::Matrix2d aEa = a*(E*a).transpose();
      double asr = a.dot(E*a);
      Eigen::Matrix2d T;
      T=2*mu1*E + mu2*(a*a.transpose()*asr) + 2*mu3*(aEa+aEa.transpose());
      r.segment<2>(2*i0)+=0.5*T*rot(x2-x1);
      r.segment<2>(2*i1)+=0.5*T*rot(x0-x2);
      r.segment<2>(2*i2)+=0.5*T*rot(x1-x0);
    }
  for(int i=0; i<ss.Nv; ++i)
    {
      // Velocity-dependent drag forces 
      r.segment<2>(2*i)+=-ss.mu_eps*v.segment<2>(2*i);
    }
  for(int i=0; i<ss.Nf; ++i)
    {
      // Fixed vertex components
      r.segment<2>(2*ss.fixed[i])=v.segment<2>(2*ss.fixed[i]);
    }

  return r;

}



/*! \brief Numerical calculation of Jacobian matrix
 *
 * Calculates J = \theta_x \partial r / \partial x +
 *                \theta_v \partial r / \partial v
 * by finite differences, where r(x,v) is the residual function
 *
 *
 * The columns of the Jacobian matrix are arranged into groups
 *
 * e.g. with the sparsity pattern
 *
 *       / 1 0 0 0 0 \ 
 *       | 1 1 0 0 0 |
 *       | 0 0 1 0 1 |
 *       | 0 0 1 0 0 |
 *       \ 0 0 0 1 0 /
 *
 * the column groups are { {0,2,3}, {1,4} }, as these columns can be 
 * calculated by finite differences simultaneously.
 * These groupings are stored in J.igrp[Ng+1] and J.jgrp[n] 
 * where the column indices for the m-th group are given by
 * J.jgrp[k] for  J.igrp[m]<=k<J.igrp[m+1]
 *
 * \param J  Jacobian matrix
 * \param x0 Simulation state vector (vertex positions + edge natural lengths
 * \param v0 Simulation velocity vector (v = dx/dt)
 * \param theta_x Coefficient of dr/dx
 * \param theta_v Coefficient of dr/dv
 * \param mu_eps Damping parameter
 * \param ss Simulation state structure */
void J_num(cscmat& J, const Eigen::VectorXd& x0, const Eigen::VectorXd& v0, double theta_x, double theta_v, const sim_st& ss)
{
  const double aeps=1e-6;
  const double reps=0e-6;

  int Ng;
  // Count number of column groups
  for(Ng=0; J.igrp[Ng]!=J.n; ++Ng)
      ;
  
  //   This calculation is parallelized using OpenMP
  //   set the environment variable  OMP_NUM_THREADS to 1 
  //   to force single-threaded computation
  //   export OMP_NUM_THREADS=1
  //   or compile without openmp options

  // This starts the parallel section of code (thread per processor) 
    #pragma omp parallel 
    {
    Eigen::VectorXd r2(J.m), r1(J.m);
    Eigen::VectorXd x(J.n), v(J.n);
  
    // Loop over each column group (distributed over the threads)
    #pragma omp for
    for(int g=0; g<Ng; ++g)
      //for(int g=0; J.igrp[g]!=J.n; ++g)
    {
      x=x0;
      v=v0;
      // Calculate location of first point (increase those members of
      // x which are within the column group by a "small" amount)
      for(int k=J.igrp[g]; k!=J.igrp[g+1]; ++k)
	{
	  x[J.jgrp[k]]=x0[J.jgrp[k]]+std::max(aeps,reps*fabs(x0[J.jgrp[k]]));
	}
      // Calculate residual at first point
      r1=res_tri(x, v, ss.mu_eps, ss);
      // Calculate location of second point (decrease those members of
      // x which are within the column group by a "small" amount)
      for(int k=J.igrp[g]; k!=J.igrp[g+1]; ++k)
	{
	  x[J.jgrp[k]]=x0[J.jgrp[k]]-std::max(aeps,reps*fabs(x0[J.jgrp[k]]));
	
	}
      // Calculate residual at second point
      r2=res_tri(x, v, ss.mu_eps, ss);
      // Calculate contribution to jacobian by centred finite differences
      for(int k=J.igrp[g]; k!=J.igrp[g+1]; ++k) 
	for(int l=J.Ap[J.jgrp[k]];l!=J.Ap[J.jgrp[k]+1];++l) 
	  J.Ax[l]=0.5*(r1[J.Ai[l]]-r2[J.Ai[l]])*
	    theta_x/std::max(aeps,reps*fabs(x0[J.jgrp[k]]));
    }

    // Now calculate dependence on velocity variables
    #pragma omp for
    //  for(int g=0; J.igrp[g]!=J.n; ++g)
    for(int g=0; g<Ng; ++g)
        {
      v=v0;
      x=x0;
      // Calculate velocity of first point (increase those members of
      // v which are within the column group by a "small" amount)
      for(int k=J.igrp[g]; k!=J.igrp[g+1]; ++k)
	{
	  v[J.jgrp[k]]=v0[J.jgrp[k]]+std::max(aeps,reps*fabs(v0[J.jgrp[k]]));
	}
      // Calculate residual at first point      
      r1=res_tri(x, v, ss.mu_eps, ss);
      // Calculate velocity of second point (decrease those members of
      // v which are within the column group by a "small" amount)
      for(int k=J.igrp[g]; k!=J.igrp[g+1]; ++k)
	{
	  v[J.jgrp[k]]=v0[J.jgrp[k]]-std::max(aeps,reps*fabs(v0[J.jgrp[k]]));
	}
      // Calculate residual at second point
      r2=res_tri(x, v, ss.mu_eps, ss);
      // Calculate contribution to jacobian by centred finite differences
      for(int k=J.igrp[g]; k!=J.igrp[g+1]; ++k) 
	for(int l=J.Ap[J.jgrp[k]];l!=J.Ap[J.jgrp[k]+1];++l) 
	  J.Ax[l]+=0.5*(r1[J.Ai[l]]-r2[J.Ai[l]])*
	    theta_v/std::max(aeps,reps*fabs(v0[J.jgrp[k]]));
    }
   
}
       
}

/*! \brief Norm for error tolerance (in NR convergence) for BE
 * (TODO) Explain this norm
 * \param w Vector
 * \param y Current simulation state vector
 * \param rtol Relative error tolerance
 * \param atol Absolute error tolerance
 */

inline double nrm(const Eigen::VectorXd& w, const Eigen::VectorXd& y, const double rtol, const double atol)
{
  return (w.cwiseAbs().cwiseQuotient(rtol*y.cwiseAbs()+atol*Eigen::VectorXd::Ones(y.size()))).maxCoeff();
}

/*! \brief Evolve mechanical state of simulation using backwards Euler
 * Simulation using Backwards Euler
 * \param tend          Length of period to integrate over
 * \param dt0           Initial timestep to attempt
 * \param init          If true, do not use initial rate of change vector xp
 * \param cell_idx      Cell edge index array
 * \param cell_attribs  Cell attribute array
 * \param edge_idx      Edge vertex index array
 * \param edge_attribs  Edge attribute array
 * \param edge_vars     Edge variable array
 * \param tri_idx      Tri vertex index array
 * \param tri_attribs  Tri attribute array
 * \param fixed         Fixed vertex coordinate index array
 * \param verts         Vertex location array
 * \param xp            Rate of change of state variable array
 */ 
void evolve_be(double tend, double dt0, double mu_eps, int Nc, int cell_idx[], double cell_attribs[], int Ne, int edge_idx[][2], double edge_attribs[][3], double edge_vars[],  int Nt, int tri_idx[][3], double tri_attribs[][12], int Nf, int fixed[], int Nv, double verts[][2], double xp[], bool init)
{

  //  std::cerr << "evolvebe" << std::endl;
  //  std::cout << "Nt: " << Nt << std::endl;

  // Absolute error tolerance
  const double atol=1e-4;
  // Relative error tolerance
  const double rtol=0.0;
  // Maximum number of iterations in the NR to try before
  // step is reattempted with smaller timestep
  const int niter_max=100;
  // Tolerance for convergence of NR iteration
  const double kappa=0.1;
 
  const int Nr=150;
  // Total number of unknowns
  const int n=2*Nv+Ne;

  // Initialise simulation state structure from arguments 
  // See definition of struct sim_st for details
  sim_st ss;
  ss.mu_eps=mu_eps;
  ss.Nc=Nc;
  ss.cell_idx=cell_idx;
  ss.cell_attribs=cell_attribs; 
  ss.Ne=Ne; 
  ss.edge_idx=edge_idx; 
  ss.edge_attribs=edge_attribs;
  ss.Nt=Nt;
  ss.tri_idx=tri_idx;
  ss.tri_attribs=tri_attribs;
  ss.Nv=Nv;
  ss.fixed=fixed;
  ss.Nf=Nf;
  ss.visc=init;

  // Calculate Jacobian pattern, and group columns for numerical
  // evaluation of J using finite differences
  cscmat J(2*Nv+Ne, 2*Nv+Ne);
  JPat(J, ss);
  J.group();
  // std::cerr << "calc_pat" << std::endl;
  // std::cerr << "array_size" << 400*Nv << std::endl;

  spmat_umf M(2*Nv, 2*Nv, 400*Nv);
  // std::cerr << "alloc M" << std::endl;

  // Structure to store statistics about integration
  // Note that this is 
  struct
  {
    int steps;
    int jacs;
    int iters;
    int itfails;
    int jacfails;
    int accyfails;
    int extendstep;
  } stats;
  // Zero this structure
  stats.steps=0;
  stats.jacs=0;
  stats.iters=0;
  stats.itfails=0;
  stats.jacfails=0;
  stats.accyfails=0;
  stats.extendstep=0;
  

  // New timestep length
  double dt_new;

  // Time from start of timestep
  double t=0;
  // Current timestep
  double dt=dt0;

  // Iteration counter for NR
  int      niter;
  // 
  double   dist, err;

  // Flag as to whether first step; could be used to treat this differently
  bool first_step=true;

  Eigen::VectorXd y0(n), y1(n), z0(n), z1(n), r(n), delta(n), f(n), v(n);
  y0.segment(0,2*Nv)=Eigen::Matrix<double, 1, Eigen::Dynamic>::Map(verts[0],2*Nv);
  y0.segment(2*Nv,Ne)=Eigen::Matrix<double, 1, Eigen::Dynamic>::Map(edge_vars,Ne);
  if(init)
    z0.setZero();
  else
    z0=Eigen::Map<Eigen::VectorXd>(xp,n);

  //  res_tri(y0,z0,ss.mu_eps,ss);

  std::cerr << "J num" << std::endl;
  J_num(J, y0, z0, 0.0, -1.0, ss);
  std::cerr << "J.lu_symb" << std::endl;
  J.lu_symb();
  if(J.lu_num()==1)
    std::cout << "*\n*\n*\n*\nInit J singular\n*\n*\n*\n*\n";

  // Flag for whether the jacobian needs updating; either for change in
  // stepsize or convergence failure of NR method (note that currently code
  // uses full NR method
  bool needs_jac=true;
  // Whether the jacobian J was calculated at a previous time-step
  // (for modified NR, not used currently)
  bool jac_current=false;
  // Flag set if NR iteration failed
  bool iter_fail;
  // Loop until the end of the calculation period
  while(t<tend) 
    {
      std::cout << "t " << t << " dt " << dt << std::endl;
      /*
      if(needs_jac)
	{
	  // Calculate a new Jacobian matrix by finite differences
	  stats.jacs++;
	  J_num(J, y0, z0, -dt, -1.0, ss);
	  needs_jac=false;
	  jac_current=true;
	  if(J.lu_num()==1)
	  {
	    std::cout << J << "\n";
	  }
      	}
      */
      niter=0;
      iter_fail=false;
      z1=z0;
      while(1)
	{
	  niter++;
	  stats.iters++;
	  y1=y0+dt*z1;

	  r=res_tri(y1,z1,ss.mu_eps,ss);
	  J_num(J, y1, z1, -dt, -1.0, ss);
	  needs_jac=false;
	  jac_current=true;
	  stats.jacs++;
	  J.lu_num();
	  J.lu_solve(delta.data(), r.data());
	  std::cout << delta[0] << " z1: " << z1[0]+delta[0] << "\n";
	 
	  dist=nrm(delta, y0, rtol, atol);

	  std::cout << niter << " " << r.cwiseAbs().maxCoeff() << " " << delta.cwiseAbs().maxCoeff() << " " << dist << " " << kappa << " " << nrm(r, y0, rtol, atol) << "\n";

	  if(dist>1e30)
	    {
	      iter_fail=true;
	      break;
	    }

	  if(dist<kappa)
	    break;

	  z1+=delta;	 
	  
	  if(niter>niter_max)
	    {
	      iter_fail=true;
	      break;
	    }
	}
      std::cout << niter << " " << r.cwiseAbs().maxCoeff() << " " << delta.cwiseAbs().maxCoeff() << " "<< delta.cwiseAbs().sum() << " " << dist << " " << kappa << "\n";
      dt_new=dt;
      if(iter_fail)
	{
      	  if(jac_current)
	    {
	      stats.itfails++;
	      dt_new=0.5*dt;
	      z0=z0;
	      dt=dt_new;
	      needs_jac=true;
	      continue;
	    } 
	  else
	    {
	      stats.jacfails++;
	      needs_jac=true;
	      continue;
	    }
	}
      if(niter<niter_max/10 && dt<dt0)
	{
	  dt_new=1.3*dt;
	  needs_jac=true;
	}
      if(first_step) first_step=false;
      t+=dt;
      stats.steps++;
      
      y0=y1;
      z0=z1;

      dt=dt_new;
      jac_current=false;
    }
  // Print information about the integration over this time-step
  std::cout << "jacs: " << stats.jacs << " steps: "<< stats.steps << std::endl;
  std::cout << "itfails: " << stats.itfails << " jacfails: "<< stats.jacfails <<  std::endl;
  std::cout << "accyfails: " << stats.accyfails << " stepextendss: " << stats.extendstep << std::endl;

  // Update numpy matrix containing vertex positions
  Eigen::Matrix<double, 1, Eigen::Dynamic >::Map(verts[0],2*Nv)=y0.segment(0,2*Nv);
  // Update numpy matrix containing edge natural lengths
  Eigen::Matrix<double, 1, Eigen::Dynamic >::Map(edge_vars,Ne)=y0.segment(2*Nv,Ne);
  // Update numpy matrix containing current velocity
  // (To help restart integration, particularly if IDA is used for
  //    following timestep
  Eigen::Map<Eigen::VectorXd>(xp,n)=z0;
}



/*! \brief Python wrapper for simulation using Backwards Euler
 * Python wrapper for simulation using Backwards Euler
 *   All numpy arrays must be of the type shown, contiguous and column major
 *   (e.g. the 3x3 matrix a is stored as a11, a21, s31, a12, a22, a32, 
 *   a13, a23, a33 ; this is the normal storage order for numpy arrays)
 *   See numpy.ascontiguousarray() and numpy.asfortranarray()
 *   Note that C/C++ arrays are stored in transpose (row-major) order
 *

 *
 * \param uniforms Python list of floats
 * - mu_eps Per-vertex drag
 * - tend  Length of period to integrate over
 * - dt0   Initial timestep to attempt
 * - init  If true, do not use initial rate of change vector xp
 *
 * \param arrays Python list
 * - cells Numpy array (Nc+1 elements, one-dimensional, dtype=int32) 
 *   indicating which elements in the edges array correspond to which cells:
 *   cell i being { edge[j] for  cell[i] <= j <= cell[i+1]-1 } 
 * - cell_attribs  Numpy array (Nc elements, one-dimensional, 
 *   dtype=float64) of cell attributes (turgor pressures)
 * - cell_vars Numpy array of cell variables (not used)
 * - edges Numpy array (2 by Ne, dtype=int32) each column being 
 *   the indices of the vertices at the start and end of each edge
 * - edge_attribs Numpy array (3 by Ne, dtype=float64);
 *   each column c being the attributes of an edge
 *     c[0] - lambda, spring constant
 *     c[1] - gamma,  spring natural length relaxation rate
 *     c[2] - phiinv, edge viscosity / inverse of extensibility
 * - edge_vars Numpy array (Ne elements, one-dimensional, 
 *   dtype=float64) of edge spring natural lengths. Updated at the end
 *   point of the integration.
 * - tris Numpy array (3 by Nq, dtype=int32); each column being
 *   the indices of the vertices at the four corners of the each 
 *   triangular element, in counter-clockwise order.
 * - tri_attribs Numpy array (7 by Nq, dtype=float64);
 *   each column t being the properties of one triangular element:
 *     t[0]          - lambda, elastic constant
 *     t[1] to t[6] - (X[0],Y[0],X[1],Y[1],X[2],Y[2])
 *                     locations of triangular vertices in the initial
 *                     (reference) configuration.
 * - tri_vars Numpy array of variables on triangles (not used)
 * - fixed Numpy array (one-dimensional, Nf in length, dtype=int32) 
 *   indicating the vertex coordinates on which drag is applied 
 *   (to prevent solid-body rotation and translation of the whole tissue). 
 *   Values correspond to indices in verts, when verts is converted to a 
 *   one-dimensional array [x0,y0,x1,y1,..]
 * - xp Numpy array (one-dimensional, 2*Nv+Ne in length, dtype=float64)
 *   of the rate of change of the state vector. Used as first guess for z
 *   if init is False. Modified on output to be z for the last simulation 
 *   step. */

void evolve_be_python(boost::python::list& uniforms, boost::python::list& arrays)
{
  //  std::cerr << "evolvebepython" << std::endl;  
  double mu_eps = boost::python::extract<double>(uniforms[0]);
  double tend = boost::python::extract<double>(uniforms[1]);
  double dt0 = boost::python::extract<double>(uniforms[2]);
  int init = boost::python::extract<int>(uniforms[3]);
  boost::python::object cells = arrays[0];
  boost::python::object cell_attribs = arrays[1];
  boost::python::object cell_vars = arrays[2];
  boost::python::object edges = arrays[3];
  boost::python::object edge_attribs = arrays[4];
  boost::python::object edge_vars = arrays[5];
  boost::python::object tris = arrays[6];
  boost::python::object tri_attribs = arrays[7];
  boost::python::object tri_vars = arrays[8];
  boost::python::object vertex_attribs = arrays[9];
  boost::python::object vertex_vars = arrays[10];
  boost::python::object fixed = arrays[11];
  boost::python::object xp = arrays[12];
  evolve_be(tend, dt0, mu_eps,
	    PyArray_DIM(cells.ptr(), 0)-1, 
	    static_cast<int*>(PyArray_DATA(cells.ptr())), 
	    static_cast<double(*)>(PyArray_DATA(cell_attribs.ptr())), 
	    PyArray_DIM(edges.ptr(),0), 
	    static_cast<int (*)[2]>(PyArray_DATA(edges.ptr())), 
	    static_cast<double(*)[3]>(PyArray_DATA(edge_attribs.ptr())),
	    static_cast<double*>(PyArray_DATA(edge_vars.ptr())),
	    PyArray_DIM(tris.ptr(),0), 
	    static_cast<int (*)[3]>(PyArray_DATA(tris.ptr())), 
	    static_cast<double(*)[12]>(PyArray_DATA(tri_attribs.ptr())),
	    PyArray_DIM(fixed.ptr(),0),
	    static_cast<int*>(PyArray_DATA(fixed.ptr())),
	    PyArray_DIM(vertex_vars.ptr(),0),
	    static_cast<double (*)[2]>(PyArray_DATA(vertex_vars.ptr())),
	    static_cast<double*>(PyArray_DATA(xp.ptr())),
	                init); 
}

// boost::python stuff to expose functions to Python 
using namespace boost::python;
BOOST_PYTHON_MODULE(module_tri_visc)
{
  def("evolve_be", evolve_be_python);
}
