// $Author: pmzjaf $
// $Id: sparse.cpp,v 1.1 2007/07/04 09:59:44 pmzjaf Exp $
#include <vector>
#include <suitesparse/umfpack.h>
#include "sparse.h"

//#define VERBOSE

spmat_umf& spmat_umf::lu_conv()
{ 
  #ifdef VERBOSE
  std::cout << "lu_conv: " <<
  #endif
    umfpack_di_triplet_to_col(m, n, nnz, i, j, a, Ap, Ai, Ax, Map);
  return *this;
}


spmat_umf& spmat_umf::lu_convm()
{
  for (int p = 0; p < Ap[n]; p++) Ax[p]= 0;
  for (int k = 0; k < nnz; k++) Ax[Map [k]]+=a[k];
  return *this;
}


spmat_umf& spmat_umf::lu_symb()
{
  if(symbolic) 
      lu_fsymb();
  #ifdef VERBOSE
  std::cout << "lu_symb: " <<
  #endif
  umfpack_di_symbolic(m, n, Ap, Ai, Ax, &symbolic, 0, 0);
  return *this;
}

spmat_umf& spmat_umf::lu_fsymb()
{
  umfpack_di_free_symbolic(&symbolic);
  symbolic=0;
  return *this;
}


spmat_umf& spmat_umf::lu_num()
{
  if(numeric) lu_fnum();
  #ifdef VERBOSE
  std::cout << "lu_num: " <<
  #endif
  umfpack_di_numeric(Ap, Ai, Ax, symbolic, &numeric, 0, 0);
  return *this;
}

spmat_umf& spmat_umf::lu_fnum()
{
  umfpack_di_free_numeric(&numeric);
  numeric=0;
  return *this;
}

void spmat_umf::lu_solve(double* x, double* b)
{
  #ifdef VERBOSE
  std::cout << "lu_solve: " <<
  #endif
  umfpack_di_solve(UMFPACK_A, Ap, Ai, Ax, x, b, numeric, 0, 0);
}




void cscmat::lu_symb()
{
  if(symbolic) lu_fsymb();
  #ifdef VERBOSE
  std::cout << "lu_symb: " <<
  #endif
  umfpack_di_symbolic(m, n, &Ap[0], &Ai[0], &Ax[0], &symbolic, 0, 0);
  #ifdef VERBOSE
  std::cout << "\n";
  #endif
}

void cscmat::lu_fsymb()
{
  umfpack_di_free_symbolic(&symbolic);
  symbolic=0;
}

int  cscmat::lu_num()
{
  int i;
  if(numeric) lu_fnum();
  double Info[UMFPACK_INFO];

  i=umfpack_di_numeric(&Ap[0], &Ai[0], &Ax[0], symbolic, &numeric, 0, Info);
  #ifdef VERBOSE
  std::cout << "lu_num: " << i << "\n";
  #endif
  std::cout << "cond_est: " << Info[UMFPACK_RCOND] << "\n";
  return i;
}

void cscmat::lu_fnum()
{
  umfpack_di_free_numeric(&numeric);
  numeric=0;
}

void cscmat::lu_solve(double* x, double* b)
{
  #ifdef VERBOSE
  std::cout << "lu_solve: " <<
  #endif
  umfpack_di_solve(UMFPACK_A, &Ap[0], &Ai[0], &Ax[0], x, b, numeric, 0, 0);
  #ifdef VERBOSE
  std::cout << "\n";
  #endif
}

/*
int cscmat::group()
{
  int count=0;
  std::vector<int> incl(m);
  std::vector<int> jdone(n);
  for(int j=0;j!=n;++j) jdone[j]=0;
   igrp[0]=0;
  for(int l=0;l!=n;++l)
    {
      for(int i=0;i!=m;++i) 
	incl[i]=0;
      for(int j=0;j!=n;++j)
	{
	  if(!jdone[j]) 
	    {
	      bool in_group=true;
	      for(int k=Ap[j];k!=Ap[j+1];++k)
		{
		  if(incl[Ai[k]])
		    {
		      in_group=false;
		      break;
		    }
		}
	      if(in_group)
		{
		  for(int k=Ap[j];k!=Ap[j+1];++k) 
		    incl[Ai[k]]=1;
		  jdone[j]=1;
		  jgrp[count]=j;
		  count++;
		}
	    }
	}
      igrp[l+1]=count;
      if(count==n)
	{
	  return l;
	}
    }
  return 0;
}
*/

int cscmat::group()
{
  int count=0;
  std::vector<int> incl(m);
  std::vector<int> jdone(n);
  for(int j=0;j!=n;++j) jdone[j]=0;
   igrp[0]=0;
  for(int l=0;l!=n;++l)
    {
      for(int i=0;i!=m;++i) 
	incl[i]=0;
      for(int j=0;j!=n;++j)
	{
	  if(!jdone[j]) 
	    {
	      bool in_group=true;
	      for(int k=Ap[j];k!=Ap[j+1];++k)
		{
		  if(incl[Ai[k]])
		    {
		      in_group=false;
		      break;
		    }
		}
	      if(in_group)
		{
		  for(int k=Ap[j];k!=Ap[j+1];++k) 
		    incl[Ai[k]]=1;
		  jdone[j]=1;
		  jgrp[count]=j;
		  count++;
		}
	    }
	}
      igrp[l+1]=count;
      if(count==n)
	{
	  return l;
	}
    }
  return 0;
}



void coomat::to_csc(cscmat& M)
{
  M.m=m;
  M.n=n;
  int nnz=Ax.size();
  M.resize(m,n,nnz);
  M.Ai.resize(nnz);
  M.Ax.resize(nnz);
  umfpack_di_triplet_to_col(m, n, nnz, &Ai[0], &Aj[0], &Ax[0], &(M.Ap[0]), &(M.Ai[0]), &(M.Ax[0]), 0);
}
