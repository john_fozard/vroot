
#from vplants.plantgl.math import Vector2
from openalea.container import ordered_pids
from vroot2D.utils.db_utilities import get_mesh, get_parameters, set_parameters
from vroot2D.utils.geom import centroid, area, long_axis
from openalea.tissueshape import face_surface_2D
from vroot2D.utils.db_geom  import ordered_wids_pids
from openalea.tissueshape import edge_length
from max_strain import max_strain
from numpy import array as Vector2

from vroot2D.growth.growth_midline import midline_distance_point

from random import random

def rot(v):
    """
    rotate a 2d vector (Vector2) by 90 degrees anticlockwise
    :param v: input vector
    :type v: Vector2/list/tuple
    :returns: Vector2 rotated v
    """
    return Vector2((-v[1],v[0]))

"""
Division rules for periclinal cells
"""

def get_midpoint(db, cid, wall_type):
    """
    Calculate the midpoint of those walls in a cell with
    a particular wall type
    :param db: Tissue database
    :type db: TissueDB
    :param cid: Cell Id number
    :type cid: int
    :param wall_type: Type of the walls to consider
    :returns: Midpoint of the walls (Vector2).
    """
    # extract mesh, wall dictionary and properties from TissueDB
    mesh = get_mesh(db)
    pos = db.get_property('position')
    wall_types= db.get_property('wall_types')
    # Calculate the walls and points of the cell in counterclockwise
    # order
    o_wids, o_pids = ordered_wids_pids(mesh, pos, cid)

    # Create a list of the wall ids with the specified type
    idx = [i for i, wid in enumerate(o_wids) \
               if wall_types[(cid, wid)]==wall_type]
    # Calculate the total length of these walls
    l_tot = sum(edge_length(mesh, pos, o_wids[i]) for i in idx) 
    # Find the first wall (treating the cell as a circular list).
    for start, i  in enumerate(idx):
        if (i-1)%len(o_wids) not in idx:
            break
    idx = idx[start:]+idx[:start] # rotate the indices.
    # distance along the walls 
    l = 0
    for i in idx:
        el = edge_length(mesh, pos, o_wids[i])
        if l+el > l_tot/2:
            # if the midpoint is within this wall
            # interpolate to find its position
            alpha = (l_tot/2 - l)/el
            # find the two endpoints v0 and v1
            v1 = pos[o_pids[i]]
            s = set(mesh.borders(1, o_wids[i]))
            s.remove(o_pids[i])
            v0 = pos[s.pop()]
            return alpha*v1 + (1-alpha)*v0
        else:
            l += el
    
        
def get_centroid(db, cid):
    """
    Routine to calculate the centroid of a cell
    :param db: The tissue database
    :type db: TissueDB
    :param cid: Id number of cell to divide
    :type cid: int
    :returns: Vector2 - centroid of cell
    """
    mesh = get_mesh(db)
    pos = db.get_property('position')
    # Get list of vertex ids in order around the cell
    pids=ordered_pids(mesh, cid)
    points=[(pos[pid][0], pos[pid][1]) for pid in pids]
    pt=centroid(points)
    return Vector2(pt)


def get_periclinal_axis(db, cid):
    """
    Routine to calculate the periclinal axes of a cell
    :param db: The tissue database
    :type db: TissueDB
    :param cid: Id number of cell to divide
    :type cid: int
    :returns: (Vector2, Vector2) -- plane to divide cell at.
              First element of tuple is a point in the plane
              Second element is the normal to the plane.
    """
    p1 = get_midpoint(db, cid, 1)
    p3 = get_midpoint(db, cid, 3)
    mesh = get_mesh(db)
    pos = db.get_property('position')
    # Get list of vertex ids in order around the cell
    pids=ordered_pids(mesh, cid)
    # Ensure that this is in anticlockwise order
    points=[(pos[pid][0], pos[pid][1]) for pid in pids]
    if area(points)<0:
        points.reverse()

    pt=centroid(points)
#    return 0.5*(p1+p3), rot(p3-p1)
    return Vector2(pt), rot(p3-p1)

def get_anticlinal_axis(db, cid):
    """
    Routine to calculate the anticlinal axes of a cell
    :param db: The tissue database
    :type db: TissueDB
    :param cid: Id number of cell to divide
    :type cid: int
    :returns: (Vector2, Vector2) -- plane to divide cell at.
              First element of tuple is a point in the plane
              Second element is the normal to the plane.
    """
#    print 'midpoint', get_midpoint(db, cid, 0)
    mesh = get_mesh(db)
    pos = db.get_property('position')
    # Get list of vertex ids in order around the cell
    pids=ordered_pids(mesh, cid)
#    # Ensure that this is in anticlockwise order
    points=[(pos[pid][0], pos[pid][1]) for pid in pids]
    if area(points)<0:
        points.reverse()
    pt=centroid(points)
    p0 = get_midpoint(db, cid, 0)
    p2 = get_midpoint(db, cid, 2)
   # return 0.5*(p0+p2), rot(p2-p0)
    return Vector2(pt), rot(p2-p0)
    
def get_long_axis(db, cid):
    """
    Routine to calculate the centroid and long axis of a cell.
    This is the line in 2D about which the cell has smallest
    moment of inertia.
    :param db: The tissue database
    :type db: TissueDB
    :param cid: Id number of cell to divide
    :type cid: int
    :returns: (Vector2, Vector2) --
              First element of tuple is the centroid of cell.
              Second element is the long axis of the cell.
    """
    mesh = get_mesh(db)
    pos = db.get_property('position')
    # Get list of vertices in their order around the cell
    pids = ordered_pids(mesh, cid)
    # Ensure that this is in anticlockwise order
    points = [(pos[pid][0], pos[pid][1]) for pid in pids]
    if area(points)<0:
        points.reverse()
    pt = centroid(points)
    axis = long_axis(points)
    return Vector2(pt), Vector2(axis)


class PrimordiumDivisionRule(object):
    """
    Class to describe how primodium cells choose to divide
    anticlinally or periclinally, depending on their internal
    levels of the TFs PLT and E2F, and rules for how the
    levels of these are reset after a division event
    """

    name = 'PrimordiumDivisionRule'

    def get_dividing_cids(self, db):
        """
        Generate the list of cell ids for all cells which
        currently are ready to divide
        :param db: Tissue database
        :type db: TissueDB
        :returns: list(int) - a list of cell ids
        """
        cids=set()
        if self.cell_divides(self, db, cid):
            cids.add(cid)
        return cids

    def set_default_parameters(self, db):
        p = { 'E2F_threshold': 0.05, 'PLT_threshold': 0.05, 'V_threshold': 1 }
        set_parameters(db, self.name, p)

    def does_cell_divide(self, db, cid):
        """
        Query whether a cell is ready to divide; depends
        on the cell type (is it a primordium cell), size
        (is it bigger than V_threshold) and whether the
        level of E2F in the cell is bigger than the
        E2F threshold.
        :param db: Tissue database
        :type db: TissueDB
        :param cid: Id number of the cell to query
        :type cid: int
        :returns: bool -- whether the cell is ready to divide
        """
        # Extract variables from TissueDB
        mesh = get_mesh(db)
        pos = db.get_property('position')
        border = db.get_property('border')
        E2F = db.get_property('E2F')
        params = get_parameters(db, self.name)
        E2F_threshold = params['E2F_threshold']
        V_threshold = params['V_threshold']
        if border[cid]==7 and E2F[cid] > E2F_threshold and face_surface_2D(mesh, pos, cid)>V_threshold:
            return True
        else:
            return False

    def get_axis_and_post_division_rule(self, db, cid):
        """
        Routine to obtain the axis of division (anticlinal or
        periclinal, depending on E2F levels) and the post-division
        rule (i.e. reset E2F, or reset both E2F and PLT) for that
        division.

        """
        E2F = db.get_property('E2F')
        PLT = db.get_property('PLT')
        params = db.get_property('division_parameters')
  #      print cid, type(cid), PLT[cid]
        if PLT[cid]> params['PLT_threshold']:
            return (get_periclinal_axis(db, cid), self.post_division_periclinal)
        else:
            return (get_anticlinal_axis(db, cid), self.post_division_anticlinal)


    def post_division_anticlinal(self, db, lineage):
        """
        Rule to reset E2F levels in daughter cells following division
        (this function is an option for the 
        second return value from get_axis_and_post_division_rule)
        :param db: Tissue database
        :type db: TissueDB
        :param lineage: Lineage dictionary
        :type lineage: dict - output from Division routine
        :param db: Tissue database
        :type db: TissueDB
        :param cid: Id number of the cell to query
        :type cid: int
        :returns: ((Vector2, Vector2), function)
                  First return value is a point in the cell division
                  plane and the normal to that plane.
                  The second return value is a function, 
                  which here is either self.post_division_E2F_PLT
                  or self.post_division_E2F_PLT, which tells
                  the simulator how to reset the properties of
                  the new cells following division.
        """
        # reset E2F
        new_cells = next(lineage[2].itervalues())
        E2F = db.get_property('E2F')
        for cid in new_cells:
            E2F[cid] = 0.0

        mesh = get_mesh(db)
#        print lineage
#        print lineage[1][None][0] in mesh.wisps(1)


        wall_types=db.get_property('wall_types')
        new_walls = lineage[1][None]
        for new_wid in new_walls:
            for nb_cid in mesh.regions(1, new_wid):
                if nb_cid in new_cells:
                    wt = [wall_types[(nb_cid, wid)] for wid in mesh.borders(2, nb_cid) if wid not in new_walls]
                    if 1 in wt:
                        wall_types[(nb_cid, new_wid)] = 3
                    else:
                        wall_types[(nb_cid, new_wid)] = 1
        
        

    def post_division_periclinal(self, db, lineage):
        """
        Rule to reset E2F and PLT levels following division
        (this function is an another option for the 
        second return value from get_axis_and_post_division_rule)
        :param db: Tissue database
        :type db: TissueDB
        :param lineage: Lineage dictionary
        :type lineage: dict - output from Division routine
        """
        new_cells = next(lineage[2].itervalues())
#        print lineage[2]
        E2F = db.get_property('E2F')
        PLT = db.get_property('PLT')
        for cid in new_cells:
            E2F[cid] = 0.0
            PLT[cid] = 0.0

        mesh = get_mesh(db)
        wall_types = db.get_property('wall_types')
        new_walls = lineage[1][None]
        for new_wid in new_walls:
            for nb_cid in mesh.regions(1, new_wid):
                if nb_cid in new_cells:
                    wt = [wall_types[(nb_cid, wid)] for wid in mesh.borders(2, nb_cid)  if wid not in new_walls]
                    if 2 in wt:
                        wall_types[(nb_cid, new_wid)] = 0
                    else:
                        wall_types[(nb_cid, new_wid)] = 2
        



class FullrootDivisionRule(object):
    """
    Division rule for fullroot model
    """

    name = 'FullrootDivisionRule'

    def get_dividing_cids(self, db):
        """
        Generate the list of cell ids for all cells which
        currently are ready to divide
        :param db: Tissue database
        :type db: TissueDB
        :returns: list(int) - a list of cell ids
        """
        cids=set()
        if self.does_cell_divide(self, db, cid):
            cids.add(cid)
        return cids

    def set_default_parameters(self, db):
        p = { 'rate': 0.05, 'kp': 1e-5 }
        set_parameters(db, self.name, p)

    def does_cell_divide(self, db, cid):

        # Extract variables from TissueDB
        mesh = get_mesh(db)
        pos = db.get_property('position')
        cell_type = db.get_property('cell_type')
        DIV = db.get_property('DIV')[cid]
        GP = db.get_property('GP')[cid]
        dt=get_parameters(db, 'timestep')
        p = get_parameters(db, self.name)
        r = p['rate']
        ml = p['kp']
        return (DIV>1.0) and (GP>ml) and (cell_type[cid] in (0,1,2,3))

    def get_axis_and_post_division_rule(self, db, cid):

        mesh = get_mesh(db)
        pos = db.get_property('position')
    # Get list of vertices in their order around the cell
        pids = ordered_pids(mesh, cid)
    # Ensure that this is in anticlockwise order
        points = [(pos[pid][0], pos[pid][1]) for pid in pids]
        if area(points)<0:
            points.reverse()
        pt = centroid(points)
        

        return ((pt, max_strain(db, cid)), self.post_division)


    def post_division(self, db, lineage):


        new_cells = next(lineage[2].itervalues())
        DIV = db.get_property('DIV')
        for cid in new_cells:
            DIV[cid] = 0.0

        mesh = get_mesh(db)
        wall_types=db.get_property('wall_types')
        new_walls = lineage[1][None]
        for new_wid in new_walls:
            for nb_cid in mesh.regions(1, new_wid):
                if nb_cid in new_cells:
                    wt = [wall_types[(nb_cid, wid)] for wid in mesh.borders(2, nb_cid) if wid not in new_walls]
                    if 1 in wt:
                        wall_types[(nb_cid, new_wid)] = 3
                    else:
                        wall_types[(nb_cid, new_wid)] = 1
        


class MidlineDivisionRule(object):
    """
    """

    name = 'MidlineDivisionRule'

    def get_dividing_cids(self, db):
        """
        Generate the list of cell ids for all cells which
        currently are ready to divide
        :param db: Tissue database
        :type db: TissueDB
        :returns: list(int) - a list of cell ids
        """
        cids=set()
        if self.does_cell_divide(self, db, cid):
            cids.add(cid)
        return cids

    def set_default_parameters(self, db):
        p = { 'rate': 0.1, 'meristem_length': 200 }
        set_parameters(db, self.name, p)

    def does_cell_divide(self, db, cid):

        # Extract variables from TissueDB
        mesh = get_mesh(db)
        pos = db.get_property('position')
        cell_type = db.get_property('cell_type')
        
        p = get_parameters(db, self.name)
        r = p['rate']
        ml = p['meristem_length']
        
        return cell_type[cid] in (0,1,2,3) and random()>1-r

    def get_axis_and_post_division_rule(self, db, cid):

#        print get_anticlinal_axis(db, cid)
        mesh = get_mesh(db)
        pos = db.get_property('position')
    # Get list of vertices in their order around the cell
        pids = ordered_pids(mesh, cid)
    # Ensure that this is in anticlockwise order
        points = [(pos[pid][0], pos[pid][1]) for pid in pids]
        if area(points)<0:
            points.reverse()
        pt = centroid(points)
        

        return ((pt, max_strain(db, cid)), self.post_division_anticlinal)


 #       return (get_anticlinal_axis(db, cid), self.post_division_anticlinal)


    def post_division_anticlinal(self, db, lineage):


        new_cells = next(lineage[2].itervalues())


        mesh = get_mesh(db)


        wall_types=db.get_property('wall_types')
        new_walls = lineage[1][None]
        for new_wid in new_walls:
            for nb_cid in mesh.regions(1, new_wid):
                if nb_cid in new_cells:
                    wt = [wall_types[(nb_cid, wid)] for wid in mesh.borders(2, nb_cid) if wid not in new_walls]
                    if 1 in wt:
                        wall_types[(nb_cid, new_wid)] = 3
                    else:
                        wall_types[(nb_cid, new_wid)] = 1

class SyntheticMidlineDivisionRule(object):
    """
    """

    name = 'SyntheticMidlineDivisionRule'

    def get_dividing_cids(self, db):
        """
        Generate the list of cell ids for all cells which
        currently are ready to divide
        :param db: Tissue database
        :type db: TissueDB
        :returns: list(int) - a list of cell ids
        """
        cids=set()
        if self.does_cell_divide(self, db, cid):
            cids.add(cid)
        return cids

    def set_default_parameters(self, db):
        p = { 'rate': 0.1, 'meristem_length': 200, 'division_area': 800 }
        set_parameters(db, self.name, p)

    def does_cell_divide(self, db, cid):

        # Extract variables from TissueDB
        mesh = get_mesh(db)
        pos = db.get_property('position')
        cell_type = db.get_property('cell_type')

        

        pos = db.get_property('position')
    # Get list of vertices in their order around the cell
        pids = ordered_pids(mesh, cid)
        points = [(pos[pid][0], pos[pid][1]) for pid in pids]
        if area(points)<0:
            points.reverse()
        c = centroid(points)

        a = area(points)

        d = midline_distance_point(db, c)
        
        p = get_parameters(db, self.name)
        r = p['rate']
        ml = p['meristem_length']
        div_area = p['division_area']

        print d, ml
        return a>div_area and d < ml and random()>1-r

    def get_axis_and_post_division_rule(self, db, cid):

#        print get_anticlinal_axis(db, cid)
        mesh = get_mesh(db)
        pos = db.get_property('position')
    # Get list of vertices in their order around the cell
        pids = ordered_pids(mesh, cid)
    # Ensure that this is in anticlockwise order
        points = [(pos[pid][0], pos[pid][1]) for pid in pids]
        if area(points)<0:
            points.reverse()
        pt = centroid(points)
        

        return ((pt, max_strain(db, cid)), self.post_division_anticlinal)


 #       return (get_anticlinal_axis(db, cid), self.post_division_anticlinal)


    def post_division_anticlinal(self, db, lineage):


        new_cells = next(lineage[2].itervalues())


        mesh = get_mesh(db)


        wall_types=db.get_property('wall_types')
        new_walls = lineage[1][None]
        for new_wid in new_walls:
            for nb_cid in mesh.regions(1, new_wid):
                if nb_cid in new_cells:
                    wt = [wall_types[(nb_cid, wid)] for wid in mesh.borders(2, nb_cid) if wid not in new_walls]
                    if 1 in wt:
                        wall_types[(nb_cid, new_wid)] = 3
                    else:
                        wall_types[(nb_cid, new_wid)] = 1
