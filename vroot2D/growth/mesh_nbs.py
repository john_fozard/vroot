
import meshpy.triangle as triangle
from vroot2D.utils.db_geom import face_surface_2D,edge_length
from openalea.container import ordered_pids, topo_divide_edge
from vroot2D.utils.geom import centroid, dist_segment_pt, pt_in_tri, pt_in_polygon, area
import numpy as np
#from vplants.plantgl.math import Vector2
from numpy import array as Vector2
from operator import itemgetter
from vroot2D.utils.db_utilities import get_mesh, get_graph
from copy import copy
from vroot2D.utils.db_geom import ordered_wids_pids
from vroot2D.utils.geom import ccw, dist
from vroot2D.utils.utils import pairs
import random
import tempfile
import os
import itertools
from collections import defaultdict

gp_i=0

def plot_gnuplot(pos, tris, x):
    global gp_i
    f = open('test%03d.gp'%gp_i, 'w')
    gp_i+=1
    for t in tris:
        for pid in t:
            f.write(str(pos[pid][0])+" "+str(pos[pid][1])+ "\n")
        f.write(str(pos[t[0]][0])+" "+str(pos[t[0]][1])+ "\n")
        f.write("\n")
    f.write(str(x[0])+" "+str(x[1])+"\n")
    f.close()


def sort_pair(x,y):
    if x<=y:
        return (x,y)
    else:
        return (y,x)

def triangulate_mesh_gmsh(db, vol_max=300, cid_list=None):
    mesh=get_mesh(db) 
    pos = db.get_property('position')
    
    f = open("test.geo","w")
    write_gmsh(db, f, cid_list)
    os.system("gmsh test.geo -2 -clmax "+str(vol_max))
    f.close()
    f = open("test.msh","r")
    l = f.readline
    while l:
        l = f.readline()
        if l == "$Nodes\n":
            break
    l = f.readline()
    nodes = int(l)
    verts = {}
    for i in range(nodes):
        l = f.readline().split()
        verts[int(l[0])]=tuple(map(float, l[1:3]))
    while l:
        l = f.readline()
        if l == "$Elements\n":
            break

    elements = int(f.readline())
    vertex_pids = {}
    lines = []
    tris = []


    nb_map = defaultdict(list)
    for i in range(elements):
        ll = f.readline().split()
        if ll[1]=="15": # Vertex
            vertex_pids[int(ll[4])] = int(ll[3])
        if ll[1]=="1": #line
            wid = int(ll[3])
            v0 = int(ll[-2])
            v1 = int(ll[-1])
            lines.append([wid, v0, v1])
        if ll[1]=="2":
            cid = int(ll[3])
            t = tuple(map(int, ll[-3:]))
            i0, i1, i2 = t
            tris.append((cid, t))
            nb_map[sort_pair(i0,i1)].append(t)
            nb_map[sort_pair(i1,i2)].append(t)
            nb_map[sort_pair(i2,i0)].append(t)
            

    new_walls = {}
    for wid, v0, v1 in lines:
        for v in (v0, v1):
            if v not in vertex_pids:
                if wid not in new_walls:
                    new_wall_pair, new_pid=insert_pt_in_wall(db, wid, Vector2(verts[v]))
                    new_walls[wid]=set(new_wall_pair)
                    vertex_pids[v]=new_pid
                else:
                    new_wall_dists=[]
                    for nwid in new_walls[wid]:
                        pid1, pid2, =mesh.borders(1, nwid)
                        new_wall_dists.append((nwid,dist_segment_pt(pos[pid1], pos[pid2], Vector2(verts[v]))))
                    new_wall_dists.sort(key=itemgetter(1))
                    nwid=new_wall_dists[0][0]
                    new_walls[wid].discard(nwid)
                    new_wall_pair, new_pid=insert_pt_in_wall(db, nwid, Vector2(verts[v]))
                    new_walls[wid].update(new_wall_pair)
                    vertex_pids[v]=new_pid
  
    cell_triangles=dict((cid,[]) for cid in mesh.wisps(2))
    for cid, t in tris:
        for v in t:
            if v not in vertex_pids:
                new_pid = mesh.add_wisp(0)
                pos[new_pid]=np.array(verts[v])
                vertex_pids[v]=new_pid
        cell_triangles[cid].append(tuple(int(vertex_pids[v]) for v in t))
    
    db.set_property('cell_triangles', cell_triangles)
    db.set_description('cell_triangles', 'triangulation of each cell')

    tri_neighbours = {}

    for cid, t in tris:
        nbs = []
        ti =  tuple(int(vertex_pids[i]) for i in t)

        l = set(nb_map[sort_pair(t[1],t[2])])
        l.remove(t)

#        print t, ti, l, set(nb_map[sort_pair(t[0],t[1])])

        if l:
            nbs.append(tuple(int(vertex_pids[i]) for i in l.pop()))
        else:
            nbs.append(None)

        l = set(nb_map[sort_pair(t[2],t[0])])
        l.remove(t)
        if l:
            nbs.append(tuple(int(vertex_pids[i]) for i in l.pop()))
        else:
            nbs.append(None)

        l = set(nb_map[sort_pair(t[0],t[1])])
        l.remove(t)
        if l:
            nbs.append(tuple(int(vertex_pids[i]) for i in l.pop()))
        else:
            nbs.append(None)

        tri_neighbours[ti]=nbs

    db.set_property('tri_neighbours', tri_neighbours)
    db.set_description('tri_neighbours', '')
    
    print 'tri_neighbours - gmsh'
    for t, nt in tri_neighbours.iteritems():
        for tt in nt:
            if tt is not None:
                assert t in tri_neighbours[tt]


def write_gmsh(db, f, cid_list= None):

    mesh=get_mesh(db)
    pos=db.get_property('position')
    pos_list=[]
    pid_map={}
    idx_to_pid=[]

    if cid_list:
        wid_set = set()
        for cid in cid_list:
            wid_set.update(mesh.borders(2, cid))
        wid_list = wid_set
        pid_set = set()
        for wid in wid_list:
            pid_set.update(mesh.borders(1, wid))
        pid_list = pid_set
    else:
        cid_list = mesh.wisps(2)
        wid_list = mesh.wisps(1)
        pid_list = mesh.wisps(0)

    for j,pid in enumerate(pid_list):
        pid_map[pid]=j
        pos_list.append(pos[pid])
        idx_to_pid.append(pid)

    for i, x in enumerate(pos_list):
        f.write("Point("+str(i+1)+")= { %f, %f, %f };"%(x[0], x[1], 0)+"\n")
        f.write("Physical Point("+str(idx_to_pid[i])+")= {"+str(i+1)+"};\n")

    wid_map = {}

    

    for j, wid in enumerate(wid_list):
        wid_map[wid]=j+10000
        pid0, pid1 = mesh.borders(1, wid)
        f.write("Line ("+str(wid_map[wid])+") = {"+str(pid_map[pid0]+1)+", "+str(pid_map[pid1]+1)+"};\n")
        f.write("Physical Line ("+str(wid)+") = {"+str(wid_map[wid])+"};\n")
    

    for j, cid in enumerate(cid_list):
        wids, pids = ordered_wids_pids(mesh, pos, cid)
        print area([pos[p] for p in pids])

        idx = 100000+j
        f.write("Line Loop ("+str(idx)+") = {")
        start = True
        for k, wid in enumerate(wids):
            pid0, pid1 = mesh.borders(1, wid)
            if not start:
                f.write(", ")
            if pid0==pids[k]:
                f.write(str(wid_map[wid]))
            else:
                f.write(str(-(wid_map[wid])))
            start = False

        f.write("};\n")
        f.write("Plane Surface("+str(idx)+") = {"+str(idx)+"};\n")
        f.write("Physical Surface("+str(cid)+") = {"+str(idx)+"};\n")
    f.flush()
    f.close()



def d2(x):
    return np.dot(x,x)


def barycentric_coords(pos, tris, tri_neighbours, x):
    # pos - array of points (numpy 2-vectors)
    # data - array of data, defined at each point
    # tris - array of triangle vertex indices
    # x - location of point (numpy 2-vectors)

    m = []
    for i in range(5):
        t = random.choice(tris)
        d = dist(1.0/3.0*(pos[t[0]]+pos[t[1]]+pos[t[2]]), x)
        m.append((d,t))
    start = min(m)[1]
    tri_found = False
    to_visit = set(tris)
    j = 0
    while not tri_found:
        dirs = []
        tri_found=True
        to_visit.remove(start)
        if ccw(x, pos[start[0]], pos[start[1]])==-1:
            tri_found=False
            # On right of line through first two points
            nt = tri_neighbours[start][2]
            if(nt!=None and nt in tris and nt in to_visit):
                dirs.append(nt)
        if ccw(x, pos[start[1]], pos[start[2]])==-1:
            tri_found=False
            # On right of line through first two points
            nt = tri_neighbours[start][0]
            if(nt!=None and nt in tris and nt in to_visit):
                dirs.append(nt)
        if ccw(x, pos[start[2]], pos[start[0]])==-1:
            tri_found=False
            # On right of line through first two points
            nt = tri_neighbours[start][1]
            if(nt!=None and nt in tris and nt in to_visit):
                dirs.append(nt)
        if tri_found:
            break
        if not to_visit:
            tri_found=False
            break
        if dirs:
            start=dirs[0]#random.choice(dirs)
        else:
            j+=1
            start=random.sample(to_visit,1)[0]
    if tri_found:
        t0, t1, t2 = start
        d=float((pos[t1][1]-pos[t2][1])*(pos[t0][0]-pos[t2][0])+(pos[t2][0]-pos[t1][0])*(pos[t0][1]-pos[t2][1]))
        l1=((pos[t1][1]-pos[t2][1])*(x[0]-pos[t2][0])+(pos[t2][0]-pos[t1][0])*(x[1]-pos[t2][1]))/d
        l2=((pos[t2][1]-pos[t0][1])*(x[0]-pos[t2][0])+(pos[t0][0]-pos[t2][0])*(x[1]-pos[t2][1]))/d
        l3=1.0-l1-l2
        return t0, t1, t2, l1, l2, l3
    else:
        print "TRI NOT FOUND"
        print x
        for t0, t1, t2 in tris:
            if pt_in_tri((pos[t0], pos[t1], pos[t2]), x):
                break
        else:
            print "NOT IN ANY TRI"
            dists = []
            for t0, t1, t2 in tris:
                dists.append(d2(x-(pos[t0]+pos[t1]+pos[t2])/3.0))
            i = dists.index(min(dists))
            t0, t1, t2 = tris[i]
        d=float((pos[t1][1]-pos[t2][1])*(pos[t0][0]-pos[t2][0])+(pos[t2][0]-pos[t1][0])*(pos[t0][1]-pos[t2][1]))
        l1=((pos[t1][1]-pos[t2][1])*(x[0]-pos[t2][0])+(pos[t2][0]-pos[t1][0])*(x[1]-pos[t2][1]))/d
        l2=((pos[t2][1]-pos[t0][1])*(x[0]-pos[t2][0])+(pos[t0][0]-pos[t2][0])*(x[1]-pos[t2][1]))/d
        l3=1.0-l1-l2
        print l1, l2, l3
        return t0, t1, t2, l1, l2, l3

 
def insert_pt_in_wall(db, old_wid, p):
    # Insert a point at locatuion p in wall with wall id old_wid
    # Note that this does not consider the triangle mesh
    mesh=get_mesh(db)
    old_wall_regions=list(mesh.regions(1, old_wid))
    old_pid1, old_pid2 = mesh.borders(1, old_wid)
    # Also need to deal with the old graph edge
    wall = db.get_property('wall')
    graph=get_graph(db)
    old_eids=[]
    old_sources=[]
    old_targets=[]
    for eid in wall:
        if wall[eid] == old_wid:
            old_eids.append(eid)
            old_sources.append(graph.source(eid))
            old_targets.append(graph.target(eid))

    wid1, wid2, pid=topo_divide_edge(mesh, old_wid)
    pos=db.get_property('position')
    pos[pid]=p

    graph_lineage={}

    for old_eid in old_eids:
        del wall[old_eid]

    for old_eid, source, target in zip(old_eids, old_sources, old_targets):
        graph.remove_edge(old_eid)
        new_eid1=graph.add_edge(source, target)
        new_eid2=graph.add_edge(source, target)
        graph_lineage[old_eid]=[new_eid1, new_eid2]
        wall[new_eid1]=wid1
        wall[new_eid2]=wid2
        graph_lineage[old_eid]=[new_eid1, new_eid2]

    # And update the properties associated with wid ....
    if 'divided_props' in db.properties():
        for prop_name, rule in db.get_property('divided_props').iteritems():
            prop=db.get_property(prop_name)
            if rule[0]=='(cell, wall)':
                for old_cid in old_wall_regions:
                    mem=prop.pop((old_cid, old_wid))
                    if rule[1]!='amount':
                        for new_wid in (wid1, wid2):
                            prop[(old_cid, new_wid)]=mem
                    else:
                        total_length=sum(edge_length(mesh, pos, new_wid) for new_wid in (wid1, wid2))
                        for new_wid in (wid1, wid2):                                
                            prop[(old_cid, new_wid)]=mem*edge_length(mesh, pos, new_wid)/total_length
            elif rule[0]=='wall':
                mem=prop.pop(old_wid)
                if rule[1]!='amount':
                    for new_wid in (wid1, wid2):
                        prop[new_wid]=mem
                else:
                    raise
            elif rule[0]=='edge':
                for old_eid, new_eids in graph_lineage.iteritems():
                    if old_eid:
                        old_value=prop.pop(old_eid)
                    else:
                        old_value=rule[2]
                    for eid in new_eids:
                        prop[eid]=old_value
                
    return (wid1, wid2), pid


from vroot2D.utils.geom import ear_clip

def triangulate_mesh_ear_clip(db, vol_max='ignored'):
    mesh = get_mesh(db)
    pos = db.get_property('position')
    cell_triangles = {}
    for cid in mesh.wisps(2):
        wids, pids = ordered_wids_pids(mesh, pos, cid)
        poly = [pos[pid] for pid in pids]
        tris = ear_clip(poly)
        cell_triangles[cid] = [(pids[i0], pids[i1], pids[i2]) for i0,i1,i2 in tris]
    db.set_property('cell_triangles', cell_triangles)
    db.set_description('cell_triangles', 'triangulation of each cell')


def triangulate_mesh(db, vol_max=300):

    mesh=get_mesh(db)
    pos=db.get_property('position')
    
    pos_list=[]
    pid_map={}
    idx_to_pid=[]
    for pid in mesh.wisps(0):
        pid_map[pid]=len(pos_list)
        pos_list.append(pos[pid])
        idx_to_pid.append(pid)

    p_idx=[ [pid_map[pid] for pid in ordered_pids(mesh, cid)] for cid in mesh.wisps(2)]
    facets=[tuple(pid_map[pid] for pid in mesh.borders(1,wid)) for wid in mesh.wisps(1)]
    facet_markers=[]
    facet_markers=[wid+2 for wid in mesh.wisps(1)]
    info = triangle.MeshInfo()
    info.set_points(pos_list)
    info.set_facets(facets, facet_markers)
    regions=[]
    cell_cols=[]
    for cid in mesh.wisps(2):
        c=centroid([pos[pid] for pid in ordered_pids(mesh, cid)])
        regions.append([c[0], c[1], cid+1, 0.1])
    info.regions.resize(len(regions))
    for i in range(len(regions)):
        info.regions[i]=regions[i]
    triangle_output = triangle.build(info, attributes=True, allow_boundary_steiner=True, max_volume=vol_max)
    new_pts=np.asarray(triangle_output.points)
    new_elements=np.asarray(triangle_output.elements)
    new_facets=np.asarray(facets)
    p_markers=np.asarray(triangle_output.point_markers, dtype='int')
    el_markers=np.asarray(triangle_output.element_attributes,dtype='int')
    neighbors=list(triangle_output.neighbors)

    """
    Look carefully at the output of Triangle,
    and determine which are old pts (first points in the array), 
    new pts on the boundaries between cells, and new pts in cell 
    interiors
    """
    new_walls={}


    for i in range(len(pos_list), len(new_pts)):
        wid=int(p_markers[i]-2)# This type conversion seems to be needed
        v=new_pts[i]
        if not wid<0:
            if wid not in new_walls:
                new_wall_pair, new_pid=insert_pt_in_wall(db, wid, Vector2(v))
                new_walls[wid]=set(new_wall_pair)
                idx_to_pid.append(new_pid)
            else:
                # Find which new wall the point is really in!
                new_wall_dists=[]
                for nwid in new_walls[wid]:
                    pid1, pid2, =mesh.borders(1, nwid)
                    new_wall_dists.append((nwid,dist_segment_pt(pos[pid1], pos[pid2], v)))
                new_wall_dists.sort(key=itemgetter(1))
                nwid=new_wall_dists[0][0]
                new_walls[wid].discard(nwid)
                new_wall_pair, new_pid=insert_pt_in_wall(db, nwid, Vector2(v))
                new_walls[wid].update(new_wall_pair)
                idx_to_pid.append(new_pid)
        else:
            new_pid=mesh.add_wisp(0)
            pos[new_pid]=np.array(v)
            idx_to_pid.append(new_pid)
            
    """ Now add list of triangles to each cell """

    cell_triangles=dict((cid,[]) for cid in mesh.wisps(2))
    tri_neighbours={}
    for n,t in enumerate(new_elements):
        try:
            cid = el_markers[n]-1
            print cid
            cell_triangles[cid].append(tuple(int(idx_to_pid[i]) for i in t))
        except IndexError:
            pass

    for t, tt in zip(new_elements, neighbors):
        ti =  tuple(int(idx_to_pid[i]) for i in t)
        nbs = [  tuple(int(idx_to_pid[i]) for i in new_elements[j]) if j!=-1 else None for j in tt]
#       print ti, nbs
        tri_neighbours[ti]=nbs

    db.set_property('cell_triangles', cell_triangles)
    db.set_description('cell_triangles', 'triangulation of each cell')
    db.set_property('tri_neighbours', tri_neighbours)
    db.set_description('tri_neighbours', '')

    print "tri_neighbours meshpy"
    for t, nt in tri_neighbours.iteritems():
        for tt in nt:
            if tt is not None:
                assert t in tri_neighbours[tt]


def find_containing_tri(db, cid, x):
    pos = db.get_property('position')
    cell_triangles = db.get_property('cell_triangles')
    for t in cell_triangles[cid]:
        if pt_in_tri([pos[pid] for pid in t], x):
            return t

def find_containing_cell(db, x):
    mesh = get_mesh(db)
    pos = db.get_property('position')
    for cid in mesh.wisps(2):
        wids, pids = ordered_wids_pids(mesh, pos, cid)
        if pt_in_polygon([pos[pid] for pid in pids], x):
            return cid
    return None

def bary_inverse(p, x0, x1, x2):
    lambda0 = ((x1[1]-x2[1])*(p[0]-x2[0])+(x2[0]-x1[0])*(p[1]-x2[1]))/ \
        ((x1[1]-x2[1])*(x0[0]-x2[0])+(x2[0]-x1[0])*(x0[1]-x2[1]))
    lambda1 = ((x2[1]-x0[1])*(p[0]-x2[0])+(x0[0]-x2[0])*(p[1]-x2[1]))/ \
        ((x1[1]-x2[1])*(x0[0]-x2[0])+(x2[0]-x1[0])*(x0[1]-x2[1]))
    lambda2 = 1.0 - lambda0 - lambda1
    return lambda0, lambda1, lambda2


def remesh_cells(db, old_cids, cell_lineage, old_pos, vol_max=300): 

    print 'remesh_cells', cell_lineage

    new_cids = []
    for cid in old_cids:
        if cid in cell_lineage:
            new_cids.extend(cell_lineage[cid])
        else:
            new_cids.append(cid)

    mesh=get_mesh(db)
    pos=db.get_property('position')
    cell_triangles=db.get_property('cell_triangles')
    tri_ref_pos=db.get_property('tri_ref_pos')
    tri_neighbours=db.get_property('tri_neighbours')



    if 'midline_pids' in db.properties():
        midline_pids = db.get_property('midline_pids')
        midline = [ d[0]*pos[t[0]]+d[1]*pos[t[1]]+d[2]*pos[t[2]] for t, d in midline_pids ]

    old_triangles=dict((cid, cell_triangles[cid]) for cid in old_cids)
    old_points=set(pid for cid in old_cids for t in cell_triangles[cid] for pid in t)
    internal_pts=set(pid for pid in old_points if mesh.nb_regions(0, pid)==0)

    pids=set(pid for cid in new_cids for pid in mesh.borders(2, cid, 2))
    wids=set(wid for cid in new_cids for wid in mesh.borders(2, cid))

    def sort_remove(t, i):
        l = list(t)
        del l[i]
        if l[0]<l[1]:
            return tuple(l)
        else:
            return (l[1],l[0])

    border_triangles = {}
#    print tri_neighbours
    cont_triangles = set(itertools.chain(*old_triangles.itervalues()))
    for t in cont_triangles:
        for i in range(3):
            nt = tri_neighbours[t][i]
            if nt and (nt not in cont_triangles):
#                print t,i, tri_neighbours[t][i]
                border_triangles[sort_remove(t,i)]=(nt,tri_neighbours[nt].index(t))
#                print t,i, tri_neighbours[t][i], (nt,tri_neighbours[nt].index(t))
 
#    print 'border_triangles', border_triangles

   

    idx_to_pid={}
    pos_list=[]
    pid_map={}
    for i, pid in enumerate(pids):
        pid_map[pid]=len(pos_list)
        pos_list.append(pos[pid])
        idx_to_pid[i]=pid

    facets=[tuple(map(pid_map.get, mesh.borders(1,wid))) for wid in wids]
    facet_markers=[int(wid+2) for wid in wids]

    info = triangle.MeshInfo()
    info.set_points(pos_list)
    info.set_facets(facets, facet_markers)
    regions=[]
    cell_cols=[]
    for cid in new_cids:
        c=centroid([pos[pid] for pid in ordered_pids(mesh, cid)]) 
#       c=centroid([pos[pid] for pid in cell_triangles[cid][0]])
        regions.append([c[0], c[1], cid+1, 0.1])

    info.regions.resize(len(regions))
    for i in range(len(regions)):
        info.regions[i]=regions[i]

    triangle_output = triangle.build(info, attributes=True, allow_boundary_steiner=False, max_volume=vol_max)

    new_pts=np.asarray(triangle_output.points)
    new_elements=np.asarray(triangle_output.elements)
    new_facets=np.asarray(facets)
    p_markers=np.asarray(triangle_output.point_markers, dtype='int')
    el_markers=np.asarray(triangle_output.element_attributes,dtype='int')


    neighbors=list(triangle_output.neighbors)

    """
    Now need to look carefully at the output of Triangle,
    and determine which are old pts, new pts on the boundaries 
    between cells, and which are new pts in cell interiors
    """
    new_walls={}
    for i in range(len(pos_list), len(new_pts)):
        wid=int(p_markers[i]-2)
        v=new_pts[i]
        if wid>0:
            if wid not in new_walls:
                new_wall_pair, new_pid=insert_pt_in_wall(db, wid, Vector2(v))
                new_walls[wid]=set(new_wall_pair)
                idx_to_pid[i]=new_pid
            else:
                new_wall_dists=[]
                for nwid in new_walls[wid]:
                    pid1, pid2, =mesh.borders(1, nwid)
                    new_wall_dists.append((nwid,dist_segment_pt(pos[pid1], pos[pid2], v)))
                new_wall_dists.sort(key=itemgetter(1))
                nwid=new_wall_dists[0][0]
                new_walls[wid].discard(nwid)
                new_wall_pair, new_pid=insert_pt_in_wall(db, nwid, Vector2(v))
                new_walls[wid].update(new_wall_pair)
                idx_to_pid[i]=new_pid



    for cid in old_cids:
        del cell_triangles[cid]

    for cid in new_cids:
        cell_triangles[cid]=[]
    
    vels = db.get_property('velocity')

    for old_cid in old_cids:
        old_ref_pos={}
        for t in old_triangles[old_cid]:
            for i, pid in enumerate(t):
                old_ref_pos[pid]=np.array(tri_ref_pos[(old_cid,t)][2*i:2*i+2])

        for cid, t in list(tri_ref_pos.iterkeys()):
            if cid==old_cid:
                del tri_ref_pos[(cid, t)]

        new_ref_pos = old_ref_pos
    
        for n,t in enumerate(new_elements):
            cid=el_markers[n]-1
            if cid in cell_lineage.get(old_cid, [old_cid]):
                for i in t:
                    if i not in idx_to_pid:
                        new_pid=mesh.add_wisp(0)
                        pos[new_pid]=Vector2(new_pts[i])
                        idx_to_pid[i]=new_pid 
                t_pids=tuple(idx_to_pid[i] for i in t)
                cell_triangles[cid].append(t_pids)
                ref_pos=[]
                if len(t)!=3:
                    raise
                for j in t:
                    v=new_pts[j]
                    pid = idx_to_pid[j]
                    if pid not in new_ref_pos:
                        pid1, pid2, pid3, l1, l2, l3 = barycentric_coords(old_pos, old_triangles[old_cid], tri_neighbours, v)
                        new_ref_pos[pid] = old_ref_pos[pid1]*l1+old_ref_pos[pid2]*l2+old_ref_pos[pid3]*l3
                        vels[pid] = vels[pid1]*l1+vels[pid2]*l2+vels[pid3]*l3
                    ref_pos += list(new_ref_pos[pid])
                tri_ref_pos[(cid, t_pids)]=ref_pos

    # make this work with partial remeshing .....
    for t in cont_triangles:
        del tri_neighbours[t]

    for t, tt in zip(new_elements, neighbors):
        ti =  tuple(int(idx_to_pid[i]) for i in t)
        nbs = []
        for k in range(3):
            j=tt[k]
            if j!=-1:
                nbs.append(tuple(int(idx_to_pid[i]) for i in new_elements[j]))
            else:
                nw = sort_remove(ti,k)
#                print 'nw ', nw
                #print border_triangles
                if nw in border_triangles:
                    d = border_triangles[nw]
#                    print 'ti: ', ti, ' d: ', d
                    nbs.append(d[0])
                    tri_neighbours[d[0]][d[1]]=ti
                else:
                    nbs.append(None)

        tri_neighbours[ti]=nbs


    for pid in internal_pts:
        mesh.remove_wisp(0, pid)
        del pos[pid]
        del vels[pid]

    if 'midline_pids' in db.properties():
        midline_pids = db.get_property('midline_pids')
        for i in range(len(midline_pids)):
            p = midline[i]
            cid = find_containing_cell(db, p)
            if cid:
                t = find_containing_tri(db, cid, p)
                if t:
                    b = bary_inverse(p, pos[t[0]], pos[t[1]], pos[t[2]])
                    midline_pids[i]=(t,b)

    # Check tri_neighbours
    print "tri_neighbours remesh cells meshpy"
    for t, nt in tri_neighbours.iteritems():
        for tt in nt:
            if tt is not None:
                assert t in tri_neighbours[tt]

def remesh_all(db, vol_max=300): 

    mesh=get_mesh(db)
    pos=db.get_property('position')

    cell_triangles=db.get_property('cell_triangles')
    tri_ref_pos=db.get_property('tri_ref_pos')
    tri_neighbours=db.get_property('tri_neighbours')

    if 'midline_pids' in db.properties():
        midline_pids = db.get_property('midline_pids')
        midline = [ d[0]*pos[t[0]]+d[1]*pos[t[1]]+d[2]*pos[t[2]] for t, d in midline_pids ]


    new_cids = [cid for cid in mesh.wisps(2)]
    old_cids = new_cids




    old_triangles=copy(cell_triangles)
    old_points=set(pid for cid in old_cids for t in cell_triangles[cid] for pid in t)
    internal_pts=set(pid for pid in old_points if mesh.nb_regions(0, pid)==0)

    pids=set(pid for cid in new_cids for pid in mesh.borders(2, cid, 2))
    wids=set(wid for cid in new_cids for wid in mesh.borders(2, cid))


    idx_to_pid={}
    pos_list=[]
    pid_map={}
    for i, pid in enumerate(pids):
        pid_map[pid]=len(pos_list)
        pos_list.append(pos[pid])
        idx_to_pid[i]=pid

    facets=[tuple(map(pid_map.get, mesh.borders(1,wid))) for wid in wids]
    facet_markers=[int(wid+2) for wid in wids]

    info = triangle.MeshInfo()
    info.set_points(pos_list)
    info.set_facets(facets, facet_markers)
    regions=[]
    cell_cols=[]
    for cid in new_cids:
        c=centroid([pos[pid] for pid in cell_triangles[cid][0]])
        regions.append([c[0], c[1], cid+1, 0.1])
    info.regions.resize(len(regions))
    for i in range(len(regions)):
        info.regions[i]=regions[i]

    triangle_output = triangle.build(info, attributes=True, allow_boundary_steiner=True, max_volume=vol_max)

    new_pts=np.asarray(triangle_output.points)
    new_elements=np.asarray(triangle_output.elements)
    new_facets=np.asarray(facets)
    p_markers=np.asarray(triangle_output.point_markers, dtype='int')
    el_markers=np.asarray(triangle_output.element_attributes,dtype='int')

    neighbors=list(triangle_output.neighbors)

    """
    Now need to look carefully at the output of Triangle,
    and determine which are old pts, new pts on the boundaries 
    between cells, and which are new pts in cell interiors
    """
    new_walls={}
    for i in range(len(pos_list), len(new_pts)):
        wid=int(p_markers[i]-2)
        v=new_pts[i]
        if wid>0:
            if wid not in new_walls:
                new_wall_pair, new_pid=insert_pt_in_wall(db, wid, Vector2(v))
                new_walls[wid]=set(new_wall_pair)
                idx_to_pid[i]=new_pid
            else:
                new_wall_dists=[]
                for nwid in new_walls[wid]:
                    pid1, pid2, =mesh.borders(1, nwid)
                    new_wall_dists.append((nwid,dist_segment_pt(pos[pid1], pos[pid2], v)))
                new_wall_dists.sort(key=itemgetter(1))
                nwid=new_wall_dists[0][0]
                new_walls[wid].discard(nwid)
                new_wall_pair, new_pid=insert_pt_in_wall(db, nwid, Vector2(v))
                new_walls[wid].update(new_wall_pair)
                idx_to_pid[i]=new_pid



    for cid in old_cids:
        del cell_triangles[cid]

    for cid in new_cids:
        cell_triangles[cid]=[]
    
    vels = db.get_property('velocity')

    for old_cid in old_cids:
        old_ref_pos={}

        for t in old_triangles[old_cid]:
            for i, pid in enumerate(t):
                old_ref_pos[pid]=np.array(tri_ref_pos[(old_cid,t)][2*i:2*i+2])

        for cid, t in list(tri_ref_pos.iterkeys()):
            if cid==old_cid:
                del tri_ref_pos[(cid, t)]

        new_ref_pos = old_ref_pos
    
        for n,t in enumerate(new_elements):
            cid=el_markers[n]-1
            if cid==old_cid:
                for i in t:
                    if i not in idx_to_pid:
                        new_pid=mesh.add_wisp(0)
                        pos[new_pid]=Vector2(new_pts[i])
                        idx_to_pid[i]=new_pid 
                t_pids=tuple(idx_to_pid[i] for i in t)
                cell_triangles[cid].append(t_pids)
                ref_pos=[]
                if len(t)!=3:
                    raise
                for j in t:
                    v=new_pts[j]
                    pid = idx_to_pid[j]
                    if pid not in new_ref_pos:
                        pid1, pid2, pid3, l1, l2, l3 = barycentric_coords(pos, old_triangles[old_cid], tri_neighbours, v)
                        new_ref_pos[pid] = old_ref_pos[pid1]*l1+old_ref_pos[pid2]*l2+old_ref_pos[pid3]*l3
                        vels[pid] = vels[pid1]*l1+vels[pid2]*l2+vels[pid3]*l3
                    ref_pos += list(new_ref_pos[pid])
                tri_ref_pos[(cid, t_pids)]=ref_pos


    tri_neighbours.clear()
    for t, tt in zip(new_elements, neighbors):
        ti =  tuple(int(idx_to_pid[i]) for i in t)
        nbs = [ tuple(int(idx_to_pid[i]) for i in new_elements[j]) if j!=-1 else None for j in tt]
        tri_neighbours[ti]=nbs


    for pid in internal_pts:
        mesh.remove_wisp(0, pid)
        del pos[pid]
        del vels[pid]

    if 'midline_pids' in db.properties():
        midline_pids = db.get_property('midline_pids')
        for i in range(len(midline_pids)):
            p = midline[i]
            cid = find_containing_cell(db, p)
            if cid:
                t = find_containing_tri(db, cid, p)
                if t:
                    b = bary_inverse(p, pos[t[0]], pos[t[1]], pos[t[2]])
                    midline_pids[i]=(t,b)

    print "tri_neighbours remesh cells meshpy"
    for t, nt in tri_neighbours.iteritems():
        for tt in nt:
            if tt is not None:
                assert t in tri_neighbours[tt]
 

def remesh_all_gmsh(db, vol_max=300):

    mesh=get_mesh(db) 
    pos = db.get_property('position')

    internal_pts=set(pid for pid in pos if mesh.nb_regions(0, pid)==0)

    cell_triangles=db.get_property('cell_triangles')
    tri_ref_pos=db.get_property('tri_ref_pos')
    tri_neighbours=db.get_property('tri_neighbours')

    if 'midline_pids' in db.properties():
        midline_pids = db.get_property('midline_pids')
        midline = [ d[0]*pos[t[0]]+d[1]*pos[t[1]]+d[2]*pos[t[2]] for t, d in midline_pids ]

    vels = db.get_property('velocity')
    
    f = open("test.geo","w")
    write_gmsh(db, f)
    os.system("gmsh test.geo -2 -clmax "+str(vol_max))
    f.close()
    f = open("test.msh","r")
    l = f.readline
    while l:
        l = f.readline()
        if l == "$Nodes\n":
            break
    l = f.readline()
    nodes = int(l)
    verts = {}
    for i in range(nodes):
        l = f.readline().split()
        verts[int(l[0])]=tuple(map(float, l[1:3]))
    while l:
        l = f.readline()
        if l == "$Elements\n":
            break

    nb_map = defaultdict(list)
    elements = int(f.readline())
    vertex_pids = {}
    lines = []
    tris = []
    for i in range(elements):
        ll = f.readline().split()
        if ll[1]=="15": # Vertex
            vertex_pids[int(ll[4])] = int(ll[3])
        if ll[1]=="1": #line
            wid = int(ll[3])
            v0 = int(ll[-2])
            v1 = int(ll[-1])
            lines.append([wid, v0, v1])
        if ll[1]=="2":
            cid = int(ll[3])
            t = tuple(map(int, ll[-3:]))
            i0, i1, i2 = t
            tris.append((cid, t))
            nb_map[sort_pair(i0,i1)].append(t)
            nb_map[sort_pair(i1,i2)].append(t)
            nb_map[sort_pair(i2,i0)].append(t)


    new_walls = {}
    for wid, v0, v1 in lines:
        for v in (v0, v1):
            if v not in vertex_pids:
                if wid not in new_walls:
                    new_wall_pair, new_pid=insert_pt_in_wall(db, wid, Vector2(verts[v]))
                    new_walls[wid]=set(new_wall_pair)
                    vertex_pids[v]=new_pid
                else:
                    new_wall_dists=[]
                    for nwid in new_walls[wid]:
                        pid1, pid2, =mesh.borders(1, nwid)
                        new_wall_dists.append((nwid,dist_segment_pt(pos[pid1], pos[pid2], Vector2(verts[v]))))
                    new_wall_dists.sort(key=itemgetter(1))
                    nwid=new_wall_dists[0][0]
                    new_walls[wid].discard(nwid)
                    new_wall_pair, new_pid=insert_pt_in_wall(db, nwid, Vector2(verts[v]))
                    new_walls[wid].update(new_wall_pair)
                    vertex_pids[v]=new_pid

    vels = db.get_property('velocity')

    old_cids = list(mesh.wisps(2))

    for old_cid in old_cids:
        old_ref_pos={}
        old_triangles = list(cell_triangles[old_cid])
        cell_triangles[old_cid]=[]
        for t in old_triangles:
            for i, pid in enumerate(t):
                old_ref_pos[pid]=np.array(tri_ref_pos[(old_cid,t)][2*i:2*i+2])

        for cid, t in list(tri_ref_pos.iterkeys()):
            if cid==old_cid:
                del tri_ref_pos[(cid, t)]

        new_ref_pos = old_ref_pos
    
        for cid,t in tris:
            if cid==old_cid:
                for i in t:
                    if i not in vertex_pids:
                        new_pid=mesh.add_wisp(0)
                        pos[new_pid]=Vector2(verts[i])
                        vertex_pids[i]=new_pid 
                t_pids=tuple(vertex_pids[i] for i in t)
                cell_triangles[cid].append(t_pids)
                ref_pos=[]
                if len(t)!=3:
                    raise
                for pid in t_pids:
                    v=pos[pid]
                    if pid not in new_ref_pos:
                        pid1, pid2, pid3, l1, l2, l3 = barycentric_coords(pos, old_triangles, tri_neighbours, v)
                        new_ref_pos[pid] = old_ref_pos[pid1]*l1+old_ref_pos[pid2]*l2+old_ref_pos[pid3]*l3
                        vels[pid] = vels[pid1]*l1+vels[pid2]*l2+vels[pid3]*l3
                    ref_pos += list(new_ref_pos[pid])
                tri_ref_pos[(cid, t_pids)]=ref_pos



    for pid in internal_pts:
        mesh.remove_wisp(0, pid)
        del pos[pid]
        del vels[pid]

    if 'midline_pids' in db.properties():
        for i in range(len(midline_pids)):
            p = midline[i]
            cid = find_containing_cell(db, p)
            if cid:
                t = find_containing_tri(db, cid, p)
                if t:
                    b = bary_inverse(p, pos[t[0]], pos[t[1]], pos[t[2]])
                    midline_pids[i]=(t,b)


    tri_neighbours.clear()
    for cid, t in tris:
        nbs = []
        ti =  tuple(int(vertex_pids[i]) for i in t)

        l = set(nb_map[sort_pair(t[0],t[1])])
        l.remove(t)
        if l:
            nbs.append(tuple(int(vertex_pids[i]) for i in l.pop()))
        else:
            nbs.append(None)

        l = set(nb_map[sort_pair(t[1],t[2])])
        l.remove(t)
        if l:
            nbs.append(tuple(int(vertex_pids[i]) for i in l.pop()))
        else:
            nbs.append(None)

        l = set(nb_map[sort_pair(t[2],t[0])])
        l.remove(t)
        if l:
            nbs.append(tuple(int(vertex_pids[i]) for i in l.pop()))
        else:
            nbs.append(None)

        tri_neighbours[ti]=nbs
     
    print "tri_neighbours remesh all gmsh"
    for t, nt in tri_neighbours.iteritems():
        for tt in nt:
            if tt is not None:
                assert t in tri_neighbours[tt]

