
# Calculation of max strain rate direction for growing cell
import numpy as np
import scipy.linalg as la

from vroot2D.utils.db_utilities import cell_walls, get_mesh, refresh_property, \
    get_parameters, get_graph, add_property, iter_prop_type, set_parameters

def E(x0, x1, x2):
    u0 = u2 = np.array(((0.,0.),))
    u1 = np.array(((1,0),))
    grad = np.hstack(((u1-u0).T, (u2-u0).T)).dot(F1_inv(x0, x1, x2))
    return 0.5*(grad+grad.transpose())


def max_strain(db, cid):
    mesh = get_mesh(db)
    
    pos = db.get_property('position')
    vels = db.get_property('velocity')

    cell_triangles = db.get_property('cell_triangles')
    
    EA_tot = np.zeros((2,2))
    A_tot = 0.0
    for t in cell_triangles[cid]:
        x0 = pos[t[0]]
        x1 = pos[t[1]]
        x2 = pos[t[2]]
        u0 = vels[t[0]]
        u1 = vels[t[1]]
        u2 = vels[t[2]]
        F1 = np.vstack(((x1-x0), (x2-x0))).T
        A = 0.5*la.det(F1)
        F1_inv = la.inv(F1)
        grad = np.vstack(((u1-u0), (u2-u0))).T.dot(F1_inv)
        EA_tot += A*0.5*(grad+grad.T)
        A_tot += A
    E = EA_tot/A_tot
    print E
    eigenValues,eigenVectors = la.eig(E)
    imax = np.argmax(eigenValues)#.argsort)
    return eigenVectors[:,imax]
                         
