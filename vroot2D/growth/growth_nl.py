

"""
Module to handle tissue growth
"""

from openalea.tissueshape import edge_length
from vroot2D.utils.geom import area
import numpy as np
#from vplants.plantgl.math import Vector2
import itertools
from vroot2D.utils.db_utilities import cell_walls, get_mesh, refresh_property, \
    get_parameters, get_graph, add_property, iter_prop_type, set_parameters
from vroot2D.growth.mesh_nbs import triangulate_mesh as triangulate_mesh
from vroot2D.growth.mesh_nbs import remesh_all as remesh_all
from openalea.tissueshape import face_surface_2D, edge_length
from collections import defaultdict
from vroot2D.utils.db_geom import wall_angle, wall_angle_ordered, ordered_wids_pids
import module_tri_aj as module_tri_nl
from math import sin, cos, pi
#from mesh_nbs import remesh_all


from time import time

def extract_geometry(db):
    mesh = get_mesh(db) 
    pos = db.get_property('position')

    pid_map = {}
    pid_list = []
    for pid in iter_prop_type(db, 'point'):
        pid_map[pid] = len(pid_list)
        pid_list.append(pid)

    cell_pids = {}
    cell_wids = {}
    for cid in mesh.wisps(2):
        o_wids, o_pids = ordered_wids_pids(mesh, pos, cid)
        cell_wids[cid] = o_wids
        cell_pids[cid] = o_pids
    return pid_map, cell_pids, cell_wids


class SyntheticBCs(object):

    def get_fixed(self, db):
        fixed = {}
        mesh = get_mesh(db)
        cell_type = db.get_property('cell_type')
        border = db.get_property('border')
        wall_types = db.get_property('wall_types')
       # print border
        for cid in mesh.wisps(2):
            if border[cid]==6 or border[cid]==1:
                for wid in mesh.borders(2, cid):
                    if mesh.nb_regions(1, wid)==1 and wall_types[(cid,wid)] in [1,3]:
                        for pid in mesh.borders(1, wid):
                            fixed[pid]=1

        pos = db.get_property('position')
#        for pid in fixed:
#            print 'fixed:', pid, pos[pid]
#        quit()
        return fixed
        


class CrossBCs(object):

    def get_fixed(self, db):
        fixed = {}
        return fixed
        



class FullrootGrowthBCs(object):

    def get_fixed(self, db):
        fixed = {}
        mesh = get_mesh(db)
        cell_type = db.get_property('cell_type')
        border = db.get_property('border')
        wall_types=db.get_property('wall_types')
       # print border
        for cid in mesh.wisps(2):
            if border[cid]==6 or border[cid]==1:
                for wid in mesh.borders(2, cid):
                    if mesh.nb_regions(1, wid)==1 and wall_types[(cid, wid)]==1:
                        for pid in mesh.borders(1, wid):
                            fixed[pid]=1

        pos = db.get_property('position')
#        for pid in fixed:
#            print 'fixed:', pid, pos[pid]
#        quit()
        return fixed
        

class FullrootGrowthModel(object):

    name = 'FullrootGrowthModel'

    def get_const_props(self):
        return [ ('spring_lambda', '(cell, wall)', 1),
                 ('spring_gamma', '(cell, wall)', 1),
                 ('spring_phiinv', '(cell, wall)', 1),
                 ('spring_tau', '(cell, wall)', 1),
                 ('spring_eps0', '(cell, wall)', 1),
                 ('turgor_pressure', 'cell', 1),
                 ('elastic_modulus', '(cell, tri)', 1),
                 ('mu1', '(cell, tri)', 1),
                 ('mu2', '(cell, tri)', 1),
                 ('mu3', '(cell, tri)', 1),
                 ('tau', '(cell, tri)', 1),
                 ('eps0', '(cell, tri)', 1),
                 ('A0', '(cell, tri)', 2),
                 ('A1', '(cell, tri)', 2),
                 ('tri_ref_pos', '(cell, tri)', 6) ]

    def get_var_props(self):
        return [ ('position', 'point', 2),
                 ('spring_length', '(cell, wall)', 1)]

    def get_uniform_props(self):
        return [ 'vertex_drag', 'relaxation_time', 'initial_timestep' ]

    def default_parameters(self):

        """
        p = { 'spring_lambda': 0.0,
              'spring_gamma': 0.0,
              'spring_phiinv': 3.0,
              'cross_lambda': 100.0,
              'cross_gamma': 0.0,
              'cross_phiinv': 0.1,
              'spring_tau': 0.0,
              'spring_eps0': 0.05,
              'turgor_pressure': 0.3,
              'elastic_modulus': 0.0,
              'relaxation_time': 0.1,
              'initial_timestep': 0.1,
              'vertex_drag': 0.0,
              'mu1': 0.15,
              'mu2': 20.0,
              'mu3': 0.0,
              'tau': 0.0,
              'eps0': 0.05,
#              'mu1': 0.002,
#              'mu2': 0.0,
#              'mu3': 0.0,
              'fibre_angle': 0.0,
              'tri_ref_pos': [0]*6,
              'bend': 0.13,
              'mesh_tri_area': 10.0,
              }
        """

        p = { 'spring_lambda': 0.0,
              'spring_gamma': 0.0,
              'spring_phiinv': 0.1,
              'cross_lambda': 100.0,
              'cross_gamma': 0.0,
              'cross_phiinv': 0.1,
              'spring_tau': 0.1,
              'spring_eps0': 0.05,
              'turgor_pressure': 0.3,
              'elastic_modulus': 0.0,
              'relaxation_time': 0.1,
              'initial_timestep': 0.02,
              'vertex_drag': 0.0,
              'mu1': 0.3,
              'mu2': 20.0,
              'mu3': 0.0,
              'tau': 0.1,
              'eps0': 0.05,
              'fibre_angle': 0.0,
              'tri_ref_pos': [0]*6,
              'bend': 0.14,
              'mesh_tri_area': 10.0,
              'kp': 1e-5,
              'np': 2,
              'ki': 1e-6,
              'ni': 5,
              }


        theta = p['fibre_angle']
        p['A0'] = (sin(theta), cos(theta))
        p['A1'] = (-sin(theta), cos(theta))
        return p

    def set_parameters(self, db):
        set_parameters(db, self.name, self.default_parameters())

    def set_initial_vars(self, db):
        mesh = get_mesh(db)
        pos = db.get_property('position')
        spring_length = db.get_property('spring_length')
        for cid, wid in cell_walls(mesh):
            spring_length[(cid, wid)] = edge_length(mesh, pos, wid)  

        spring_lambda = db.get_property('spring_lambda')
        spring_phiinv = db.get_property('spring_phiinv')
        spring_gamma = db.get_property('spring_gamma')
#        print spring_phiinv
        p = get_parameters(db, self.name)




        cl = p['cross_lambda']
        cp = p['cross_phiinv']
        cg = p['cross_gamma']
        sl = p['spring_lambda']
        sp = p['spring_phiinv']
        sg = p['spring_gamma']
        wall_types = db.get_property('wall_types')
        for wid, wt in wall_types.iteritems():
            if wt%2 == 1:
                p0, p1, = mesh.borders(1, wid[1])
 #               print wid, p0, p1, pos[p0], pos[p1]
                spring_lambda[wid] = cl
                spring_phiinv[wid] = cp
                spring_gamma[wid] = cg
            else:
                spring_lambda[wid] = sl
                spring_phiinv[wid] = sp
                spring_gamma[wid] = sg
 #       print spring_phiinv
 #       print spring_lambda
        tri_ref_pos = db.get_property('tri_ref_pos')
        cell_triangles = db.get_property('cell_triangles')
        for cid, tris in cell_triangles.iteritems():
            for t in tris:
                tri_ref_pos[(cid, t)] = list(itertools.chain(*(pos[pid] for pid in t)))
        db.set_property('tri_ref_pos', tri_ref_pos)
        db.set_description('tri_ref_pos','')
        self.set_visc(db)
            
    def set_visc(self, db):
        eps = 1e-4
        mesh = get_mesh(db)
        params = get_parameters(db, self.name)
        cell_type = db.get_property('cell_type')
        border = db.get_property('border')
        GP = db.get_property('GP')
        kp = params['kp']
        ki = params['ki']
        np = params['np']
        ni = params['ni']
        mu1 = db.get_property('mu1')
        mu1_default = params['mu1']
        tau = db.get_property('tau')
        tau_default = params['tau']
        spring_tau = db.get_property('spring_tau')
        spring_tau_default = params['spring_tau']
        cell_triangles = db.get_property('cell_triangles')
        for cid, tris in cell_triangles.iteritems():
            m = 1.0#/(1.0+(GP[cid]/kp)**np)+eps
            m2 = 1.0/(1.0+(kp/GP[cid])**np)
#            m2 = 1.0
####            m2 = (1.0+(GP[cid]/kp)**np)+eps
            if border[cid]==6 or border[cid]==1:
                m = 1000.0
                m2 = 1000.0
            for t in tris:
                mu1[(cid,t)]=mu1_default*m
                tau[(cid,t)]=tau_default*m2
#        print tau

        sp = params['spring_phiinv']
        spring_phiinv = db.get_property('spring_phiinv')
        wall_types = db.get_property('wall_types')
        for wid, wt in wall_types.iteritems():
            if wt%2 == 0:
                cid = wid[0]
#                m = (1.0+(GP[cid]/ki)**np) +eps
                m = 1.0
                m2 = 1.0/(1.0+(kp/GP[cid])**np)
#                m2 = 0.0
####                m2 = (1.0+(GP[cid]/kp)**np)+eps
                spring_phiinv[wid] = sp*m
                spring_tau[wid] = spring_tau_default*m2
            else:
                spring_phiinv[wid]=0
                spring_tau[wid]=0
                
 #       print spring_phiinv
 #       print spring_lambda

        

    def evolve(self, uniforms, arrays):        
        module_tri_nl.evolve_be(uniforms, arrays)


        

class EpidermalGrowthModel(object):

    name = 'FullrootGrowthModel'

    def get_const_props(self):
        return [ ('spring_lambda', '(cell, wall)', 1),
                 ('spring_gamma', '(cell, wall)', 1),
                 ('spring_phiinv', '(cell, wall)', 1),
                 ('spring_tau', '(cell, wall)', 1),
                 ('spring_eps0', '(cell, wall)', 1),
                 ('turgor_pressure', 'cell', 1),
                 ('elastic_modulus', '(cell, tri)', 1),
                 ('mu1', '(cell, tri)', 1),
                 ('mu2', '(cell, tri)', 1),
                 ('mu3', '(cell, tri)', 1),
                 ('tau', '(cell, tri)', 1),
                 ('eps0', '(cell, tri)', 1),
                 ('A0', '(cell, tri)', 2),
                 ('A1', '(cell, tri)', 2),
                 ('tri_ref_pos', '(cell, tri)', 6) ]

    def get_var_props(self):
        return [ ('position', 'point', 2),
                 ('spring_length', '(cell, wall)', 1)]

    def get_uniform_props(self):
        return [ 'vertex_drag', 'relaxation_time', 'initial_timestep' ]

    def default_parameters(self):


        p = { 'spring_lambda': 0.0,
              'spring_gamma': 0.0,
              'spring_phiinv': 0.1,
              'epidermis_phiinv': 0.1,
              'cross_lambda': 100.0,
              'cross_gamma': 0.0,
              'cross_phiinv': 0.1,
              'spring_tau': 0.1,
              'spring_eps0': 0.05,
              'turgor_pressure': 0.3,
              'elastic_modulus': 0.0,
              'relaxation_time': 0.1,
              'initial_timestep': 0.02,
              'vertex_drag': 0.0,
              'mu1': 0.3,
              'mu2': 20.0,
              'mu3': 0.0,
              'tau': 0.1,
              'eps0': 0.05,
              'fibre_angle': 0.0,
              'tri_ref_pos': [0]*6,
              'bend': 0.14,
              'mesh_tri_area': 1000.0,
              'kp': 1e-5,
              'np': 2,
              'ki': 1e-6,
              'ni': 5,
              }


        theta = p['fibre_angle']
        p['A0'] = (sin(theta), cos(theta))
        p['A1'] = (-sin(theta), cos(theta))
        return p

    def set_parameters(self, db):
        set_parameters(db, self.name, self.default_parameters())

    def set_initial_vars(self, db):
        mesh = get_mesh(db)
        pos = db.get_property('position')
        spring_length = db.get_property('spring_length')
        for cid, wid in cell_walls(mesh):
            spring_length[(cid, wid)] = edge_length(mesh, pos, wid)  

        cell_type=db.get_property('cell_type')
        spring_lambda = db.get_property('spring_lambda')
        spring_phiinv = db.get_property('spring_phiinv')
        spring_gamma = db.get_property('spring_gamma')
#        print spring_phiinv
        p = get_parameters(db, self.name)

        ep = p['epidermis_phiinv']
        sp = p['epidermis_phiinv']
        cl = p['cross_lambda']
        cp = p['cross_phiinv']
        cg = p['cross_gamma']
        sl = p['spring_lambda']
        sp = p['spring_phiinv']
        sg = p['spring_gamma']
        wall_types = db.get_property('wall_types')
        for wid, wt in wall_types.iteritems():
            if wt%2 == 1:
                p0, p1, = mesh.borders(1, wid[1])
 #               print wid, p0, p1, pos[p0], pos[p1]
                spring_lambda[wid] = cl
                spring_phiinv[wid] = cp
                spring_gamma[wid] = cg
            else:
                if cell_type[wid[0]]==1:
                    spring_phiinv[wid]=ep
                else:
                    spring_phiinv[wid]=sp
                spring_lambda[wid] = sl
                spring_phiinv[wid] = sp
                spring_gamma[wid] = sg
 #       print spring_phiinv
 #       print spring_lambda
        tri_ref_pos = db.get_property('tri_ref_pos')
        cell_triangles = db.get_property('cell_triangles')
        for cid, tris in cell_triangles.iteritems():
            for t in tris:
                tri_ref_pos[(cid, t)] = list(itertools.chain(*(pos[pid] for pid in t)))
        db.set_property('tri_ref_pos', tri_ref_pos)
        db.set_description('tri_ref_pos','')
        self.set_visc(db)
            
    def set_visc(self, db):
        eps = 1e-4
        mesh = get_mesh(db)
        params = get_parameters(db, self.name)
        cell_type = db.get_property('cell_type')
        border = db.get_property('border')
        GP = db.get_property('GP')
        kp = params['kp']
        ki = params['ki']
        np = params['np']
        ni = params['ni']
        mu1 = db.get_property('mu1')
        mu1_default = params['mu1']
        tau = db.get_property('tau')
        tau_default = params['tau']
        spring_tau = db.get_property('spring_tau')
        spring_tau_default = params['spring_tau']
        cell_triangles = db.get_property('cell_triangles')
        for cid, tris in cell_triangles.iteritems():
            m = 1.0#/(1.0+(GP[cid]/kp)**np)+eps
            m2 = 1.0/(1.0+(kp/GP[cid])**np)
#            m2 = (1.0+(GP[cid]/kp)**np)+eps
            if border[cid]==6 or border[cid]==1:
                m = 1000.0
                m2 = 1000.0
            for t in tris:
                mu1[(cid,t)]=mu1_default*m
                tau[(cid,t)]=tau_default*m2
#        print tau

        ep = params['epidermis_phiinv']
        sp = params['spring_phiinv']
        spring_phiinv = db.get_property('spring_phiinv')
        wall_types = db.get_property('wall_types')
        for wid, wt in wall_types.iteritems():
            if wt%2 == 0:
                cid = wid[0]
#                m = (1.0+(GP[cid]/ki)**np) +eps
                m = 1.0
                m2 = 1.0/(1.0+(kp/GP[cid])**np)
#                m2 = (1.0+(GP[cid]/kp)**np)+eps
#                spring_phiinv[wid] = sp*m
                if cell_type[wid[0]]==1:
                    spring_phiinv[wid]=ep
                else:
                    spring_phiinv[wid]=sp
                spring_tau[wid] = spring_tau_default*m2
            else:
                spring_phiinv[wid]=0
                spring_tau[wid]=0
                
 #       print spring_phiinv
 #       print spring_lambda

        

    def evolve(self, uniforms, arrays):        
        module_tri_nl.evolve_be(uniforms, arrays)

class SyntheticGrowthModel(object):

    name = 'SyntheticGrowthModel'

    def get_const_props(self):
        return [ ('spring_lambda', '(cell, wall)', 1),
                 ('spring_gamma', '(cell, wall)', 1),
                 ('spring_phiinv', '(cell, wall)', 1),
                 ('spring_tau', '(cell, wall)', 1),
                 ('spring_eps0', '(cell, wall)', 1),
                 ('turgor_pressure', 'cell', 1),
                 ('elastic_modulus', '(cell, tri)', 1),
                 ('mu1', '(cell, tri)', 1),
                 ('mu2', '(cell, tri)', 1),
                 ('mu3', '(cell, tri)', 1),
                 ('tau', '(cell, tri)', 1),
                 ('eps0', '(cell, tri)', 1),
                 ('A0', '(cell, tri)', 2),
                 ('A1', '(cell, tri)', 2),
                 ('tri_ref_pos', '(cell, tri)', 6) ]

    def get_var_props(self):
        return [ ('position', 'point', 2),
                 ('spring_length', '(cell, wall)', 1)]

    def get_uniform_props(self):
        return [ 'vertex_drag', 'relaxation_time', 'initial_timestep' ]

    def default_parameters(self):

        p = { 'spring_lambda': 0.0,
              'spring_gamma': 0.0,
              'spring_phiinv': 3.0,
              'cross_lambda': 100.0,
              'cross_gamma': 0.0,
              'cross_phiinv': 0.1,
              'spring_tau': 0.0,
              'spring_eps0': 0.05,
              'turgor_pressure': 0.3,
              'elastic_modulus': 0.0,
              'relaxation_time': 0.1,
              'initial_timestep': 0.1,
              'vertex_drag': 0.0,
              'mu1': 0.15,
              'mu2': 20.0,
              'mu3': 0.0,
              'tau': 0.0,
              'eps0': 0.05,
#              'mu1': 0.002,
#              'mu2': 0.0,
#              'mu3': 0.0,
              'fibre_angle': 0.0,
              'tri_ref_pos': [0]*6,
              'bend': 0.13,
              'mesh_tri_area': 10.0,
              }


        theta = p['fibre_angle']
        p['A0'] = (sin(theta), cos(theta))
        p['A1'] = (-sin(theta), cos(theta))
        return p

    def set_parameters(self, db):
        set_parameters(db, self.name, self.default_parameters())

    def set_initial_vars(self, db):
        mesh = get_mesh(db)
        pos = db.get_property('position')
        spring_length = db.get_property('spring_length')
        for cid, wid in cell_walls(mesh):
            spring_length[(cid, wid)] = edge_length(mesh, pos, wid)  

        spring_lambda = db.get_property('spring_lambda')
        spring_phiinv = db.get_property('spring_phiinv')
        spring_gamma = db.get_property('spring_gamma')
#        print spring_phiinv
        p = get_parameters(db, self.name)




        cl = p['cross_lambda']
        cp = p['cross_phiinv']
        cg = p['cross_gamma']
        sl = p['spring_lambda']
        sp = p['spring_phiinv']
        sg = p['spring_gamma']
        wall_types = db.get_property('wall_types')
        for wid, wt in wall_types.iteritems():
            if wt%2 == 1:
                p0, p1, = mesh.borders(1, wid[1])
 #               print wid, p0, p1, pos[p0], pos[p1]
                spring_lambda[wid] = cl
                spring_phiinv[wid] = cp
                spring_gamma[wid] = cg
            else:
                spring_lambda[wid] = sl
                spring_phiinv[wid] = sp
                spring_gamma[wid] = sg
 #       print spring_phiinv
 #       print spring_lambda
        tri_ref_pos = db.get_property('tri_ref_pos')
        cell_triangles = db.get_property('cell_triangles')
        for cid, tris in cell_triangles.iteritems():
            for t in tris:
                tri_ref_pos[(cid, t)] = list(itertools.chain(*(pos[pid] for pid in t)))
        db.set_property('tri_ref_pos', tri_ref_pos)
        db.set_description('tri_ref_pos','')
        self.set_visc(db)


    def set_visc(self, db):
        mesh = get_mesh(db)
        params = get_parameters(db, self.name)
        bend = params['bend']
        cell_type = db.get_property('cell_type')        
        cell_triangles = db.get_property('cell_triangles')
        mu1 = db.get_property('mu1')
        for cid, tris in cell_triangles.iteritems():
            if cell_type[cid]==1:
                for t in tris:
                    mu1[(cid,t)]=bend
            
        

    def evolve(self, uniforms, arrays):        
#        module_tri_nl.evolve_ida(uniforms, arrays)
        module_tri_nl.evolve_ida(uniforms, arrays)

class CrossGrowthModel(object):

    name = 'CrossGrowthModel'

    def get_const_props(self):
        return [ ('spring_lambda', '(cell, wall)', 1),
                 ('spring_gamma', '(cell, wall)', 1),
                 ('spring_phiinv', '(cell, wall)', 1),
                 ('spring_tau', '(cell, wall)', 1),
                 ('spring_eps0', '(cell, wall)', 1),
                 ('turgor_pressure', 'cell', 1),
                 ('elastic_modulus', '(cell, tri)', 1),
                 ('mu1', '(cell, tri)', 1),
                 ('mu2', '(cell, tri)', 1),
                 ('mu3', '(cell, tri)', 1),
                 ('tau', '(cell, tri)', 1),
                 ('eps0', '(cell, tri)', 1),
                 ('A0', '(cell, tri)', 2),
                 ('A1', '(cell, tri)', 2),
                 ('tri_ref_pos', '(cell, tri)', 6) ]

    def get_var_props(self):
        return [ ('position', 'point', 2),
                 ('spring_length', '(cell, wall)', 1)]

    def get_uniform_props(self):
        return [ 'vertex_drag', 'relaxation_time', 'initial_timestep' ]

    def default_parameters(self):

        p = { 'spring_lambda': 10000.0,
              'spring_gamma': 0.0,
              'spring_phiinv': 1.0,
              'cross_lambda': 100.0,
              'cross_gamma': 0.0,
              'cross_phiinv': 0.1,
              'spring_tau': 0.0,
              'spring_eps0': 0.05,
              'turgor_pressure': 0.1,
              'elastic_modulus': 0.0,
              'relaxation_time': 1.0,
              'initial_timestep': 1.0,
              'vertex_drag': 1.0,
              'mu1': 0.0,
              'mu2': 0.0,
              'mu3': 0.0,
              'tau': 0.0,
              'eps0': 0.05,
#              'mu1': 0.002,
#              'mu2': 0.0,
#              'mu3': 0.0,
              'fibre_angle': 0.0,
              'tri_ref_pos': [0]*6,
              'bend': 0.13,
              'mesh_tri_area': 10.0,
              }


        theta = p['fibre_angle']
        p['A0'] = (sin(theta), cos(theta))
        p['A1'] = (-sin(theta), cos(theta))
        return p

    def set_parameters(self, db):
        set_parameters(db, self.name, self.default_parameters())

    def set_initial_vars(self, db):
        mesh = get_mesh(db)
        pos = db.get_property('position')
        spring_length = db.get_property('spring_length')
        for cid, wid in cell_walls(mesh):
            spring_length[(cid, wid)] = edge_length(mesh, pos, wid)  

        spring_lambda = db.get_property('spring_lambda')
        spring_phiinv = db.get_property('spring_phiinv')
        spring_gamma = db.get_property('spring_gamma')
#        print spring_phiinv
        p = get_parameters(db, self.name)




        cl = p['cross_lambda']
        cp = p['cross_phiinv']
        cg = p['cross_gamma']
        sl = p['spring_lambda']
        sp = p['spring_phiinv']
        sg = p['spring_gamma']
        wall_types = db.get_property('wall_types')
        for wid, wt in wall_types.iteritems():
            spring_lambda[wid] = sl
            spring_phiinv[wid] = sp
            spring_gamma[wid] = sg
 #       print spring_phiinv
 #       print spring_lambda
        tri_ref_pos = db.get_property('tri_ref_pos')
        cell_triangles = db.get_property('cell_triangles')
        for cid, tris in cell_triangles.iteritems():
            for t in tris:
                tri_ref_pos[(cid, t)] = list(itertools.chain(*(pos[pid] for pid in t)))
        db.set_property('tri_ref_pos', tri_ref_pos)
        db.set_description('tri_ref_pos','')
        self.set_visc(db)


    def set_visc(self, db):
        mesh = get_mesh(db)
        params = get_parameters(db, self.name)
        bend = params['bend']
        cell_type = db.get_property('cell_type')        
        cell_triangles = db.get_property('cell_triangles')
        mu1 = db.get_property('mu1')
        for cid, tris in cell_triangles.iteritems():
            if cell_type[cid]==1:
                for t in tris:
                    mu1[(cid,t)]=bend
            
        

    def evolve(self, uniforms, arrays):        
#        module_tri_nl.evolve_ida(uniforms, arrays)
        module_tri_nl.evolve_be(uniforms, arrays)





class Growth(object):   
    """ Object to handle tissue growth """
    def __init__(self, db, param_changes={}, model=SyntheticGrowthModel(), bcs = SyntheticBCs(), use_old_vels=False):
        """
        Initialise growth-related properties in the TissueDB
        :param db: Tissue database 
        :type db: TissueDB
        """
        print 'GROWTH INIT'

        self.db = db
        self.model = model #FullrootGrowthModel()
        self.model.set_parameters(db)
        self.bcs = bcs #FullrootGrowthBCs()
        self.use_old_vels = use_old_vels
        
        
        params = get_parameters(db, self.model.name)
        params.update(param_changes)

        mesh = get_mesh(db)
        graph = get_graph(db)

        # Generate initial mesh triangulation
        triangulate_mesh(db, params['mesh_tri_area'])
      #  db.set_property('cell_triangles',{})



        pos = db.get_property('position')
        cell_type=db.get_property('cell_type')
        cell_triangles=db.get_property('cell_triangles')

        print 'TRIANGLES', sum(len(x) for x in cell_triangles.itervalues())

        divided_props=db.get_property('divided_props')



        model_const_props = self.model.get_const_props()


        self.var_props = defaultdict(list)
        self.const_props = defaultdict(list)
        self.var_prop_lens = defaultdict(int)
        self.const_prop_lens = defaultdict(int)

        for prop_name, prop_type, prop_len in model_const_props:
            if prop_name not in db.properties():
                add_property(db, prop_name, prop_type, params[prop_name])
            if prop_name not in divided_props:
                divided_props[prop_name] = ( prop_type, 'property',
                                             params[prop_name] )
            self.const_props[prop_type].append(
                (db.get_property(prop_name), prop_len))
            self.const_prop_lens[prop_type]+=prop_len

        model_var_props = self.model.get_var_props()
        for prop_name, prop_type, prop_len in model_var_props:
            if prop_name not in db.properties():
                add_property(db, prop_name, prop_type, 0.0)
            if prop_name not in divided_props and prop_name!='position':
                if prop_name == 'spring_length':
                    divided_props[prop_name] = ( prop_type, 'amount',
                                             0.0 )
                else:
                    divided_props[prop_name] = ( prop_type, 'property',
                                                 0.0 )
            self.var_props[prop_type].append(
                (db.get_property(prop_name), prop_len))
            self.var_prop_lens[prop_type]+=prop_len

        self.model.set_initial_vars(db)

        add_property(db, 'velocity', 'point', np.array((0,0)))

        

    def convert_to_mechanics_arrays(self):
        """
        Extract numpy arrays from the TissueDB, containing
        the topology and geometry of the mesh, the triangulation
        of the cell walls in the plane of the simulation, and all
        the mechanical properties of the tissue.
        Used as input to the C++ mechanics solver.
        :returns:  arrays, mappings
        """
        
        # Write this in a (slightly) more generic form.

        db = self.db
        mesh = get_mesh(db)
        
        # Have a mechanics_prop structure


        fixed_list=self.bcs.get_fixed(db)

    # Number vertices

        pid_map, cell_pids, cell_wids = extract_geometry(db)
        
        c_idx = [0]
        c_attrib = []
        c_vars = []

        cid_list = []

        e_idx = []
        e_attrib = []
        e_vars = []

        e_list = []

        v_attrib = []
        v_vars = []

        t_idx = []
        t_attrib = []
        t_vars = []

        for cid in mesh.wisps(2):
            pids = cell_pids[cid]
            for i, wid in enumerate(cell_wids[cid]):
                ei_attrib=[]
                for prop, n_prop in self.const_props['(cell, wall)']:
#                    print 'edge: ', wid, prop[(cid, wid)] 
                    if n_prop==1:
                        ei_attrib.append(prop[(cid, wid)])
                    else:
                        ei_attrib.extend(prop[(cid, wid)])
                e_attrib.append(ei_attrib)
                ei_vars=[]
                for prop, n_prop in self.var_props['(cell, wall)']:
                    if n_prop==1:
                        ei_vars.append(prop[(cid, wid)])
                    else:
                        ei_vars.extend(prop[(cid, wid)])
                e_vars.append(ei_vars)
                e_idx.append((pid_map[pids[i]], pid_map[pids[(i+1)%len(pids)]]))
#                print 'eidx:', (pids[i], pids[(i+1)%len(pids)]), list(mesh.borders(1,wid))
                e_list.append((cid, wid))
                                  
            ci_attrib=[]
            for prop, n_prop, in self.const_props['cell']:
                if n_prop==1:
                    ci_attrib.append(prop[cid])
                else:
                    ci_attrib.extend(prop[cid])
            c_attrib.append(ci_attrib)
            ci_vars=[]
            for prop, n_prop, in self.var_props['cell']:
                if n_prop==1:
                    ci_vars.append(prop[cid])
                else:
                    ci_vars.extend(prop[cid])
            c_vars.append(ci_vars)
            c_idx.append(len(e_idx))
            cid_list.append(cid)
    
        for pid in mesh.wisps(0):
            vi_attrib=[]
            for prop, n_prop, in self.const_props['point']:
                if n_prop==1:
                    vi_attrib.append(prop[pid])
                else:
                    vi_attrib.extend(prop[pid])
            v_attrib.append(vi_attrib)
            vi_vars=[]
            for prop, n_prop, in self.var_props['point']:
                if n_prop==1:
                    vi_vars.append(prop[pid])
                else:
                    vi_vars.extend(prop[pid])
            v_vars.append(vi_vars)

        for cid, t in iter_prop_type(db, '(cell, tri)'):
            t_idx.append(map(pid_map.get,t))
            ti_attrib=[]
            for prop, n_prop in self.const_props['(cell, tri)']:
                if n_prop==1:
                    ti_attrib.append(prop[(cid, t)])
                else:
                    ti_attrib.extend(prop[(cid, t)])
            t_attrib.append(ti_attrib)
            ti_vars=[]
            for prop, n_prop in self.var_props['(cell, tri)']:
                if n_prop==1:
                    ti_vars.append(prop[(cid, t)])
                else:
                    ti_vars.extend(prop[(cid, t)])
            t_vars.append(ti_vars)

        v_fixed=[pid_map[pid] for pid in fixed_list]

        c_idx = np.ascontiguousarray(c_idx, dtype='int32')
        e_idx = np.ascontiguousarray(e_idx, dtype='int32')
        t_idx = np.ascontiguousarray(t_idx, dtype='int32')
        
        v_fixed = np.ascontiguousarray(v_fixed, dtype='int32')

        c_attrib=np.ascontiguousarray(c_attrib, dtype='float64')
        c_vars=np.ascontiguousarray(c_vars, dtype='float64')
        e_attrib=np.ascontiguousarray(e_attrib, dtype='float64')
        e_vars=np.ascontiguousarray(e_vars, dtype='float64')
        v_attrib=np.ascontiguousarray(v_attrib, dtype='float64')
        v_vars=np.ascontiguousarray(v_vars, dtype='float64')
        t_attrib=np.ascontiguousarray(t_attrib, dtype='float64')
        t_vars=np.ascontiguousarray(t_vars, dtype='float64')

        xp = np.zeros((c_vars.size+e_vars.size+v_vars.size+t_vars.size,), dtype='float64')

        if self.use_old_vels:
            vel = db.get_property('velocity')

            for idx, key in enumerate(iter_prop_type(db, 'point')):
                xp[idx*2:idx*2+2]=vel[key]

        xp = np.ascontiguousarray(xp)


#        print xp.shape, t_idx.shape, len(db.get_property('tri_ref_pos'))
#        print 'e_vars shape:', e_vars.shape


        return [c_idx, c_attrib, c_vars, e_idx, e_attrib, e_vars, \
                    t_idx, t_attrib, t_vars, \
                    v_attrib, v_vars, v_fixed, xp], \
                    (e_list,)

    def get_uniforms(self):
        db = self.db
        uniform_names = self.model.get_uniform_props()
        uniforms = []
        params = get_parameters(db, self.model.name)
        for prop_name in uniform_names:
            if prop_name in params:
                uniforms.append(params[prop_name])
            else:
                uniforms.append(get_parameters(db, prop_name))
        uniforms.append(False)
        return uniforms


    def remesh(self):
        db = self.db
        model_params = get_parameters(db, self.model.name)
        t = time()
        remesh_all(db, model_params['mesh_tri_area'])
        print 'REMESH_TIME: ', time()-t
        mesh = get_mesh(db)
        pos = db.get_property('position')
        tri_ref_pos = db.get_property('tri_ref_pos')
        print 'TRIS: ', len(tri_ref_pos)
        print 'VERTS: ', len(pos)

        model_params = get_parameters(db, self.model.name)

        # fix up cell properties

        model_const_props = self.model.get_const_props()
        for prop_name, prop_type, prop_len in model_const_props:
            if prop_type == '(cell, tri)' and prop_name !='tri_ref_pos':
                prop = db.get_property(prop_name)
                prop.clear()
                for k in tri_ref_pos:
                    prop[k] = model_params[prop_name]

        model_var_props = self.model.get_var_props()
        for prop_name, prop_type, prop_len in model_var_props:
            if prop_type == '(cell, tri)' and prop_name !='tri_ref_pos':
                prop = db.get_property(prop_name)
                prop.clear()
                for k in tri_ref_pos:
                    prop[k] = model_params[prop_name]


   
    def step(self):
        """
        Perform one step for the mechanical solver
        """
        db = self.db

#        remesh_all(db)
        mesh = get_mesh(db)
        pos = db.get_property('position')

#        print db.get_property('spring_phiinv')
#        print db.get_property('spring_lambda')


        #self.model.stress_relax(db)

        # Area of each cell
        old_V = {}
        for cid in mesh.wisps(2):
            old_V[cid]=face_surface_2D(mesh, pos, cid)

        self.model.set_visc(db)
                       
        arrays, mappings = self.convert_to_mechanics_arrays()

        uniforms = self.get_uniforms()
       # print uniforms, len(arrays)
       # print arrays
#        print 'pids:', list(get_mesh(self.db).wisps(0))

        self.model.evolve(uniforms, arrays)
        self.restore_from_mechanics_arrays(arrays, mappings)

        mV = {}
        for cid in mesh.wisps(2):
            mV[cid]=face_surface_2D(mesh, pos, cid)/old_V[cid]
        
        for prop_name, rule in db.get_property('divided_props').iteritems():
            if rule[0]=='cell' and rule[1]=='concentration':
                prop = db.get_property(prop_name)
                for cid in mesh.wisps(2):
                    if prop[cid] is not None:
                        prop[cid] = prop[cid]/mV[cid]

                    
        

    def restore_from_mechanics_arrays(self, arrays, mappings):
        """
        Take the information about the mechanical simulation
        state from the numpy arrays, and update this information
        within the TissueDB
        :param arrays: Numpy arrays endcoding tissue state
        :param mappings: Mappings from numpy arrays to tissue properties
        """
        db = self.db
        mesh = get_mesh(db)
        c_idx, c_attrib, c_vars, e_idx, e_attrib, e_vars, \
            t_idx, t_attrib, t_vars, \
            v_attrib, v_vars, v_fixed, xp = arrays
        e_list, = mappings


        off = 0
        N = self.var_prop_lens['(cell, wall)']
        for prop, n_prop in self.var_props['(cell, wall)']:
            for idx, key in enumerate(e_list):
                if n_prop==1:
                    prop[key]=e_vars[idx][off]
                else:
                    prop[key]=e_vars[idx][off:off+n_prop]
            off+=n_prop


        for prop_type, d in [('cell', c_vars), \
                             ('point', v_vars),
                             ('(cell, tri)', t_vars)]:
            off = 0
            N = self.var_prop_lens[prop_type]
            for prop, n_prop in self.var_props[prop_type]:
                for idx, key in enumerate(iter_prop_type(db, prop_type)):
                    if n_prop==1:
                        prop[key]=d[idx][off]
                    else:
                        prop[key]=d[idx][off:off+n_prop]
                off+=n_prop
        

        vel = db.get_property('velocity')
        vel_list = xp
        for idx, key in enumerate(iter_prop_type(db, 'point')):
            vel[key]=vel_list[idx*2:idx*2+2]
#        print 'VL: ', vel_list[0:20]
                     


class CrossGrowth(object):   
    """ Object to handle tissue growth """
    def __init__(self, db, param_changes={}, model=CrossGrowthModel(), bcs = CrossBCs(), use_old_vels=False):
        """
        Initialise growth-related properties in the TissueDB
        :param db: Tissue database 
        :type db: TissueDB
        """
        print 'GROWTH INIT'

        self.db = db
        self.model = model #FullrootGrowthModel()
        self.model.set_parameters(db)
        self.bcs = bcs #FullrootGrowthBCs()
        self.use_old_vels = use_old_vels
        
        
        params = get_parameters(db, self.model.name)
        params.update(param_changes)

        mesh = get_mesh(db)
        graph = get_graph(db)

        # Generate initial mesh triangulation
        triangulate_mesh(db, params['mesh_tri_area'])
        #db.set_property('cell_triangles',{})

        pos = db.get_property('position')
        cell_type=db.get_property('cell_type')
        cell_triangles=db.get_property('cell_triangles')

        print 'TRIANGLES', sum(len(x) for x in cell_triangles.itervalues())

        divided_props=db.get_property('divided_props')

        model_const_props = self.model.get_const_props()


        self.var_props = defaultdict(list)
        self.const_props = defaultdict(list)
        self.var_prop_lens = defaultdict(int)
        self.const_prop_lens = defaultdict(int)

        for prop_name, prop_type, prop_len in model_const_props:
            if prop_name not in db.properties():
                add_property(db, prop_name, prop_type, params[prop_name])
            if prop_name not in divided_props:
                divided_props[prop_name] = ( prop_type, 'property',
                                             params[prop_name] )
            self.const_props[prop_type].append(
                (db.get_property(prop_name), prop_len))
            self.const_prop_lens[prop_type]+=prop_len

        model_var_props = self.model.get_var_props()
        for prop_name, prop_type, prop_len in model_var_props:
            if prop_name not in db.properties():
                add_property(db, prop_name, prop_type, 0.0)
            if prop_name not in divided_props and prop_name!='position':
                if prop_name == 'spring_length':
                    divided_props[prop_name] = ( prop_type, 'amount',
                                             0.0 )
                else:
                    divided_props[prop_name] = ( prop_type, 'property',
                                                 0.0 )
            self.var_props[prop_type].append(
                (db.get_property(prop_name), prop_len))
            self.var_prop_lens[prop_type]+=prop_len

        self.model.set_initial_vars(db)

        add_property(db, 'velocity', 'point', np.array((0,0)))

        

    def convert_to_mechanics_arrays(self):
        """
        Extract numpy arrays from the TissueDB, containing
        the topology and geometry of the mesh, the triangulation
        of the cell walls in the plane of the simulation, and all
        the mechanical properties of the tissue.
        Used as input to the C++ mechanics solver.
        :returns:  arrays, mappings
        """
        
        # Write this in a (slightly) more generic form.

        db = self.db
        mesh = get_mesh(db)
        
        # Have a mechanics_prop structure


        fixed_list=self.bcs.get_fixed(db)

    # Number vertices

        pid_map, cell_pids, cell_wids = extract_geometry(db)
        
        c_idx = [0]
        c_attrib = []
        c_vars = []

        cid_list = []

        e_idx = []
        e_attrib = []
        e_vars = []

        e_list = []

        v_attrib = []
        v_vars = []

        t_idx = []
        t_attrib = []
        t_vars = []

        for cid in mesh.wisps(2):
            pids = cell_pids[cid]
            for i, wid in enumerate(cell_wids[cid]):
                ei_attrib=[]
                for prop, n_prop in self.const_props['(cell, wall)']:
#                    print 'edge: ', wid, prop[(cid, wid)] 
                    if n_prop==1:
                        ei_attrib.append(prop[(cid, wid)])
                    else:
                        ei_attrib.extend(prop[(cid, wid)])
                e_attrib.append(ei_attrib)
                ei_vars=[]
                for prop, n_prop in self.var_props['(cell, wall)']:
                    if n_prop==1:
                        ei_vars.append(prop[(cid, wid)])
                    else:
                        ei_vars.extend(prop[(cid, wid)])
                e_vars.append(ei_vars)
                e_idx.append((pid_map[pids[i]], pid_map[pids[(i+1)%len(pids)]]))
#                print 'eidx:', (pids[i], pids[(i+1)%len(pids)]), list(mesh.borders(1,wid))
                e_list.append((cid, wid))
                                  
            ci_attrib=[]
            for prop, n_prop, in self.const_props['cell']:
                if n_prop==1:
                    ci_attrib.append(prop[cid])
                else:
                    ci_attrib.extend(prop[cid])
            c_attrib.append(ci_attrib)
            ci_vars=[]
            for prop, n_prop, in self.var_props['cell']:
                if n_prop==1:
                    ci_vars.append(prop[cid])
                else:
                    ci_vars.extend(prop[cid])
            c_vars.append(ci_vars)
            c_idx.append(len(e_idx))
            cid_list.append(cid)
    
        for pid in mesh.wisps(0):
            vi_attrib=[]
            for prop, n_prop, in self.const_props['point']:
                if n_prop==1:
                    vi_attrib.append(prop[pid])
                else:
                    vi_attrib.extend(prop[pid])
            v_attrib.append(vi_attrib)
            vi_vars=[]
            for prop, n_prop, in self.var_props['point']:
                if n_prop==1:
                    vi_vars.append(prop[pid])
                else:
                    vi_vars.extend(prop[pid])
            v_vars.append(vi_vars)

        for cid, t in iter_prop_type(db, '(cell, tri)'):
            t_idx.append(map(pid_map.get,t))
            ti_attrib=[]
            for prop, n_prop in self.const_props['(cell, tri)']:
                if n_prop==1:
                    ti_attrib.append(prop[(cid, t)])
                else:
                    ti_attrib.extend(prop[(cid, t)])
            t_attrib.append(ti_attrib)
            ti_vars=[]
            for prop, n_prop in self.var_props['(cell, tri)']:
                if n_prop==1:
                    ti_vars.append(prop[(cid, t)])
                else:
                    ti_vars.extend(prop[(cid, t)])
            t_vars.append(ti_vars)

        v_fixed=[pid_map[pid] for pid in fixed_list]

        c_idx = np.ascontiguousarray(c_idx, dtype='int32')
        e_idx = np.ascontiguousarray(e_idx, dtype='int32')
        t_idx = np.ascontiguousarray(t_idx, dtype='int32')
        
        v_fixed = np.ascontiguousarray(v_fixed, dtype='int32')

        c_attrib=np.ascontiguousarray(c_attrib, dtype='float64')
        c_vars=np.ascontiguousarray(c_vars, dtype='float64')
        e_attrib=np.ascontiguousarray(e_attrib, dtype='float64')
        e_vars=np.ascontiguousarray(e_vars, dtype='float64')
        v_attrib=np.ascontiguousarray(v_attrib, dtype='float64')
        v_vars=np.ascontiguousarray(v_vars, dtype='float64')
        t_attrib=np.ascontiguousarray(t_attrib, dtype='float64')
        t_vars=np.ascontiguousarray(t_vars, dtype='float64')

        xp = np.zeros((c_vars.size+e_vars.size+v_vars.size+t_vars.size,), dtype='float64')

        if self.use_old_vels:
            vel = db.get_property('velocity')

            for idx, key in enumerate(iter_prop_type(db, 'point')):
                xp[idx*2:idx*2+2]=vel[key]

        xp = np.ascontiguousarray(xp)


#        print xp.shape, t_idx.shape, len(db.get_property('tri_ref_pos'))
#        print 'e_vars shape:', e_vars.shape


        return [c_idx, c_attrib, c_vars, e_idx, e_attrib, e_vars, \
                    t_idx, t_attrib, t_vars, \
                    v_attrib, v_vars, v_fixed, xp], \
                    (e_list,)

    def get_uniforms(self):
        db = self.db
        uniform_names = self.model.get_uniform_props()
        uniforms = []
        params = get_parameters(db, self.model.name)
        for prop_name in uniform_names:
            if prop_name in params:
                uniforms.append(params[prop_name])
            else:
                uniforms.append(get_parameters(db, prop_name))
        uniforms.append(False)
        return uniforms


    def remesh(self):
        db = self.db
        model_params = get_parameters(db, self.model.name)
        t = time()
        remesh_all(db, model_params['mesh_tri_area'])
        print 'REMESH_TIME: ', time()-t
        mesh = get_mesh(db)
        pos = db.get_property('position')
        tri_ref_pos = db.get_property('tri_ref_pos')
        print 'TRIS: ', len(tri_ref_pos)
        print 'VERTS: ', len(pos)

        model_params = get_parameters(db, self.model.name)

        # fix up cell properties

        model_const_props = self.model.get_const_props()
        for prop_name, prop_type, prop_len in model_const_props:
            if prop_type == '(cell, tri)' and prop_name !='tri_ref_pos':
                prop = db.get_property(prop_name)
                prop.clear()
                for k in tri_ref_pos:
                    prop[k] = model_params[prop_name]

        model_var_props = self.model.get_var_props()
        for prop_name, prop_type, prop_len in model_var_props:
            if prop_type == '(cell, tri)' and prop_name !='tri_ref_pos':
                prop = db.get_property(prop_name)
                prop.clear()
                for k in tri_ref_pos:
                    prop[k] = model_params[prop_name]


   
    def step(self):
        """
        Perform one step for the mechanical solver
        """
        db = self.db

#        remesh_all(db)
        mesh = get_mesh(db)
        pos = db.get_property('position')

#        print db.get_property('spring_phiinv')
#        print db.get_property('spring_lambda')


        #self.model.stress_relax(db)

        # Area of each cell
        old_V = {}
        for cid in mesh.wisps(2):
            old_V[cid]=face_surface_2D(mesh, pos, cid)

        self.model.set_visc(db)
                       
        arrays, mappings = self.convert_to_mechanics_arrays()

        uniforms = self.get_uniforms()
       # print uniforms, len(arrays)
       # print arrays
#        print 'pids:', list(get_mesh(self.db).wisps(0))

        self.model.evolve(uniforms, arrays)
        self.restore_from_mechanics_arrays(arrays, mappings)

        mV = {}
        for cid in mesh.wisps(2):
            mV[cid]=face_surface_2D(mesh, pos, cid)/old_V[cid]
        
        for prop_name, rule in db.get_property('divided_props').iteritems():
            if rule[0]=='cell' and rule[1]=='concentration':
                prop = db.get_property(prop_name)
                for cid in mesh.wisps(2):
                    if prop[cid] is not None:
                        prop[cid] = prop[cid]/mV[cid]

                    
        

    def restore_from_mechanics_arrays(self, arrays, mappings):
        """
        Take the information about the mechanical simulation
        state from the numpy arrays, and update this information
        within the TissueDB
        :param arrays: Numpy arrays endcoding tissue state
        :param mappings: Mappings from numpy arrays to tissue properties
        """
        db = self.db
        mesh = get_mesh(db)
        c_idx, c_attrib, c_vars, e_idx, e_attrib, e_vars, \
            t_idx, t_attrib, t_vars, \
            v_attrib, v_vars, v_fixed, xp = arrays
        e_list, = mappings


        off = 0
        N = self.var_prop_lens['(cell, wall)']
        for prop, n_prop in self.var_props['(cell, wall)']:
            for idx, key in enumerate(e_list):
                if n_prop==1:
                    prop[key]=e_vars[idx][off]
                else:
                    prop[key]=e_vars[idx][off:off+n_prop]
            off+=n_prop


        for prop_type, d in [('cell', c_vars), \
                             ('point', v_vars),
                             ('(cell, tri)', t_vars)]:
            off = 0
            N = self.var_prop_lens[prop_type]
            for prop, n_prop in self.var_props[prop_type]:
                for idx, key in enumerate(iter_prop_type(db, prop_type)):
                    if n_prop==1:
                        prop[key]=d[idx][off]
                    else:
                        prop[key]=d[idx][off:off+n_prop]
                off+=n_prop
        

        vel = db.get_property('velocity')
        vel_list = xp
        for idx, key in enumerate(iter_prop_type(db, 'point')):
            vel[key]=vel_list[idx*2:idx*2+2]
        print 'VL: ', vel_list
                     
