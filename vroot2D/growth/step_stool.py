
from math import exp, log
import numpy as np
import matplotlib.pylab as plt

def step_stool(x, p):
    # Parameters
    # b1 - 
    b1, b2, c1, c2, d1, d2 = p
    
    print x
    K = log(exp(b2*c1*d1)+exp(b2*d1*x))
    K0 = log(exp(b2*c1*d1)+1)          
    L = log(exp(b2*c1*d1)+exp(b2*c2*d1))
    M = b1 + b2/(exp(b2*d1*(c1-x))+1)
    N = exp(d2*(b1*(c2-x)+(L-K)/d1))

#    K = b2*c1*d1+log(1+exp(b2*d1*(x-c1)))
# #   K0 = b2*c1*d1+log(1+exp(-b2*c1*d1))          
#  #  L = b2*c2*d1+log(1+exp(b2*(c1-c2)*d1))
#    M = b1 + b2/(exp(b2*d1*(c1-x))+1)
#    N = exp(d2*(b1*(c2-x)+(L-K)/d1))
    
    regr = M*(1-1/(N+1))
    
    gII = b1*x + K/d1
    gII0 = K0/d1
    gIII = gII - log(exp(d2*(b1*c2 + L/d1)) + exp(d2*gII))/d2
    gIII0 = gII0 - log(exp(d2*(b1*c2 + L/d1))   + exp(d2*gII0))/d2
                        
    gIV = gIII - gIII0
    
    return gIV, regr



def main():
    import seaborn as sns
    from matplotlib.patches import Rectangle
    from matplotlib.ticker import MaxNLocator


    sns.set_context('poster')
    from matplotlib import rcParams

    params = {'font.family': 'sans',
          'axes.labelsize': 40,
          'text.fontsize': 30,
          'legend.fontsize': 45,
          'legend.frameon': False,
          'legend.linewidth': 0,
          'xtick.labelsize': 30,
          'ytick.labelsize': 30}

    rcParams.update(params)


   # p = 0.2, 1.69, 0.3, 0.8, 10.0, 10.0
    def regr(z):
        l0 = 1000.0 
        p0 = (0.05, 0.45, 400.0/l0, 1000.0/l0, 150.0, 80.0) 
        return max(step_stool(z/l0, p0)[1], 1e-4)
    x = np.array(range(1200))
    y = []

    for t in x:
        print regr(t)
        y.append(regr(t))

    print y

    plt.figure(figsize=(8, 4))
    ax = plt.axes([0.2,0.1,0.75,0.75])
#    ax.add_patch(Rectangle((0, 0), 200, 0.25, facecolor=(1.0, 0.85, 0.85), edgecolor='none'))

    plt.plot(x,y)

#plt.plot(times, np.array(hdata[1][2])*(180.0/pi),'b--')                                 
    ax.xaxis.set_major_locator(MaxNLocator(nbins=2))
    ax.yaxis.set_major_locator(MaxNLocator(3))


    plt.xlabel("d ($\mu$ m)")
    plt.ylabel("$\gamma_{PB}$ (hr$^{-1}$)")


    plt.show()

if __name__=="__main__":
    main()
