
from math import exp, log, erfc
import numpy as np
import matplotlib.pylab as plt



def midline_fn(s, p):
    eps = p['eps']
    g = (p['g1'] + (p['g2'] - p['g1'])*erfc((p['s1']-s)/eps)/2.0)*erfc((s-p['s2'])/eps)/2.0
    return g



def main():
    import seaborn as sns
    from matplotlib.patches import Rectangle
    from matplotlib.ticker import MaxNLocator


    sns.set_context('poster')
    from matplotlib import rcParams

    params = {'font.family': 'sans',
          'axes.labelsize': 40,
          'text.fontsize': 30,
          'xtick.labelsize': 30,
          'ytick.labelsize': 30}

    rcParams.update(params)

    p = { 
        'mu1': 0.1,
        'g1': 0.1,
        'g2': 0.4,
        's1': 200.,
        's2': 450.,
        'eps': 50.,
    }

    def regr(s):
        return midline_fn(s, p)


    x = np.array(range(500))
    y = []

    for t in x:
        print regr(t)
        y.append(regr(t))

    print y

    plt.figure(figsize=(8, 4))
    ax = plt.axes([0.2,0.1,0.75,0.75])
    ax.add_patch(Rectangle((0, 0), 200, 0.5, facecolor=(1.0, 0.85, 0.85), edgecolor='none'))

    plt.plot(x,y)

#plt.plot(times, np.array(hdata[1][2])*(180.0/pi),'b--')                                 
    ax.xaxis.set_major_locator(MaxNLocator(nbins=2))
    ax.yaxis.set_major_locator(MaxNLocator(3))


    plt.xlabel("d ($\mu$ m)")
    plt.ylabel("$\gamma$ (hr$^{-1}$)")


    plt.show()

if __name__=="__main__":
    main()
