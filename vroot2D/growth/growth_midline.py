

"""
Module to handle tissue growth
"""


from vroot2D.utils.geom import area
import numpy as np
import itertools
from vroot2D.utils.db_utilities import cell_walls, get_mesh, \
     refresh_property, get_parameters, get_graph, add_property, \
     iter_prop_type, set_parameters
from vroot2D.growth.mesh_nbs import triangulate_mesh_gmsh as triangulate_mesh
#from openalea.tissueshape import face_surface_2D, edge_length
from collections import defaultdict
from vroot2D.utils.db_geom import wall_angle, ordered_wids_pids, \
     face_surface_2D, edge_length
from math import sin, cos, exp, sqrt, asin, atan2, erfc
import scipy.linalg as la
from operator import itemgetter
from scipy.special import erf

from midline_fn import midline_fn

def rot(v):
    """                                                                         
    rotate a 2d vector (numpy) by 90 degrees anticlockwise                    
    :param v: input vector                                                      
    :type v: Vector2/list/tuple                                                 
    :returns: Vector2 rotated v                                                 
    """
    return np.array((-v[1],v[0]))

def ml_to_xy(m, kappa, l, x0, theta0, bp_data=None):
    """
    Given a point in the piecewise circular coordinate system,
    find the cartesian coordinates of the points

    :param m: Midline section 

    """
    j = m[0]
    xi = m[1]
    eta = m[2]
    if bp_data:
        x = np.array(bp_data[0][j])
        theta = bp_data[1][j]
    else:
        x = np.array(x0)
        theta = theta0
        for i in range(j):
            t = np.array((cos(theta), sin(theta)))
            n = np.array((-sin(theta), cos(theta)))
            if abs(kappa[i]*l[i])>1e-15:
                x += (1.0/kappa[i])*\
                    (sin(kappa[i]*l[i])*t+(2*sin(kappa[i]*l[i]/2)**2)*n)
            else:
                x += l[i]*t        
            theta += kappa[i]*l[i]
             
    t = np.array((cos(theta), sin(theta)))
    n = np.array((-sin(theta), cos(theta)))
    if abs(kappa[j]*l[j])>1e-15:
        x += (1/kappa[j]*sin(kappa[j]*l[j]*xi)-eta*sin(kappa[j]*l[j]*xi))*t+ \
        (1/kappa[j]*(2*sin(kappa[j]*l[j]*xi/2)**2)+eta*cos(kappa[j]*l[j]*xi))*n
    else:
        x += xi*l[j]*t+eta*n 
    return x


def breakpoints(kappa, l, x0, theta0):
    """
    Calculate the positions, angles and tangents to the midline
    at each of the endpoints of the piecewise circular sections
    """

    x = np.array(x0)
    theta = theta0
    bp = [ np.array(x) ]
    angles = [ theta ]
    tangents = [ np.array((cos(theta), sin(theta)))]
    for i in range(len(kappa)):
        t = np.array((cos(theta), sin(theta)))
        n = np.array((-sin(theta), cos(theta)))
        if abs(kappa[i]*l[i])>1e-15:
            x += (1.0/kappa[i])*(sin(kappa[i]*l[i])*t+2*sin(kappa[i]*l[i]/2)**2*n)
        else:
            x += l[i]*t        
        theta += kappa[i]*l[i]
        bp.append(np.array(x))
        angles.append(theta)
        tangents.append(np.array((cos(theta), sin(theta))))
    return bp, angles, tangents

#@profile
def xy_to_ml_segment(p, kappa, l, x0, t0):
    """
    Convert cartesian to midline coordinates (for a specific
    circular section).
    """

   # t0 = np.array((cos(theta0), sin(theta0)))
   # n0 = np.array((-sin(theta0), cos(theta0)))
#    n0 = rot(t0)
    dp = p-x0
    if kappa>0:
        n0 = rot(t0)
        s = 1.0/kappa*n0-dp
        #eta = 1.0/kappa - sqrt(s[0]*s[0]+s[1]*s[1])
        eta = (2*(dp[0]*n0[0]+dp[1]*n0[1])/kappa-(dp[0]*dp[0]+dp[1]*dp[1]))\
            / (1.0/kappa + sqrt(s[0]*s[0]+s[1]*s[1]))
        #1.0/kappa-la.norm(1.0/kappa*n0-dp)
        xi = asin((t0[0]*dp[0]+t0[1]*dp[1])/(1.0/kappa - eta))/kappa/l
    elif kappa<0:
        n0 = rot(t0)
        s = 1.0/kappa*n0-dp
        #eta = sqrt(s[0]*s[0]+s[1]*s[1])+1.0/kappa
        eta = (2*(dp[0]*n0[0]+dp[1]*n0[1])/kappa-(dp[0]*dp[0]+dp[1]*dp[1]))\
            / (1.0/kappa - sqrt(s[0]*s[0]+s[1]*s[1]))
        # la.norm(1.0/kappa*n0-dp)+1.0/kappa
        xi = asin((t0[0]*dp[0]+t0[1]*dp[1])/(1.0/kappa - eta))/kappa/l
        #asin(np.dot(t0, dp)/(1.0/kappa - eta))/kappa/l
    else:
        n0 = rot(t0)
        eta = n0[0]*dp[0]+n0[1]*dp[1]    #np.dot(n0, dp)
        xi = (t0[0]*dp[0]+t0[1]*dp[1])/l  #np.dot(t0, dp)/l
    return xi, eta
        
def dist(a, b) :
    """
    Cartesian distance between two points in 2D
    """
    return sqrt((b[1]-a[1])**2 + (b[0]-a[0])**2)
    
def nearest_point_index(p, l) :
    """
    Find the index of the point in the list l nearest to p
    """
 #   print p, l.tolist()
 #   if p in l.tolist():
 #       return l.tolist().index(p)
 #   else :
    d=[dist(p,b) for b in l]
    minindex, minvalue = min(enumerate(d), key=itemgetter(1)) 
    return minindex
    

def xy_to_ml(p, kappa, l, x0, theta0, bp_data=None):
    """
    Convert cartesian position to piecewise circular coordinates
    """
    if bp_data:
        bp_list, angles, tangents = bp_data
    else:
        bp_list, angles, tangents = breakpoints(kappa, l, x0, theta0)
    bp = np.asarray(bp_list)
#    print bp
#    print tangents
    sides = np.array([cmp(np.dot(p-xi,ti),0.0) for xi, ti in zip(bp, tangents)])
#    print sides

#    print list(zip(bp, tangents))
#    print [np.dot(p-xi,ti) for xi, ti in zip(bp, tangents)]
    change_sign = np.where(sides[1:]!=sides[0:-1])
    idx = change_sign[0]
    if len(idx)>1:
        #print sides
        midpoints = 0.5*(bp[1:,:]+bp[0:-1,:]) 
        # find midpoint which is nearest to the observation points
        i = nearest_point_index(p, midpoints)
    elif len(idx)==1:
        i = idx[0]
    else:
        #print sides
        if sides[-1]==1:
            i = len(kappa)-1
        if sides[0]==-1:
            i = 0
    ml=xy_to_ml_segment(p, kappa[i], l[i], bp[i], tangents[i])
    return (i, ml[0], ml[1])

def ml_to_vel(m, kappa, l, kdot, regr, x0, theta0):
    """
    Midline coordinates to velocity in cartesian coordinates
    """
    ldot = l * regr
    v = np.array((0,0))
    
    j, xi, eta = m
    theta = theta0
    tdot = 0.0
    x = x0
    # Find velocity of breakpoint before
    for i in range(j):
        t = np.array((cos(theta), sin(theta)))
        n = np.array((-sin(theta), cos(theta)))
        if abs(kappa[i]*l[i])>1e-15:
            x1 = x + 1.0/kappa[i]*\
                (sin(kappa[i]*l[i])*t+2*sin(kappa[i]*l[i]/2)**2*n)
        else:
            x1 = x + l[i]*t        
        theta1 = theta + kappa[i]*l[i]
        t1 = np.array((cos(theta1), sin(theta1)))
        v1 = v + ldot[i]*t1
        if abs(kappa[i]*l[i])>1e-15:
            v1 += kdot[i]/kappa[i]*(l[i]*t1-(x1-x))
        else:
            v1 += 0.5*l[i]*l[i]*kdot[i]*n
        v1 += tdot*rot(x1-x)
        
        tdot += kdot[i]*l[i]+kappa[i]*ldot[i]
        v = v1
#        print 'i, v, tdot: ', i, v, tdot
        x = x1
        theta = theta1
     #   print v, x, theta, tdot
    t = np.array((cos(theta), sin(theta)))
    n = np.array((-sin(theta), cos(theta)))
    if abs(kappa[j]*l[j])>1e-15:
        x_p = x + (1/kappa[j]*sin(kappa[j]*l[j]*xi)-eta*sin(kappa[j]*l[j]*xi)) \
        * t+ \
        (1/kappa[j]*(2*sin(kappa[j]*l[j]*xi/2)**2)+eta*cos(kappa[j]*l[j]*xi))*n
    else:
        x_p = x + xi*l[j]*t+eta*n 
    theta_p = theta + kappa[j]*l[j]*xi
    t_p = np.array((cos(theta_p), sin(theta_p)))
    v_p = v + ldot[j]*xi*t_p*(1-kappa[j]*eta) + tdot*rot(x_p-x)
    if abs(kappa[j]*l[j])>1e-15:
        v_p += kdot[j]/kappa[j]*(xi*l[j]*(1-eta*kappa[j])*t_p-1/kappa[j]*(sin(kappa[j]*l[j]*xi)*t+(2*sin(kappa[j]*l[j]*xi/2)**2)*n))
    else:
        v_p += -eta*kdot[j]*l[j]*xi*t+0.5*l[j]*l[j]*xi*xi*n*kdot[j]
#    print 'v_p: ', v_p
    return v_p

from vroot2D.growth.step_stool import step_stool
        
def target_regr(x):
    l0 = 1000.0 
    #(0.01, 0.25, 200.0/l0, 600.0/l0, 150.0, 80.0))
    p0 = (0.05, 0.45, 400.0/l0, 1000.0/l0, 150.0, 80.0) 
    return max(step_stool(x/l0, p0)[1], 1e-4)

def sens(x):
    l0 = 300.0
    return min(1, exp(-(x/l0-1)))
#    return min(-1, max(1, (1-x/l0)*exp(-(x/l0-1)**2)))

def sens2(x):
    l0 = 300.0
    return 0.5*erfc(0.2*(300.0-x))


from matplotlib import pylab as plt

"""
class HydroGrowthModel(object):

    name = 'HydroGrowthModel'

    def get_const_props(self):
        return [ ('mu1', '(cell, tri)', 1) ]
               
    def get_var_props(self):
        return [ ('position', 'point', 2) ]

    def get_uniform_props(self):
        return [ 'relaxation_time', 'initial_timestep' ]

    def set_initial_vars(self, db):
        mesh = get_mesh(db)
        pos = db.get_property('position')
        
        cell_triangles = db.get_property('cell_triangles')
        tri_ref_pos = {}
        for cid, tris in cell_triangles.iteritems():
            for t in tris:
                tri_ref_pos[(cid, t)] = list(itertools.chain(*(pos[pid] for pid in t)))
        db.set_property('tri_ref_pos', tri_ref_pos)
        db.set_description('tri_ref_pos','')
             

    def default_parameters(self):
        p = { 'turgor_pressure': 0.3,
              'relaxation_time': 0.1,
              'initial_timestep': 0.1,
              'mu1': 0.3,
              'mu2': 20.0,
              'mu3': 0.0,
              'bend': 0.3,
              'ka': 1.0,
              'na': 1.0,
              'midline_max': 100.0,
              'mesh_tri_area': 10.0,
              'section_max_length': 100.0}
        return p
        
    def set_parameters(self, db):
        set_parameters(db, self.name, self.default_parameters()) 
    
    def set_visc(self, db):
        mesh = get_mesh(db)
        params = get_parameters(db, self.name)
        cell_type = db.get_property('cell_type')
        ka = params['ka']
        na = params['na']
        turgor_pressure = params['turgor_pressure']
        mu1 = db.get_property('mu1')
        mu1_default = params['mu1']
        cell_triangles = db.get_property('cell_triangles')
        cell_type = db.get_property('cell_type')
        auxin = db.get_property('auxin')
#        side = db.get_property('side')
        dist = midline_distances(db)
        time = db.get_property('time')
        for cid, tris in cell_triangles.iteritems():
            if cell_type[cid]==0:
                # Epidermis
                k = (auxin[cid]/ka)**na
                dm = k/(1.0+k)+1.0
                #dm = 1.0
                ds = 0.0
#            elif cell_type[cid]==1:
                # Cortex
                # Early onset of EZ behaviour
#                dm = 1.0
#                if side[cid]==1 and time<3.0:
#                    ds = 100.0
#                else:
#                    ds = 0.0
            else:
                dm = 1.0
                ds = 0.0
            for t in tris:
                d = (dist[t[0]]+dist[t[1]]+dist[t[2]])/3.
                d += ds*erfc(d-800.0)
#                mu1[(cid,t)]=0.5*turgor_pressure/(target_regr(d)*sens(d)*dm+1e-3)
                mu1[(cid,t)]=0.5*turgor_pressure/(target_regr(d)+1e-3)*(1+sens2(d)*dm)

class MidlineGrowthModel(object):

    name = 'MidlineGrowthModel'

    def get_const_props(self):
        return [ ('mu1', '(cell, tri)', 1) ]
               
    def get_var_props(self):
        return [ ('position', 'point', 2) ]

    def get_uniform_props(self):
        return [ 'relaxation_time', 'initial_timestep' ]

    def set_initial_vars(self, db):
        mesh = get_mesh(db)
        pos = db.get_property('position')
        
        cell_triangles = db.get_property('cell_triangles')
        tri_ref_pos = {}
        for cid, tris in cell_triangles.iteritems():
            for t in tris:
                tri_ref_pos[(cid, t)] = list(itertools.chain(*(pos[pid] for pid in t)))
        db.set_property('tri_ref_pos', tri_ref_pos)
        db.set_description('tri_ref_pos','')
             

    def default_parameters(self):
        p = { 'turgor_pressure': 0.3,
              'relaxation_time': 0.1*60*60,
              'initial_timestep': 0.1,
              'mu1': 0.3,
              'mu2': 20.0,
              'mu3': 0.0,
              'bend': 0.3,
              'ka': 0.25,
              'na': 1.0,
              'midline_max': 100.0,
              'mesh_tri_area': 10.0,
              'section_max_length': 100.0}
        return p
        
    def set_parameters(self, db):
        set_parameters(db, self.name, self.default_parameters()) 
    
    def set_visc(self, db):
        mesh = get_mesh(db)
        params = get_parameters(db, self.name)
        cell_type = db.get_property('cell_type')
        ka = params['ka']
        na = params['na']
        turgor_pressure = params['turgor_pressure']
        mu1 = db.get_property('mu1')
        mu1_default = params['mu1']
        cell_triangles = db.get_property('cell_triangles')
        cell_type = db.get_property('cell_type')
        auxin = db.get_property('auxin')
        dist = midline_distances(db)
        for cid, tris in cell_triangles.iteritems():
            if cell_type[cid]==0:
                # Epidermis
                k = (auxin[cid]/ka)**na
                dm = k/(1.0+k)+1.0
            else:
                dm = 1.0
            for t in tris:
                d = (dist[t[0]]+dist[t[1]]+dist[t[2]])/3.
#                mu1[(cid,t)]=0.5*turgor_pressure/(target_regr(d)*sens(d)*dm+1e-3)
                mu1[(cid,t)]=0.5*turgor_pressure/target_regr(d)*(1.0+sens(d)*dm)
"""                

class MidlineSyntheticGrowthModel(object):

    name = 'MidlineSyntheticGrowthModel'

    def get_const_props(self):
        return [ ('mu1', '(cell, tri)', 1) ]

    def get_var_props(self):
        return [ ('position', 'point', 2) ]

    def get_uniform_props(self):
        return [ 'relaxation_time' ]

    def default_parameters(self):
        p = { 
              'turgor_pressure': 0.3,
              'relaxation_time': 0.1,
              'initial_timestep': 0.1,
              'mu1': 0.15,
              'mu2': 20.0,
              'mu3': 0.0,
              'bend': 0.13,
              'midline_max': 100.0,
              'section_max_length': 90.0,
              'mesh_tri_area': 5.0 }
        return p

    def set_parameters(self, db):
        set_parameters(db, self.name, self.default_parameters())

    def set_initial_vars(self, db):
        mesh = get_mesh(db)
        pos = db.get_property('position')

        cell_triangles = db.get_property('cell_triangles')
        tri_ref_pos = {}
        for cid, tris in cell_triangles.iteritems():
            for t in tris:
                tri_ref_pos[(cid, t)] = list(itertools.chain(*(pos[pid] for pid in t)))
        db.set_property('tri_ref_pos', tri_ref_pos)
        db.set_description('tri_ref_pos','')


    def set_visc(self, db):
        mesh = get_mesh(db)
        params = get_parameters(db, self.name)
        bend = params['bend']
        cell_type = db.get_property('cell_type')        
        cell_triangles = db.get_property('cell_triangles')
        mu1 = db.get_property('mu1')
        for cid, tris in cell_triangles.iteritems():
            if cell_type[cid]==1:
                for t in tris:
                    mu1[(cid,t)]=bend


class MidlineSyntheticDistanceGrowthModel(object):

    name = 'MidlineSyntheticDistanceGrowthModel'

    def get_const_props(self):
        return [ ('mu1', '(cell, tri)', 1) ]

    def get_var_props(self):
        return [ ('position', 'point', 2) ]

    def get_uniform_props(self):
        return [ 'relaxation_time' ]

    def default_parameters(self):
        p = { 
              'turgor_pressure': 0.3,
              'relaxation_time': 0.5,
              'initial_timestep': 0.5,
              'mu1': 0.1,
              'g1': 0.1,
              'g2': 0.4,
              's1': 200.,
              's2': 450.,
              'eps': 50.,
              'bend': 0.13,
              'midline_max': 100.0,
              'section_max_length': 90.0,
              'mesh_tri_area': 5.0 
        }
        return p

    def set_parameters(self, db):
        set_parameters(db, self.name, self.default_parameters())

    def set_initial_vars(self, db):
        mesh = get_mesh(db)
        pos = db.get_property('position')

        cell_triangles = db.get_property('cell_triangles')
        tri_ref_pos = {}
        for cid, tris in cell_triangles.iteritems():
            for t in tris:
                tri_ref_pos[(cid, t)] = list(itertools.chain(*(pos[pid] for pid in t)))
        db.set_property('tri_ref_pos', tri_ref_pos)
        db.set_description('tri_ref_pos','')

    def growth_function(self, s, p):
        g =  midline_fn(s, p)
        #eps = p['eps']
        #g = (p['g1'] + (p['g2'] - p['g1'])*erfc((p['s1']-s)/eps)/2.0)*erfc((s-p['s2'])/eps)/2.0
        return p['turgor_pressure']/2.0/(g+1e-6)

    def set_visc(self, db):
        mesh = get_mesh(db)
        params = get_parameters(db, self.name)
        bend = params['bend']
        cell_type = db.get_property('cell_type')        
        cell_triangles = db.get_property('cell_triangles')
        mu1 = db.get_property('mu1')
        dist = midline_distances(db)
        for cid, tris in cell_triangles.iteritems():
            for t in tris:
                d = (dist[t[0]] + dist[t[1]] + dist[t[2]])/3.0
                mu1[(cid,t)] = self.growth_function(d, params)


class MidlineSyntheticGradientGrowthModel(object):

    name = 'MidlineSyntheticGradientGrowthModel'

    def get_const_props(self):
        return [ ('mu1', '(cell, tri)', 1) ]

    def get_var_props(self):
        return [ ('position', 'point', 2) ]

    def get_uniform_props(self):
        return [ 'relaxation_time' ]

    def default_parameters(self):
        p = { 
              'turgor_pressure': 0.3,
              'relaxation_time': 0.1,
              'initial_timestep': 0.1,
              'eps_t': 20,
              'midline_max': 100.0,
              'section_max_length': 90.0,
              'mesh_tri_area': 5.0 }
        return p

    def set_parameters(self, db):

        set_parameters(db, self.name, self.default_parameters())

    def set_initial_vars(self, db):
        mesh = get_mesh(db)
        pos = db.get_property('position')

        cell_triangles = db.get_property('cell_triangles')
        tri_ref_pos = {}
        for cid, tris in cell_triangles.iteritems():
            for t in tris:
                tri_ref_pos[(cid, t)] = list(itertools.chain(*(pos[pid] for pid in t)))
        db.set_property('tri_ref_pos', tri_ref_pos)
        db.set_description('tri_ref_pos','')


    def set_visc(self, db):
        p = self.default_parameters
        mesh = get_mesh(db)
        params = get_parameters(db, self.name)
        bend = params['bend']
        cell_type = db.get_property('cell_type')        
        cell_triangles = db.get_property('cell_triangles')
        mu1 = db.get_property('mu1')
        for cid, tris in cell_triangles.iteritems():
            if cell_type[cid]==1:
                for t in tris:
                    mu1[(cid,t)]=bend




class MidlineGravGrowthModel(object):

    name = 'MidlineGravGrowthModel'

    def get_const_props(self):
        return [ ('mu1', '(cell, tri)', 1) ]
               
    def get_var_props(self):
        return [ ('position', 'point', 2) ]

    def get_uniform_props(self):
        return [ 'relaxation_time', 'initial_timestep' ]

    def set_initial_vars(self, db):
        mesh = get_mesh(db)
        pos = db.get_property('position')
        
        cell_triangles = db.get_property('cell_triangles')
        tri_ref_pos = {}
        for cid, tris in cell_triangles.iteritems():
            for t in tris:
                tri_ref_pos[(cid, t)] = list(itertools.chain(*(pos[pid] for pid in t)))
        db.set_property('tri_ref_pos', tri_ref_pos)
        db.set_description('tri_ref_pos','')
             

    def default_parameters(self):
        p = { 'turgor_pressure': 0.3,
              'relaxation_time': 1.0/60,
              'initial_timestep': 1.0/60,
              'mu1': 0.3,
              'mu2': 20.0,
              'mu3': 0.0,
              'bend': 0.3,
              'ka': 0.6,
              'As': 0.8,
              'na': 1.0,
              'midline_max': 100.0,
              'mesh_tri_area': 10.0,
              'section_max_length': 90.0}
        return p
        
    def set_parameters(self, db):
        set_parameters(db, self.name, self.default_parameters()) 
    
    def set_visc(self, db):
        mesh = get_mesh(db)
        params = get_parameters(db, self.name)
        cell_type = db.get_property('cell_type')
        ka = params['ka']
        na = params['na']
        As = params['As']
        turgor_pressure = params['turgor_pressure']
        mu1 = db.get_property('mu1')
        mu1_default = params['mu1']
        cell_triangles = db.get_property('cell_triangles')
        cell_type = db.get_property('cell_type')
        auxin = db.get_property('auxin')
        dist = midline_distances(db)
        for cid, tris in cell_triangles.iteritems():
            if cell_type[cid]==2:
                # Epidermis
                k = (auxin[cid]/ka)**na
                dm = 2*k/(1+k)
            else:
                dm = 1.0
            for t in tris:
                d = (dist[t[0]]+dist[t[1]]+dist[t[2]])/3.
#                mu1[(cid,t)]=0.5*turgor_pressure/(target_regr(d)*sens(d)*dm+1e-3)
                mu1[(cid,t)]=0.5*turgor_pressure/target_regr(d)*(As*sens2(d)*dm+1.0-As*sens2(d))







class GrowthMidline(object):

    def __init__(self, db,  param_changes={}, model = MidlineGravGrowthModel(),  LR=False, N_sec=9):

        self.db = db

        self.model = model
        self.model.set_parameters(db)

        params = get_parameters(db, self.model.name)
        params.update(param_changes)

        mesh = get_mesh(db)
        graph = get_graph(db)

        triangulate_mesh(db, params['mesh_tri_area'])

        pos = db.get_property('position')
        cell_type=db.get_property('cell_type')
        cell_triangles=db.get_property('cell_triangles')

        divided_props=db.get_property('divided_props')

        model_const_props = self.model.get_const_props()

        for prop_name, prop_type, prop_len in model_const_props:
            if prop_name not in db.properties():
                add_property(db, prop_name, prop_type, params[prop_name])
            if prop_name not in divided_props:
                divided_props[prop_name] = ( prop_type, 'property',
                                             params[prop_name] )

        model_var_props = self.model.get_var_props()

        for prop_name, prop_type, prop_len in model_var_props:
            if prop_name not in db.properties():
                add_property(db, prop_name, prop_type, 0.0)
            if prop_name not in divided_props and prop_name!='position':
                divided_props[prop_name] = ( prop_type, 'property',
                                             0.0 )

        self.model.set_initial_vars(db)

        add_property(db, 'velocity', 'point', 
                     np.array((0.0,0.0)))

        pos = db.get_property('position')
        x = [p[0] for p in pos.itervalues()]
        y = [p[1] for p in pos.itervalues()]
        ymid = 0.5*(max(y)+min(y))
        
        x0 = np.array([min(x), ymid])
        x1 = np.array([max(x), ymid])
        if not LR:
            x1,x0 = x0, x1
        self.N = N_sec +1
        # Add properties to tissue_db?!
        init_ml = [(1-s)*x0+s*x1 for s in np.linspace(0,1,self.N)]
        theta0 = atan2(x1[1]-x0[1],x1[0]-x0[0])
        self.theta0 = theta0
        self.x0 = x0

        self.kappa = [0.0 for i in range(self.N-1)]
        self.l = [ la.norm(init_ml[i+1]-init_ml[i]) \
                               for i in range(self.N-1) ]
        db.set_property('kappa', self.kappa)
        db.set_property('l', self.l)
        db.set_property('theta0', self.theta0)
        db.set_property('x0', self.x0)
        db.set_property('kdot', [0.0 for i in range(self.N-1)])
        db.set_property('regr', [0.0 for i in range(self.N-1)])
        db.set_property('sn', {})
        self.calculate_ml_coords()

    def calculate_averages(self, correct_dist=True):
        db = self.db
        pos = db.get_property('position')
        cell_triangles = db.get_property('cell_triangles')
        mu1 = db.get_property('mu1')
        kappa = db.get_property('kappa')
        l = db.get_property('l')
        x0 = db.get_property('x0')
        theta0 = db.get_property('theta0')
        N = self.N-1

        A_tot = np.zeros((N,))
        Aeta_tot = np.zeros((N,))
        Amu_tot = np.zeros((N,))
        Amueta_tot = np.zeros((N,))
        Amueta2_tot = np.zeros((N,))
        
        bp_data = breakpoints(kappa, l, x0, theta0)

        sn = db.get_property('sn')

        # Rework this to calculate areas in the
        # straightened-out coordinate system
        

        for cid, tris in cell_triangles.iteritems():
            for t in tris:
                mu = 2*mu1[(cid,t)]
                # Where does this factor of 2 come from?
                
                
                pts = [pos[pid] for pid in t]
                c = (pts[0]+pts[1]+pts[2])/3
                A = area(pts)

                """
                ml_t = [sn.get(pid) for pid in t]
                if len(set(u[0] for u in ml_t))==1:
                    # All vertices contained in the same midline section
                    A_ml = area([u[1:] for u in ml_t])
                    A_tot[i] += A_ml
                    A_eta

                else:
                """

                for pid in t:
                    i, s, eta = sn[pid]
#                i, s, eta = xy_to_ml(c, kappa, l, x0, theta0, bp_data)
               # print A, mu, eta, A*mu, A*eta
                    if correct_dist:
                        f = 1.0/(1.0 - kappa[i]*eta)
                    else:
                        f = 1.0
                    A_tot[i] += A
                    Aeta_tot[i] += A*eta*f
                    Amu_tot[i] += A*mu
                    Amueta_tot[i] += A*mu*eta*f
                    Amueta2_tot[i] += A*mu*eta*eta*f*f
                    # Note, should be /3 but fine once averaged
#        raw_input("?")
        mu_ave = Amu_tot/A_tot
        eta_ave = Aeta_tot/A_tot
        mueta_ave = Amueta_tot/A_tot
        mueta2_ave = Amueta2_tot/A_tot
        return mu_ave, eta_ave, mueta_ave, mueta2_ave

    def get_l(self):
        return self.db.get_property('l')

    def get_regr_kdot(self):
        db = self.db
        pos = db.get_property('position')


        self.model.set_visc(db)

        mu, eta, mueta, mueta2 = self.calculate_averages()

        print 'mu', mu
        print 'eta', eta
        print 'mueta', mueta
        print 'mueta2', mueta2

        params = get_parameters(db, self.model.name)
        p = params['turgor_pressure']

        D = 1/(mueta*mueta-mu*mueta2)

        regr = p*(mueta*eta - mueta2)*D

        kdot = (regr*mueta-p*eta)/mueta2

        return regr, kdot
                

    def divide_segment_no_recalc(self, m):
        db = self.db
        kappa = db.get_property('kappa')
        kappa.append(None)
        kappa[m+1:] = kappa[m:-1]
        l = db.get_property('l')
        l.append(None)
        l[m+2:] = l[m+1:-1]
        l[m] = l[m]/2
        l[m+1] = l[m]
        self.N+=1

    def divide_segment(self, m):
        self.divide_segment(self, m)
        self.calculate_ml_coords()

    def divide_long(self, l_crit):
        db = self.db
        l = db.get_property('l')
        i = 0
        recalc = False
        while i<len(l):
            if l[i] > l_crit:
                self.divide_segment_no_recalc(i)
                print "divide: ", i
                i+=2
                recalc = True
            else:
                i+=1
        if recalc:
            self.calculate_ml_coords()
            
                
    def step(self):

        db = self.db

        self.calculate_ml_coords()
        params = get_parameters(db, self.model.name)
        self.divide_long(params['section_max_length'])


        pos = db.get_property('position')
        vel = db.get_property('velocity')


        kappa = db.get_property('kappa')
        l = db.get_property('l')
        x0 = db.get_property('x0')
        theta0 = db.get_property('theta0')

	bp_old = breakpoints(kappa, l, x0, theta0)

        m = db.get_property('sn')

        self.model.set_visc(db)

        mesh = get_mesh(db)
        old_V = {}
        for cid in mesh.wisps(2):
            old_V[cid]=face_surface_2D(mesh, pos, cid)
    

        regr, kdot = self.get_regr_kdot()

        print 'kdot: ', kdot
        print 'regr: ', regr
#        a = raw_input("?")

        params = get_parameters(db, self.model.name)

        dt = params['relaxation_time']#/60.0/60.0        

        vel = db.get_property('velocity')

        v_bp = []
        for x in bp_old[0]:
            m0 =  xy_to_ml(x, kappa, l, x0, theta0, bp_old)
            v_bp.append(ml_to_vel(m0, kappa, l, kdot, regr, 
                                 x0, theta0))
   
        for i in range(len(kappa)):
            kappa[i] += kdot[i]*dt
        print 'kappa: ', kappa

        for i in range(len(l)):
            l[i] *= exp(regr[i]*dt)
        print 'l: ', l
	bp_new = breakpoints(kappa, l, x0, theta0)

              

        for pid, sn in m.iteritems():
            new_pos = ml_to_xy(sn, kappa, l, x0, theta0, bp_new)
            pos[pid] = new_pos #ml_to_xy(sn, self.kappa, self.l, self.x0, self.theta0)

        for pid, sn in m.iteritems():
            vel[pid] = ml_to_vel(sn, kappa, l, kdot, regr, x0, theta0)

        mV = {}
        for cid in mesh.wisps(2):
            mV[cid]=face_surface_2D(mesh, pos, cid)/old_V[cid]
        
        for prop_name, rule in db.get_property('divided_props').iteritems():
            if rule[0]=='cell' and rule[1]=='concentration':
                prop = db.get_property(prop_name)
                for cid in mesh.wisps(2):
                    if prop[cid] is not None:
                        prop[cid] = prop[cid]/mV[cid]
        db.set_property('kdot', kdot)
        db.set_property('regr', regr)



    def calculate_ml_coords(self):
        db = self.db
        sn = db.get_property('sn')
        kappa = db.get_property('kappa')
        l = db.get_property('l')
        x0 = db.get_property('x0')
        theta0 = db.get_property('theta0')
        bp_data = breakpoints(kappa, l, x0, theta0)
        pos = db.get_property('position')
        mesh = get_mesh(db)
        sn.clear()
        for pid, v in pos.iteritems():
            sn[pid] = xy_to_ml(v, kappa, l, x0, theta0, bp_data)
        

def midline_distances(db):
    l = db.get_property('l')
    sn = db.get_property('sn')
    l_tot = sum(l)
    cl = [0.0]
    for d in l[:-1]:
        cl.append(cl[-1]+d)
    dist = {}
    for pid, (i, xi, eta) in sn.iteritems():
        dist[pid] = l_tot - (cl[i]+l[i]*xi)
    return dist

def midline_distance_point(db, p):
    l = db.get_property('l')
    kappa = db.get_property('kappa')
    x0 = db.get_property('x0')
    theta0 = db.get_property('theta0')
                        
    l_tot = sum(l)
    cl = [0.0]
    for d in l[:-1]:
        cl.append(cl[-1]+d)
    dist = {}
    i, xi, eta = xy_to_ml(p, kappa, l, x0, theta0)
    dist = l_tot - (cl[i]+l[i]*xi)
    return dist
    

def apply_bend(db, d_kappa):
        kappa = db.get_property('kappa')
        l = db.get_property('l')
        x0 = db.get_property('x0')
        theta0 = db.get_property('theta0')
        bp_data = breakpoints(kappa, l, x0, theta0)
        pos = db.get_property('position')
        mesh = get_mesh(db)
        m = {}
        for pid, v in pos.iteritems():
            m[pid] = xy_to_ml(v, kappa, l, x0, theta0, bp_data)    

        for i in range(len(kappa)):
            kappa[i] += d_kappa[i]
        print 'kappa: ', kappa
        bp_new = breakpoints(kappa, l, x0, theta0)
        for pid, sn in m.iteritems():
            new_pos = ml_to_xy(sn, kappa, l, x0, theta0, bp_new)
            pos[pid] = new_pos #ml_to_xy(sn, self.kappa, self.l, self.x0, self.theta0)

        
