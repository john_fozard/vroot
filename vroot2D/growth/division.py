
from openalea.tissueshape import centroid,divide_face,face_main_axes_3D,face_surface_3D,edge_length

from numpy import array as Vector2

from vroot2D.utils.geom import centroid, long_axis, area
from vroot2D.growth.mesh_nbs import remesh_cells

"""
Module to perform cell division.
"""


class AbstractDivisionRule(object):
    """
    Abstract class to document the interface for cell division
    rules
    """

    def set_default_parameters(self, db):
        """
        """
        pass

    def does_cell_divide(self, db, cid):
        """
        Called for every cell to see if it divides at this timestep.
        :param db: Tissue database
        :type db: TissueDB
        :param cid: Id number of the cell to query
        :type cid: int
        :returns: bool -- whether the cell is ready to divide
        """
        pass


    def get_axis_and_post_division_rule(self, db, cid):
        """
        This is called to obtain the division axis and rule for
        post-division.
        :param db: Tissue database
        :type db: TissueDB
        :param cid: Id number for cell to divide
        :returns: (point, normal), post_rule
                  (point, normal) is a point on and a normal to the division
                  plane; post_rule is the post-division rule
                  (see AbstractDivisionRule.post_division_rule).
        """
        pass

    def post_division_rule(self, db, lineage):
        """
        Rule for post-division behaviour (not part of the interface;
        but to show the API for the function returned from 
        get_axis_and_post_division_rule).
        This post-division rule is called after the tissue has been
        divided, but before the sub-cellular mesh is updated.

        :param db: Tissue database
        :param lineage: Lineage dictionary from the division routines.
                        This is a collection of dictionaries,
                        where lineage[0] is the one for points, 
                        lineage[1] that for walls, etc.
                        Each dictionary maps the ids of parent objects
                        to those of new ones
                        e.g. lineage[2][pid] is the list of cells
                        with parent pid. 
                        Those objects with no parent have pid None 
                        e.g. lineage[0][None] are the new points with
                        no parent.
        :type lineage: dict

        """
        pass


class Division(object):
    """
    Class to handle cell division.

    There are two key inputs - the division_rule, which is an implementation
    of AbstractDivisionRule, and the tissue property
    'divided_props'.
    
    An implementation of AbstractDivisionRule has two key methods:
    
    does_cell_divide(self, db, cid) - called to tell if a
    particular cell is about to divide at a given timestep.
    
    get_axis_and_post_division_rule(self, db, cid) - called
    to obtain the axis of division, and also a post-division rule
    which may reset certain properties of the cells following the division
    event.

    'divided_props' describes how tissue properties 
    are to be updated locally (and transferred onto the new cell) following
    the division event. It is a dictionary which represents a 
    map from property names to rules, 
    which are tuples with three elements: 
    rule[0] is the type of element
    on which the property is defined, e.g. 'cell', 'edge' or '(cell, wall').
    rule[1] is a description of the type of quantity this property is. 
    Currently only 'amount' is handled as a special case - the property 
    is treated
    as the total amount of substance associated with the new element,
    and apportioned to the two daughter elements in proportion to their
    size. Any other case is treated as a concentration / description, 
    and simply copied verbatim to the daughter elements.
    (This may be extended to treat 'nuclear concentrations' in future.)
    rule[2] is the default value for those elements which have no parents -
    e.g. the new wall in the case of cell division.
    """

    def __init__(self, db, division_rule):
        """
        :param db: Tissue database
        :type db: TissueDB
        :param division_rule: Cell division rule
        :type division_rule: Implements AbstractDivisionRule interface
        """
        print 'DIVISION INITIATION'

        self.db=db
        # Rule for timing of cell division, division axis,
        # and post-division effects
        self.division_rule = division_rule
        division_rule.set_default_parameters(db)
        # Keep track of lineage of all cells
        self.cell_lineage={}
        db.set_property('cell_lineage', self.cell_lineage)
        db.set_description('cell_lineage', '')
        self.extractDB()



    def divide_cell_axis(self, dividing_cid, division_axis, post_rule):
        """
        Actually perform cell division (ignoring cell triangulation)
        :param dividing_cid: Id of dividing cell
        :param division_axis: (point, normal) point within and normal to
                              division plane             
        :type division_axis: tuple
        :param post_rule: function(TissueDB, lineage) called after
                          cell division
        """

        division_plane_pt,  division_plane_normal = division_axis

        mesh = self.mesh
        graph = self.graph
        pos = self.pos
        wall = self.wall

        # Make a copy of some of the tissue information in the state
        # immediately prior to cell division
        # (all) vertex positions
        old_pos=list(pos)
        #  walls bounding the dividing cell
        old_walls=list(mesh.borders(2, dividing_cid)) 
        old_wall_regions=dict( (wid, list(mesh.regions(1,wid))) for wid in old_walls)

        # Also make a copy of the edges in the 
        old_out_edges=[(eid, graph.source(eid), graph.target(eid)) \
                       for eid in graph.out_edges(dividing_cid)]

        old_in_edges=[(eid, graph.source(eid), graph.target(eid)) \
                      for eid in graph.in_edges(dividing_cid)]

        
#        print 'pre: ', set(type(cid) for cid in mesh.wisps(2))

        #divide cell and update mesh,pos
        lineage = divide_face(mesh, pos, dividing_cid, 
                              division_plane_pt, 
                              division_plane_normal,
                              .001)
        self.cell_lineage[dividing_cid]=lineage[2][dividing_cid]

        #update properties



        for pid in lineage[0][None]:
            pos[pid]=Vector2(pos[pid])

        # It's entirely unclear what divide_face has done to graph!


        #update graph & wall with new edges between previously existing cells and two new cells
        graph_lineage={}
        for eid, sid, tid in old_out_edges:
            wid=wall[eid]
            wall.pop(eid)
            if wid not in lineage[1]:
                new_sid=(set(mesh.regions(1,wid)) - set([tid])).pop()
                new_eid=graph.add_edge(new_sid, tid)
                graph_lineage[eid]=[ new_eid ]
                wall[new_eid]=wid

            else:
                graph_lineage[eid]=[]
                for new_wid in lineage[1][wid]:
                    new_sid=(set(mesh.regions(1,new_wid)) - set([tid])).pop()
                    new_eid=graph.add_edge(new_sid, tid)
                    graph_lineage[eid].append(new_eid)
                    wall[new_eid]=new_wid

        for eid, sid, tid in old_in_edges:
            wid=wall[eid]
            wall.pop(eid)
            if wid not in lineage[1]:
                new_tid=(set(mesh.regions(1,wid)) - set([sid])).pop()
                new_eid=graph.add_edge(sid, new_tid)
                graph_lineage[eid]=[ new_eid ]
                wall[new_eid]=wid

            else:
                graph_lineage[eid]=[]
                for new_wid in lineage[1][wid]:
                    new_tid=(set(mesh.regions(1,new_wid)) - set([sid])).pop()
                    new_eid=graph.add_edge(sid, new_tid)
                    graph_lineage[eid].append(new_eid)
                    wall[new_eid]=new_wid


        #add edges between two new cells
        graph_lineage[None]=[]
        new_eid=graph.add_edge(lineage[2][dividing_cid][0], lineage[2][dividing_cid][1])
        wall[new_eid]=lineage[1][None][0]
        graph_lineage[None].append(new_eid)

        new_eid=graph.add_edge(lineage[2][dividing_cid][1], lineage[2][dividing_cid][0])
        wall[new_eid]=lineage[1][None][0]
        graph_lineage[None].append(new_eid)

        divided_props=self.db.get_property('divided_props')
        for prop_name, rule in divided_props.iteritems():
            prop = self.db.get_property(prop_name)
            # Cell based properties
            if rule[0]=='cell':
                for old_cid in lineage[2]:
                    if old_cid is not None:
                        mem=prop.pop(old_cid)
                        if rule[1]!='amount':
                            # If not a total amount of substance, copy value of parent to daughter cells
                            for new_cid in lineage[2][old_cid]:
                                prop[new_cid]=mem
                        else:
                            # If total amount, apportion to daughter cells in proportion to their volume
                            total_area=sum(face_surface_2D(mesh, pos, new_cid) for new_cid in lineage[2][old_cid])
                            for new_cid in lineage[2][old_cid]:
                                prop[new_cid]=mem*face_surface_2D(mesh, pos, new_cid)/total_area
                    else:
                        # No parent cell: update with default value
                        for new_cid in lineage[2][old_cid]:
                            prop[new_cid]=rule[2]

            if rule[0]=='wall':
                for old_wid in lineage[1]:
                    if old_wid is not None:
                        mem=prop.pop(old_wid)
                        if rule[1]!='amount':
                            # If not a total amount of substance, copy value of parent to daughter cells
                            for new_wid in lineage[1][old_wid]:
                                prop[wid_cid]=mem
                        else:
                            raise
                        # Not implemented
                    else:
                        # No parent cell: update with default value
                        for new_wid in lineage[1][old_wid]:
                            prop[ne]=rule[2]
            if rule[0]=='(cell, wall)':
                # Properties that are associated with a particular wall, viewed from one of the two
                # cells on either side.
                for old_wid in lineage[1]:
                    if old_wid is not None:
                        for old_cid in old_wall_regions[old_wid]:
                            mem=prop.pop((old_cid, old_wid))
                            if rule[1]!='amount':
                                for new_wid in lineage[1][old_wid]:
                                    if old_cid in lineage[2]:
                                        ncids=set(lineage[2][old_cid]) & set(mesh.regions(1, new_wid))
                                        new_cid=ncids.pop()
                                    else:
                                        new_cid=old_cid
                                    prop[(new_cid, new_wid)]=mem

                            else:
                                total_length=sum(edge_length(mesh, pos, new_wid) for new_wid in lineage[1][old_wid])
                                for new_wid in lineage[1][old_wid]:                                
                                    if old_cid in lineage[2]:
                                        ncids=set(lineage[2][old_cid]) & set(mesh.regions(1, new_wid))
                                        new_cid=ncids.pop()
                                    else:
                                        new_cid=old_cid
                                    prop[(new_cid, new_wid)]=mem*edge_length(mesh, pos, new_wid)/total_length
                    else:
                        if rule[1]!='amount':
                            for new_wid in lineage[1][None]:
                                for new_cid in mesh.regions(1,new_wid):                
                                    prop[(new_cid, new_wid)]=rule[2]
                        else:
                            for new_wid in lineage[1][None]:
                                for new_cid in mesh.regions(1,new_wid):                 
                                    prop[(new_cid, new_wid)]=edge_length(mesh, pos, new_wid)
                for old_wid in set(old_walls).difference(lineage[1]):
                    ncids=set(lineage[2][dividing_cid]) & set(mesh.regions(1, old_wid))
                    new_cid=ncids.pop()
                    prop[(new_cid, old_wid)]=prop.pop((dividing_cid, old_wid))
            if rule[0]=='edge':
                for old_eid, new_eids in graph_lineage.iteritems():
                    if old_eid:
                        old_value=prop.pop(old_eid)
                    else:
                        old_value=rule[2]
                    for eid in new_eids:
                        prop[eid]=old_value
        post_rule(self.db, lineage)
        return lineage


#*****************************************************************************

    def extractDB(self):
        db=self.db
        t = db.tissue()
        cfg = db.get_config('config')

        self.graph = t.relation(cfg.graph_id) # directed cell to cell graph
        self.mesh = t.relation(cfg.mesh_id) # vertex,wall,cell topomesh

        self.wall=db.get_property('wall') 
        # wall ids for each directed graph edge
        
        self.cell_type = db.get_property('cell_type') 
        # cell_types for each cell_id

        self.pos = db.get_property('position')

    def step(self):
        print 'division_step'

#        print self.db.get_property('PLT')
        self.extractDB()
        division_occurs=False
        for cid in list(self.mesh.wisps(2)):
            if self.division_rule.does_cell_divide(self.db, cid):
                print 'divide: '
                axis, post_rule = self.division_rule.\
                    get_axis_and_post_division_rule(self.db, cid)
                print cid, axis
                lineage = self.divide_cell_axis(cid, axis, post_rule)
                division_occurs = True

        print 'end_division'
        return division_occurs

from copy import copy

class MeshDivision(Division):
    """
    Cell division for triangulated cells. Specializes
    Division to handle the meshes within each cell.
    """

    def __init__(self, db, division_rule, mesh_tri_area=300):
        super(MeshDivision, self).__init__(db, division_rule)
        self.mesh_tri_area = mesh_tri_area
        
    def divide_cell_axis(self, dividing_cid, division_axis, post_rule):
        """
        Actually perform cell division (ignoring cell triangulation)
        :param dividing_cid: Id of dividing cell
        :param division_axis: (point, normal) point within and normal to
                              division plane             
        :type division_axis: tuple
        :param post_rule: function(TissueDB, lineage) called after
                          cell division
        """


        self.extractDB()
        old_pos=copy(self.db.get_property('position'))

        nb_cells={}
        for wid in self.mesh.borders(2,dividing_cid):
            nb=set(self.mesh.regions(1,wid))-set([dividing_cid])
            if len(nb)>0:
                nb_cells[wid]=nb.pop()

        lineage=super(MeshDivision, self).divide_cell_axis(dividing_cid,
                                                           division_axis,
                                                           post_rule)
        cells_to_remesh=[ nb_cells[wid] for wid in lineage[1] if wid!=None and wid in nb_cells ]
        
        remesh_cells(self.db, cells_to_remesh+[dividing_cid], lineage[2], old_pos, self.mesh_tri_area)
        new_cells = cells_to_remesh+list(lineage[2][dividing_cid])
 #       print new_cells

        divided_props = self.db.get_property('divided_props')
        cell_triangles = self.db.get_property('cell_triangles')

        for prop_name, ( prop_type, rule, default ) in divided_props.iteritems():
            if prop_type == '(cell, tri)' and prop_name !='tri_ref_pos':
                prop = self.db.get_property(prop_name)
                r_list = []
                for cid, t in prop:
                    if cid in cells_to_remesh:
                        r_list.append((cid,t))
                for cid, t in r_list:
                    del prop[(cid, t)]

                for cid in new_cells:
                    for t in cell_triangles[cid]:
                        prop[(cid,t)] = default
                
  #              print prop_name, prop, default
            
        return lineage
