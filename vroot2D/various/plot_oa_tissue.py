
import sys

from openalea.celltissue import TissueDB
from openalea.container import ordered_pids
from matplotlib.collections import PolyCollection, CircleCollection
from matplotlib.text import Text

from random import random

from matplotlib.lines import Line2D

from openalea.tissueshape import centroid

import matplotlib.pylab as plt




def plotDB(db):

    db.set_property('scale', 1.0)
    db.set_description('scale', 'distance unit in microns')

    t = db.tissue()
    cfg = db.get_config('config')

    #Read in properties
    graph = t.relation(cfg.graph_id) # directed cell to cell graph
    mesh = t.relation(cfg.mesh_id) # vertex,wall,cell topomesh

    pos = db.get_property('position')
    cell_type = db.get_property('cell_type')
    border = db.get_property('border')
    print 'cell_types', cfg.cell_types
    print 'border', cfg.border

    e = 0.9
#    fc_map = dict((i, (e*random()+1-e, e*random()+1-e, e*random()+1-e)) for i in cfg.cell_types)
    

    fc_map = { 0:(1,0,0) }
    for i in range(10):
        fc_map[i+1]=(0.9,0.9,0.9)

    plt.figure(1)
    polys = []
    cols = []
    for cid in mesh.wisps(2):
        pids = ordered_pids(mesh, cid)
        polys.append([pos[pid] for pid in pids])
        if border.get(cid, None) == 1:
            cols.append((0.95, 0.95, 0.95))
        else:
            cols.append(fc_map[cell_type[cid]]) 
    a=plt.gcf().add_axes([0.1, 0.1, 0.8, 0.8])
    a.set_aspect('equal')
    col=PolyCollection(polys, facecolors=cols)
    a.add_collection(col, autolim=True)

    centroids = {}
    for cid in mesh.wisps(2):
        centroids[cid] = centroid(mesh, pos, 2, cid)
    # old cell ids
    if 'old_cell_id' in db.properties():
        old_cid=db.get_property('old_cell_id')
        for cid in mesh.wisps(2):
            c = centroids[cid]
            txt = Text(c[0], c[1], str(old_cid[cid]), ha='center', va='center', size='small')
#            a.add_artist(txt)
    else:
        for cid in mesh.wisps(2):
            c = centroids[cid]
            txt = Text(c[0], c[1], str(cid), ha='center', va='center', size='x-small')
#            a.add_artist(txt)

        
 #   for cid in mesh.wisps(2):
 #       c = centroids[cid]
 #       txt = Text(c[0], c[1], str(cell_type[cid]), ha='center', va='center', size='small')
 #       a.add_artist(txt)

    eps = 0.07
    


    if 'PIN' in db.properties() and 'wall' in db.properties():
        PIN = db.get_property('PIN')
        wall = db.get_property('wall')
        for eid in PIN:
            if PIN[eid]>0:
                sid = graph.source(eid)
                tid = graph.target(eid)
                wid = wall[eid]
                c = centroids[sid]
                pid0, pid1 = mesh.borders(1,wid)
                p0 = [pos[pid0][0]*(1-eps)+c[0]*eps,
                      pos[pid0][1]*(1-eps)+c[1]*eps]
                p1 = [pos[pid1][0]*(1-eps)+c[0]*eps,
                      pos[pid1][1]*(1-eps)+c[1]*eps]		
#                a.add_line(Line2D((p0[0],p1[0]),(p0[1],p1[1]), color='g', lw=4))

    a.autoscale_view(tight=False)
    plt.show()
    return 
    for eid in graph.edges():
        p0 = centroids[graph.source(eid)]
        p1 = centroids[graph.target(eid)]
#        a.add_line(Line2D((p0[0],p1[0]),(p0[1],p1[1]), color='r', lw=1))


    if 'edge_type' in db.properties():
        edge_type = db.get_property('edge_type')
        edge_cols = dict((i, (random(), random(), random())) for i in set(edge_type.itervalues()))
        for eid in edge_type:
            sid = graph.source(eid)
            tid = graph.target(eid)
            wid = wall[eid]
            c = centroids[sid]
            pid0, pid1 = mesh.borders(1,wid)
            p0 = [pos[pid0][0]*(1-eps)+c[0]*eps,
                  pos[pid0][1]*(1-eps)+c[1]*eps]
            p1 = [pos[pid1][0]*(1-eps)+c[0]*eps,
                  pos[pid1][1]*(1-eps)+c[1]*eps]		
#            a.add_line(Line2D((p0[0],p1[0]),(p0[1],p1[1]), color=edge_cols[edge_type[eid]], lw=4))

    a.autoscale_view(tight=False)
    plt.show()

    

    print pos

def main():
    if len(sys.argv)<2:
        print "Usage: python plot_oa_tissue.py filename.zip"
        sys.exit(0)
    db = TissueDB()
    db.read(sys.argv[1])
    plotDB(db)

if __name__=='__main__':
    main()
