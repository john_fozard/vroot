
#!/usr/bin/env python

import numpy as np
from openalea.celltissue import TissueDB
from openalea.container import ordered_pids
from openalea.tissueshape import centroid
from vroot2D.utils.geom import area
import scipy.linalg as la
from vroot2D.utils.db_utilities import get_mesh, get_graph
from collections import defaultdict
from PyQt4 import QtCore, QtGui, QtOpenGL
from PyQt4.QtCore import  pyqtSlot


def to_val(s):
    s=str(s)
    if s=='None':
        return None
    else:
        try:
            s=int(s)
        except:
            try:
                s=float(s)
            except:
                pass
            pass
        return s

class PropertyEditor(QtGui.QDialog):
    def __init__(self, db, prop_names, cid, parent):        
        QtGui.QDialog.__init__(self, parent)
        layout = QtGui.QVBoxLayout()
        self.db = db
        self.cid = cid
        self.setLayout(layout)
        self.tw=QtGui.QTableWidget(0,2)
        self.tw.setHorizontalHeaderLabels(["property","value"])
        self.keys=[]
        self.props=[(pn, db.get_property(pn)[cid]) for pn in prop_names]
        self.tw.setRowCount(len(self.props))      
        for n,ki in enumerate(self.props):
            self.tw.setItem(n,0,QtGui.QTableWidgetItem(ki[0]))
            self.tw.setItem(n,1,QtGui.QTableWidgetItem(str(ki[1])))
            self.keys.append(ki[0])
        layout.addWidget(self.tw)
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.show()
        #self.setMainWidget(self.tw)
        self.connect(self.tw, QtCore.SIGNAL("itemChanged(QTableWidgetItem*)"), self.change_item)
    
    def change_item(self, item):
        """
        if item.column()==0:
            if item.row()==self.tw.rowCount()-1:
                val_item=self.tw.item(item.row(),1)
                key=str(item.text())
                if val_item:
                    self.props[key]=to_val(val_item.text())
                self.tw.insertRow(item.row()+1)
                self.keys.append(key)
            else:
                key=str(item.text())
                self.props[key]=self.props.pop(self.keys[item.row()])
                self.keys[item.row()]=key
        """
        if item.column()==1:
            key_item=self.tw.item(item.row(),0)
            if key_item:
                key=self.keys[item.row()]
                self.db.get_property(key)[self.cid]=to_val(item.text())
#        self.item.setCol()
            
def ordered_wids_pids(mesh, pos, cid):
    wid_set = set(mesh.borders(2,cid))
    wid = wid_set.pop()
    o_wids = [wid]
    o_pids = list(mesh.borders(1, wid))
    end = o_pids[-1]
    while wid_set:
        for wid in wid_set:
            if end in mesh.borders(1, wid):
                wid_set.discard(wid)
                o_wids.append(wid)
                end = (set(mesh.borders(1, wid)) - set([end])).pop()
                o_pids.append(end)
                break
    o_pids.pop()
    if area([pos[pid] for pid in o_pids])<0:
        o_wids.reverse()
        o_pids=[o_pids[0]]+o_pids[-1:0:-1]
    return o_wids, o_pids
   

class GraphicsView(QtGui.QGraphicsView):
    def __init__(self, parent=None):
        QtGui.QGraphicsView.__init__(self, parent)
#        self.setViewport(QtOpenGL.QGLWidget(QtOpenGL.QGLFormat(QtOpenGL.QGL.SampleBuffers)))
        self.setDragMode(QtGui.QGraphicsView.RubberBandDrag)
        self.setRenderHint(QtGui.QPainter.Antialiasing)
        self.setRenderHint(QtGui.QPainter.TextAntialiasing)
        self.scale(1,-1)
        self.initTransform=self.transform()
        self.angle=0
    
    def wheelEvent(self, event):
        factor=1.41**(-event.delta()/240.0)
        
        self.scale(factor, factor)


class PointItem(QtGui.QGraphicsEllipseItem):
    def __init__(self, scene, pid, r=1.0):
        print 'POINT', pid
        self.scene = scene
        self.pid = pid
        self.r = r
        v = self.scene.db.get_property('position')[pid]
        rect=QtCore.QRectF(-r,-r,2*r,2*r)
        QtGui.QGraphicsEllipseItem.__init__(self,rect,None,scene)
        self.setPos(v[0],v[1])
        self.setZValue(1.0)
        self.setFlags(QtGui.QGraphicsItem.ItemIsSelectable | 
                      QtGui.QGraphicsItem.ItemIsMovable |
                      QtGui.QGraphicsItem.ItemSendsGeometryChanges )
        
        self.setBrush(QtCore.Qt.darkRed)

    def itemChange(self, change, value):
        if (change == QtGui.QGraphicsItem.ItemPositionChange): 
            newPos = value.toPointF()
            print "Move", newPos.x(), newPos.y()

            self.scene.pointMoved(self.pid, newPos.x(), newPos.y())
        return  QtGui.QGraphicsItem.itemChange(self, change, value)




class EdgeItem(QtGui.QGraphicsPolygonItem):
    def __init__(self, scene, pts, eid=None):
        self.scene=scene
        self.eid = eid
#        QtGui.QGraphicsLineItem.__init__(self, pts[0][0], pts[0][1], pts[1][0], pts[1][1], None, scene) 
        QtGui.QGraphicsPolygonItem.__init__(self, None, scene)
        self.poly=QtGui.QPolygonF([QtCore.QPointF(v[0],v[1]) for v in pts])
        self.setPolygon(self.poly)
        self.setPen(QtGui.QPen(1))
        if eid:
            self.setFlag(QtGui.QGraphicsItem.ItemIsPanel)
        else:
            self.setEnabled(False)
        self.setZValue(-0.9)

    def setCol(self, r, g, b):
        self.col=QtGui.QColor.fromRgbF(r, g, b)
        self.setBrush(self.col)
   

class PolyItem(QtGui.QGraphicsPolygonItem):
    def __init__(self, scene, idx, pts, col):
        self.scene=scene
        self.idx=idx
        QtGui.QGraphicsPolygonItem.__init__(self,None,scene)
        self.setZValue(-1.0)
        self.setFlags(QtGui.QGraphicsItem.ItemIsSelectable)
        self.poly=QtGui.QPolygonF([QtCore.QPointF(v[0],v[1]) for v in pts])
        self.setPolygon(self.poly)
        self.col=QtGui.QColor.fromRgbF(*col)
        self.setBrush(self.col)

    def setPoints(self, pts):
        self.poly=QtGui.QPolygonF([QtCore.QPointF(*p) for p in pts])
        self.setPolygon(self.poly)
      
    def mousePressEvent(self, event):
        print type(self.scene.parent())
        db = self.scene.db
        prop_names = [ pn for pn in db.properties() if type(db.get_property(pn))==dict and self.idx in db.get_property(pn) ]
        p=PropertyEditor(db, prop_names, self.idx, self.scene.parent())

    def setCol(self, r, g, b):
        self.col=QtGui.QColor.fromRgbF(r, g, b)
        self.setBrush(self.col)

from time import sleep
class GraphicsScene(QtGui.QGraphicsScene):
    
    def __init__(self, parent, db):
        QtGui.QGraphicsScene.__init__(self,parent)
        self.db=db
        self.delta=1
        self.generateScene()
        self.parent().prop_combo.currentIndexChanged['QString'].connect(self.set_prop)
        self.parent().PIN_combo.currentIndexChanged['QString'].connect(self.set_PIN)
        self.parent().vert_box.toggled['bool'].connect(self.toggled)


    def generateScene(self):
        self.polyItems = {}
        self.edgeItems = {}
        self.pointItems = {}
        mesh = get_mesh(self.db)
        graph = get_graph(self.db)
        pos = self.db.get_property('position')
        cell_type = self.db.get_property('cell_type')
        if 'wall' in self.db.properties():
            wall = self.db.get_property('wall')
        else:
            wall = {}
        #PIN = self.db.get_property('PIN')
        self.walls_to_edges = defaultdict(list)

        for pid in mesh.wisps(0):
            pi = PointItem(self, pid)
            self.pointItems[pid]=pi
            
        for eid, wid in wall.iteritems():
            self.walls_to_edges[wid].append(eid)
        for cid in mesh.wisps(2):
            wids, pids=ordered_wids_pids(mesh, pos, cid)
            pts=[np.array(pos[pid]) for pid in pids]
            centre = centroid(mesh, pos, 2, cid)
            col=(0.5, 0.5, 0.5)
            pi=PolyItem(self, cid, pts, col)
            self.polyItems[cid] = pi
            i_pos = {}
            N = len(pids)
            for i in range(N):
                t2 = pts[(i+1)%N]-pts[i]
                t1 = pts[i]-pts[i-1]
                t1=t1/la.norm(t1)
                t2=t2/la.norm(t2)
                t = (t1+t2)/(1+np.dot(t1,t2))
                #t = t/la.norm(t)
                i_pos[pids[i]] = pts[i] - self.delta*np.array((t[1],-t[0]))
            for wid in mesh.borders(2, cid):
                pid1, pid2 = mesh.borders(1,wid)
                p1 = pos[pid1]
                p2 = pos[pid2]
                i_p1 = i_pos[pid1]
                i_p2 = i_pos[pid2]
                eid = next((eid for eid in self.walls_to_edges[wid] if \
                                graph.source(eid)==cid), None)
#                print wid, eid, self.walls_to_edges[wid]
                if eid:
                    self.edgeItems[eid] = EdgeItem(self, [p1, p2, i_p2, i_p1], eid)



#            for ei,e in enumerate(c.edges):

        """
        if 'col_file' not in self.db.properties():
            col_file = dict((cid, 0) for cid in mesh.wisps(2))
            self.db.set_property('col_file', col_file)
            self.db.set_description('col_file', '')
#          
        """

        # find cell properties
        cid = next(mesh.wisps(2))
        self.prop_names = [pn for pn in self.db.properties() if type(self.db.get_property(pn))==dict and cid in self.db.get_property(pn)]
        eid = next(graph.edges())
        self.PIN_names = [pn for pn in self.db.properties() if type(self.db.get_property(pn))==dict and eid in self.db.get_property(pn)]
        
        self.parent().prop_combo.clear()
        self.parent().prop_combo.addItems(self.prop_names)
        self.parent().PIN_combo.clear()
        self.parent().PIN_combo.addItems(self.PIN_names)

    
#    @pyqtSlot('const QString &')
    def set_prop(self, pn):
        print "changed", pn
        pn = str(pn)
        if pn in self.prop_names:
            prop = self.db.get_property(pn)
            prop_max = max(prop.itervalues())
            prop_min = min(prop.itervalues())
            for cid in self.polyItems:
                if cid in prop:
                    v = float(prop[cid] - prop_min)/max(prop_max-prop_min,1e-10)
                    self.polyItems[cid].setCol(1.0-v, 1.0-v, 1.0)
                    print cid, v
                else:
                    self.polyItems[cid].setCol(0.5, 0.5, 0.5)

    def set_PIN(self, pn):
        print "changed", pn
        pn = str(pn)
        if pn in self.PIN_names:
            prop = self.db.get_property(pn)
            prop_max = max(prop.itervalues())
            prop_min = min(prop.itervalues())
            for eid in self.edgeItems:
                if eid in prop:
                    v = float(prop[eid] - prop_min)/max(prop_max-prop_min,1e-10)
                    self.edgeItems[eid].setCol(1.0-v, 1.0, 1.0-v)
                    print eid, v
                else:
                    self.edgeItems[eid].setCol(0.5, 0.5, 0.5)

    def toggled(self, state):
        for pid in self.pointItems:
            self.pointItems[pid].setVisible(state)
                    
        
    def pointMoved(self, pid, x, y):
        mesh=get_mesh(self.db)
        pos = self.db.get_property('position')
        pos[pid] = np.array((x,y), dtype='float')
        for cid in mesh.wisps(2):
            if pid in mesh.borders(2, cid, 2):
#                del self.polyItems[cid]
                wids, pids=ordered_wids_pids(mesh, pos, cid)
                self.polyItems[cid].setPoints([pos[x] for x in pids]  )
        graph = get_graph(self.db)



class MeshViewer(QtGui.QMainWindow):

    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        

        self.createActions()
        self.createMenus()
        self.createToolBar()
        
        self.db=TissueDB()

        self.view = GraphicsView()
   #     self.createScene()
                
        self.setCentralWidget(self.view)        

        self.setWindowTitle("Auxin Transport")
        self.view.fitInView(self.view.sceneRect(), QtCore.Qt.KeepAspectRatio)

    def createMenus(self):
        self.fileMenu = self.menuBar().addMenu("&File")
        self.fileMenu.addAction(self.openAct)
        self.fileMenu.addAction(self.saveAct)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.exitAct)
        
    def createToolBar(self):
        self.toolbar=QtGui.QToolBar()
        self.addToolBar(QtCore.Qt.TopToolBarArea, self.toolbar)
        self.prop_combo = QtGui.QComboBox()
        self.toolbar.addWidget(self.prop_combo)
        self.PIN_combo = QtGui.QComboBox()
        self.toolbar.addWidget(self.PIN_combo)
        self.vert_box = QtGui.QCheckBox("Show vertices")
        self.vert_box.setChecked(True)
        self.toolbar.addWidget(self.vert_box)
               
    def open(self):
        fname=QtGui.QFileDialog.getOpenFileName(self, 
             "Mesh - Load Mesh", ".", "Tissue files (*.zip);;All files (*)")
        if not fname.isEmpty():
            self.loadFile(unicode(fname))
    
    def saveAs(self):
        fname=QtGui.QFileDialog.getSaveFileName(self, 
             "Mesh - Load Mesh", ".", "Tissue files (*.zip);;All files (*)")
        if not fname.isEmpty():
            self.saveFile(unicode(fname))

    def loadFile(self, fname):
        self.db=TissueDB()
        self.db.read(fname)
        self.createScene()
        self.view.fitInView(self.view.sceneRect(), QtCore.Qt.KeepAspectRatio)

    def saveFile(self, fname):
        self.db.write(fname)

            
    def createActions(self):
        self.openAct = QtGui.QAction("&Open...", self,
                shortcut=QtGui.QKeySequence.Open,
                statusTip="Open an existing mesh", triggered=self.open)

        self.saveAct = QtGui.QAction("&Save As...", self,
                shortcut=QtGui.QKeySequence.SaveAs,
                statusTip="Save the mesh to disk", triggered=self.saveAs)
        

        self.exitAct = QtGui.QAction("E&xit", self, shortcut="Ctrl+Q",
                statusTip="Exit the application", triggered=self.close)

        
              
    def createScene(self):
        self.scene = GraphicsScene(self, self.db)
        self.view.setScene(self.scene)
    

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
#    fmt = QtOpenGL.QGLFormat.defaultFormat()
#    fmt.setProfile(QtOpenGL.QGLFormat.CompatibilityProfile)
#    QtOpenGL.QGLFormat.setDefaultFormat(fmt)
    meshViewer = MeshViewer()
    if len(sys.argv)>1:
        meshViewer.loadFile(sys.argv[1])
    meshViewer.resize(800,600)
    meshViewer.show()#Maximized()
    sys.exit(app.exec_())
