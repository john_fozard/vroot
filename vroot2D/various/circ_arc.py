
import numpy as np
import matplotlib.pylab as plt
from math import pi
# Piecewise circular curve (circular arc spline) through points

def ml_to_xy(m, kappa, l, x0, theta0):
    j = m[0]
    xi = m[1]
    eta = m[2]
    x = np.array(x0)
    theta = theta0
    for i in range(j):
        t = np.array((cos(theta), sin(theta)))
        n = np.array((-sin(theta), cos(theta)))
        if abs(kappa[i]*l[i])>1e-15:
            x += (1.0/kappa[i])*\
                (sin(kappa[i]*l[i])*t+(2*sin(kappa[i]*l[i]/2)**2)*n)
        else:
            x += l[i]*t        
        theta += kappa[i]*l[i]
    t = np.array((cos(theta), sin(theta)))
    n = np.array((-sin(theta), cos(theta)))
    if abs(kappa[j]*l[j])>1e-15:
        x += (1/kappa[j]*sin(kappa[j]*l[j]*xi)-eta*sin(kappa[j]*l[j]*xi))*t+ \
        (1/kappa[j]*(2*sin(kappa[j]*l[j]*xi/2)**2)+eta*cos(kappa[j]*l[j]*xi))*n
    else:
        x += xi*l[j]*t+eta*n 
    return x

def set_init_midline(db):
    kappa = db.get_property('kappa')
    l = db.get_property('l')
    theta0 = db.get_property('theta0')
    x0 = db.get_property('x0')

    print x0, theta0
    db.set_property('x0', np.array((8340.0, 488.0)))
    db.set_property('theta0', pi-0.02)
    kappa[-6:]+=-0.0001  

# Fit curve through points - this is piecewise biarc interpolation ?!
# Do this manually for now

def main():
    from import_tissue import import_tissue



    db=import_tissue('fullroot2.zip')

    from parameters_fullroot import setSimParameters
    setSimParameters(db)

    from genenetwork import GeneNetworkJPat
    from fullrootModels import GrowthRegulators
    gn=GeneNetworkJPat(db, GrowthRegulators())

    from growth_midline import GrowthMidline
    from parameters_fullroot import setGrowthViscParameters
    setGrowthViscParameters(db)
    g = GrowthMidline(db)

    set_init_midline(db)



    from mpl_plotter import MPLPlotter
    mpl = MPLPlotter(db, 'GP')
    mpl.redraw()

    from mpl_plot_midline import MPLPlotMidline
    mpm = MPLPlotMidline(db)
    mpm.redraw()
    
    plt.show()
