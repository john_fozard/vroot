from matplotlib.ticker import MaxNLocator

import matplotlib.pylab as plt
from scipy.linalg import eig, solve
import numpy as np
import sys

#from matplotlib import rc

#rc('text', usetex=True)

from matplotlib import rc

params = {
          'axes.labelsize': 18,
          'text.fontsize': 18,
          'legend.fontsize': 12,
          'legend.labelspacing' : 0.0,
          'legend.borderaxespad' : 5.0,
          'xtick.labelsize': 18,
          'ytick.labelsize': 19}

plt.rcParams.update(params)
plt.figure(figsize=(8,6))
plt.hold(True)
for fn in sys.argv[1:]:
    data = np.loadtxt(fn)
    plt.plot(data[:,0], data[:,1])
plt.xlabel('t')
plt.ylabel('L')
plt.savefig('length.eps')
plt.show()
