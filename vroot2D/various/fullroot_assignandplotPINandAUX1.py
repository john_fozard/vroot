# Program by Leah, 20th Oct 2011
# Automatically addes PIN, AUX1 and LAX2 membrane proteins on to a root tip geometry, specified using the wild-type rules described in the SI Appendix of Band et al (in prep).
# Makes figures showing the positions of the PINs, AUX1 and LAX.
# Makes new zip file containing the extra dictionaries, PIN, AUX1 and LAX.

# This works automatically unless we observe a double layer of endodermis, in which case we need to specify the intervening wall ids to remove the lateral PINs from the membranes adjacent to these.

import os, sys
from openalea.container import ordered_pids
from celltissue_util import *


from numpy import *
from pylab import *

import matplotlib
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
import matplotlib.pyplot as plt

# Decide what mutant is simulated (all present for wild type):
AUX1_proteins='present'
LAX_proteins='present'

########################################################################################################
# Read in the tissue file:
#########################################################################################################
PATH='R2_data_Q1220'
db,=read('%s/R2_Q1220data_wsn_6pix_OA_fl_wceinit_scaled.zip'%PATH)

doubleendodermis=True
if doubleendodermis:
	if PATH=='R2_data_J951':
		rmPINs_wallids=[2740,2738,2818,2909,2915,3025,3041,3119,3117,3166,3283,3281,3336,3484,3482,3480,3478,3701,3703,3705,420,418,416,414,636,638,640,642,4876,4874,4872,4877,5084,5082,5080,5246,5244]
	if PATH=='R12_data_aux1':
		rmPINs_wallids=[2434,2432,2430,2548,2550,2648,2646,2836,2838]
	if PATH=='R2_data_Q1220':
		rmPINs_wallids=[5187,5185,5183,4782,4784,4786,4788,4543,4545,4547,4405,4378,4380,4382,4168,4161,4163,4165,4167,3843,3841,3839,3696,3680,3678,3676,3523,3525,3405,3395,3397,3284,3250,3163,3165,3167,3011,3013,2887,2889,2742,2740,2625,2627,2480,2482,2097,2099,2101,2023,1959,1961,1918,1727,1729,1731,1733]
		print rmPINs_wallids


t = db.tissue()
cfg = db.get_config('config')
graph = t.relation(cfg.graph_id)
mesh = t.relation(cfg.mesh_id)
position = db.get_property('position')
cell_type = db.get_property('cell_type')
border_type = db.get_property('border')
db,AUX1 = def_property(db, "AUX1", 0.0, "EDGE", "config","")
db,PIN = def_property(db, "PIN", 0.0, "EDGE", "config","")
db,LAX = def_property(db, "LAX", 0.0, "EDGE", "config","")
V = db.get_property("V")
S = db.get_property("S")
edges_to_wall = db.get_property("wall")

# cell -> walls
cell_to_walls = dict((cid,tuple(mesh.borders(2,cid))) for cid in mesh.wisps(2))
# wall -> borders - I never use this one.
#wall_to_borders = dict((cid,tuple(mesh.borders(1,cid))) for cid in mesh.wisps(1))
# border -> coordinates,  dictionary of walls to co-ordinates of vertices, note mesh.wisps(1) is a list of wall ids.
border_to_coordinates = dict((wid,\
                        [[position[tuple(mesh.borders(1,wid))[0]][0],\
                          position[tuple(mesh.borders(1,wid))[0]][1]],\
                         [position[tuple(mesh.borders(1,wid))[1]][0],\
                          position[tuple(mesh.borders(1,wid))[1]][1]]]\
                        ) for wid in mesh.wisps(1))
 # cell -> coordinates           
cell_to_coordinates = CellToCoordinates(mesh,cell_to_walls,border_to_coordinates)

# Make dictionary linking the wall ids to the co-ordinate of the wall centre.
wall_centres={}
for wid in mesh.wisps(1):
	coords=border_to_coordinates[wid] 
	wall_centres[wid]=[(coords[0][0]+coords[1][0])/2,(coords[0][1]+coords[1][1])/2]


# Make a dictionary linking each wall to the ids of it's neighbouring cells
# this is a little like wall_decomposition, 
# This is useful for iterating over the walls at the edge of the tissue, since these aren't included in wall_decomposition.
walls_to_cells={}
for cid, walls in cell_to_walls.iteritems():
	for j in range(0, size(walls)):	
		if cell_to_walls[cid][j] not in walls_to_cells:
			walls_to_cells[walls[j]]=[cid]
		else :
			walls_to_cells[walls[j]].append(cid)



wall_decomposition = {}
for eid in edges_to_wall :
	if edges_to_wall[eid] not in wall_decomposition:
               	wall_decomposition[edges_to_wall[eid]] = [eid]
	else :
		wall_decomposition[edges_to_wall[eid]].append(eid)

# Make dictionary linking the cell ids to the co-ordinate of the cell centre.
centroid={}
xmin=10000
xmax=0
ymin=10000
ymax=0
for cid in mesh.wisps(2):
	xc=0
	yc=0	
	coords=cell_to_coordinates[cid]
	for j in range(0, size(coords)/2-1):
		xc=xc+coords[j][0]
		yc=yc+coords[j][1]		
		centroid[cid]=[xc/(size(coords)/2-1),yc/(size(coords)/2-1)]
		if xmax<coords[j][0]:
			xmax=coords[j][0]
		if xmin>coords[j][0]:
			xmin=coords[j][0]
		if ymax<coords[j][1]:
			ymax=coords[j][1]
		if ymin>coords[j][1]:
			ymin=coords[j][1]



xEZ=xmax
for cid in mesh.wisps(2):
	if cell_type[cid]==6 or cell_type[cid]==7 or cell_type[cid]==8 or cell_type[cid]==9:
	    	if centroid[cid][0]<xEZ:	
	        	xEZ=centroid[cid][0]
	if cell_type[cid]==17:
		x_distalmeri=centroid[cid][0]-40
		ycentreline=centroid[cid][1]

for wid,(eid1,eid2) in wall_decomposition.iteritems() :
	cid1=graph.source(eid1)
	cid2=graph.source(eid2)
	if cell_type[cid1]==17 and cell_type[cid2]==17:
		ycentreline=wall_centres[wid][1]   # This over-writes previous value if there are two QC cells.
		x_distalmeri=wall_centres[wid][0]-40
	

print x_distalmeri

# Make a dictionary linking the co-ordinates of each vertex to the ids of the neighbouring walls.
coords_to_wallids={}
for wid, coords in border_to_coordinates.iteritems():
	#if tuple(border_to_coordinates[wid][0]) not in coords_to_wallids:
	if tuple(coords[0]) not in coords_to_wallids:
		coords_to_wallids[tuple(coords[0])]=[wid]
	else:
		coords_to_wallids[tuple(coords[0])].append(wid)
	if tuple(coords[1]) not in coords_to_wallids:
		coords_to_wallids[tuple(coords[1])]=[wid]
	else:
		coords_to_wallids[tuple(coords[1])].append(wid)

for wid,(eid1,eid2) in wall_decomposition.iteritems() :
	cid1=graph.source(eid1)
	cid2=graph.source(eid2)

	# Shootward PINs between any two epidermal cells for cells that are shootward of x_distalmeri,
	# Rootward PINs between any two epidermal cells for cells that are rootward of x_distalmeri:
	if cell_type[cid1]==2 and cell_type[cid2]==2:
		if centroid[cid1][0]>x_distalmeri:
			if centroid[cid1][0]-centroid[cid2][0]>0:
				PIN[eid2]=1
			else:
				PIN[eid1]=1
		if centroid[cid1][0]<x_distalmeri:
			if centroid[cid1][0]-centroid[cid2][0]<0:
				PIN[eid2]=1
			else:
				PIN[eid1]=1
	# PINs on epidermal cells facing the columella or QC.
	if cell_type[cid1]==2 and cell_type[cid2]==10:
		PIN[eid1]=1
	if cell_type[cid1]==10 and cell_type[cid2]==2:
		PIN[eid2]=1
	if cell_type[cid1]==2 and cell_type[cid2]==11:
		PIN[eid1]=1
	if cell_type[cid1]==11 and cell_type[cid2]==2:
		PIN[eid2]=1
	if cell_type[cid1]==2 and cell_type[cid2]==12:
		PIN[eid1]=1	
	if cell_type[cid1]==12 and cell_type[cid2]==2:
		PIN[eid2]=1
	if cell_type[cid1]==2 and cell_type[cid2]==3:
		PIN[eid1]=1
	if cell_type[cid1]==3 and cell_type[cid2]==2:
		PIN[eid2]=1
	if cell_type[cid1]==2 and cell_type[cid2]==17:
		PIN[eid1]=1
	if cell_type[cid1]==17 and cell_type[cid2]==2:
		PIN[eid2]=1
#	if cell_type[cid1]==2 and cell_type[cid2]==18:
#		PIN[eid1]=1
#	if cell_type[cid1]==18 and cell_type[cid2]==2:
#		PIN[eid2]=1



	# Shootward PINs between any two LRC1 cells:
	if cell_type[cid1]==6 and cell_type[cid2]==6:
		if centroid[cid1][0]-centroid[cid2][0]>0:
			PIN[eid1]=1
		else:
			PIN[eid2]=1

	# Shootward PINs between any two LRC2 cells:
	if cell_type[cid1]==7 and cell_type[cid2]==7:
		if centroid[cid1][0]-centroid[cid2][0]>0:
			PIN[eid1]=1
		else:
			PIN[eid2]=1
	
	# Shootward PINs between any two LRC3 cells:
	if cell_type[cid1]==8 and cell_type[cid2]==8:
		if centroid[cid1][0]-centroid[cid2][0]>0:
			PIN[eid1]=1
		else:
			PIN[eid2]=1

	# Shootward PINs between any two LRC4 cells:
	if cell_type[cid1]==9 and cell_type[cid2]==9:
		if centroid[cid1][0]-centroid[cid2][0]>0:
			PIN[eid1]=1
		else:
			PIN[eid2]=1

	# Shootward PINs between any two LRC5 cells:
	if cell_type[cid1]==19 and cell_type[cid2]==19:
		if centroid[cid1][0]-centroid[cid2][0]>0:
			PIN[eid1]=1
		else:
			PIN[eid2]=1



	# Shootward PINs between any two cortical cells in the EZ:
	# Rootward PINs between any two cortical cells in the meristem:
	if cell_type[cid1]==4 and cell_type[cid2]==4:
		if centroid[cid1][0]<xEZ:
			if centroid[cid1][0]-centroid[cid2][0]>0:
				PIN[eid1]=1
			else:
				PIN[eid2]=1
		if centroid[cid1][0]>xEZ:
			if centroid[cid1][0]-centroid[cid2][0]>0:
				PIN[eid2]=1
			else:
				PIN[eid1]=1

	# Rootward PINs between any two endodermal cells:
	if cell_type[cid1]==3 and cell_type[cid2]==3:
		if centroid[cid1][0]-centroid[cid2][0]>0:
			PIN[eid2]=1
		else:
			PIN[eid1]=1

	# Inward PINs between endodermal and vaculature cells.
	if cell_type[cid1]==3 and cell_type[cid2]==5:
		PIN[eid1]=1
	if cell_type[cid1]==5 and cell_type[cid2]==3:
		PIN[eid2]=1



# Add PINs to the rootward of the vasculature cells - version 1
# This requires making a dictionary linking cell ids to lists of the x-coordinates of the surrounding walls,
# then selecting the walls with x coordinates greater than the (maximum x)-1 microns. 
#cells_wallsxs={}
#for cid in cell_type.keys():
#	if cell_type[cid]==5:
#		for wid in cell_to_walls[cid]:
#			if cid not in cells_wallsxs:
#				cells_wallsxs[cid]=[wall_centres[wid][0]]
#			else :
#				cells_wallsxs[cid].append(wall_centres[wid][0])
#		maxwallx=max(cells_wallsxs[cid])
#		for wid in cell_to_walls[cid]:
#			if wall_centres[wid][0]>maxwallx-1:
#				(eid1,eid2)=wall_decomposition[wid]
#				if cid==graph.source(eid1):		
#					PIN[eid1]=1
#				else:	
#					PIN[eid2]=1

# Add PINs to the rootward of the vasculature cells - version 2.
# Considers wall groups between two cell-wall junctions - this works better!
from collections import defaultdict
wall_groups = defaultdict(list)
for wid,(eid1,eid2) in wall_decomposition.iteritems() :
	cid1 = graph.source(eid1)
	cid2 = graph.target(eid1)
	if cid1 > cid2:
		cid2, cid1 = cid1, cid2
	wall_groups[(cid1, cid2)].append(wid)

wall_groups_xcentre= defaultdict(list)
for (cid1, cid2), wl in wall_groups.iteritems():
	pid_set = set()
	for wid in wl:
		pid_set = pid_set ^ set(mesh.borders(1, wid))
	pid0, pid1 = pid_set
	p0 = list(position[pid0])
	p1 = list(position[pid1])
	wall_groups_xcentre[(cid1,cid2)]=0.5*p0[0]+0.5*p1[0]
	wall_groups_xcentre[(cid2,cid1)]=0.5*p0[0]+0.5*p1[0]

neighbours={}
for cid1 in mesh.wisps(2):
	for wid in cell_to_walls[cid1]:
		for cid2 in walls_to_cells[wid]:
			if cid2 != cid1:
				if cid1 not in neighbours:
              				neighbours[cid1] = [cid2]
				else:
					if cid2 not in neighbours[cid1]:
						neighbours[cid1].append(cid2)

for cid1 in cell_type.keys():
	xmax_cid1=0
	if cell_type[cid1]==5:
		for cid2 in neighbours[cid1]:
			if wall_groups_xcentre[(cid1,cid2)]>xmax_cid1:
				xmax_cid1=wall_groups_xcentre[(cid1,cid2)]
				cidtarget=cid2
		for wid in wall_groups[(cid1,cidtarget)]:
			(eid1,eid2)=wall_decomposition[wid]
			if graph.source(eid1)==cid1:
				PIN[eid1]=1
		for cid2 in neighbours[cid1]:
			if wall_groups_xcentre[(cid1,cid2)]>xmax_cid1-0.75 and cid2 != cidtarget:
				for wid in wall_groups[(cid1,cid2)]:
					(eid1,eid2)=wall_decomposition[wid]
					if graph.source(eid1)==cid1:
						PIN[eid1]=1


# Putting PINs on the shootward faces of the end lateral root cap cells (i.e. towards the epidermal cells),
# Here rhs and lhs refer to the different sides of the root.
minx_LRC1_lhs=1000
minx_LRC1_rhs=1000
minx_LRC2_lhs=1000
minx_LRC2_rhs=1000
minx_LRC3_lhs=1000
minx_LRC3_rhs=1000
minx_LRC4_lhs=1000
minx_LRC4_rhs=1000
minx_LRC5_lhs=1000
minx_LRC5_rhs=1000

LRC5_lhs=False  # These variables are automatically changed to True if LRC5 exists on the rhs or lhs of the root, respectively.
LRC5_rhs=False

for cid in cell_type.keys():
	if cell_type[cid]==9 and centroid[cid][1]>ycentreline:
		if centroid[cid][0]<minx_LRC4_rhs:
			minx_LRC4_rhs=centroid[cid][0]
			cid_minx_LRC4_rhs=cid
	if cell_type[cid]==9 and centroid[cid][1]<ycentreline:
		if centroid[cid][0]<minx_LRC4_lhs:
			minx_LRC4_lhs=centroid[cid][0]
			cid_minx_LRC4_lhs=cid
	if cell_type[cid]==8 and centroid[cid][1]>ycentreline:
		if centroid[cid][0]<minx_LRC3_rhs:
			minx_LRC3_rhs=centroid[cid][0]
			cid_minx_LRC3_rhs=cid
	if cell_type[cid]==8 and centroid[cid][1]<ycentreline:
		if centroid[cid][0]<minx_LRC3_lhs:
			minx_LRC3_lhs=centroid[cid][0]
			cid_minx_LRC3_lhs=cid
	if cell_type[cid]==7 and centroid[cid][1]>ycentreline:
		if centroid[cid][0]<minx_LRC2_rhs:
			minx_LRC2_rhs=centroid[cid][0]
			cid_minx_LRC2_rhs=cid
	if cell_type[cid]==7 and centroid[cid][1]<ycentreline:
		if centroid[cid][0]<minx_LRC2_lhs:
			minx_LRC2_lhs=centroid[cid][0]
			cid_minx_LRC2_lhs=cid
	if cell_type[cid]==6 and centroid[cid][1]>ycentreline:
		if centroid[cid][0]<minx_LRC1_rhs:
			minx_LRC1_rhs=centroid[cid][0]
			cid_minx_LRC1_rhs=cid
	if cell_type[cid]==6 and centroid[cid][1]<ycentreline:
		if centroid[cid][0]<minx_LRC1_lhs:
			minx_LRC1_lhs=centroid[cid][0]
			cid_minx_LRC1_lhs=cid
	if cell_type[cid]==19 and centroid[cid][1]>ycentreline:
		LRC5_rhs=True
		if centroid[cid][0]<minx_LRC5_rhs:
			minx_LRC5_rhs=centroid[cid][0]
			cid_minx_LRC5_rhs=cid
	if cell_type[cid]==19 and centroid[cid][1]<ycentreline:
		LRC5_lhs=True
		if centroid[cid][0]<minx_LRC5_lhs:
			minx_LRC5_lhs=centroid[cid][0]
			cid_minx_LRC5_lhs=cid
	
for wid in cell_to_walls[cid_minx_LRC1_rhs]:
	if wall_centres[wid][0]<centroid[cid_minx_LRC1_rhs][0]:
		if len(walls_to_cells[wid])==2:
			(eid1,eid2)=wall_decomposition[wid]
			cid1=graph.source(eid1)
			cid2=graph.source(eid2)
			if cell_type[cid1]==6:
				PIN[eid1]=1
			if cell_type[cid2]==6:
				PIN[eid2]=1

for wid in cell_to_walls[cid_minx_LRC1_lhs]:
	if wall_centres[wid][0]<centroid[cid_minx_LRC1_lhs][0]:
		if len(walls_to_cells[wid])==2:
			(eid1,eid2)=wall_decomposition[wid]
			cid1=graph.source(eid1)
			cid2=graph.source(eid2)
			if cell_type[cid1]==6:
				PIN[eid1]=1
			if cell_type[cid2]==6:
				PIN[eid2]=1

for wid in cell_to_walls[cid_minx_LRC2_rhs]:
	if wall_centres[wid][0]<centroid[cid_minx_LRC2_rhs][0]:
		if len(walls_to_cells[wid])==2:
			(eid1,eid2)=wall_decomposition[wid]
			cid1=graph.source(eid1)
			cid2=graph.source(eid2)
			if cell_type[cid1]==7 and cell_type[cid2]==2:
				PIN[eid1]=1
			if cell_type[cid1]==2 and cell_type[cid2]==7:
				PIN[eid2]=1

for wid in cell_to_walls[cid_minx_LRC2_lhs]:
	if wall_centres[wid][0]<centroid[cid_minx_LRC2_lhs][0]:
		if len(walls_to_cells[wid])==2:
			(eid1,eid2)=wall_decomposition[wid]
			cid1=graph.source(eid1)
			cid2=graph.source(eid2)
			if cell_type[cid1]==7 and cell_type[cid2]==2:
				PIN[eid1]=1
			if cell_type[cid1]==2 and cell_type[cid2]==7:
				PIN[eid2]=1

for wid in cell_to_walls[cid_minx_LRC2_lhs]:
	if wall_centres[wid][0]<centroid[cid_minx_LRC2_lhs][0]:
		if len(walls_to_cells[wid])==2:
			(eid1,eid2)=wall_decomposition[wid]
			cid1=graph.source(eid1)
			cid2=graph.source(eid2)
			if cell_type[cid1]==7 and cell_type[cid2]==8:
				PIN[eid1]=1
			if cell_type[cid1]==8 and cell_type[cid2]==7:
				PIN[eid2]=1


for wid in cell_to_walls[cid_minx_LRC3_rhs]:
	if wall_centres[wid][0]<centroid[cid_minx_LRC3_rhs][0]:
		(eid1,eid2)=wall_decomposition[wid]
		cid1=graph.source(eid1)
		cid2=graph.source(eid2)
		if cell_type[cid1]==8 and cell_type[cid2]==2:
			PIN[eid1]=1
		if cell_type[cid1]==2 and cell_type[cid2]==8:
			PIN[eid2]=1
		if cell_type[cid1]==8 and cell_type[cid2]==9:
			PIN[eid1]=1
		if cell_type[cid1]==9 and cell_type[cid2]==8:
			PIN[eid2]=1


for wid in cell_to_walls[cid_minx_LRC3_lhs]:
	if wall_centres[wid][0]<centroid[cid_minx_LRC3_lhs][0]:
		(eid1,eid2)=wall_decomposition[wid]
		cid1=graph.source(eid1)
		cid2=graph.source(eid2)
		if cell_type[cid1]==8 and cell_type[cid2]==2:
			PIN[eid1]=1
		if cell_type[cid1]==2 and cell_type[cid2]==8:
			PIN[eid2]=1


for wid in cell_to_walls[cid_minx_LRC3_lhs]:
	if wall_centres[wid][0]<centroid[cid_minx_LRC3_lhs][0]:
		(eid1,eid2)=wall_decomposition[wid]
		cid1=graph.source(eid1)
		cid2=graph.source(eid2)
		if cell_type[cid1]==8 and cell_type[cid2]==9:
			PIN[eid1]=1
		if cell_type[cid1]==9 and cell_type[cid2]==8:
			PIN[eid2]=1


for wid in cell_to_walls[cid_minx_LRC4_rhs]:
	if wall_centres[wid][0]<centroid[cid_minx_LRC4_rhs][0]:
		(eid1,eid2)=wall_decomposition[wid]
		cid1=graph.source(eid1)
		cid2=graph.source(eid2)
		if cell_type[cid1]==9 and cell_type[cid2]==2:
			PIN[eid1]=1
		if cell_type[cid1]==2 and cell_type[cid2]==9:
			PIN[eid2]=1

for wid in cell_to_walls[cid_minx_LRC4_lhs]:
	if wall_centres[wid][0]<centroid[cid_minx_LRC4_lhs][0]:
		(eid1,eid2)=wall_decomposition[wid]
		cid1=graph.source(eid1)
		cid2=graph.source(eid2)
		if cell_type[cid1]==9 and cell_type[cid2]==2:
			PIN[eid1]=1
		if cell_type[cid1]==2 and cell_type[cid2]==9:
			PIN[eid2]=1
if LRC5_rhs:
	for wid in cell_to_walls[cid_minx_LRC5_rhs]:
		if wall_centres[wid][0]<centroid[cid_minx_LRC5_rhs][0]:
			(eid1,eid2)=wall_decomposition[wid]
			cid1=graph.source(eid1)
			cid2=graph.source(eid2)
			if cell_type[cid1]==19 and cell_type[cid2]==2:
				PIN[eid1]=1
			if cell_type[cid1]==2 and cell_type[cid2]==19:
				PIN[eid2]=1

if LRC5_lhs:
	for wid in cell_to_walls[cid_minx_LRC5_lhs]:
		if wall_centres[wid][0]<centroid[cid_minx_LRC5_lhs][0]:
			(eid1,eid2)=wall_decomposition[wid]
			cid1=graph.source(eid1)
			cid2=graph.source(eid2)
			if cell_type[cid1]==19 and cell_type[cid2]==2:
				PIN[eid1]=1
			if cell_type[cid1]==2 and cell_type[cid2]==19:
				PIN[eid2]=1


# Put PINs on the cortical-endodermal initials, facing the QC:
for wid,(eid1,eid2) in wall_decomposition.iteritems() :
	cid1=graph.source(eid1)
	cid2=graph.source(eid2)
	if cell_type[cid1]==18 and cell_type[cid2]==17:
		PIN[eid1]=1
	if cell_type[cid1]==17 and cell_type[cid2]==18:
		PIN[eid2]=1

# Put PINs on the final endodermal and cortical cells facing the cortical-endodermal initials:
for wid,(eid1,eid2) in wall_decomposition.iteritems() :
	cid1=graph.source(eid1)
	cid2=graph.source(eid2)
	if cell_type[cid1]==3 and cell_type[cid2]==18:
		PIN[eid1]=1
	if cell_type[cid1]==18 and cell_type[cid2]==3:
		PIN[eid2]=1
	if cell_type[cid1]==4 and cell_type[cid2]==18:
		PIN[eid1]=1
	if cell_type[cid1]==18 and cell_type[cid2]==4:
		PIN[eid2]=1

# Put PINs on the final endodermal and cortical cells facing the QC or columella cells (i.e. if there are no C-E initials):
for wid,(eid1,eid2) in wall_decomposition.iteritems() :
	cid1=graph.source(eid1)
	cid2=graph.source(eid2)
	if cell_type[cid1]==3 and cell_type[cid2]==17:
		PIN[eid1]=1
	if cell_type[cid1]==17 and cell_type[cid2]==3:
		PIN[eid2]=1
	if cell_type[cid1]==4 and cell_type[cid2]==17:
		PIN[eid1]=1
	if cell_type[cid1]==17 and cell_type[cid2]==4:
		PIN[eid2]=1
	if cell_type[cid1]==3 and cell_type[cid2]==10:
		PIN[eid1]=1
	if cell_type[cid1]==10 and cell_type[cid2]==3:
		PIN[eid2]=1
	if cell_type[cid1]==4 and cell_type[cid2]==10:
		PIN[eid1]=1
	if cell_type[cid1]==10 and cell_type[cid2]==4:
		PIN[eid2]=1


# Put PINs on all membranes of the QC and columella cells.
for cell in cell_type.keys():
	if cell_type[cell]==17 or cell_type[cell]==10 or cell_type[cell]==11 or cell_type[cell]==12 or cell_type[cell]==12 or cell_type[cell]==13 or cell_type[cell]==14 or cell_type[cell]==15:
		for eid in graph.out_edges(cell):
			PIN[eid]=1

# Removing PINs on intervenining wall when we observe a double endodermal layer.
if doubleendodermis:
	for wid,(eid1,eid2) in wall_decomposition.iteritems() :
		if wid in rmPINs_wallids:
			PIN[eid1]=0
			PIN[eid2]=0

# Add PINs onto final cortical cell for R12_data_aux1
#if wid in [6060,6062]:
#	if cell_type[graph.source(eid1)]==4:
#		PIN[eid1]=1
#	if cell_type[graph.source(eid2)]==4:
#		PIN[eid2]=1
#

# Add PINs on to annoying vasculature cell for R2_J951:
#for wid,(eid1,eid2) in wall_decomposition.iteritems() :
#	if wid in [621,623,625]:		
#		if graph.source(eid1)==6864:
#			PIN[eid1]=1
#			print graph.source(eid1)
#		else:
#			PIN[eid2]=1
#			print graph.source(eid1)
#	if wid in [5145,5147]:
#		if graph.source(eid1)==7200:
#			PIN[eid1]=1
#		else:
#			PIN[eid2]=1



# Put AUX1 on the epidermal and lateral root cap, QC and columella cells:
if AUX1_proteins=='present':
	for cell in cell_type.keys():
		if cell_type[cell]==6 or cell_type[cell]==7 or cell_type[cell]==8 or cell_type[cell]==9 or cell_type[cell]==17 or cell_type[cell]==10 or cell_type[cell]==11 or cell_type[cell]==12 or cell_type[cell]==19 or cell_type[cell]==13 or cell_type[cell]==14 or cell_type[cell]==15:
			for eid in graph.out_edges(cell):
				AUX1[eid]=1
		if cell_type[cell]==2 and centroid[cell][0]<xEZ+75:
			for eid in graph.out_edges(cell):
				AUX1[eid]=1

if LAX_proteins=='present':
	for cell in cell_type.keys():
		if cell_type[cell]==10 or cell_type[cell]==17:
			for eid in graph.out_edges(cell):
				LAX[eid]=1

########################################################################################################
# Make figures to show positions of PINs and AUX1
#########################################################################################################
# PYLAB

# AUX1 figure
fig=figure(figsize=(25,7))
ax=fig.add_subplot(111,aspect='equal')
ax.set_yticks([])
ax.set_xticks([])
box(on=None)


xlim(xmin,xmax )    # set the xlim to xmin, xmax
ylim(ymin, ymax )    # set the ylim to ymin, ymax

for cid in mesh.wisps(2):
	c = centroid[cid]
	for wid in mesh.borders(2,cid):  # mesh.borders(2,cid) lists the ids of walls surrounding cell cellid.
		coords=border_to_coordinates[wid]
		p0=coords[0]
		p1=coords[1]
		gca().add_line(Line2D((p0[0],p1[0]),(p0[1],p1[1]), lw=1))

		for eid in wall_decomposition.get(wid, []):
			if graph.source(eid) == cid and AUX1[eid]>0:
				coords=border_to_coordinates[wid]
				pw0=coords[0]
				pw1=coords[1]
				p0 = [pw0[0]*0.85+c[0]*0.15,pw0[1]*0.85+c[1]*0.15]
				p1 = [pw1[0]*0.85+c[0]*0.15,pw1[1]*0.85+c[1]*0.15]		
				gca().add_line(Line2D((p0[0],p1[0]),(p0[1],p1[1]), color='g', lw=4))


figtext(0.7, 0.85, 'AUX1 positions',size='35')

fname = '%s/R2_Q1220data_wsn_6pix_OA_fl_wceinit_AUX1positions_wxauto.png'%PATH
savefig(fname)
clf()

# LAX figure
fig=figure(figsize=(25,7))
ax=fig.add_subplot(111,aspect='equal')
ax.set_yticks([])
ax.set_xticks([])
box(on=None)

xlim(xmin,xmax )    # set the xlim to xmin, xmax
ylim(ymin, ymax )    # set the ylim to ymin, ymax

for cid in mesh.wisps(2):
	c = centroid[cid]
	for wid in mesh.borders(2,cid):  # mesh.borders(2,cid) lists the ids of walls surrounding cell cellid.
		coords=border_to_coordinates[wid]
		p0=coords[0]
		p1=coords[1]
		gca().add_line(Line2D((p0[0],p1[0]),(p0[1],p1[1]), lw=1))

		for eid in wall_decomposition.get(wid, []):
			if graph.source(eid) == cid and LAX[eid]>0:
				coords=border_to_coordinates[wid]
				pw0=coords[0]
				pw1=coords[1]
				p0 = [pw0[0]*0.85+c[0]*0.15,pw0[1]*0.85+c[1]*0.15]
				p1 = [pw1[0]*0.85+c[0]*0.15,pw1[1]*0.85+c[1]*0.15]		
				gca().add_line(Line2D((p0[0],p1[0]),(p0[1],p1[1]), color='g', lw=4))


figtext(0.7, 0.85, 'LAX2 positions',size='35')

fname = '%s/R2_Q1220data_wsn_6pix_OA_fl_wceinit_LAXpositions_wxauto.png'%PATH
savefig(fname)
clf()

#print PIN

# PIN figure
fig=figure(figsize=(25,7))
ax=fig.add_subplot(111,aspect='equal')
ax.set_yticks([])
ax.set_xticks([])
box(on=None)

xlim(xmin,xmax )    # set the xlim to xmin, xmax
ylim(ymin, ymax )    # set the ylim to ymin, ymax

for cid in mesh.wisps(2):
	c = centroid[cid]
	for wid in mesh.borders(2,cid):  # mesh.borders(2,cid) lists the ids of walls surrounding cell cellid.
		coords=border_to_coordinates[wid]
		p0=coords[0]
		p1=coords[1]
		gca().add_line(Line2D((p0[0],p1[0]),(p0[1],p1[1]), lw=1))

		for eid in wall_decomposition.get(wid, []):
			if graph.source(eid) == cid and PIN[eid]>0:
				coords=border_to_coordinates[wid]
				pw0=coords[0]
				pw1=coords[1]
				p0 = [pw0[0]*0.85+c[0]*0.15,pw0[1]*0.85+c[1]*0.15]
				p1 = [pw1[0]*0.85+c[0]*0.15,pw1[1]*0.85+c[1]*0.15]		
				gca().add_line(Line2D((p0[0],p1[0]),(p0[1],p1[1]), color='r', lw=2))
			


figtext(0.7, 0.85, 'PIN positions',size='35')

fname = '%s/R2_Q1220data_wsn_6pix_OA_fl_wceinit_PINpositions_wxauto_newvasc.png'%PATH

savefig(fname)
clf()

db.set_property('PIN', PIN)
db.set_description('PIN', '')
db.set_property('AUX1', AUX1)
db.set_property('LAX', LAX)
db.set_description('AUX1', '')
db.write('%s/R2_Q1220data_wsn_6pix_OA_fl_wceinit_wAUX1PINLAX_wc_wxauto_scaled_newvasc.zip'%PATH)





