2

import numpy as np
import itertools
from math import sin, cos, exp, sqrt, asin, atan2
import scipy.linalg as la
from operator import itemgetter
import re

from openalea.celltissue import TissueDB
from db_utilities import get_mesh 
from db_geom import ordered_wids_pids

from lxml import etree
import copy, os

from subprocess import call, Popen, PIPE

TEXTEXT_NS = u"http://www.iki.fi/pav/software/textext/"
SVG_NS = u"http://www.w3.org/2000/svg"
XLINK_NS = u"http://www.w3.org/1999/xlink"


nsmap = {
    'sodipodi': 'http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd',
    'svg': 'http://www.w3.org/2000/svg',
    'xlink': 'http://www.w3.org/1999/xlink',
    'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
    'inkscape': 'http://www.inkscape.org/namespaces/inkscape'
    }

ID_PREFIX = "textext-"

NSS = {
    u'textext': TEXTEXT_NS,
    u'svg': SVG_NS,
    u'xlink': XLINK_NS,
}



def fix_xml_namespace(node):
    svg = '{%s}' % SVG_NS
        
    if node.tag.startswith(svg):
        node.tag = node.tag[len(svg):]
        
    for key in node.attrib.keys():
        if key.startswith(svg):
            new_key = key[len(svg):]
            node.attrib[new_key] = node.attrib[key]
            del node.attrib[key]
        
    for c in node:
        fix_xml_namespace(c)

it = 0
def latex_to_svg(latex_string):
    global it
    doc = '\\documentclass{article}\\begin{document}\\pagestyle{empty}'+latex_string+'\\end{document}'
    f = open('/tmp/article%d.tex'%it, 'w')
    f.write(doc)
    f.close()
    call(['pdflatex', '-output-directory=/tmp', '/tmp/article%d.tex'%it])
    os.system('pdfcrop /tmp/article%d.pdf /tmp/crop%d.pdf'%(it,it))
    call(['pdf2svg', '/tmp/crop%d.pdf'%it, '/tmp/article%d.svg'%it])
    tree = etree.parse('/tmp/article%d.svg'%it)
    it +=1
    root = tree.getroot()

    fix_xml_namespace(root)
    href_map = {}

    ahash = hash(latex_string)
    # Map items to new ids
    for i, el in enumerate(root.xpath('//*[attribute::id]')):
        cur_id = el.attrib['id']
        new_id = "%s%s-%d" % (ID_PREFIX, ahash, i)
        href_map['#' + cur_id] = "#" + new_id
        el.attrib['id'] = new_id

    # Replace hrefs
    url_re = re.compile('^url\((.*)\)$')

    for el in root.xpath('//*[attribute::xlink:href]', namespaces=NSS):
        print el
        href = el.attrib['{%s}href'%XLINK_NS]
        el.attrib['{%s}href'%XLINK_NS] = href_map.get(href, href)

    for el in root.xpath('//*[attribute::svg:clip-path]', namespaces=NSS):
        value = el.attrib['clip-path']
        m = url_re.match(value)
        if m:
            el.attrib['clip-path'] = \
                'url(%s)' % href_map.get(m.group(1), m.group(1))

    for el in root.xpath('//*[attribute::style]', namespaces=NSS):
        style = el.attrib['style']
        sl = style.split(';')
        bad = []
        for s in sl:
            if s.split(':')[0].strip()=='fill':
                bad.append(s)
        el.set('style', ';'.join(s for s in sl if s not in bad))

        # Bundle everything in a single group
    master_group = etree.SubElement(root, 'g')
    for c in root:
        if c is master_group: continue
        master_group.append(c)

    return copy.copy(master_group)
    
    


def rot(v):
    """                                                                         
    rotate a 2d vector (numpy) by 90 degrees anticlockwise                    
    :param v: input vector                                                      
    :type v: Vector2/list/tuple                                                 
    :returns: Vector2 rotated v                                                 
    """
    return np.array((-v[1],v[0]))

def ml_to_xy(m, kappa, l, x0, theta0, bp_data=None):
    j = m[0]
    xi = m[1]
    eta = m[2]
    if bp_data:
        x = np.array(bp_data[0][j])
        theta = bp_data[1][j]
    else:
        x = np.array(x0)
        theta = theta0
        for i in range(j):
            t = np.array((cos(theta), sin(theta)))
            n = np.array((-sin(theta), cos(theta)))
            if abs(kappa[i]*l[i])>1e-15:
                x += (1.0/kappa[i])*\
                    (sin(kappa[i]*l[i])*t+(2*sin(kappa[i]*l[i]/2)**2)*n)
            else:
                x += l[i]*t        
            theta += kappa[i]*l[i]
             
    t = np.array((cos(theta), sin(theta)))
    n = np.array((-sin(theta), cos(theta)))
    if abs(kappa[j]*l[j])>1e-15:
        x += (1/kappa[j]*sin(kappa[j]*l[j]*xi)-eta*sin(kappa[j]*l[j]*xi))*t+ \
        (1/kappa[j]*(2*sin(kappa[j]*l[j]*xi/2)**2)+eta*cos(kappa[j]*l[j]*xi))*n
    else:
        x += xi*l[j]*t+eta*n 
    return x

def breakpoints(kappa, l, x0, theta0):
    x = np.array(x0)
    theta = theta0
    bp = [ np.array(x) ]
    angles = [ theta ]
    tangents = [ np.array((cos(theta), sin(theta)))]
    for i in range(len(kappa)):
        t = np.array((cos(theta), sin(theta)))
        n = np.array((-sin(theta), cos(theta)))
        if abs(kappa[i]*l[i])>1e-15:
            x += (1.0/kappa[i])*(sin(kappa[i]*l[i])*t+2*sin(kappa[i]*l[i]/2)**2*n)
        else:
            x += l[i]*t        
        theta += kappa[i]*l[i]
        bp.append(np.array(x))
        angles.append(theta)
        tangents.append(np.array((cos(theta), sin(theta))))
    return bp, angles, tangents


x0 = np.array((0.0,0.0))
theta0 = 0.0
kappa = [0.005, -0.008, 0.004, 0.003]
l = [100.0, 120.0, 120.0, 100.0]

bp, angles, tangents = breakpoints(kappa, l, x0, theta0)
print bp



celltype_colours = { 0:'cadetblue', 1:'blue', 2:'green', 3:'purple',
                     4:'gray', 5:'orange', 6:'chocolate',
                     7:'linen', 8:'white', 9:'azure'}



def plot_tissue(db, fname):
    svg = etree.Element("svg")    
    mesh = get_mesh(db)
    pos = db.get_property('position')
    cell_type = db.get_property('cell_type')
    ct_groups = []
    for ct in celltype_colours:
        ct_groups.append(etree.SubElement(svg, 'g'))
    for cid in list(mesh.wisps(2)):
        wids, pids = ordered_wids_pids(mesh, pos, cid)
        pts = [(pos[pid][0]*0.1, pos[pid][1]*0.1) for pid in pids]
        path = 'M %f,%f '%tuple(pts[0]) + ' '.join('%f,%f'%tuple(x) for x in pts[1:]) +' Z'
        etree.SubElement(ct_groups[cell_type[cid]], 'path', { 'd':path, 'stroke': 'black', 'fill':celltype_colours[cell_type[cid]]})     
    for pid in mesh.wisps(0):
        etree.SubElement(svg, 'circle', cx=str(pos[pid][0]*0.1), cy=str(pos[pid][1]*0.1), r=str(1.2))
    etree.ElementTree(svg).write(fname)
    


db = TissueDB()
db.read('fullroot2.zip')
plot_tissue(db, 'tissue.svg')






