

import sys


from vroot2D.growth.step_stool import step_stool
from vroot2D.utils.db_utilities import get_parameters, set_parameters, get_mesh
import matplotlib.pylab as plt
from math import exp
from random import random
from openalea.tissueshape import centroid

def target_regr(x):
    l0 = 800.0
    p0 = (0.05, 0.35, 250.0/l0, 800.0/l0, 90.0, 40.0) 
    return step_stool(x/l0, p0)[1]

def optimize_midline(db):

    from parameters_fullroot import setSimParameters
    setSimParameters(db)

    # Initialize gene network
    # Class for combining multiple models into one system of ODEs
    from genenetwork import CombinedModel, GeneNetworkJPat, GeneNetwork
#    # E2F and auxin transport models
#    from primordiumModels import AuxinTransportAUX1 
#    from fullrootModels import GrowthRegulators
##    gn=GeneNetwork(db, CombinedModel([AuxinTransportAUX1(),
##                                      GrowthRegulators()]))
#    gn=GeneNetworkJPat(db, GrowthRegulators())
#   
#    ga = GeneNetworkJPat(db, AuxinTransportAUX1()) 
#    ga.steady_state()

    from diffuse_ss import  DiffuseInhibitorSS, DiffusePromoterSS, AuxinSS
    ssi = DiffuseInhibitorSS(db)
    ssp = DiffusePromoterSS(db)
    ssa = AuxinSS(db)

    from parameters_fullroot import setGrowthViscParameters
    setGrowthViscParameters(db)

    from growth_midline import GrowthMidline
    g = GrowthMidline(db)
    
#    from circ_arc import set_init_midline
#    set_init_midline(db)


    def objective():
        ssi.step()
        ssp.step()
        ssa.step()
        l = g.get_l()
        regr, kdot = g.get_regr_kdot()
        
        regr = regr[::-1]
        kdot = kdot[::-1]
        l = l[::-1]
        l_tot = l[0]/2
        xm = [ l[0]/2 ]
        for i in range(len(l)-1):
            l_tot += (l[i]+l[i+1])/2
            xm.append(l_tot)
        
        print 'xm: ', xm
        regr_obj = [ target_regr(x) for x in xm ]

        plt.plot(xm, regr, 'r-', xm, regr_obj, 'b-')
        plt.draw()
        plt.show()
        return regr, regr_obj

    return objective

def do_opt():
    plt.ion()
    from import_tissue import import_tissue
    db=import_tissue('fullroot_longer.zip')
    from import_tissue import divide_long_cells
    divide_long_cells(db, 100)
    from carrier_rearrangements import PINAUX
    PINAUX(db)

    o = optimize_midline(db)
    pi = 'DiffuseInhibitorSS'
    pp = 'DiffusePromoterSS'
    p2 = 'growth_parameters'
    mod_params = [ (pp, 'D_p'), (pp, 'lambda_p'), (pp, 'alpha_p'),
                   (pi, 'D_i'), (pi, 'lambda_i'), (pi, 'alpha_i'),
                   (p2, 'kp'), (p2, 'ki'), (p2, 'np'), (p2, 'ni'),
                   (p2, 'eps'), (p2, 'mu1') ]
    start_values = [ get_parameters(db, p[0])[p[1]] for p in mod_params ]

    def obj(v):
        for p, x in zip(mod_params, v):
            get_parameters(db, p[0])[p[1]]=x
        x, t = o()
        print x, t

    new_values = start_values
    for i in range(1):
        print new_values
        obj(new_values)
        new_values = [v*exp(0.99+0.02*random()) for v in start_values]        

    GP = db.get_property('GP')
    GI = db.get_property('GI')
    mesh = get_mesh(db)
    pos = db.get_property('position')
    cx = {}
    cy={}
    for cid in mesh.wisps(2):
        cx[cid] = centroid(mesh, pos, 2, cid)[0]
        cy[cid] = centroid(mesh, pos, 2, cid)[1]

    x = [cx[cid] for cid in mesh.wisps(2)]
    yp = [GP[cid] for cid in mesh.wisps(2)]
    yi = [GI[cid] for cid in mesh.wisps(2)]
    auxin = db.get_property('GP')
    cell_type = db.get_property('cell_type')
    epi_cells = [cid for cid in mesh.wisps(2) if cell_type[cid]==0]
    xe = [cx[cid] for cid in epi_cells]
    ye = [cy[cid] for cid in epi_cells]
    a = [auxin[cid] for cid in epi_cells]
    plt.clf()
    plt.hold(True)
    plt.semilogy(x, yp, 'r*', label='GP')
    plt.semilogy(x, yi, 'go', label='GI')
    plt.figure()
    plt.semilogy(xe, a, 'bx', label='auxin')
    plt.draw()

def main():
    
    do_opt()
    plt.show()

main()
