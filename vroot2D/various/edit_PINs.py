#!/usr/bin/env python

import numpy as np
from openalea.celltissue import TissueDB
from openalea.container import ordered_pids
from openalea.tissueshape import centroid
from vroot2D.utils.geom import area
import scipy.linalg as la
from vroot2D.utils.db_utilities import get_mesh, get_graph
from collections import defaultdict
from PyQt4 import QtCore, QtGui, QtOpenGL

PIN_name = 'PIN'
            
def ordered_wids_pids(mesh, pos, cid):
    wid_set = set(mesh.borders(2,cid))
    wid = wid_set.pop()
    o_wids = [wid]
    o_pids = list(mesh.borders(1, wid))
    end = o_pids[-1]
    while wid_set:
        for wid in wid_set:
            if end in mesh.borders(1, wid):
                wid_set.discard(wid)
                o_wids.append(wid)
                end = (set(mesh.borders(1, wid)) - set([end])).pop()
                o_pids.append(end)
                break
    o_pids.pop()
    if area([pos[pid] for pid in o_pids])<0:
        o_wids.reverse()
        o_pids=[o_pids[0]]+o_pids[-1:0:-1]
    return o_wids, o_pids
   

class GraphicsView(QtGui.QGraphicsView):
    def __init__(self, parent=None):
        QtGui.QGraphicsView.__init__(self, parent)
#        self.setViewport(QtOpenGL.QGLWidget(QtOpenGL.QGLFormat(QtOpenGL.QGL.SampleBuffers)))
        self.setDragMode(QtGui.QGraphicsView.RubberBandDrag)
        self.setRenderHint(QtGui.QPainter.Antialiasing)
        self.setRenderHint(QtGui.QPainter.TextAntialiasing)
        self.scale(1,-1)
        self.initTransform=self.transform()
        self.angle=0
    
    def wheelEvent(self, event):
        factor=1.41**(-event.delta()/240.0)
        
        self.scale(factor, factor)


class EdgeItem(QtGui.QGraphicsPolygonItem):
    def __init__(self, scene, pts, eid=None):
        self.scene=scene
        #self.cidx=cidx
        #self.eidx=eidx
        self.eid = eid
#        QtGui.QGraphicsLineItem.__init__(self, pts[0][0], pts[0][1], pts[1][0], pts[1][1], None, scene) 
        QtGui.QGraphicsPolygonItem.__init__(self, None, scene)
        self.poly=QtGui.QPolygonF([QtCore.QPointF(v[0],v[1]) for v in pts])
        self.setPolygon(self.poly)
        self.setCol()
        self.setPen(QtGui.QPen(1))
        if eid:
            self.setFlag(QtGui.QGraphicsItem.ItemIsPanel)
        else:
            self.setEnabled(False)
        self.setZValue(-0.9)
   
    
    def setCol(self):
        PROP=PIN_name 
        p = self.scene.db.get_property(PROP)
#        if self.scene.mesh.cells[self.cidx].edges[self.eidx].props[PROP]>0.0:
        if self.eid:
            if p.get(self.eid,0)>0:
                self.setBrush(QtGui.QBrush(QtCore.Qt.green))
            else:
                self.setBrush(QtGui.QBrush(QtCore.Qt.gray))
        self.update()

    def mousePressEvent(self, event):
#        p=PropertyEditor(self.scene.mesh.cells[self.cidx].edges[self.eidx].props, self, self.scene.parent())
        PROP=PIN_name
        p = self.scene.db.get_property(PROP)
        print PROP+'=',p.get(self.eid,0)
        if p.get(self.eid, 0)>0:
            p[self.eid]=0.0
        else:
            p[self.eid]=1.0
        self.setCol()
        self.update()
        super(EdgeItem,self).mousePressEvent(event)

class PolyItem(QtGui.QGraphicsPolygonItem):
    def __init__(self, scene, idx, pts, col):
        self.scene=scene
        self.idx=idx
        QtGui.QGraphicsPolygonItem.__init__(self,None,scene)
        self.setZValue(-1.0)
#        self.setFlags(QtGui.QGraphicsItem.ItemIsSelectable)
        self.poly=QtGui.QPolygonF([QtCore.QPointF(v[0],v[1]) for v in pts])
        self.setPolygon(self.poly)
        self.col=QtGui.QColor.fromRgbF(*col)
        self.setBrush(self.col)
        self.edgeItems = []


from time import sleep
class GraphicsScene(QtGui.QGraphicsScene):
    
    def __init__(self, parent, db):
        QtGui.QGraphicsScene.__init__(self,parent)
        self.db=db
        self.delta=1
        self.generateScene()    

    def generateScene(self):
        self.polyItems=[]
        mesh = get_mesh(self.db)
        graph = get_graph(self.db)
        pos = self.db.get_property('position')
        cell_type = self.db.get_property('cell_type')
        wall = self.db.get_property('wall')
        PIN = self.db.get_property(PIN_name)
        self.walls_to_edges = defaultdict(list)
        for eid, wid in wall.iteritems():
            self.walls_to_edges[wid].append(eid)
        for cid in mesh.wisps(2):
            wids, pids=ordered_wids_pids(mesh, pos, cid)
            pts=[np.array(pos[pid]) for pid in pids]
            centre = centroid(mesh, pos, 2, cid)
            col=(0.5, 0.5, 0.5)
            pi=PolyItem(self, cid, pts, col)
            self.polyItems.append(pi)
            i_pos = {}
            N = len(pids)
            for i in range(N):
                t2 = pts[(i+1)%N]-pts[i]
                t1 = pts[i]-pts[i-1]
                t1=t1/la.norm(t1)
                t2=t2/la.norm(t2)
                t = (t1+t2)/(1+np.dot(t1,t2))
                #t = t/la.norm(t)
                i_pos[pids[i]] = pts[i] - self.delta*np.array((t[1],-t[0]))
            for wid in mesh.borders(2, cid):
                pid1, pid2 = mesh.borders(1,wid)
                p1 = pos[pid1]
                p2 = pos[pid2]
                i_p1 = i_pos[pid1]
                i_p2 = i_pos[pid2]
                eid = next((eid for eid in self.walls_to_edges[wid] if \
                                graph.source(eid)==cid), None)
#                print wid, eid, self.walls_to_edges[wid]
                pi.edgeItems.append(EdgeItem(self, [p1, p2, i_p2, i_p1], eid))
                     
#            for ei,e in enumerate(c.edges):
#                

            


class MeshViewer(QtGui.QMainWindow):

    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        

        self.col_map = {0: (0.9,0,0.1),
                        1: (0.44,0.86,0.3),
                        2: (1,1,0.2),
                        3: (0.66,0.2,0.66),
                        4: (0.8, 0.8, 0.85), 
                        5: (0.5,0.47,0.16), 
                        6: (0.6,0.6,0.6), 
                        7: (0.9,0.95,0.92)}

        self.createActions()
        self.createMenus()
        
        self.db=TissueDB()

        self.view = GraphicsView()
   #     self.createScene()
                
        self.setCentralWidget(self.view)        

        self.setWindowTitle("Auxin Transport")
        self.view.fitInView(self.view.sceneRect(), QtCore.Qt.KeepAspectRatio)

    def createMenus(self):
        self.fileMenu = self.menuBar().addMenu("&File")
        self.fileMenu.addAction(self.openAct)
        self.fileMenu.addAction(self.saveAct)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.exitAct)
        
               
    def open(self):
        fname=QtGui.QFileDialog.getOpenFileName(self, 
             "Mesh - Load Mesh", ".", "Tissue files (*.zip);;All files (*)")
        if not fname.isEmpty():
            self.loadFile(unicode(fname))
    
    def saveAs(self):
        fname=QtGui.QFileDialog.getSaveFileName(self, 
             "Mesh - Load Mesh", ".", "Tissue files (*.zip);;All files (*)")
        if not fname.isEmpty():
            self.saveFile(unicode(fname))

    def loadFile(self, fname):
        self.db=TissueDB()
        self.db.read(fname)
        self.createScene()
        self.view.fitInView(self.view.sceneRect(), QtCore.Qt.KeepAspectRatio)

    def saveFile(self, fname):
        self.db.write(fname)

            
    def createActions(self):
        self.openAct = QtGui.QAction("&Open...", self,
                shortcut=QtGui.QKeySequence.Open,
                statusTip="Open an existing mesh", triggered=self.open)

        self.saveAct = QtGui.QAction("&Save As...", self,
                shortcut=QtGui.QKeySequence.SaveAs,
                statusTip="Save the mesh to disk", triggered=self.saveAs)
        

        self.exitAct = QtGui.QAction("E&xit", self, shortcut="Ctrl+Q",
                statusTip="Exit the application", triggered=self.close)

        
              
    def createScene(self):
        self.scene = GraphicsScene(self, self.db)
        self.view.setScene(self.scene)
    

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
#    fmt = QtOpenGL.QGLFormat.defaultFormat()
#    fmt.setProfile(QtOpenGL.QGLFormat.CompatibilityProfile)
#    QtOpenGL.QGLFormat.setDefaultFormat(fmt)
    meshViewer = MeshViewer()
    if len(sys.argv)>1:
        meshViewer.loadFile(sys.argv[1])
    meshViewer.resize(800,600)
    meshViewer.show()#Maximized()
    sys.exit(app.exec_())
