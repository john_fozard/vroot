from openalea.celltissue import TissueDB, Tissue, ConfigFormat
from openalea.tissueshape import tovec
from openalea.tissueshape import edge_length,face_surface_2D
from openalea.scheduler import Scheduler,Task
from vroot2D.growth.mesh import triangulate_mesh
from vroot2D.utils.geom import area

from vplants.plantgl.math import Vector2
from math import atan2, pi

def ordered_wids_pids(mesh, pos, cid):
    wids = list(mesh.borders(2,cid))
    wid = wids.pop()
    o_wids = [wid]
    end = list(mesh.borders(1, wid))[-1]
    o_pids= [end]
    while wids:
        for wid in wids:
            if end in mesh.borders(1, wid):
                wids.remove(wid)
                o_wids.append(wid)
                pids = set(mesh.borders(1, wid))
                pids.remove(end)
                end = pids.pop()
                o_pids.append(end)
                break
    if area([pos[pid] for pid in o_pids])<0:
        o_wids.reverse()
        o_pids=o_pids[-2::-1]+[o_pids[-1]]
    return o_wids, o_pids
 


def single_cell_2D():
    size = 10.0
    db = TissueDB()
    t = Tissue()
    POINT = t.add_type("POINT")
    EDGE = t.add_type("EDGE")
    WALL = t.add_type("WALL")
    CELL = t.add_type("CELL")
    mesh_id = t.add_relation("mesh",(POINT, WALL, CELL) )
    mesh = t.relation(mesh_id)

    graph_id = t.add_relation("graph", (CELL, EDGE))
    graph = t.relation(graph_id)

    cell_types = { 0:'single' }

    cfg=ConfigFormat(locals())
    cfg.add_section("tissue descr")
    cfg.add("POINT")
    cfg.add("WALL")
    cfg.add("EDGE")
    cfg.add("CELL")
    cfg.add("mesh_id")
    cfg.add("graph_id")

    
    cfg.add("cell_types")

    
    v_list=[]
    w_list=[]
    for i in range(4):
        v_list.append(mesh.add_wisp(0))

    pos = { v_list[0]: Vector2(0., 0.),
                 v_list[1]: Vector2(1., 0.),
                 v_list[2]: Vector2(1., 1.),
                 v_list[3]: Vector2(0., 1.) }

    for i in range(4):
        w_list.append(mesh.add_wisp(1))
    cid = mesh.add_wisp(2)
    for i in range(4):
        mesh.link(2, cid, w_list[i])
    for i in range(4):
        mesh.link(1, w_list[i], v_list[i])
        mesh.link(1, w_list[i], v_list[(i+1)%4])
    ct_dict = { cid: 0 }


    
    PIN = {}
    AUX = {}
    
    
    db.set_config("config", cfg.config())
    db.set_tissue(t)
    db.set_property('position', pos)
    db.set_description('position','')
    db.set_property('cell_type', ct_dict)
    db.set_description('cell_type','')
    db.set_property('border', {cid: 0})
    db.set_description('border','')

    db.set_property('wall', {})
    db.set_description('wall','')

#    db.set_property('wall_type', wt_dict)
#    db.set_description('wall_type','')
#    db.set_property('edge_type', et_dict)
#    db.set_description('edge_type','')
#    db.set_property('wall', wall)
#    db.set_description('wall','')
#    db.set_property('old_cid', old_cid)
#    db.set_description('old_cid','')

#    wall_types={}
#    for cid in mesh.wisps(2):
#        for wid, pid in zip(*ordered_wids_pids(mesh, pos, cid)):
#            v1 = pos[pid]
#            s = set(mesh.borders(1, wid))
#            s.remove(pid)
#            v0 = pos[s.pop()]
#            angle = atan2(v1[1]-v0[1], v1[0]-v0[0])
#            if -pi/4 < angle < pi/4:
#                wall_types[(cid, wid)] = 0
#            if pi/4 < angle < 3*pi/4:
#                wall_types[(cid, wid)] = 1
#            if 3*pi/4 < angle or angle<-3*pi/4:
#                wall_types[(cid, wid)] = 2
#            if  -3*pi/4 < angle < -pi/4:
#                wall_types[(cid, wid)] = 3
#    db.set_property('wall_types', wall_types)
#    db.set_description('wall_types', ' wall annotations')

    #properties (loaded from tissue file) which updated on cell division
    divided_props={'cell_type': ('cell', 'attribute', -1),
                   'PIN': ('edge', 'concentration', 1),
                   'border': ('cell', 'attribute', 0) }
#                   'wall_types': ('(cell, wall)', 'attribute', 0) }
    db.set_property('divided_props', divided_props)
    db.set_description('divided_props', 'property division rules')


    return db

