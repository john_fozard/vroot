"""
Module to import tissue in OpenAlea format (converted
from svg file), and do further processing:
rescaling the tissue, adding zero
PINs to edges which have none, adding AUX1 dictionary,
and classifying cell wall types according to angle
made with the x-axis
Also add rules for how to treat basic properties
on cell division.
"""


from openalea.celltissue import TissueDB
from openalea.tissueshape import edge_length, face_surface_2D
from openalea.scheduler import Scheduler,Task
from vroot2D.utils.geom import area
from vroot2D.utils.db_geom import ordered_wids_pids, centroid
from vroot2D.utils.db_utilities import get_mesh

from math import atan2, pi
import numpy as np

def extend_tissue(db, k=1200):
    """
    Take cellset tissue, extend the cells at the end to generate an elongation zone.
    """
    border = db.get_property('border')

    border_cells = [cid for cid,v in border.iteritems() if v!=None]

    # find walls between border and non-border cells

    border_walls = []

    mesh = get_mesh(db)

    border_points = set() #pid for cid in border_cells for pid in mesh.borders(2, cid, 2))

    pos = db.get_property('position')

    wall_types = db.get_property('wall_types')
    for cid in mesh.wisps(2):
        if border[cid]==6 or border[cid]==1:
            for wid in mesh.borders(2, cid):
                if mesh.nb_regions(1, wid)==1 and wall_types[(cid,wid)] in [1,3]:
                    for pid in mesh.borders(1, wid):
                        border_points.add(pid)


    L = -k
    
    for pid in border_points:
        pos[pid][0] += L

    new_cids = divide_long_cells(db, k-10)

    pos=db.get_property('position')
    pos=dict((pid, np.array(v)) for pid, v in pos.iteritems())
    db.set_property('position', pos)


    return new_cids

def import_tissue(fname, scale=1.0):
    """
    Load OpenAlea tissue .zip file (made by create_tissue.py
    from svg).
    Generate initial mesh triangulation; extend properties
    that are defined on a subset of the edges/cells by
    create_tissue.py (i.e. PIN, border) to all of the
    things of that class, and set up the divided_props
    structure that informs the division algorithm how
    to update properties at division.
    """

    db=TissueDB()
    db.read(fname)


    print "properties", list(db.properties())

    db.set_property('scale', 1.0)
    db.set_description('scale', 'distance unit in microns')
    
    

    # Convert the position database to the plantGL Vector2 type
    #scale = 10.0
    pos=db.get_property('position')
    pos=dict((pid, np.array(v)/scale) for pid, v in pos.iteritems())
    db.set_property('position', pos)
    

    

    t = db.tissue()
    cfg = db.get_config('config')

    #Read in properties
    graph = t.relation(cfg.graph_id) # directed cell to cell graph
    mesh = t.relation(cfg.mesh_id) # vertex,wall,cell topomesh

    # Fix PINs from tissue zip file
    if 'PIN' in db.properties():
        PIN = db.get_property('PIN')
    else:
    	PIN = {}
    PIN = dict((eid, PIN.get(eid, 0.0)) for eid in graph.edges()) # dictionary of pin concentrations for each directed graph edge
    db.set_property('PIN', PIN)
    db.set_description('PIN', '')
    # Triangulate all of the cells in the mesh

    # Fix border values in tissue file
    border = db.get_property('border')
    border = dict((cid, border.get(cid, None)) for cid in mesh.wisps(2))
    db.set_property('border', border)

    cell_type = db.get_property('cell_type')
    AUX = dict((eid, 0) for eid in graph.edges())
    P = dict((eid, 1) for eid in graph.edges())
 


    """
    for eid in graph.edges():
        sid = graph.source(eid)
        if cell_type[sid]==3 and border[sid]==7:
            AUX[eid] = .1
        else:
            AUX[eid] = 0
    """
    db.set_property('AUX', AUX)
    db.set_description('AUX','')
    db.set_property('P', P)
    db.set_description('P','')

    
    db.set_property('LAX', dict((eid, 0) for eid in graph.edges()))
    db.set_description('LAX', '')
    wall_types={}
    for cid in mesh.wisps(2):
        for wid, pid in zip(*ordered_wids_pids(mesh, pos, cid)):
            v1 = pos[pid]
            s = set(mesh.borders(1, wid))
            s.remove(pid)
            v0 = pos[s.pop()]
            angle = atan2(v1[1]-v0[1], v1[0]-v0[0])
            if -pi/4 <= angle < pi/4:
                wall_types[(cid, wid)] = 0
            if pi/4 <= angle < 3*pi/4:
                wall_types[(cid, wid)] = 1
            if 3*pi/4 <= angle or angle<-3*pi/4:
                wall_types[(cid, wid)] = 2
            if  -3*pi/4 <= angle < -pi/4:
                wall_types[(cid, wid)] = 3
    db.set_property('wall_types', wall_types)
    db.set_description('wall_types', ' wall annotations')

    #properties (loaded from tissue file) which updated on cell division
    divided_props={'cell_type': ('cell', 'attribute', -1),
                   'PIN': ('edge', 'concentration', 1),
                   'AUX': ('edge', 'concentration', 0),
                   'LAX': ('edge', 'concentration', 0),
                   'P': ('edge', 'attribute', 1),
                   'border': ('cell', 'attribute', 0),
                   'wall_types': ('(cell, wall)', 'attribute', 0),
                   }

    db.set_property('divided_props', divided_props)
    db.set_description('divided_props', 'property division rules')
  
    return db



from vroot2D.growth.division_rule import get_long_axis
from vroot2D.utils.db_utilities import get_mesh
from vroot2D.utils.db_geom import ordered_wids_pids
from vroot2D.utils.geom import intersect
from scipy.linalg import norm as norm
from random import random, seed
from operator import itemgetter


def length_profile(x):
    return 10

ct_lengths = { 0 :1.0, 1: 1.0, 2: 1.0, 3: 1.0, 
               4: 1.0, 7: 1.0, 8: 1.0, 9: 1.0 }

from vroot2D.growth.division import Division, AbstractDivisionRule

class DummyDivisionRule(object):
    def __init__(self):
        pass

    def set_default_parameters(self, db):
        """
        """
        pass
    def post_division_rule(self, db, lineage):
        """
        Rule for post-division behaviour (not part of the interface;
        but to show the API for the function returned from 
        get_axis_and_post_division_rule).
        This post-division rule is called after the tissue has been
        divided, but before the sub-cellular mesh is updated.

        :param db: Tissue database
        :param lineage: Lineage dictionary from the division routines.
                        This is a collection of dictionaries,
                        where lineage[0] is the one for points, 
                        lineage[1] that for walls, etc.
                        Each dictionary maps the ids of parent objects
                        to those of new ones
                        e.g. lineage[2][pid] is the list of cells
                        with parent pid. 
                        Those objects with no parent have pid None 
                        e.g. lineage[0][None] are the new points with
                        no parent.
        :type lineage: dict

        """
        new_cells = next(lineage[2].itervalues())
        mesh = get_mesh(db)
        wall_types = db.get_property('wall_types')
        new_walls = lineage[1][None]
        for new_wid in new_walls:
            for nb_cid in mesh.regions(1, new_wid):
                if nb_cid in new_cells:
                    wt = [wall_types[(nb_cid, wid)] for wid in mesh.borders(2, nb_cid)  if wid not in new_walls]
                    if 3 in wt:
                        wall_types[(nb_cid, new_wid)] = 1
                    else:
                        wall_types[(nb_cid, new_wid)] = 3


mature_lengths = { 'epiderm':300., 'cortex':175., 'endoderm':125., 
                   'pericycle':75., 'vasculature':200. }

def profile(x):
    if x<400:
        return 50.
    if x>400+600:
        return 200.
    return 50.0+(x-400)/600.0*(200.0-50.0)

#def profile(x):
#    return 25
        
def divide_long_cells(db, l_min):
    mesh = get_mesh(db)
    pos = db.get_property('position')
    wall_types = db.get_property('wall_types')
    cell_type = db.get_property('cell_type')
    div_rule = DummyDivisionRule() 
    divider = Division(db, div_rule)
    border = db.get_property("border")


    cfg = db.get_config("config")
    ct_map = dict((j,k) for k,j in cfg.cell_types.iteritems())
    
    print ct_map

    for type_name in mature_lengths:
        if type_name in ct_map:
            ct_lengths[ct_map[type_name]]= mature_lengths[type_name]/ \
                                           mature_lengths['cortex']

    old_cids = [ cid for cid in mesh.wisps(2) if cell_type[cid] in ct_lengths ]

    # Use distance from the QC.

    QC_cids = [ cid for cid, ct in cell_type.iteritems() if cell_type[cid]==ct_map['QC'] ]

    QC_centre = np.mean([centroid(mesh, pos, 2, cid) for cid in QC_cids], axis=1)

    print QC_centre

    seed(0)
    new_cids = []
    for cid in old_cids:
        pts = [pos[pid] for pid in mesh.borders(2, cid, 2)]
        l = max(p[0] for p in pts)-min(p[0] for p in pts)
        ct = cell_type[cid]
        if l>l_min:
            border[cid] = 0
            pt, axis = get_long_axis(db, cid)
            n = np.array((-axis[1],axis[0]))
            #n = (0, 1)
            wids, pids = ordered_wids_pids(mesh, pos, cid)
            is_pts = []
            for wid in wids:
                pid0, pid1 = mesh.borders(1, wid)
                p = intersect(pos[pid0], pos[pid1], pt, n)
             
                if p is not None:
                    is_pts.append(p)
            is_pts.sort(key=itemgetter(0))
            apical_pt = is_pts[1]
            basal_pt = is_pts[0]

            apical_distance = abs(apical_pt[0] - QC_centre[0])

            length = norm(basal_pt-apical_pt)
            division_lengths=[]
            distance=0
            while True:
                distance+=( profile(distance+apical_distance)*ct_lengths[ct]*(0.8+0.4*random()) )
                if distance>length:
                    break
                division_lengths.append(distance)
#                print distance, len(division_lengths)
#            print ' *** ', cell_type[cid], length, apical_pt, basal_pt, division_lengths[0:3]
            division_pts=[ apical_pt+(basal_pt-apical_pt)*(d/length)
                           for d in division_lengths ]
            a=apical_pt-basal_pt
            a=a/norm(a)
            #a = (1, 0)
            for p in division_pts:
#                print 'p, a, apical_pt, basal_pt: ', p, a, apical_pt, basal_pt
                lineage = divider.divide_cell_axis(cid, (p, a), div_rule.post_division_rule)
                cid1, cid2 = lineage[2][cid]
                cx1 = centroid(mesh, pos, 2, cid1)[0]
                cx2 = centroid(mesh, pos, 2, cid2)[0]
                if cx2<cx1:
                    cid = cid2
                    new_cids.append(cid1)
                else:
                    cid = cid1
                    new_cids.append(cid2)

            new_cids.append(cid)
            border[cid] = 1

    return new_cids
                
        
    
