from openalea.celltissue import TissueDB, Tissue, ConfigFormat, ConfigItem
from openalea.tissueshape import edge_length,face_surface_2D
from openalea.scheduler import Scheduler,Task
from vroot2D.growth.mesh_nbs import triangulate_mesh_gmsh as triangulate_mesh
from vroot2D.utils.geom import area

#from vplants.plantgl.math import Vector2
from numpy import array 
from math import atan2, pi

# From python docs
import operator
def accumulate(iterable, func=operator.add):
    'Return running totals'
    # accumulate([1,2,3,4,5]) --> 1 3 6 10 15
    # accumulate([1,2,3,4,5], operator.mul) --> 1 2 6 24 120
    it = iter(iterable)
    total = next(it)
    yield total
    for element in it:
        total = func(total, element)
        yield total

def add_graph(db):
    t = db.tissue()
    cfg = db.get_config('config')
    mesh = t.relation(cfg.mesh_id)
    pos = db.get_property('position')
    EDGE = t.add_type('EDGE')    
    graph_id = t.add_relation("graph",(cfg.CELL, EDGE))
    graph = t.relation(graph_id)
    cfg.add_item(ConfigItem('graph_id', graph_id))
    cfg.add_item(ConfigItem('EDGE', EDGE))

    wall = {}

    for wid in mesh.wisps(1) :
	if mesh.nb_regions(1,wid) == 2 :
            print list(mesh.regions(1, wid)), mesh.nb_regions(1, wid)
            cid1,cid2 = mesh.regions(1,wid)
            eid1 = graph.add_edge(cid1,cid2)
            wall[eid1] = wid
            eid2 = graph.add_edge(cid2,cid1)
            wall[eid2] = wid

    db.set_property('wall', wall)
    db.set_description('wall', "wall corresponding to a given edge")

def ordered_wids_pids(mesh, pos, cid):
    wids = list(mesh.borders(2,cid))
    wid = wids.pop()
    o_wids = [wid]
    end = list(mesh.borders(1, wid))[-1]
    o_pids= [end]
    while wids:
        for wid in wids:
            if end in mesh.borders(1, wid):
                wids.remove(wid)
                o_wids.append(wid)
                pids = set(mesh.borders(1, wid))
                pids.remove(end)
                end = pids.pop()
                o_pids.append(end)
                break
    if area([pos[pid] for pid in o_pids])<0:
        o_wids.reverse()
        o_pids=o_pids[-2::-1]+[o_pids[-1]]
    return o_wids, o_pids



def cell_array_2D(Nx=5, Ny=3, Lx=400.0, Ly=50.0, bend=False, file_idx=False,
                  Ly_heights=None):
    db = TissueDB()
    t = Tissue()
    POINT = t.add_type("POINT")
    EDGE = t.add_type("EDGE")
    WALL = t.add_type("WALL")
    CELL = t.add_type("CELL")
    mesh_id = t.add_relation("mesh", (POINT, WALL, CELL))
    mesh = t.relation(mesh_id)

    graph_id = t.add_relation("graph", (CELL, EDGE))
    graph = t.relation(graph_id)

    cell_types = { 0:'normal', 1:'bend' }
    border = {0: None }

    file_indices = {}

    cfg=ConfigFormat(locals())
    cfg.add_section("tissue descr")
    cfg.add("POINT")
    cfg.add("WALL")
    cfg.add("EDGE")
    cfg.add("CELL")
    cfg.add("mesh_id")
    cfg.add("graph_id")   
    cfg.add("cell_types")
    cfg.add("border")

    # Vertices
    v_list = []
    for i in range((Nx+1)*(Ny+1)):
        v_list.append(mesh.add_wisp(0))

    if Ly_heights==None:
        y_pos = [Ly*j for j in range(0,Ny+1)]
    else:
        y_pos = [0.0]+list(accumulate(Ly_heights))

    print Ly_heights, y_pos

    # Set positions
    pos = {}
    for j in range(Ny+1):
        for i in range(Nx+1):
            pos[v_list[i+(Nx+1)*j]]=array((Lx*i, y_pos[j]), dtype='float')

    # Horizontal walls
    hw_list = []
    for i in range(Nx*(Ny+1)):
        hw_list.append(mesh.add_wisp(1))            
    for j in range(Ny+1):            
        for i in range(Nx):
            mesh.link(1, hw_list[i+j*Nx], v_list[i+j*(Nx+1)])
            mesh.link(1, hw_list[i+j*Nx], v_list[i+1+j*(Nx+1)])
    # Vertical walls
    vw_list = []
    for i in range(Ny*(Nx+1)):
        vw_list.append(mesh.add_wisp(1))            
    for j in range(Ny):            
        for i in range(Nx+1):
            mesh.link(1, vw_list[i+j*(Nx+1)], v_list[i+j*(Nx+1)])
            mesh.link(1, vw_list[i+j*(Nx+1)], v_list[i+(j+1)*(Nx+1)])

    ct_dict = {}
    c_list = []
    for j in range(Ny):
        for i in range(Nx):
            cid = mesh.add_wisp(2)
            c_list.append(cid)
            mesh.link(2, cid, hw_list[i+j*Nx])
            mesh.link(2, cid, hw_list[i+(j+1)*Nx])
            mesh.link(2, cid, vw_list[i+j*(Nx+1)])
            mesh.link(2, cid, vw_list[i+1+j*(Nx+1)])
            border[cid] = 1 if i==Nx-1 else 0
            ct_dict[cid] = 1 if (j==Ny-1 and bend) else 0
            file_indices[cid] = j
#   ct_dict = dict((cid, 0) for cid in mesh.wisps(2))
    


    
    PIN = {}
    AUX = {}
    
    
    db.set_config("config", cfg.config())
    db.set_tissue(t)
    db.set_property('position', pos)
    db.set_description('position','')
    db.set_property('cell_type', ct_dict)
    db.set_description('cell_type','')
    db.set_property('border', border)
    db.set_description('border','')

    if file_idx:
        db.set_property('file_indices', file_indices)
        db.set_description('file_indices', '')
    add_graph(db)


    wall_types={}
    for cid in mesh.wisps(2):
        for wid, pid in zip(*ordered_wids_pids(mesh, pos, cid)):
            v1 = pos[pid]
            s = set(mesh.borders(1, wid))
            s.remove(pid)
            v0 = pos[s.pop()]
            angle = atan2(v1[1]-v0[1], v1[0]-v0[0])
            if -pi/4 < angle < pi/4:
                wall_types[(cid, wid)] = 0
            if pi/4 < angle < 3*pi/4:
                wall_types[(cid, wid)] = 1
            if 3*pi/4 < angle or angle<-3*pi/4:
                wall_types[(cid, wid)] = 2
            if  -3*pi/4 < angle < -pi/4:
                wall_types[(cid, wid)] = 3
    db.set_property('wall_types', wall_types)
    db.set_description('wall_types', ' wall annotations')

    #properties (loaded from tissue file) which updated on cell division
    divided_props={'cell_type': ('cell', 'attribute', -1),
#                   'PIN': ('edge', 'concentration', 1),
                   'border': ('cell', 'attribute', 0),
                   'wall_types': ('(cell, wall)', 'attribute', 0) }
    if file_idx:
        divided_props['file_indices'] = ('cell', 'attribute', -1)

    db.set_property('divided_props', divided_props)
    db.set_description('divided_props', 'property division rules')


    return db

from vroot2D.growth.division import Division, AbstractDivisionRule

class DummyDivisionRule(object):
    def __init__(self):
        pass

    def set_default_parameters(self, db):
        """
        """
        pass
    def post_division_rule(self, db, lineage):
        """
        Rule for post-division behaviour (not part of the interface;
        but to show the API for the function returned from 
        get_axis_and_post_division_rule).
        This post-division rule is called after the tissue has been
        divided, but before the sub-cellular mesh is updated.

        :param db: Tissue database
        :param lineage: Lineage dictionary from the division routines.
                        This is a collection of dictionaries,
                        where lineage[0] is the one for points, 
                        lineage[1] that for walls, etc.
                        Each dictionary maps the ids of parent objects
                        to those of new ones
                        e.g. lineage[2][pid] is the list of cells
                        with parent pid. 
                        Those objects with no parent have pid None 
                        e.g. lineage[0][None] are the new points with
                        no parent.
        :type lineage: dict

        """
        return lambda x,y,z: None


from openalea.tissueshape import centroid
from vroot2D.utils.db_utilities import get_mesh
from random import Random
from math import sin, cos




def staggered_array_2D(bend=False, random_axis=False, seed=None,
                       Lx=50, LxT=500, Ly=20, Ny=5, file_idx=False,
                       Ly_heights = None):
    db = cell_array_2D(1, Ny, LxT, Ly, bend, file_idx, Ly_heights)
    mesh = get_mesh(db)
    pos = db.get_property('position')
    border = db.get_property('border')
    orig_cells = list(mesh.wisps(2))
    divider = Division(db, DummyDivisionRule())
    
    gen1 = Random()
    gen1.seed(seed)
    gen2 = Random()
    gen2.seed(seed)

    for old_cid in orig_cells:
        border[old_cid]=0
        cid = old_cid
        x = 0
        axis =(1,0)
        while True:
            x+=Lx*(1+gen1.random())
            if x>=LxT:
                border[cid]=1
                break
            if random_axis == True:
                theta = 0.3*(gen2.random()-0.5)
                axis = (cos(theta), sin(theta))

            lineage = divider.divide_cell_axis(cid, ((x,0), axis), lambda a,b: None)
            cid1, cid2 = lineage[2][cid]
            cx1 = centroid(mesh, pos, 2, cid1)[0]
            cx2 = centroid(mesh, pos, 2, cid2)[0]
            cid = cid2 if cx2>cx1 else cid1
            
                                           

    return db

