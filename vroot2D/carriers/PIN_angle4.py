
from vroot2D.utils.db_utilities import get_mesh, get_graph, get_wall_decomp
from vroot2D.utils.db_geom import ordered_wids_pids
import numpy as np
from math import sqrt, sin, cos, pi
from vroot2D.utils.geom import area, centroid
from vroot2D.utils.utils import pairs
from openalea.container import topo_divide_edge
from vroot2D.growth.mesh_nbs import triangulate_mesh_gmsh

import numpy as np
import matplotlib.pylab as plt
from matplotlib.patches import Polygon
from matplotlib.collections import PolyCollection
import heapq
from math import sqrt, acos
from collections import defaultdict

"""
Simple implementation of polygon sweep-line algorithm to find
the lowest n% of a polygons area
"""


def insert_pt_in_wall(db, old_wid, p):
    # Insert a point at location p in wall with wall id old_wid
    # Note that this does not consider the triangle mesh
    mesh=get_mesh(db)
    old_wall_regions=list(mesh.regions(1, old_wid))
    old_pid1, old_pid2 = mesh.borders(1, old_wid)
    # Also need to deal with the old graph edge
    wall = db.get_property('wall')
    graph=get_graph(db)
    old_eids=[]
    old_sources=[]
    old_targets=[]
    for eid in wall:
        if wall[eid] == old_wid:
            old_eids.append(eid)
            old_sources.append(graph.source(eid))
            old_targets.append(graph.target(eid))

    wid1, wid2, pid=topo_divide_edge(mesh, old_wid)
    pos=db.get_property('position')
    pos[pid]=p

    graph_lineage={}

    for old_eid in old_eids:
        del wall[old_eid]

    for old_eid, source, target in zip(old_eids, old_sources, old_targets):
        graph.remove_edge(old_eid)
        new_eid1=graph.add_edge(source, target)
        new_eid2=graph.add_edge(source, target)
        graph_lineage[old_eid]=[new_eid1, new_eid2]
        wall[new_eid1]=wid1
        wall[new_eid2]=wid2
        graph_lineage[old_eid]=[new_eid1, new_eid2]

    # And update the properties associated with wid ....
    if 'divided_props' in db.properties():
        for prop_name, rule in db.get_property('divided_props').iteritems():
            prop=db.get_property(prop_name)
            if rule[0]=='(cell, wall)':
                for old_cid in old_wall_regions:
                    mem=prop.pop((old_cid, old_wid))
                    if rule[1]!='amount':
                        for new_wid in (wid1, wid2):
                            prop[(old_cid, new_wid)]=mem
                    else:
                        total_length=sum(edge_length(mesh, pos, new_wid) for new_wid in (wid1, wid2))
                        for new_wid in (wid1, wid2):                                
                            prop[(old_cid, new_wid)]=mem*edge_length(mesh, pos, new_wid)/total_length
            elif rule[0]=='wall':
                mem=prop.pop(old_wid)
                if rule[1]!='amount':
                    for new_wid in (wid1, wid2):
                        prop[new_wid]=mem
                else:
                    raise
            elif rule[0]=='edge':
                for old_eid, new_eids in graph_lineage.iteritems():
                    if old_eid:
                        old_value=prop.pop(old_eid)
                    else:
                        old_value=rule[2]
                    for eid in new_eids:
                        prop[eid]=old_value
                
    return (wid1, wid2), pid


def refine_columella_walls(db):
    mesh = get_mesh(db)
    col_walls = []
    cell_type = db.get_property('cell_type')
    pos = db.get_property('position')
    for wid in mesh.wisps(1):
        if any(cell_type[cid] in (11,12,13) for cid in mesh.regions(1, wid)):
            col_walls.append(wid)
    for wid in col_walls:
        pid1, pid2 = mesh.borders(1, wid)
        v1 = pos[pid1]
        v2 = pos[pid2]
        c_wid = wid
        NP = 10
        for i in range(NP):
            t = (i+1)/float(NP+1)
            v = v1*(1-t)+v2*t
            (wid_a, wid_b), pid = insert_pt_in_wall(db, c_wid, v)
            if pid2 in mesh.borders(1, wid_a):
                c_wid = wid_a
            else:
                c_wid = wid_b
        



def ccw(a, b, c) :
    """ Test whether three points are in a counter
        clockwise order in the plane

        Args:
            a, b, c: Positions of the three points (tuple / numpy.ndarray)

        Returns:    
            int. Order of points in the plane:: 
              1  --  counter-clockwise 
              0  --  colinear 
              -1  --  clockwise

        It is probably unwise to use this as a test 
        for colinearity, owing to the inexactness of floating
        point arithmetic.
    """ 
    # test for some of the degenerate cases (two point coninciding)
    # ignore collinear but not coincident points
    if np.all(a==b) or np.all(a==c) or np.all(b==c) :
        return 0
    # rule from 
    # http://compgeom.cs.uiuc.edu/~jeffe/teaching/
    #        algorithms/notes/xn-convexhull.pdf
    else :
        s=(b[0]-a[0])*(c[1]-b[1])-(b[1]-a[1])*(c[0]-b[0])
        if abs(s)<1e-6:
            return 0
        return cmp(s, 0.0)
    


def pairs(lst):
    """Generaterator which returns all the sequential pairs of lst,
    treating lst as a circular list
    
    >>> list(pairs([1,2,3]))
    [(1, 2), (2, 3), (3, 1)]
    """     

    i = iter(lst)
    first = prev = i.next()
    for item in i:
        yield prev, item
        prev = item
    yield item, first

def area(poly):
    """ 
    Calculates the area of a polygon
    
    Args:
        poly: list of the vertices of a polygon in counterclockwise order

    Returns:
        float. Area of the polygon
    """
    return sum((0.5*(pp[0][0]*pp[1][1]-pp[1][0]*pp[0][1]) 
                for pp in pairs(poly)))


def order(p, q):
    """
    Input - points p and q.
    Return true if p is "above" q
    """
    return cmp((p[1], -p[0]), (q[1], -q[0]))

        

def rot(x):
    return np.array([-x[1], x[0]])


def sweep(poly, angle, theta):
    """
    Sweep-line algorithm to calculate the lower theta proportion of
    the polygon, in the direction indicated by d
    """
    d = np.array((-sin(angle), -cos(angle)))
#    d = np.array(d)/sqrt(np.dot(d,d))

    return sweep_d(poly, d, theta)

# Make robust when parallel to face

def sweep_d(poly, d, theta):

    N = len(poly)

    t = -rot(d)
    r_poly = [(np.dot(t,v), np.dot(d,v)) for v in poly ]

    
    # pq = [ (np.dot(v, d), np.dot(v,r), i) for i, v in enumerate(r_poly) ]
    pq = [ (-v[1], v[0], i) for i, v in enumerate(r_poly) ]
    
    heapq.heapify(pq)
    T = set()

    y0 = None
    A = 0
    A_target = area(poly)*theta
    prop_edge = {}
    while pq:
        i = heapq.heappop(pq)[2]
        v = poly[i]
        r_v = r_poly[i]
        y1 = r_v[1]
        dA_t = 0
        for e in T:
            if y1<y0:
                il0 = (e[0]-1)%N
                il1 = e[0]
                ir0 = (e[1]+1)%N
                ir1 = e[1]

                vl0 = r_poly[il0]
                vl1 = r_poly[il1]
                vr0 = r_poly[ir0]
                vr1 = r_poly[ir1]

                sl = (vl1[0]-vl0[0])/(vl1[1]-vl0[1])
                sr = (vr1[0]-vr0[0])/(vr1[1]-vr0[1])
                xl = vl0[0]+0.5*((y0-vl0[1])+(y1-vl0[1]))*sl
                xr = vr0[0]+0.5*((y0-vr0[1])+(y1-vr0[1]))*sr
                dA = (xr-xl)*(y0-y1)

                dA_t+= dA
                lxl = vl0[0]+(y1-vl0[1])*sl
                rxl = vr0[0]+(y1-vr0[1])*sr
                pl = (y1-vl0[1])/(vl1[1]-vl0[1])
                pr = (y1-vr0[1])/(vr1[1]-vr0[1])
                vl = np.array(poly[il0])*(1-pl)+np.array(poly[il1])*pl
                vr = np.array(poly[ir0])*(1-pr)+np.array(poly[ir1])*pr
                #print 'line ...', vl, vr, pl, pr
                #plt.plot((vl[0], vr[0]), (vl[1], vr[1]), 'r:', lw=2)
        if A+dA_t<A_target:
            A += dA_t
        else:
            dA_e = A_target - A
            l0 = 0.0
            s0 = 0.0
            for e in T:
                if y1<y0:
                    il0 = (e[0]-1)%N
                    il1 = e[0]
                    ir0 = (e[1]+1)%N
                    ir1 = e[1]

                    vl0 = r_poly[il0]
                    vl1 = r_poly[il1]
                    vr0 = r_poly[ir0]
                    vr1 = r_poly[ir1]
                
                    pl = (y0-vl0[1])/(vl1[1]-vl0[1])
                    pr = (y0-vr0[1])/(vr1[1]-vr0[1])
                    sl = (vl1[0]-vl0[0])/(vl1[1]-vl0[1])
                    sr = (vr1[0]-vr0[0])/(vr1[1]-vr0[1])
                    #print y0, sl, sr, pl, pr
                    #print vr0[0]*(1-pr)+vr1[0]*pr, vl0[0]*(1-pl)+vl1[0]*pl
                    

                    l0 += (vr0[0]*(1-pr)+vr1[0]*pr)-(vl0[0]*(1-pl)+vl1[0]*pl)
                    s0 += sl-sr
       #     print l0, s0, dA_e, dA_t
            if s0>0:
                dy = (-l0+sqrt(l0*l0+2*s0*dA_e))/(s0)
            elif s0<0:
                dy = (-l0+sqrt(l0*l0+2*s0*dA_e))/(s0)        
            else:
                dy = dA_e/l0

            #print 'dy', dy, dy*l0, s0

            y1 = y0 - dy
                     
            for e in T:
                if y1<y0:
                     il0 = (e[0]-1)%N
                     il1 = e[0]
                     ir0 = (e[1]+1)%N
                     ir1 = e[1]

                     vl0 = r_poly[il0]
                     vl1 = r_poly[il1]
                     vr0 = r_poly[ir0]
                     vr1 = r_poly[ir1]
                     
                     sl = (vl1[0]-vl0[0])/(vl1[1]-vl0[1])
                     sr = (vr1[0]-vr0[0])/(vr1[1]-vr0[1])
                     xl = vl0[0]+0.5*((y0-vl0[1])+(y1-vl0[1]))*sl
                     xr = vr0[0]+0.5*((y0-vr0[1])+(y1-vr0[1]))*sr
                     dA = (xr-xl)*(y0-y1)

                     A += dA 

                     pl = (y1-vl0[1])/(vl1[1]-vl0[1])
                     pr = (y1-vr0[1])/(vr1[1]-vr0[1])
                     vl = np.array(poly[il0])*(1-pl)+np.array(poly[il1])*pl
                     vr = np.array(poly[ir0])*(1-pr)+np.array(poly[ir1])*pr
                #print 'line ...', vl, vr, pl, pr
                     #plt.plot((vl[0], vr[0]), (vl[1], vr[1]), 'r-', lw=3)
                     prop_edge[il0] = pl
                     prop_edge[ir1] = pr

            break

            
                
                
        c = classify_point(r_poly, i)
#        print i, r_poly[i], classify_point(r_poly, i), T
        if c =='regular':
            #plt.plot([v[0]], [v[1]], marker='o', markerfacecolor='k')
            for oe in T:
                if oe[0] == i:
                    ne = ((i+1)%N, oe[1])
                    prop_edge[(i-1)%N] = 1.0
                    T.remove(oe)
                    T.add(ne)
                    break
                if oe[1] == i:
                    ne = (oe[0], (i-1)%N)
                    prop_edge[i] = 1.0
                    T.remove(oe)
                    T.add(ne)
                    break
        if c =='start':
            #plt.plot([v[0]], [v[1]], marker='s', markerfacecolor='w')
            T.add(((i+1)%N, (i-1)%N))
        if c =='end':            
            #plt.plot([v[0]], [v[1]], marker='s', markerfacecolor='k')
            T.remove((i, i))
            prop_edge[(i-1)%N] = 1.0
            prop_edge[i] = 1.0
        if c =='split':
            #plt.plot([v[0]], [v[1]], marker='^', markerfacecolor='k')
            # Split correct line
            for oe in T:
                le = r_poly[(oe[0]-1)%N], r_poly[oe[0]]
                re = r_poly[(oe[0]+1)%N], r_poly[oe[0]]
                if ccw(le[0], le[1], r_v)==1 and ccw(re[0], re[1], r_v)!=1:
                    T.add((oe[0], (i-1)%N))
                    T.add(((i+1)%N, oe[1]))
                    T.remove(oe)
                    break
            
        if c =='merge':
            #plt.plot([v[0]], [v[1]], marker='v', markerfacecolor='k')
            for oe in T:
                if oe[0] == i:
                    re = oe
                if oe[1] == i:
                    le = oe
            ne = (le[0], re[1])
            T.remove(le)
            T.remove(re)
            T.add(ne)
            prop_edge[(i-1)%N] = 1.0
            prop_edge[i] = 1.0
#        print 'T: ', T
#        print 'A: ', A, area(poly)
        y0 = y1
   # print 'A_final : ', A, area(poly), theta*area(poly)
    return prop_edge

def classify_point(poly, i):
    N = len(poly)
    vm = poly[(i-1)%N]
    v = poly[i]
    vp = poly[(i+1)%N]
    op = order(v, vp)
    om = order(v, vm)
    if om != op:
        return 'regular'
    if op == 1:
        # Either start or split vertex
        if ccw(vm, v, vp)!=-1:
            return 'start'
        else:
            return 'split'
    else:
        # Either end or merge vertex
        if ccw(vm, v, vp)==1:
            return 'end'
        else:
            return 'merge'

def sgn(x):
    return cmp(x,0)

class PINAngle_fullroot(object):
    def __init__(self, db, gv=(0,-1), theta=0.0, ablated=[]):
        self.db = db
        self.theta = theta
        db.set_property('theta', 0.0)
        refine_columella_walls(db)
        cell_type = db.get_property('cell_type')
        col_cells = [cid for cid, ct in cell_type.iteritems() if ct in (11,12,13)]        
        col_normals = {}
        for cid in col_cells:
            col_normals[cid] = -np.array(gv)
        db.set_property('col_normals', col_normals)
        db.set_description('col_normals', '')
        self.gv = gv
        self.ablated=ablated

    def reset_normals(self):
        db = self.db
        cell_type = db.get_property('cell_type')
        col_cells = [cid for cid, ct in cell_type.iteritems() if ct in (11,12,13)]
        col_normals = db.get_property('col_normals')
        for cid in col_cells:
            col_normals[cid] = -np.array(self.gv)

    def apolar(self):
        db = self.db
        pos = db.get_property('position')
        mesh = get_mesh(db)
        graph = get_graph(db)
        cell_type = db.get_property('cell_type')

        col_cells = [cid for cid, ct in cell_type.iteritems() if ct in (11,12,13) and cid not in self.ablated]
        col_normals = db.get_property('col_normals')
        wall_type = db.get_property('wall_types')
        PIN = db.get_property('PIN')
        w_d = get_wall_decomp(db)
        for cid in col_cells:
            n = col_normals[cid]
            wids, pids = ordered_wids_pids(mesh, pos, cid)
            poly = [pos[pid] for pid in pids]    
     
            for wid in wids:
                try:
                    eid1, eid2 = w_d[wid]
                except KeyError:
                    continue
                if graph.source(eid1)==cid:
                    PIN[eid1]=1.0
                else:
                    PIN[eid2]=1.0

        for cid in self.ablated:
            for eid in graph.out_edges(cid):
                PIN[eid] = 0
#        print PIN


    def step(self):
        print 'PA STEP'
        theta = self.theta
        db = self.db
        pos = db.get_property('position')
        mesh = get_mesh(db)
        graph = get_graph(db)
        cell_type = db.get_property('cell_type')

        col_cells = [cid for cid, ct in cell_type.iteritems() if ct in (11,12,13) and cid not in self.ablated]
        col_normals = db.get_property('col_normals')
        wall_type = db.get_property('wall_types')
        PIN = db.get_property('PIN')
        w_d = get_wall_decomp(db)
        for cid in col_cells:
            n = col_normals[cid]
            wids, pids = ordered_wids_pids(mesh, pos, cid)
            poly = [pos[pid] for pid in pids]    

     
#       if n[1]<cos(theta):
            print 'reorientate', cid, n
            n = col_normals[cid] = -np.array(self.gv) #np.array((sgn(n[0])*sin(theta), cos(theta)))
            lower = sweep_d(poly, -n, 0.4)
#                lower = sweep_d(poly, -n, 0.4)
#                lower = sweep(poly, 0.0, 0.4)
                
            #else:
            #    print 'non-reorientate', cid, n
               # lower = sweep(poly, db.get_property('theta'), 0.4)
#                lower = sweep_d(poly, np.array((0., -1.)), 0.4)
            #    lower = sweep_d(poly, -n, 0.4)
#            print lower
            lower_wids = dict( (wids[i], v) for i,v in lower.iteritems() )
            for wid in wids:
                try:
                    eid1, eid2 = w_d[wid]
                except KeyError:
                #    print 'KEY error', wid
                    continue
                val = lower_wids.get(wid, 0.0)
                if graph.source(eid1)==cid:
                    PIN[eid1]=val
                else:
                    PIN[eid2]=val

        for cid in self.ablated:
            for eid in graph.out_edges(cid):
                PIN[eid] = 0
#        print PIN


class PINAngle_fullroot_jump(object):
    def __init__(self, db, gv=(0,-1), theta=45/180.0*pi, ablated=[]):
        self.db = db
        self.theta = theta
        db.set_property('theta', 0.0)
        refine_columella_walls(db)
        cell_type = db.get_property('cell_type')
        col_cells = [cid for cid, ct in cell_type.iteritems() if ct in (11,12,13)]        
        col_normals = {}
        for cid in col_cells:
            col_normals[cid] = -np.array(gv)
        db.set_property('col_normals', col_normals)
        db.set_description('col_normals', '')
        self.gv = gv
        self.ablated=ablated

    def reset_normals(self):
        db = self.db
        cell_type = db.get_property('cell_type')
        col_cells = [cid for cid, ct in cell_type.iteritems() if ct in (11,12,13)]
        col_normals = db.get_property('col_normals')
        for cid in col_cells:
            col_normals[cid] = -np.array(self.gv)

    def step(self):
        print 'PA STEP'
        theta = self.theta
        db = self.db
        pos = db.get_property('position')
        mesh = get_mesh(db)
        graph = get_graph(db)
        cell_type = db.get_property('cell_type')

        col_cells = [cid for cid, ct in cell_type.iteritems() if ct in (11,12,13) and cid not in self.ablated]
        col_normals = db.get_property('col_normals')
        wall_type = db.get_property('wall_types')
        PIN = db.get_property('PIN')
        w_d = get_wall_decomp(db)
        for cid in col_cells:
            n = col_normals[cid]
            wids, pids = ordered_wids_pids(mesh, pos, cid)
            poly = [pos[pid] for pid in pids]    
     
            if -np.dot(n, self.gv)<cos(theta):
                print 'reorientate', cid, n
                n = col_normals[cid] = -np.array(self.gv) #np.array((sgn(n[0])*sin(theta), cos(theta)))
            lower = sweep_d(poly, -n, 0.4)
#                lower = sweep_d(poly, -n, 0.4)
#                lower = sweep(poly, 0.0, 0.4)
                
            #else:
            #    print 'non-reorientate', cid, n
               # lower = sweep(poly, db.get_property('theta'), 0.4)
#                lower = sweep_d(poly, np.array((0., -1.)), 0.4)
            #    lower = sweep_d(poly, -n, 0.4)
#            print lower
            lower_wids = dict( (wids[i], v) for i,v in lower.iteritems() )
            for wid in wids:
                try:
                    eid1, eid2 = w_d[wid]
                except KeyError:
                #    print 'KEY error', wid
                    continue
                val = lower_wids.get(wid, 0.0)
                if graph.source(eid1)==cid:
                    PIN[eid1]=val
                else:
                    PIN[eid2]=val

        for cid in self.ablated:
            for eid in graph.out_edges(cid):
                PIN[eid] = 0
#        print PIN


class PINAngle_fullroot_PINs(object):
    def __init__(self, db, gv=(0,-1), theta=0.0, ablated=[]):
        self.db = db
        self.theta = theta
        db.set_property('theta', 0.0)
        refine_columella_walls(db)
        cell_type = db.get_property('cell_type')
        col_cells = [cid for cid, ct in cell_type.iteritems() if ct in (11,12,13)]        
        col_normals = {}
        for cid in col_cells:
            col_normals[cid] = -np.array(gv)
        db.set_property('col_normals', col_normals)
        db.set_description('col_normals', '')
        self.gv = gv
        self.ablated=ablated
        self.cell_PIN_values = { 'PIN3':{}, 'PIN7':{} }
        PIN3 = db.get_property('PIN3')
        PIN7 = db.get_property('PIN7')
        graph = get_graph(db)
        for cid in col_cells:
            out_eids = list(graph.out_edges(cid))
            self.cell_PIN_values['PIN3'][cid] = sum(PIN3[eid] for eid in out_eids)/float(len(out_eids))
            self.cell_PIN_values['PIN7'][cid] = sum(PIN7[eid] for eid in out_eids)/float(len(out_eids))



    def reset_normals(self):
        db = self.db
        cell_type = db.get_property('cell_type')
        col_cells = [cid for cid, ct in cell_type.iteritems() if ct in (11,12,13)]
        col_normals = db.get_property('col_normals')
        for cid in col_cells:
            col_normals[cid] = -np.array(self.gv)

    def step(self):
        print 'PA STEP'
        theta = self.theta
        db = self.db
        pos = db.get_property('position')
        mesh = get_mesh(db)
        graph = get_graph(db)
        cell_type = db.get_property('cell_type')

        col_cells = [cid for cid, ct in cell_type.iteritems() if ct in (11,12,13) and cid not in self.ablated]
        col_normals = db.get_property('col_normals')
        wall_type = db.get_property('wall_types')
        PIN3 = db.get_property('PIN3')
        PIN7 = db.get_property('PIN7')
        w_d = get_wall_decomp(db)
        for cid in col_cells:
            n = col_normals[cid]
            wids, pids = ordered_wids_pids(mesh, pos, cid)
            poly = [pos[pid] for pid in pids]    

            val3 = 1.0 # self.cell_PIN_values['PIN3'][cid]
            val7 = 0.0 # self.cell_PIN_values['PIN7'][cid]
     
#       if n[1]<cos(theta):
            print 'reorientate', cid, n
            n = col_normals[cid] = -np.array(self.gv) #np.array((sgn(n[0])*sin(theta), cos(theta)))
            lower = sweep_d(poly, -n, 0.4)
#                lower = sweep_d(poly, -n, 0.4)
#                lower = sweep(poly, 0.0, 0.4)
                
            #else:
            #    print 'non-reorientate', cid, n
               # lower = sweep(poly, db.get_property('theta'), 0.4)
#                lower = sweep_d(poly, np.array((0., -1.)), 0.4)
            #    lower = sweep_d(poly, -n, 0.4)
#            print lower
            lower_wids = dict( (wids[i], v) for i,v in lower.iteritems() )
            for wid in wids:
                try:
                    eid1, eid2 = w_d[wid]
                except KeyError:
                #    print 'KEY error', wid
                    continue
                val = lower_wids.get(wid, 0.0)
                if graph.source(eid1)==cid:
                    PIN3[eid1]=val3*val
                    PIN7[eid1]=val7*val
                else:
                    PIN3[eid2]=val3*val
                    PIN7[eid2]=val7*val

        for cid in self.ablated:
            for eid in graph.out_edges(cid):
                PIN3[eid] = 0.0
                PIN7[eid] = 0.0
#        print PIN
