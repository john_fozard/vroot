from vroot2D.utils.db_utilities import get_mesh, get_graph, get_parameters, set_parameters, get_wall_decomp, add_property
from numpy import *
from pylab import *

#from plotfigures import plotPINs, plotAUX1

def manipulate(db, dictParams=[]):

    graph = get_graph(db) 
    mesh = get_mesh(db)    
    PIN = db.get_property('PIN')
    AUX1 = db.get_property('AUX')
    if 'LAX' in db.properties():
        LAX = db.get_property('LAX')
    else:
        LAX = {}
        add_property(db, 'LAX', 'edge', 0.0)
        
    wall_decomposition = get_wall_decomp(db)
    cell_type = db.get_property('cell_type')

    AUX1.clear()
    LAX.clear()
    for eid in graph.edges():
        AUX1[eid]=0.0
        LAX[eid]=0.0
    # Calculate the co-ordinates of each cell.
    position = db.get_property('position') # Does this exist?
    cell_to_coordinates = CellToCoordinates_manipulate(mesh,position)  # This is defined at the end of this script.

    # Calculate the centroid of each cell and maximum x-co-ordinate of the geometry:
    centroid={}
    xmax=0
    for cid in mesh.wisps(2):        
 	    xc=0
	    yc=0	
	    coords=cell_to_coordinates[cid]
	    for j in range(0, size(coords)/2-1):
		xc=xc+coords[j][0]
		yc=yc+coords[j][1]		
		centroid[cid]=[xc/(size(coords)/2-1),yc/(size(coords)/2-1)]
                if xmax<coords[j][0]:
		    xmax=coords[j][0]
 
    # Calculate xEZ, as the x-position of the centre of the most shootward LRC cell and xQC.      
    xEZ=xmax
    for cid in mesh.wisps(2):
	if cell_type[cid]==6 or cell_type[cid]==7 or cell_type[cid]==8 or cell_type[cid]==9:
	    if centroid[cid][0]<xEZ:	
	        xEZ=centroid[cid][0]
	if cell_type[cid]==17:
		xQC=centroid[cid][0]		


    print 'xEZ', xEZ
    for wid,(eid1,eid2) in wall_decomposition.iteritems() :
	cid1=graph.source(eid1)
	cid2=graph.source(eid2)
	if cell_type[cid1]==17 and cell_type[cid2]==17:
	    xQC=(centroid[cid1][0]+centroid[cid2][0])/2

    # AUX1 in S1,S2 and S3:
    S1S2S3_AUX1=True
    if 'S1S2S3_AUX1' in dictParams:
        S1S2S3_AUX1=dictParams['S1S2S3_AUX1']

    if S1S2S3_AUX1:
        for cid in mesh.wisps(2):            
	    if cell_type[cid]==11 or cell_type[cid]==12 or cell_type[cid]==13:
		for eid in graph.out_edges(cid):
		    AUX1[eid]=1

    # AUX1 in outer LRC:
    LRC1LRC2_AUX1=True
    if 'LRC1LRC2_AUX1' in dictParams:
        LRC1LRC2_AUX1=dictParams['LRC1LRC2_AUX1']

    if LRC1LRC2_AUX1:
        for cid in mesh.wisps(2):
	    if cell_type[cid]==6 or cell_type[cid]==7:
		for eid in graph.out_edges(cid):
		    AUX1[eid]=1


    # AUX1 in inner LRC:
    LRC3LRC4LRC5_AUX1=True
    if 'LRC3LRC4LRC5_AUX1' in dictParams:
        LRC3LRC4LRC5_AUX1=dictParams['LRC3LRC4LRC5_AUX1']

    if LRC3LRC4LRC5_AUX1:
        for cid in mesh.wisps(2):
	    if cell_type[cid]==8 or cell_type[cid]==9 or cell_type[cid]==19:
		for eid in graph.out_edges(cid):
		    AUX1[eid]=1


    # AUX1 in elongating epidermis:
    elongepi_AUX1=True
    if 'elongepi_AUX1' in dictParams:
        elongepi_AUX1=dictParams['elongepi_AUX1']

    if elongepi_AUX1:
        for cid in mesh.wisps(2):
	    if cell_type[cid]==2 and centroid[cid][0]<xEZ+75:
		for eid in graph.out_edges(cid):
		    AUX1[eid]=1
	   
    # AUX1 in meristematic epidermis:
    meriepi_AUX1=False
    if 'meriepi_AUX1' in dictParams:
        meriepi_AUX1=dictParams['meriepi_AUX1']

    if meriepi_AUX1:
        for cid in mesh.wisps(2):
	    if cell_type[cid]==2 and centroid[cid][0]>=xEZ+75:
		for eid in graph.out_edges(cid):
		    AUX1[eid]=1

    # AUX1 in elongating ctx:
    elongcor_AUX1=True
    if 'elongcor_AUX1' in dictParams:
        elongcor_AUX1=dictParams['elongcor_AUX1']

    if elongcor_AUX1:
        for cid in mesh.wisps(2):
	    if cell_type[cid]==4 and centroid[cid][0]<xEZ:
		for eid in graph.out_edges(cid):
		    AUX1[eid]=1

    # LAX in QC and columella initials:
    QCinit_LAX=True
    if 'QCinit_LAX' in dictParams:
        QCinit_LAX=dictParams['QCinit_LAX']

    if QCinit_LAX:
        for cid in mesh.wisps(2):
	    if cell_type[cid]==10 or cell_type[cid]==17:
		for eid in graph.out_edges(cid):
		    LAX[eid]=1

    # LAX in S2:
    S2_LAX=True
    if 'S2_LAX' in dictParams:
        S2_LAX=dictParams['S2_LAX']

    if S2_LAX:
        for cid in mesh.wisps(2):
	    if cell_type[cid]==12:
		for eid in graph.out_edges(cid):
		    LAX[eid]=1

    # LAX in stele:
    stele_LAX=True
    if 'stele_LAX' in dictParams:
        stele_LAX=dictParams['stele_LAX']

    if stele_LAX:
        for cid in mesh.wisps(2):
	    if cell_type[cid]==5 and centroid[cid][0]>=(xQC+xEZ)/2:
		for eid in graph.out_edges(cid):
		    LAX[eid]=1

    # Inner periclinal PIN in epidermis:
    periepi_PIN=False
    if 'periepi_PIN' in dictParams:
        periepi_PIN=dictParams['periepi_PIN']

    if periepi_PIN:
        for wid,(eid1,eid2) in wall_decomposition.iteritems() :
            cid1 = graph.source(eid1)
	    cid2 = graph.source(eid2)
	    if cell_type[cid1]==2 and cell_type[cid2]==4:
		PIN[eid1]=1
	    if cell_type[cid1]==4 and cell_type[cid2]==2:
		PIN[eid2]=1

   # Inner periclinal PIN in cortex:
    pericor_PIN=False
    if 'pericor_PIN' in dictParams:
        pericor_PIN=dictParams['pericor_PIN']

    if pericor_PIN:
        for wid,(eid1,eid2) in wall_decomposition.iteritems() :
            cid1 = graph.source(eid1)
	    cid2 = graph.source(eid2)
	    if cell_type[cid1]==4 and cell_type[cid2]==3:
		PIN[eid1]=1
	    if cell_type[cid1]==3 and cell_type[cid2]==4:
		PIN[eid2]=1


    periendo_PIN=False
    if 'periendo_PIN' in dictParams:
        periendo_PIN=dictParams['periendo_PIN']

    if periendo_PIN:
        for wid,(eid1,eid2) in wall_decomposition.iteritems() :
            cid1 = graph.source(eid1)
	    cid2 = graph.source(eid2)
	    if cell_type[cid1]==3 and cell_type[cid2]==5 and centroid[cid1][0]<xEZ:
		PIN[eid1]=1
	    if cell_type[cid1]==5 and cell_type[cid2]==3 and centroid[cid2][0]<xEZ:
		PIN[eid2]=1

    # Inner periclinal PIN in LRC:
    periLRC_PIN=False
    if 'periLRC_PIN' in dictParams:
        periLRC_PIN=dictParams['periLRC_PIN']

    periLRC_PIN = False
    if periLRC_PIN:
        for wid,(eid1,eid2) in wall_decomposition.iteritems() :
            cid1 = graph.source(eid1)
	    cid2 = graph.source(eid2)
	    if  cell_type[cid1]==6:
		if cell_type[cid2]==7 or cell_type[cid2]==8 or cell_type[cid2]==9 or cell_type[cid2]==19 or cell_type[cid2]==2:
			PIN[eid1]=1
	    if cell_type[cid2]==6:
		if cell_type[cid1]==7 or cell_type[cid1]==8 or cell_type[cid1]==9 or cell_type[cid1]==19 or cell_type[cid1]==2:
			PIN[eid2]=1
	    if  cell_type[cid1]==7:
		if cell_type[cid2]==8 or cell_type[cid2]==9 or cell_type[cid2]==19 or cell_type[cid2]==2:
			PIN[eid1]=1
	    if cell_type[cid2]==7:
		if cell_type[cid1]==8 or cell_type[cid1]==9 or cell_type[cid1]==19 or cell_type[cid1]==2:
			PIN[eid2]=1
  	    if  cell_type[cid1]==8:
		if cell_type[cid2]==9 or cell_type[cid2]==19 or cell_type[cid2]==2:
			PIN[eid1]=1
	    if cell_type[cid2]==8:
		if cell_type[cid1]==9 or cell_type[cid1]==19 or cell_type[cid1]==2:
			PIN[eid2]=1
	    if  cell_type[cid1]==9:
		if cell_type[cid2]==19 or cell_type[cid2]==2:
			PIN[eid1]=1
	    if cell_type[cid2]==9:
		if cell_type[cid1]==19 or cell_type[cid1]==2:
			PIN[eid2]=1
	    if  cell_type[cid1]==19:
		if cell_type[cid2]==2:
			PIN[eid1]=1
	    if cell_type[cid2]==19:
		if cell_type[cid1]==2:
			PIN[eid2]=1

    for wid,(eid1,eid2) in wall_decomposition.iteritems() :
        cid1 = graph.source(eid1)
        cid2 = graph.source(eid2)

        if cell_type[cid1] in (11, 12):#, 13, 14, 15):
            PIN[eid1] = 1
        elif cell_type[cid1] in (13, 14,15):
            PIN[eid1] = 0
        if cell_type[cid2] in (11, 12):#, 13, 14, 15):
            PIN[eid2] = 1
        elif cell_type[cid2] in (13, 14,15):
            PIN[eid2] = 0


    if 'corBasal_PIN' in dictParams:
        for wid,(eid1,eid2) in wall_decomposition.iteritems() :
            cid1 = graph.source(eid1)
            cid2 = graph.source(eid2)

            if cell_type[cid1] == 4 and cell_type[cid2] == 4:
                if centroid[cid1][0]< centroid[cid2][0]:
                    PIN[eid1] = 0
                    PIN[eid2] = 1
                else:
                    PIN[eid1] = 1
                    PIN[eid2] = 0


    fnamePIN='PINplot'
    fnameAUX1='AUX1plot'
#    plotPINs(db,fnamePIN)
#    plotAUX1(db,fnameAUX1) # THese also use outputFolder - is this specified somewhere.
	
    return True


def CellToCoordinates_manipulate(mesh,position):
    
    cell_to_walls = dict((cid,tuple(mesh.borders(2,cid))) for cid in mesh.wisps(2))
    border_to_coordinates = dict((cid,\
                        [[position[tuple(mesh.borders(1,cid))[0]][0],\
                          position[tuple(mesh.borders(1,cid))[0]][1]],\
                         [position[tuple(mesh.borders(1,cid))[1]][0],\
                          position[tuple(mesh.borders(1,cid))[1]][1]]]\
                        ) for cid in mesh.wisps(1))

    ptc=[]
    for ncid,cid in enumerate(mesh.wisps(2)):
        ptc.append([])
        patches=[]
  
        for i in range(len(cell_to_walls[cid])):
            patches.append(border_to_coordinates[cell_to_walls[cid][i]])

        ptc[ncid]=[]
        ptc[ncid].append(patches[0][0])
        ptc[ncid].append(patches[0][1])
        patches.remove(patches[0])
        i=0
        while i <len(patches):
            if patches[i].count(ptc[ncid][-1])==1:
                id1=patches[i].index(ptc[ncid][-1])
		id2=abs(-1+id1)
                ptc[ncid].append(patches[i][id2])
                patches.remove(patches[i])
                i=-1
            i=i+1

    cell_to_coordinates = dict((cid,ptc[ncid]) for ncid,cid in enumerate(mesh.wisps(2)))
 
    return cell_to_coordinates



