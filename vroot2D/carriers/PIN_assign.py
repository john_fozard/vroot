
from vroot2D.utils.db_utilities import get_graph
from openalea.tissueshape import centroid
import numpy as np

def rot(v):
    return np.array((v[1], -v[0]))

def PIN_assign(db):

    cfg = db.get_config("config")

    cell_type = db.get_property('cell_type')
    pos = db.get_property('position')

    print cfg.cell_types

    ct_rev = dict( (v,k) for k,v in cfg.cell_types.iteritems() )

    EPI = ct_rev['epiderm']
    COR = ct_rev['cortex']
    END = ct_rev['endoderm']
    PERI = ct_rev.get('pericycle', None)
    VASC = ct_rev['vasculature']
    QC = ct_rev['QC']
    INIT = 1000000
    COL = ct_rev['columella']
    LRC = ct_rev['cap']

    ct_list = [EPI, COR, END, PERI]

    mesh = db.tissue().relation(cfg.mesh_id)
    graph = db.tissue().relation(cfg.graph_id)
    zone = dict( (cid, 0) for cid in mesh.wisps(2) )
    #border = dict( (cid, 0) for cid in mesh.wisps(2) )

    PIN = db.get_property('PIN')
    for eid in PIN:
        PIN[eid]=0.0
    AUX1 = db.get_property('AUX')
    for eid in AUX1:
        AUX1[eid]=0.0


    graph = get_graph(db)

    SW, RW, INNER, OUTER = 1, 2, 3, 4

    PM, DM, EZ = 1, 2, 3

    centres = {}
    pos = db.get_property('position')

    for cid in mesh.wisps(2):
        centres[cid] = centroid(mesh, pos, 2, cid)

    N = 0
    cy = 0.0
    for cid in mesh.wisps(2):
        if cell_type[cid] == VASC:
            cy += centres[cid][1]
            N += 1
    cy /= N

    # Find the top of the LRC
    PM_top = max(centres[cid][0] for cid in mesh.wisps(2) if cell_type[cid]==LRC)
    N_QC = 0
    QC_x = 0.0
    for cid in mesh.wisps(2):
        if cell_type[cid] == QC:
            QC_x += centres[cid][0]
            N_QC += 1
    QC_x /= N_QC

    PD_x = 0.5*(QC_x + PM_top)
    
    zone = {}
    for cid in mesh.wisps(2):
        x = centres[cid][0]
        if x < PD_x:
            zone[cid] = PM
        elif x < PM_top:
            zone[cid] = DM
        else:
            zone[cid] = EZ
 
    direction = {}
    for eid1 in graph.edges():
        cid1 = graph.source(eid1)
        cid2 = graph.target(eid1)
        if cell_type[cid1]==cell_type[cid2]:
            # Wall between two cells of the same type
            if centres[cid1][0]<centres[cid2][0]:
                direction[eid1] = SW
#                direction[eid2] = RW
            else:
                direction[eid1] = RW
#                direction[eid2] = SW
        elif (cell_type[cid1] in (COR, END, PERI) and 
              cell_type[cid2] in (QC, LRC, COL)):
            direction[eid1] = RW
        elif (cell_type[cid2] in (COR, END, PERI) and 
              cell_type[cid1] in (QC, LRC, COL)):
            direction[eid1] = SW
        elif cell_type[cid1] == EPI and cell_type[cid2] == COL:
            direction[eid1] = RW
        elif cell_type[cid2] == EPI and cell_type[cid1] == COL:
            direction[eid1] = RW
        else:
            # Wall between two cells of diferent types
            if cell_type[cid1] in ct_list and cell_type[cid2] in ct_list:
                if ct_list.index(cell_type[cid1])<ct_list.index(cell_type[cid2]):
                    direction[eid1] = INNER
                else:
                    direction[eid1] = OUTER
            else:
                direction[eid1] = None

    
    # Find last lateral root cap cells
    

    cid_upper = 0
    cid_x_upper = -10000
    cid_lower = 0
    cid_x_lower = -10000
    for cid in cell_type:
        if cell_type[cid]==LRC:
            x = centres[cid][0]
            if centres[cid][1]>cy and x>cid_x_upper:
                cid_upper = cid
                cid_x_upper = x
            if centres[cid][1]<cy and x>cid_x_lower:
                cid_lower = cid
                cid_x_lower = x

    for eid in graph.out_edges(cid_upper):
        tid = graph.target(eid)
        if cell_type[tid]==EPI and centres[tid][0]>centres[cid_upper][0]:
            direction[eid]=SW
    for eid in graph.out_edges(cid_lower):
        tid = graph.target(eid)
        if cell_type[tid]==EPI and centres[tid][0]>centres[cid_lower][0]:
            direction[eid]=SW

        
        
                
                
    PIN1_proteins=True
    PIN2_proteins=True
    PIN3_proteins=True
    PIN4_proteins=True
    PIN7_proteins=True
    AUX1_proteins=True

    if PIN1_proteins:
        for cell in cell_type:
            if cell_type[cell]==QC:
                for eid in graph.out_edges(cell):
                    PIN[eid]=1
            if cell_type[cell]==VASC:
                for eid in graph.out_edges(cell):
                    if direction[eid]==RW:
                        PIN[eid]=1
            if cell_type[cell]==PERI:
                for eid in graph.out_edges(cell):
                    if direction[eid]==RW:
                        PIN[eid]=1
            if cell_type[cell]==END:
                for eid in graph.out_edges(cell):
                    if direction[eid]==RW:
                        PIN[eid]=1

    if PIN2_proteins:
        for cell in cell_type:
            if cell_type[cell]==LRC:
                for eid in graph.out_edges(cell):
                    if direction[eid]==SW:
                        PIN[eid]=1
            if cell_type[cell]==EPI and zone[cell]!=PM:
                    for eid in graph.out_edges(cell):
                        if direction[eid]==SW:
                            PIN[eid]=1 #if centres[cell][1]>cy else 1.0
            if cell_type[cell]==COR:
                if zone[cell]==DM:
                    for eid in graph.out_edges(cell):
                        if direction[eid]==RW:
                            PIN[eid]=1
                        if zone[graph.target(eid)]==EZ and direction[eid]==SW:
                            PIN[eid]=1
                if zone[cell]==EZ:
                    for eid in graph.out_edges(cell):
                        if direction[eid]==SW:
                            PIN[eid]=1
                        if zone[graph.target(eid)]==DM and direction[eid]==RW:
                            PIN[eid]=1

    if PIN3_proteins:
        for cell in cell_type:
            if cell_type[cell] in (QC, COL):
                for eid in graph.out_edges(cell):
                    PIN[eid]=1
            if cell_type[cell] in (VASC, END, PERI):
                for eid in graph.out_edges(cell):
                    if direction[eid]==RW:
                        PIN[eid]=1
            if cell_type[cell]==END:
                for eid in graph.out_edges(cell):
                    if cell_type[graph.target(eid)]==PERI:
                        PIN[eid]=1
    
    if PIN4_proteins:
        for cell in cell_type:
            if cell_type[cell]in (QC, COL):
                for eid in graph.out_edges(cell):
                    PIN[eid]=1
            if cell_type[cell] in (COR, END, PERI, VASC):
                if zone[cell]!=EZ:
                    for eid in graph.out_edges(cell):
                        if direction[eid]==RW:
                            PIN[eid]=1

    if PIN7_proteins:
        for cell in cell_type:
            if cell_type[cell]in (QC, COL):
                for eid in graph.out_edges(cell):
                    PIN[eid]=1
            if cell_type[cell] in (PERI, VASC):
                for eid in graph.out_edges(cell):
                    if direction[eid]==RW:
                            PIN[eid]=1
            if cell_type[cell]in (EPI, COR, END) and zone[cell]==PM:
                for eid in graph.out_edges(cell):
                    if direction[eid]==RW:
                        PIN[eid]=1

    if AUX1_proteins:
        for cell in cell_type:
            if cell_type[cell] in (LRC, QC, COL):
                for eid in graph.out_edges(cell):
                    AUX1[eid]=1
            if cell_type[cell]==EPI and zone[cell]==EZ:
                for eid in graph.out_edges(cell):
                    AUX1[eid]=1
               


def PIN_assign_subset(db, cids, d=np.array([-1,0])):
    # Only apply the rules to a subset of the cells in the tissue
    # Also include an additional direction vector d, in a shootwards
    # direction, as our templates are of various different types.

    cfg = db.get_config("config")

    cell_type = db.get_property('cell_type')
    pos = db.get_property('position')

    wall = db.get_property('wall')

    print cfg.cell_types

    ct_rev = dict( (v,k) for k,v in cfg.cell_types.iteritems() )

    EPI = ct_rev['epiderm']
    COR = ct_rev['cortex']
    END = ct_rev['endoderm']
    PERI = ct_rev.get('pericycle', None)
    VASC = ct_rev['vasculature']
    QC = ct_rev['QC']
    INIT = 1000000
    COL = ct_rev.get('columella', None)
    LRC = ct_rev.get('LRC4', None)
    LRC4 = ct_rev.get('LRC4', None)
    LRC3 = ct_rev.get('LRC3', None)
    LRC2 = ct_rev.get('LRC2', None)
    LRC1 = ct_rev.get('LRC1', None)

    ct_list = [LRC4, LRC3, LRC2, LRC1, EPI, COR, END, PERI]
    LRC_types = [LRC4, LRC3, LRC2, LRC1]


    mesh = db.tissue().relation(cfg.mesh_id)
    graph = db.tissue().relation(cfg.graph_id)
    zone = dict( (cid, 0) for cid in mesh.wisps(2) )

    PIN = db.get_property('PIN')
    AUX1 = db.get_property('AUX')
    for cid in cids:
        for eid in graph.out_edges(cid):
            PIN[eid] = 0.0
            AUX1[eid] = 0.0

    graph = get_graph(db)

    SW, RW, INNER, OUTER = 1, 2, 3, 4

    PM, DM, EZ = 1, 2, 3

    centres = {}
    pos = db.get_property('position')

    for cid in mesh.wisps(2):
        centres[cid] = centroid(mesh, pos, 2, cid)

    N = 0
    cy = 0.0
    n = rot(d)

    for cid in mesh.wisps(2):
        if cell_type[cid] == VASC:
            cy += n.dot(centres[cid])
            N += 1
    cy /= N

    # Find the top of the LRC
    PM_top = max(d.dot(centres[cid]) for cid in mesh.wisps(2) if cell_type[cid]==LRC)
    N_QC = 0
    QC_x = 0.0
    for cid in mesh.wisps(2):
        if cell_type[cid] == QC:
            QC_x += d.dot(centres[cid])
            N_QC += 1
    QC_x /= N_QC

    PD_x = 0.5*(QC_x + PM_top)
    
    zone = {}
    for cid in mesh.wisps(2):
        x = d.dot(centres[cid])
        if x < PD_x:
            zone[cid] = PM
        elif x < PM_top:
            zone[cid] = DM
        else:
            zone[cid] = EZ
 
    direction = {}
    for eid1 in graph.edges():
        cid1 = graph.source(eid1)
        cid2 = graph.target(eid1)
        if cell_type[cid1]==cell_type[cid2]:
            # Wall between two cells of the same type

            if cell_type[cid1] == VASC:
                wid = wall[eid1]
                pid1, pid2 = mesh.borders(1, wid)
                h = pos[pid2] - pos[pid1]
                if abs(h[1])>abs(h[0]):
                    if d.dot(centres[cid2])>d.dot(centres[cid1]):
                        direction[eid1] = SW
                    else:
                        direction[eid1] = RW
                else:
                    direction[eid1] = INNER
#                print h, direction[eid1], centres[cid1], centres[cid2]
            else:
                if d.dot(centres[cid2])>d.dot(centres[cid1]):
                    direction[eid1] = SW
                    #                direction[eid2] = RW
                else:
                    direction[eid1] = RW
#                direction[eid2] = SW
        elif (cell_type[cid1] in (COR, END, PERI) and 
              cell_type[cid2] in (QC, LRC, COL)):
            direction[eid1] = RW
        elif (cell_type[cid2] in (COR, END, PERI) and 
              cell_type[cid1] in (QC, LRC, COL)):
            direction[eid1] = SW
        elif cell_type[cid1] == EPI and cell_type[cid2] == COL:
            direction[eid1] = RW
        elif cell_type[cid2] == EPI and cell_type[cid1] == COL:
            direction[eid1] = RW
        else:
            # Wall between two cells of diferent types
            if cell_type[cid1] in ct_list and cell_type[cid2] in ct_list:
                if ct_list.index(cell_type[cid1])<ct_list.index(cell_type[cid2]):
                    direction[eid1] = INNER
                else:
                    direction[eid1] = OUTER
            else:
                direction[eid1] = None

    # Find last lateral root cap cells
    

    cid_x_upper = -10000
    cid_x_lower = -10000
    for cid in cell_type:
        if cell_type[cid]==LRC:
            x = d.dot(centres[cid])
            if n.dot(centres[cid])>cy and x>cid_x_upper:
                cid_upper = cid
                cid_x_upper = x
            if n.dot(centres[cid])<cy and x>cid_x_lower:
                cid_lower = cid
                cid_x_lower = x

    for eid in graph.out_edges(cid_upper):
        tid = graph.target(eid)
        if cell_type[tid]==EPI and d.dot(centres[tid]) > d.dot(centres[cid_upper]):
            direction[eid]=SW
    for eid in graph.out_edges(cid_lower):
        tid = graph.target(eid)
        if cell_type[tid]==EPI and d.dot(centres[tid]) > centres[cid_lower][0]:
            direction[eid]=SW

        
        
                
                
    PIN1_proteins=True
    PIN2_proteins=True
    PIN3_proteins=True
    PIN4_proteins=True
    PIN7_proteins=True
    AUX1_proteins=True

    if PIN1_proteins:
        for cell in cids:
            if cell_type[cell]==QC:
                for eid in graph.out_edges(cell):
                    PIN[eid]=1
            if cell_type[cell]==VASC:
                for eid in graph.out_edges(cell):
                    if direction[eid]==RW:
                        PIN[eid]=1
                    else:
                        PIN[eid]=0
            if cell_type[cell]==PERI:
                for eid in graph.out_edges(cell):
                    if direction[eid]==RW:
                        PIN[eid]=1
            if cell_type[cell]==END:
                for eid in graph.out_edges(cell):
                    if direction[eid]==RW:
                        PIN[eid]=1

    if PIN2_proteins:
        for cell in cids:
            if cell_type[cell] in LRC_types:
                for eid in graph.out_edges(cell):
                    if direction[eid]==SW:
                        PIN[eid]=1
            if cell_type[cell]==EPI and zone[cell]!=PM:
                    for eid in graph.out_edges(cell):
                        if direction[eid]==SW:
                            PIN[eid]=1 #if centres[cell][1]>cy else 1.0
            if cell_type[cell]==COR:
                if zone[cell]==DM:
                    for eid in graph.out_edges(cell):
                        if direction[eid]==RW:
                            PIN[eid]=1
                        if zone[graph.target(eid)]==EZ and direction[eid]==SW:
                            PIN[eid]=1
                if zone[cell]==EZ:
                    for eid in graph.out_edges(cell):
                        if direction[eid]==SW:
                            PIN[eid]=1
                        if zone[graph.target(eid)]==DM and direction[eid]==RW:
                            PIN[eid]=1

    if PIN3_proteins:
        for cell in cids:
            if cell_type[cell] in (QC, COL):
                for eid in graph.out_edges(cell):
                    PIN[eid]=1
            if cell_type[cell] in (VASC, END, PERI):
                for eid in graph.out_edges(cell):
                    if direction[eid]==RW:
                        PIN[eid]=1
            if cell_type[cell]==END:
                for eid in graph.out_edges(cell):
                    if cell_type[graph.target(eid)]==PERI:
                        PIN[eid]=1
    
    if PIN4_proteins:
        for cell in cids:
            if cell_type[cell]in (QC, COL):
                for eid in graph.out_edges(cell):
                    PIN[eid]=1
            if cell_type[cell] in (COR, END, PERI, VASC):
                if zone[cell]!=EZ:
                    for eid in graph.out_edges(cell):
                        if direction[eid]==RW:
                            PIN[eid]=1

    if PIN7_proteins:
        for cell in cids:
            if cell_type[cell]in (QC, COL):
                for eid in graph.out_edges(cell):
                    PIN[eid]=1
            if cell_type[cell] in (PERI, VASC):
                for eid in graph.out_edges(cell):
                    if direction[eid]==RW:
                            PIN[eid]=1
            if cell_type[cell]in (EPI, COR, END) and zone[cell]==PM:
                for eid in graph.out_edges(cell):
                    if direction[eid]==RW:
                        PIN[eid]=1

    if AUX1_proteins:
        for cell in cids:
            if cell_type[cell] in (LRC, QC, COL):
                for eid in graph.out_edges(cell):
                    AUX1[eid]=1
            if cell_type[cell]==EPI and zone[cell]==EZ:
                for eid in graph.out_edges(cell):
                    AUX1[eid]=1
               


