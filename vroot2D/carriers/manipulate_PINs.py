from vroot2D.utils.db_utilities import get_mesh, get_graph, get_parameters, set_parameters, get_wall_decomp, add_property
from numpy import *
from pylab import *

#from plotfigures import plotPINs, plotAUX1

def manipulate_PINs(db, dictParams=[]):

    graph = get_graph(db) 
    mesh = get_mesh(db)    
    PIN = db.get_property('PIN')
    AUX1 = db.get_property('AUX')
    if 'LAX' in db.properties():
        LAX = db.get_property('LAX')
    else:
        LAX = {}
        add_property(db, 'LAX', 'edge', 0.0)
        
    wall_decomposition = get_wall_decomp(db)
	
    return True





