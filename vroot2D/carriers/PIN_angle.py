
from vroot2D.utils.db_utilities import get_mesh, get_graph, get_wall_decomp
from vroot2D.utils.db_geom import ordered_wids_pids
import numpy as np
from math import sqrt, sin, cos

# Change localization of PINs on columella
# cells according to the angle made the walls
# with the gravity vector
def rot(v):
    """                                                                         
    rotate a 2d vector (numpy) by 90 degrees anticlockwise                    
    :param v: input vector                                                      
    :type v: Vector2/list/tuple                                                 
    :returns: Vector2 rotated v                                                 
    """
    return np.array((-v[1],v[0]))

class PINAngle(object):
    def __init__(self, db, gv=(0,-1)):
        self.db = db
        self.gv = np.array(gv)
    
    def step(self):
        db = self.db
        pos = db.get_property('position')
        mesh = get_mesh(db)
        graph = get_graph(db)
        cell_type = db.get_property('cell_type')

        col_cells = [cid for cid, ct in cell_type.iteritems() if ct==7]
        wall_type = db.get_property('wall_types')
        PIN = db.get_property('PIN')
        w_d = get_wall_decomp(db)
        for cid in col_cells:
            wids, pids = ordered_wids_pids(mesh, pos, cid)
            dp = []
            for i, wid in enumerate(wids):
                pid1 = pids[i]
                pid2 = pids[(i+1)%len(pids)]
                x = pos[pid2]-pos[pid1]
                dp.append(np.dot(-rot(x), self.gv)/sqrt(np.dot(x,x)))
            down_wt = wall_type[(cid, wids[dp.index(max(dp))])]
            if down_wt == 1:
                # The statoliths are on the bottom of the columellar cells
                # PINs on all edges
                for wid in wids:
                    try:
                        eid1, eid2 = w_d[wid]
                    except KeyError:
                        continue
                    if graph.source(eid1)==cid:
                        PIN[eid1]=1.0
                    else:
                        PIN[eid2]=1.0
            else:
                # Apply PINs to all walls of the same type.
                for wid in wids:
                    if wall_type[(cid, wid)]==down_wt:
                        try:
                            eid1, eid2 = w_d[wid]
                        except KeyError:
                            continue
                        if graph.source(eid1)==cid:
                            PIN[eid1]=1.0
                        else:
                            PIN[eid2]=1.0
                    else:
                        try:
                            eid1, eid2 = w_d[wid]
                        except KeyError:
                            continue
                        if graph.source(eid1)==cid:
                            PIN[eid1]=0.0
                        else:
                            PIN[eid2]=0.0 
        
        
class PINAngle_fullroot(object):
    def __init__(self, db, gv=(0,-1)):
        self.db = db
        self.gv = np.array(gv)
    
    def step(self, angle=0.0):
        self.gv = np.array((sin(angle), -cos(angle)))
        db = self.db
        pos = db.get_property('position')
        mesh = get_mesh(db)
        graph = get_graph(db)
        cell_type = db.get_property('cell_type')

        col_cells = [cid for cid, ct in cell_type.iteritems() if ct in (11,12,13)]
        wall_type = db.get_property('wall_types')
        PIN = db.get_property('PIN')
        w_d = get_wall_decomp(db)
        for cid in col_cells:
            wids, pids = ordered_wids_pids(mesh, pos, cid)
            dp = []
            for i, wid in enumerate(wids):
                pid1 = pids[i]
                pid2 = pids[(i+1)%len(pids)]
                x = pos[pid2]-pos[pid1]
                dp.append(np.dot(-rot(x), self.gv)/sqrt(np.dot(x,x)))
            down_wt = wall_type[(cid, wids[dp.index(max(dp))])]
            print 'down_wt', down_wt
            if down_wt == 1:
                # The statoliths are on the bottom of the columellar cells
                # PINs on all edges
                for wid in wids:
                    try:
                        eid1, eid2 = w_d[wid]
                    except KeyError:
                        continue
                    if graph.source(eid1)==cid:
                        PIN[eid1]=1.0
                    else:
                        PIN[eid2]=1.0
            else:
                # Apply PINs to all walls of the same type.
                for wid in wids:
                    if wall_type[(cid, wid)]==down_wt:
                        try:
                            eid1, eid2 = w_d[wid]
                        except KeyError:
                            continue
                        if graph.source(eid1)==cid:
                            PIN[eid1]=1.0
                        else:
                            PIN[eid2]=1.0 #1.0
                    else:
                        try:
                            eid1, eid2 = w_d[wid]
                        except KeyError:
                            continue
                        if graph.source(eid1)==cid:
                            PIN[eid1]=0.0
                        else:
                            PIN[eid2]=0.0 
        
