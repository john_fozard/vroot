

import sys
import random

def main():
    #Define/open tissue
    random.seed(1)

    from vroot2D.geometry.import_tissue import import_tissue, divide_long_cells
    db=import_tissue('data/fullroot_longer.zip', scale=10.0)
    divide_long_cells(db, 100)
    from vroot2D.carriers.PIN_assign import PIN_assign
    PIN_assign(db)

    # Initialize scheduler
    from openalea.scheduler import Scheduler,Task
    sch = Scheduler()

###########################################################################


    from parameters_fullroot import setSimParameters
    setSimParameters(db)



    from vroot2D.genenetworks.diffuse_ss import  AuxinSS

    ssa = AuxinSS(db)
    sch.register(Task(ssa.step, 1, 9, 'Auxin'))
    ssa.step()

    from vroot2D.carriers.PIN_angle import PINAngle
    pa = PINAngle(db)
    sch.register(Task(pa.step, 1, 12, "PIN Angle") )
    pa.step()


    # Time update
    db.set_property('time', 0)
    db.set_description('time', 'current simulation time')
    from vroot2D.utils.db_utilities import get_parameters

    def update_time():
        t=db.get_property('time')
        t+=get_parameters(db, 'timestep')
        db.set_property('time', t)
    sch.register(Task(update_time, 1, 2, "update time"))

    from vroot2D.growth.growth_midline import GrowthMidline
    g = GrowthMidline(db)
    sch.register(Task(g.step, 1, 6, "growth"))

    from vroot2D.growth.division import MeshDivision
    from vroot2D.growth.division_rule import MidlineDivisionRule

    md = MeshDivision(db, MidlineDivisionRule())
    sch.register(Task(md.step, 1, 7, "division"))


    from vroot2D.model_output.svg_plot import SVGPlotter
    SVGPlotter(db, prop='auxin', plot_midline=True, log_prop=False).draw('/tmp/init.svg')
    spl = SVGPlotter(db, fn_base='/tmp/bend')

    from vroot2D.model_output.mpl_plotter import MPLPlotter
    mpl = MPLPlotter([db], 'auxin', limits=((-2000,1500),(-2500,500)),
                    format='png', plot_midline=True,
                     with_inset=True, fs=(30, 20))
    sch.register(Task(mpl.redraw, 1, 2, "redraw"))
    
    for i in range(100):
        print i
        next(sch.run())
        print db.get_property('auxin')

main()
