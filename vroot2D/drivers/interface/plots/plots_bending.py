from matplotlib.ticker import MaxNLocator

import matplotlib.pylab as plt
from scipy.linalg import eig, solve
import numpy as np
import sys
from math import pi

#from matplotlib import rc

#rc('text', usetex=True)

from matplotlib import rc

params = {
          'font.family': 'serif',
          'font.serif' : ['Times New Roman', 'Nimbus Roman No9 L', 'Times', 'Bitstream Vera Serif'],
          'axes.labelsize': 18,
          'text.fontsize': 18,
          'legend.fontsize': 20,
          'legend.frameon': False,
          'legend.linewidth': 0,
          'xtick.labelsize': 18,
          'ytick.labelsize': 19}

linecols = ['#b22222', '#4682b4']
files = ['angle/midline0', 'angle/midline1']
labels = ['transverse', 'angle']
plt.rcParams.update(params)
plt.figure(figsize=(8,6))
plt.axes([0.15,0.1,0.8,0.8])
plt.hold(True)
x0 = 3
for i,fn in enumerate(files):
    data = np.loadtxt(fn)
    plt.plot(data[:,0], data[:,1], linecols[i], label=labels[i], linewidth=1.5)
    if i==0:
        y0 = np.interp(x0, data[:,0], data[:,1])
        plt.plot(x0, y0, linecols[i], marker='x', markersize=15, markeredgewidth=3)
        plt.text(x0+0.15, y0-50, '(B)', fontsize=18)
    elif i==1:
        y0 = np.interp(x0, data[:,0], data[:,1])
        plt.plot(x0, y0, linecols[i], marker='+', markeredgewidth=3, markersize=15)
        plt.text(x0+0.05, y0-200, '(C)', fontsize=18)
plt.legend(loc=2).draw_frame(False)
plt.xlim((0,5))
plt.ylim((0,4000))
plt.xlabel('t / hr')
plt.ylabel('L / $\mu$m')
plt.savefig('fibre_len_2.eps')
files = ['angle/fibres0', 'angle/fibres1']
#labels = ['(b)', '(c)']
plt.figure(figsize=(8,6))
plt.axes([0.15,0.1,0.8,0.8])
plt.hold(True)
for i,fn in enumerate(files):
    data = np.loadtxt(fn)
    plt.plot(data[:,0], 0.5*(data[:,1]-data[:,2]), linecols[i], label=labels[i], linewidth=1.5)
    if i==0:
        y0 = np.interp(x0, data[:,0], data[:,1])
        plt.plot(x0, y0, linecols[i], marker='x', markersize=15, markeredgewidth=3)
        plt.text(x0+0.05, y0+0.03, '(B)', fontsize=18)
    elif i==1:
        y0 = np.interp(x0, data[:,0], data[:,1])
        plt.plot(x0, y0, linecols[i], marker='+', markeredgewidth=3, markersize=15)
        plt.text(x0+0.05, y0-0.04, '(C)', fontsize=18)
plt.legend(loc=2).draw_frame(False)

plt.xlim((0,5))
plt.ylim((-0.1,1.0))
plt.xlabel('t / hr')
plt.ylabel('$\phi$ / radians')
#plt.legend().draw_frame(False)
plt.savefig('fibre_angles_2.eps')
#plt.show()


