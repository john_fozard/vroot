
import matplotlib
from matplotlib.ticker import MaxNLocator

import matplotlib.pylab as plt
from scipy.linalg import eig, solve
import numpy as np
import sys
from math import pi

import seaborn as sbs

#from matplotlib import rc

#rc('text', usetex=True)

from matplotlib import rcParams

params = {'font.family': 'sans',
          'axes.labelsize': 45,
          'text.fontsize': 40,
          'legend.fontsize': 45,
          'legend.frameon': False,
          'legend.linewidth': 0,
          'xtick.labelsize': 40,
          'ytick.labelsize': 40}

rcParams.update(params)


linecols = ['#b22222', '#4682b4', '#22b222','#000000']

linestyles = ['-', '-', '-', '-']
markers = [None, None, 'o', 'D']

hybrid_files = ['hmidline1']
midline_files = ['midline1']
path = 'output/interface/curved_simulations/'

from helper import read_hybrid

hdata = [read_hybrid(path+fn) for fn in hybrid_files]
mdata = [np.loadtxt(path+fn) for fn in midline_files]

hlength0 = hdata[0][3]

times = mdata[0][:,0]

kappa0 = hdata[0][0]
l0 = hdata[0][1]
mean_kappa = [ np.mean(np.array(k)*np.array(l))/np.mean(l) for k, l in zip(kappa0, l0) ]

plt.figure(figsize=(8,6))
ax = plt.axes([0.2,0.1,0.75,0.75])
plt.plot(times, hlength0)
#plt.hold(True)
#plt.plot(times, mdata[0][:,1])
ax.xaxis.set_major_locator(MaxNLocator(nbins=1))
ax.yaxis.set_major_locator(MaxNLocator(nbins=3))
ax.tick_params(axis='x', pad=15)
plt.ylabel('L ($\mu$m)')
plt.xlabel('t (hr)')
plt.ylim([0,1500])
plt.savefig(path+'lengths.svg')

plt.figure(figsize=(8,6))
ax = plt.axes([0.2,0.1,0.75,0.75])
plt.plot(times, np.array(hdata[0][2])*(180./pi))

print 'angles', np.array(hdata[0][2])*(180./pi)
#plt.hold(True)
#plt.plot(times, mdata[0][:,2])
ax.xaxis.set_major_locator(MaxNLocator(nbins=1))
ax.yaxis.set_major_locator(MaxNLocator(2))
ax.tick_params(axis='x', pad=15)
plt.ylabel('tip angle ($^\circ$)')
plt.xlabel('t (hr)')
plt.savefig(path+'angles.svg')

plt.figure(figsize=(8,6))
ax = plt.axes([0.2,0.1,0.75,0.75])
plt.plot(times, mean_kappa)
ax.xaxis.set_major_locator(MaxNLocator(nbins=1))
ax.yaxis.set_major_locator(MaxNLocator(nbins=1))
ax.tick_params(axis='x', pad=15)
y_formatter = matplotlib.ticker.FormatStrFormatter('%.3f')
ax.yaxis.set_major_formatter(y_formatter)
plt.ylim([0, 0.004])
plt.ylabel('$\kappa$ ($\mu$ m$^{-1}$)')
plt.xlabel('t (hr)')
plt.savefig(path+'mean_kappa.svg')

