
import numpy as np

def read_hybrid(filename):
    all_lengths = []
    all_kappa = []
    with open(filename, 'r') as f:
        lengths = []
        kappa = []
        for l in f:
            if len(l.strip())==0:
                all_lengths.append(lengths)
                all_kappa.append(kappa)
                lengths = []
                kappa = []
            else:
                k, l = map(float, l.split()[0:2])
                lengths.append(l)
                kappa.append(k)
    if lengths:
        all_lengths.append(lengths)
        all_kappa.append(kappa)
    total_lengths = [np.sum(l) for l in all_lengths]
    total_angle = [np.sum(np.array(l)*np.array(k)) for l,k in zip(all_lengths, all_kappa)]
    return all_kappa, all_lengths, total_angle, total_lengths
                
        
