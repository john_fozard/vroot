from matplotlib.ticker import MaxNLocator

import matplotlib.pylab as plt
from scipy.linalg import eig, solve
import numpy as np
import sys
from math import pi

import seaborn as sbs

#from matplotlib import rc

#rc('text', usetex=True)

from matplotlib import rcParams

params = {'font.family': 'sans',
          'axes.labelsize': 50,
          'text.fontsize': 40,
          'legend.fontsize': 45,
          'legend.frameon': False,
          'legend.linewidth': 0,
          'xtick.labelsize': 40,
          'ytick.labelsize': 40}

rcParams.update(params)


linecols = ['#b22222', '#4682b4', '#22b222','#000000']

linestyles = ['-', '-', '-', '-']
markers = [None, None, 'o', 'D']

hybrid_files = ['hmidline0', 'hmidline1']
midline_files = ['midline0', 'midline1']
path = 'output/interface/bending_simulations/'
vertex_path = 'output/compare_bend/'

from helper import read_hybrid

hdata = [read_hybrid(path+fn) for fn in hybrid_files]
mdata = [np.loadtxt(path+fn) for fn in midline_files]
vdata = [np.loadtxt(vertex_path+fn) for fn in midline_files]

hlength0 = hdata[0][3]
hlength1 = hdata[1][3]

times = mdata[0][:,0]



plt.figure(figsize=(8,6))
ax = plt.axes([0.2,0.1,0.75,0.75])
#plt.plot(times, hlength0,'r:')
plt.hold(True)
plt.plot(times, mdata[1][:,1],'k-D', markevery=20, markersize=15)
plt.plot(times, mdata[0][:,1],'r-')
#plt.plot(times, hlength1,'b--')

ax.xaxis.set_major_locator(MaxNLocator(nbins=1))
ax.yaxis.set_major_locator(MaxNLocator(nbins=3))
ax.tick_params(axis='x', pad=15)
plt.ylabel('L ($\mu$m)')
plt.xlabel('t (hr)')
plt.savefig(path+'lengths.svg')


plt.figure(figsize=(8,6))
ax = plt.axes([0.2,0.1,0.75,0.75])
#plt.plot(times, np.array(hdata[0][2])*(180.0/pi),'r:')
plt.hold(True)
plt.plot(times, mdata[1][:,2]*(180/pi), 'k-D', markevery=20, markersize=15)

plt.plot(times, mdata[0][:,2]*(180/pi), 'r-')
#plt.plot(times, np.array(hdata[1][2])*(180.0/pi),'b--')
ax.xaxis.set_major_locator(MaxNLocator(nbins=1))
ax.yaxis.set_major_locator(MaxNLocator(2))
ax.tick_params(axis='x', pad=15)
plt.ylabel('tip angle ($^\circ$)')
plt.xlabel('t (hr)')
plt.savefig(path+'angles.svg')

plt.figure(figsize=(8,6))
ax = plt.axes([0.2,0.1,0.75,0.75])
#plt.plot(times, hlength0,'r:')
plt.hold(True)
plt.plot(times, vdata[0][:,1],'k-D', markevery=20, markersize=15)
plt.plot(times, mdata[0][:,1],'r-')
#plt.plot(times, hlength1,'b--')

ax.xaxis.set_major_locator(MaxNLocator(nbins=1))
ax.yaxis.set_major_locator(MaxNLocator(nbins=3))
ax.tick_params(axis='x', pad=15)
plt.ylabel('L ($\mu$m)')
plt.xlabel('t (hr)')
plt.savefig(path+'compare_lengths.svg')


plt.figure(figsize=(8,6))
ax = plt.axes([0.2,0.1,0.75,0.75])
#plt.plot(times, np.array(hdata[0][2])*(180.0/pi),'r:')
plt.hold(True)
plt.plot(times, vdata[0][:,2]*(180/pi), 'k-D', markevery=20, markersize=15)

plt.plot(times, mdata[0][:,2]*(180/pi), 'r-')
#plt.plot(times, np.array(hdata[1][2])*(180.0/pi),'b--')
ax.xaxis.set_major_locator(MaxNLocator(nbins=1))
ax.yaxis.set_major_locator(MaxNLocator(2))
ax.tick_params(axis='x', pad=15)
plt.ylabel('tip angle ($^\circ$)')
plt.xlabel('t (hr)')
plt.savefig(path+'compare_angles.svg')

