import matplotlib.pylab as plt
from scipy.linalg import eig, solve
import numpy as np
import sys
from math import pi

#from matplotlib import rc

#rc('text', usetex=True)

from matplotlib import rcParams

params = {'font.family': 'sans',
          'axes.labelsize': 25,
          'text.fontsize': 20,
          'legend.fontsize': 25,
          'legend.frameon': False,
          'legend.linewidth': 0,
          'xtick.labelsize': 20,
          'ytick.labelsize': 20}

rcParams.update(params)


linecols = ['#b22222', '#4682b4', '#22b222','#000000']

linestyles = ['-', '-', '-', '-']
markers = [None, None, 'o', 'D']

hybrid_files = ['hmidline0']
midline_files = ['midline0']
path = 'output/interface/transition_division/'

from helper import read_hybrid

hdata = [read_hybrid(path+fn) for fn in hybrid_files]
mdata = [np.loadtxt(path+fn) for fn in midline_files]

hlength0 = hdata[0][3]

times = mdata[0][:,0]

kappa0 = hdata[0][0]
l0 = hdata[0][1]
mean_kappa = [ np.mean(np.array(k)*np.array(l))/np.mean(l) for k, l in zip(kappa0, l0) ]

plt.figure()
#plt.plot(times, hlength0)
#plt.hold(True)
plt.plot(times, mdata[0][:,1])

plt.figure()
plt.plot(times, np.array(hdata[0][2])*(180./pi))
#plt.hold(True)
#plt.plot(times, mdata[0][:,2])

plt.figure()
plt.plot(times, mean_kappa)

plt.show()
