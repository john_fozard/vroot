

import sys
import random

import matplotlib
matplotlib.use("Agg")

import numpy as np
from vroot2D.utils.db_utilities import get_mesh, get_graph
from vroot2D.utils.db_geom import centroid

def classify_cells(db):
    # Classify cells as upper and lower
    # Ads new property to tissue - side. 
    # side[cid] = {1 = lower, 2 = upper, 3 = "middle" }

    # First locate the QC cells.

    pos = db.get_property('position')
    mesh = get_mesh(db)
    graph = get_graph(db)
    cell_type = db.get_property('cell_type')
    QC_cells = [cid for cid, ct in cell_type.iteritems() if ct==17]
    mid_QC = np.mean([centroid(mesh, pos, 2, cid) for cid in QC_cells], axis=0)
    y_QC = mid_QC[1]
    side = {}
    for cid in mesh.wisps(2):
        y = centroid(mesh, pos, 2, cid)[1]
        if y > y_QC:
            side[cid]=1
        else:
            side[cid]=2
    LRC_cells = [cid for cid, ct in cell_type.iteritems() if ct in (6,7,8,9)]
    minx_LRC = min(centroid(mesh, pos, 2, cid)[0] for cid in LRC_cells)
    meristem = {}
    for cid in mesh.wisps(2):
        x = centroid(mesh, pos, 2, cid)[0]
        if x < minx_LRC:
            meristem[cid] = 0
        else:
            meristem[cid] = 1

    db.set_property('side', side)
    db.set_description('side', '')
    db.set_property('meristem', meristem)
    db.set_description('meristem', '')



def main():

    random.seed(1)

    output_path = 'output/interface/fullroot_check'

    from vroot2D.geometry.import_tissue import import_tissue, divide_long_cells, extend_tissue
    db=import_tissue('data/R12data_wsn_6pix_OA_fl_wceinit_wAUX1PINLAX_wc_wxauto_wdata_scaled_newvasc_adj.zip')


#    pos = db.get_property('position')
#    print pos[2177]
#    pos[2177][0] -= 5.0

    new_cids = extend_tissue(db, 700)

    classify_cells(db)

    from vroot2D.carriers.PIN_assign import PIN_assign_subset
    PIN_assign_subset(db, new_cids)

    from vroot2D.model_output.svg_plot import SVGPlotter
    SVGPlotter(db).draw(output_path+'/init0.svg')

    # Initialize scheduler
    from openalea.scheduler import Scheduler,Task
    sch = Scheduler()

###########################################################################

    from parameters_grav import setSimParameters
    setSimParameters(db)

    from vroot2D.genenetworks.genenetwork import GeneNetworkJPat, GeneNetwork,\
                                               CombinedModel

    from vroot2D.genenetworks.genenetwork_codegen import \
        CGGeneNetwork, CodeGenModel, AuxinResponseModel, AuxinHSModel, \
        CodeGenTransportModel, CGTransportGeneNetwork,  CGat_cwd, \
        CombinedCGModel

    from vroot2D.genenetworks.AuxinTransportModels import \
        at_cwd, DIIq, Response

    gn = CGTransportGeneNetwork(db, CodeGenTransportModel(CombinedCGModel([AuxinHSModel(), AuxinResponseModel()]), CGat_cwd()))

    from vroot2D.utils.db_utilities import get_parameters   
    from vroot2D.genenetworks.diffuse_ss import acwdSS

    gnSS = acwdSS(db)
    sch.register(Task(gn.step, 1, 6, "auxin"))

    from vroot2D.carriers.manipulate import manipulate

    manipulate(db)

    from vroot2D.carriers.PIN_angle4 import PINAngle_fullroot
    pa = PINAngle_fullroot(db)
    sch.register(Task(pa.step, 1, 12, "PIN Angle") )
    pa.gv=(1,0)
#    pa.step() 
    pa.apolar()
    pa.gv=(0,-1)
    #pa.step()

    db.set_property('time', 0)
    db.set_description('time', 'current simulation time')


    def update_time():
        t=db.get_property('time')
        t+=get_parameters(db, 'timestep')
        db.set_property('time', t)
    sch.register(Task(update_time, 1, 2, "update time"))

    from vroot2D.growth.growth_midline import GrowthMidline

    g = GrowthMidline(db, LR=True)
    sch.register(Task(g.step, 1, 6, "growth"))

    from vroot2D.model_output.measure_midline import MeasureMidline
    mm = MeasureMidline(db, fn=output_path+'/midline')
    sch.register(Task(mm.step, 1, 3, "midline"))

    from vroot2D.growth.division import MeshDivision
    from vroot2D.growth.division_rule import MidlineDivisionRule

    SVGPlotter(db, prop='cell_type', plot_midline=True, log_prop=False, plot_PINAUX=True, plot_tris=True).draw('/tmp/init_tris.svg')
  
    from vroot2D.model_output.mpl_plotter import MPLPlotter
    mpl = MPLPlotter([db], 'auxin', limits=((-2000,1500),(-2500,500)),
                    format='png', fn_base=output_path+'/auxin',
                     with_inset=False, plot_midline=True)
    sch.register(Task(mpl.redraw, 1, 2, "redraw"))

    mpl2 = MPLPlotter([db], 'response', limits=((-2000,1500),(-2500,500)),
                    format='png', fn_base='/tmp/response',
                      with_inset=False, plot_midline=True)

    sch.register(Task(mpl2.redraw, 1, 2, "redraw"))
    
    gn.step_dt(100000.0) 
    mpl.redraw()
    mpl2.redraw()


    SVGPlotter(db, prop='auxin', plot_midline=True, log_prop=False, plot_PINAUX=True, plot_tris=False).draw(output_path+'/init_auxin.svg')


    print db.get_property('divided_props')

    mIAA = db.get_property('mIAA')

    IAA = db.get_property('IAA')
    R = db.get_property('response')
    
    print max(mIAA.itervalues()), min(mIAA.itervalues())
    print max(IAA.itervalues()), min(IAA.itervalues())
    print max(R.itervalues()), min(R.itervalues())

    for p in db.properties():
        db.set_description(p, '')

    db.write(output_path+'/start.zip')

    pa.step()

    for i in range(120):
        print i, db.get_property('time')

        if i%10==0:
            SVGPlotter(db, prop='auxin', plot_midline=True, log_prop=False, plot_PINAUX=True, plot_tris=False).draw(output_path+'/auxin_%03d.svg'%i)
            db.write(output_path+'/state_%03d.zip'%i)
	
        next(sch.run())


    SVGPlotter(db, prop='auxin', plot_midline=True, log_prop=False, plot_PINAUX=True, plot_tris=False).draw(output_path+'/end_auxin.svg')


main()
