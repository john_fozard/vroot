
import matplotlib
matplotlib.use("Agg")


import sys, os, errno
import copy 

import copy 
from vroot2D.utils.db_utilities import get_parameters
from vroot2D.utils.db_geom import updateVS

def main():

    from vroot2D.geometry.cell_array import cell_array_2D, staggered_array_2D
    db1 = staggered_array_2D(True, seed=10)

    db2 = copy.deepcopy(db1)

    output_path = 'output/interface/transition_check'


    from vroot2D.model_output.svg_plot import SVGPlotter
    SVGPlotter(db1, color='lightgray').draw(output_path+'/init0.svg')

    from openalea.scheduler import Scheduler,Task
    sch = Scheduler()

###########################################################################




    # Growth task

    from vroot2D.growth.growth_midline import GrowthMidline, MidlineSyntheticDistanceGrowthModel

    g = GrowthMidline(db1, {'relaxation_time':2.0/60.0}, MidlineSyntheticDistanceGrowthModel())
    p = get_parameters(db1, g.model.name)
    print 'g params', p
    #p['bend'] = p['mu1']
    # Change g2 parameters?
    sch.register(Task(g.step, 1, 8, "growth"))

#    x = db1.get_property('x0')
#    x[1] += 40
#    db1.set_property('theta0', 3.14+0.2)
#    g.calculate_ml_coords()

    
    from vroot2D.model_output.measure_midline import MeasureMidline
    mm = MeasureMidline(db1, fn=output_path+'/midline0')
    sch.register(Task(mm.step, 1, 10, "midline"))


    from vroot2D.model_output.write_hybrid_midline import WriteHybridMidline
    wm = WriteHybridMidline(db1, fn=output_path+'/hmidline0')
    sch.register(Task(wm.step, 1, 10, "hmidline"))



    from vroot2D.model_output.mpl_simple import MPLSimple
    mpl = MPLSimple([db1], 'cell_type', limits=((-1000, 450), (-1200, 150)), 
                    offset_y = 250, 
                    fn_base=output_path+'/bend', format='png',
                    colours={0:(1.0, 0.8, 0.8), 1:(0.8, 1.0, 0.8), 2: (0.8, 0.8, 1.0)},
                    plot_midline=True)
    sch.register(Task(mpl.redraw, 1, 9, "redraw"))

    updateVS(db1)
    SVGPlotter(db1, color='rgb(179,179,255)', plot_midline=True).draw(output_path+'/init0.svg')

    SVGPlotter(db1, prop='V', plot_midline=True, prop_min=0, prop_max=11000).draw(output_path+'/init_area0.svg')

    dt = get_parameters(db1, 'MidlineSyntheticDistanceGrowthModel')['relaxation_time']
    for i in range(200):
        db1.set_property('time', i*dt)
        db2.set_property('time', i*dt)
        next(sch.run())
        if (i+1)%5==0:
            updateVS(db1)
            SVGPlotter(db1, ct_colors={0:'rgb(179,179,255)', 1:'rgb(179,179,255)'}, plot_tris=True).draw(output_path+'/tri0_%03d.svg'%i)
            SVGPlotter(db1, ct_colors={0:'rgb(179,179,255)', 1:'rgb(179,179,255)'}, plot_midline=True).draw(output_path+'/plot0_%03d.svg'%i)
            SVGPlotter(db1, prop='V', plot_midline=True, prop_min=0, prop_max=11000).draw(output_path+'/area0_%03d.svg'%i)
            SVGPlotter(db1, prop='V', plot_midline=True).draw(output_path+'/areau_%03d.svg'%i)
            SVGPlotter(db1, prop='V', plot_midline=True, sub_graph=True, sub_graph_prop='V').draw(output_path+'/areag_%03d.svg'%i)


    SVGPlotter(db1, color='rgb(179,179,255)', plot_midline=True).draw(output_path+'/end0.svg')



main()
import matplotlib.pylab as plt
plt.show()
