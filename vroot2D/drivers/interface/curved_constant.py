
import matplotlib
matplotlib.use("Agg")


import sys, os, errno
import copy 
import numpy as np

import copy 
from vroot2D.utils.db_utilities import get_parameters

def main():

    from vroot2D.geometry.cell_array import cell_array_2D, staggered_array_2D
    db1 = staggered_array_2D(True, seed=10)

    db2 = copy.deepcopy(db1)

    output_path = 'output/interface/curved_simulations_check'


    from vroot2D.model_output.svg_plot import SVGPlotter
    SVGPlotter(db1, color='lightgray').draw(output_path+'/init0.svg')

    from openalea.scheduler import Scheduler,Task
    sch = Scheduler()

###########################################################################




    # Growth task

    from vroot2D.growth.growth_midline import GrowthMidline, MidlineSyntheticGrowthModel, apply_bend

    g = GrowthMidline(db1, {'relaxation_time':0.005, 'bend':0.15}, MidlineSyntheticGrowthModel())
    p = get_parameters(db1, g.model.name)
    print 'g params', p
    #p['bend'] = p['mu1']
    # Change g2 parameters?
    kappa = db1.get_property('kappa')

    x = db1.get_property('x0')
    x[1] += 40
#    db1.set_property('theta0', 3.14+0.2)
    g.calculate_ml_coords()

    apply_bend(db1, 0.001*np.ones(len(kappa),))

    sch.register(Task(g.step, 1, 8, "growth"))




    g2 = GrowthMidline(db2, {'relaxation_time':0.005, 'bend':0.15}, MidlineSyntheticGrowthModel())
    p = get_parameters(db2, g2.model.name)
    #p['bend'] = p['mu1']
    # Change g2 parameters?

    kappa = db2.get_property('kappa')
    apply_bend(db2, 0.002*np.ones(len(kappa),))

    sch.register(Task(g2.step, 1, 8, "growth"))

    
    from vroot2D.model_output.measure_midline import MeasureMidline
    mm = MeasureMidline(db1, fn=output_path+'/midline0')
    sch.register(Task(mm.step, 1, 10, "midline"))

    mm2 = MeasureMidline(db2, fn=output_path+'/midline1')
    sch.register(Task(mm2.step, 1, 10, "midline"))

    from vroot2D.model_output.write_hybrid_midline import WriteHybridMidline
    wm = WriteHybridMidline(db1, fn=output_path+'/hmidline0')
    sch.register(Task(wm.step, 1, 10, "hmidline"))

    from vroot2D.model_output.write_hybrid_midline import WriteHybridMidline
    wm2 = WriteHybridMidline(db2, fn=output_path+'/hmidline1')
    sch.register(Task(wm2.step, 1, 10, "hmidline"))


    SVGPlotter(db1, color='rgb(179,179,255)', plot_midline=True).draw(output_path+'/init0.svg')
    SVGPlotter(db2, color='rgb(179,179,255)', plot_midline=True).draw(output_path+'/init1.svg')


    from vroot2D.model_output.mpl_simple import MPLSimple
    mpl = MPLSimple([db1, db2], 'cell_type', limits=((-1000, 450), (-1200, 150)), 
                    offset_y = 250, 
                    fn_base=output_path+'/bend', format='png',
                    plot_midline=True,
                    colours={0:(1.0, 0.8, 0.8), 1:(0.8, 1.0, 0.8), 2: (0.8, 0.8, 1.0)}
                )
    sch.register(Task(mpl.redraw, 1, 9, "redraw"))

    dt = get_parameters(db1, 'MidlineSyntheticGrowthModel')['relaxation_time']
    for i in range(200):
        db1.set_property('time', i*dt)
        db2.set_property('time', i*dt)
        next(sch.run())
        if (i+1)%5==0:
            SVGPlotter(db1, ct_colors={0:'rgb(179,179,255)', 1:'rgb(179,179,255)'}, plot_midline=True).draw(output_path+'/mid0_%03d.svg'%i)
            SVGPlotter(db2, ct_colors={0:'rgb(179,179,255)', 1:'rgb(179,179,255)'}, plot_midline=True).draw(output_path+'/mid1_%03d.svg'%i)


    SVGPlotter(db1, color='rgb(179,179,255)', plot_midline=True).draw(output_path+'/end0.svg')
    SVGPlotter(db2, color='rgb(179,179,255)', plot_midline=True).draw(output_path+'/end1.svg')



main()
import matplotlib.pylab as plt
plt.show()
