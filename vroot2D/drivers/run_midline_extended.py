

import sys
import random

import matplotlib
matplotlib.use("Agg")

def classify_cells(db):
    # Classify cells as upper and lower
    # Ads new property to tissue - side. 
    # side[cid] = {1 = lower, 2 = upper, 3 = "middle" }

    # First locate the QC cells.

    pos = db.get_property('position')
    mesh = get_mesh(db)
    graph = get_graph(db)
    cell_type = db.get_property('cell_type')
    QC_cells = [cid for cid, ct in cell_type.iteritems() if ct==17]
    mid_QC = np.mean([centroid(mesh, pos, 2, cid) for cid in QC_cells], axis=0)
    y_QC = mid_QC[1]
    side = {}
    for cid in mesh.wisps(2):
        y = centroid(mesh, pos, 2, cid)[1]
        if y > y_QC:
            side[cid]=1
        else:
            side[cid]=2
    LRC_cells = [cid for cid, ct in cell_type.iteritems() if ct in (6,7,8,9)]
    minx_LRC = min(centroid(mesh, pos, 2, cid)[0] for cid in LRC_cells)
    meristem = {}
    for cid in mesh.wisps(2):
        x = centroid(mesh, pos, 2, cid)[0]
        if x < minx_LRC:
            meristem[cid] = 0
        else:
            meristem[cid] = 1

    db.set_property('side', side)
    db.set_description('side', '')
    db.set_property('meristem', meristem)
    db.set_description('meristem', '')


def main():
    #Define/open tissue

    output_path = '/tmp'

    random.seed(1)

    from vroot.geometry.import_tissue import import_tissue, divide_long_cells, extend_tissue
    db=import_tissue('data/R12data_OA_wc.zip', scale=2.0)

    pos = db.get_property('position')
    print pos[2177]
    pos[2177][0] -= 5.0

    new_cids = extend_tissue(db)


    from vroot.carriers.PIN_assign import PIN_assign_subset
    PIN_assign_subset(db, new_cids)

    classify_cells(db)


    from vroot.model_output.svg_plot import SVGPlotter
    SVGPlotter(db, prop=None).draw('/tmp/init.svg')


    # Initialize scheduler
    from openalea.scheduler import Scheduler,Task
    sch = Scheduler()

###########################################################################


    from parameters_grav import setSimParameters
    setSimParameters(db)


    from vroot.genenetworks.genenetwork import GeneNetworkJPat, GeneNetwork,\
                                               CombinedModel

    from vroot.genenetworks.genenetwork_codegen import CGGeneNetwork, CodeGenModel, AuxinResponseModel

    from vroot.genenetworks.AuxinTransportModels import at_cwd, DIIq, Response
    gn = GeneNetworkJPat(db, CombinedModel([at_cwd()]))
    #gn_DII = GeneNetworkJPat(db, DIIq())

    gn_R = CGGeneNetwork(db, CodeGenModel(AuxinResponseModel()))
#    gn_R = GeneNetworkJPat(db, CombinedModel([Response()]))
    
    sch.register(Task(gn_R.step, 1, 5, "auxin response"))



    from vroot.utils.db_utilities import get_parameters   

    from vroot.genenetworks.diffuse_ss import acwdSS

    gn2 = acwdSS(db)


    sch.register(Task(gn2.step, 1, 6, "auxin SS"))

    from vroot.carriers.manipulate import manipulate

    manipulate(db)


    p = get_parameters(db, 'acwdSS')
    p['alpha_back']*=1
    p['alpha_QCinit']*=1

#    p = get_parameters(db, 'at_cwd')
#    p['alpha_back']*=1
#    p['alpha_QCinit']*=1


#    from vroot.carriers.PIN_angle import PINAngle
#    pa = PINAngle(db)
#    sch.register(Task(pa.step, 1, 12, "PIN Angle") )
#    pa.step()
    from vroot.carriers.PIN_angle4 import PINAngle_fullroot
    pa = PINAngle_fullroot(db)
    sch.register(Task(pa.step, 1, 12, "PIN Angle") )
    pa.gv=(1,0)
    pa.step() 
    pa.gv=(0,-1)

    # Time update
    db.set_property('time', 0)
    db.set_description('time', 'current simulation time')


    def update_time():
        t=db.get_property('time')
        t+=get_parameters(db, 'timestep')
        db.set_property('time', t)
    sch.register(Task(update_time, 1, 2, "update time"))

    """
    from vroot.growth.growth_midline import GrowthMidline
    g = GrowthMidline(db, LR=True)
    sch.register(Task(g.step, 1, 6, "growth"))

    from vroot.growth.division import MeshDivision
    from vroot.growth.division_rule import MidlineDivisionRule

#    md = MeshDivision(db, MidlineDivisionRule())
#    sch.register(Task(md.step, 1, 7, "division"))


    SVGPlotter(db, prop='auxin', plot_midline=True, log_prop=False, plot_PINAUX=True).draw('/tmp/init.svg')
    """
 
    from vroot.model_output.mpl_plotter import MPLPlotter
    mpl = MPLPlotter([db], 'auxin', limits=((-2000,1500),(-2500,500)),
                    format='png', fn_base='/tmp/auxin',
                    with_inset=False)
    sch.register(Task(mpl.redraw, 1, 2, "redraw"))

    mpl2 = MPLPlotter([db], 'response', limits=((-2000,1500),(-2500,500)),
                    format='png', fn_base='/tmp/response',
                    with_inset=False)

    sch.register(Task(mpl2.redraw, 1, 2, "redraw"))

    gn2.step()
    gn_R.step_dt(10000.0)

    mpl.redraw()
    mpl2.redraw()

    mIAA = db.get_property('mIAA')
    IAA = db.get_property('IAA')
    R = db.get_property('response')
    
    print max(mIAA.itervalues()), min(mIAA.itervalues())
    print max(IAA.itervalues()), min(IAA.itervalues())
    print max(R.itervalues()), min(R.itervalues())

    for i in range(100):
        print i
        next(sch.run())

main()
