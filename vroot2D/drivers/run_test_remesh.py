


import sys, os, errno
import copy 

import matplotlib
matplotlib.use("Agg")

import copy 
from vroot2D.utils.db_utilities import get_parameters

def main():


    from vroot2D.geometry.cell_array import cell_array_2D, staggered_array_2D
    db1 = staggered_array_2D(True, seed=10)

#    db1 = cell_array_2D(Nx=2, Ny=2, Lx=100, Ly=20, bend=True)
    db2 = copy.deepcopy(db1)
 #   db3 = copy.deepcopy(db1)

    

    output_path = 'output/bend_remesh'

    try:
        os.makedirs(output_path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise


    from vroot2D.model_output.svg_plot import SVGPlotter
    SVGPlotter(db1, color='lightgray').draw(output_path+'/init0.svg')
    SVGPlotter(db2, color='lightgray').draw(output_path+'/init1.svg')

    # Initialize scheduler
    from openalea.scheduler import Scheduler,Task
    sch = Scheduler()

###########################################################################




    # Growth task
    from vroot2D.growth.growth_nl import Growth
    g=Growth(db1, {'bend':0.13, 'relaxation_time':0.1, 'mesh_tri_area':10.0 }, use_old_vels=True)#, 'cross_lambda':0.0, 'cross_phiinv':0.0 })
    sch.register(Task(g.step, 1, 8, "growth"))
#    g=Growth(db2, {'bend':0.13, 'relaxation_time':0.1, 'mesh_tri_area':1.0 })
# This is better, but still unbends
    g2=Growth(db2, {'bend':0.13, 'relaxation_time':0.1, 'mesh_tri_area':10.0}, use_old_vels=True)#, 'cross_lambda':0.0, 'cross_phiinv':0.0})
    sch.register(Task(g2.step, 1, 8, "growth"))

#    g=Growth(db3, {'bend':0.13, 'relaxation_time':0.1, 'mesh_tri_area':1.0, 'spring_lambda':0.0 })
#    sch.register(Task(g.step, 1, 8, "growth"))

    
    from vroot2D.model_output.measure_midline import MeasureMidline
    mm = MeasureMidline(db1, fn=output_path+'/midline0')
    sch.register(Task(mm.step, 1, 10, "midline"))
    from vroot2D.model_output.measure_midline import MeasureMidline
    mm = MeasureMidline(db2, fn=output_path+'/midline1')
    sch.register(Task(mm.step, 1, 10, "midline"))

    

    from vroot2D.model_output.mpl_simple import MPLSimple
    mpl = MPLSimple([db1, db2], 'cell_type', limits=((-1000, 450), (-1200, 150)), 
                    offset_y = 150, 
                    fn_base=output_path+'/bend', format='png',
                    plot_midline=False)
    sch.register(Task(mpl.redraw, 1, 9, "redraw"))

    dt = get_parameters(db1, 'SyntheticGrowthModel')['relaxation_time']
    for i in range(31):
        db1.set_property('time', i*dt)
        db2.set_property('time', i*dt)
        next(sch.run())
        if (i+1)%5==0:
            SVGPlotter(db1, ct_colors={0:'rgb(179,179,255)', 1:'lightgray'}, plot_tris=True).draw(output_path+'/mid0_%03d.svg'%i)
            SVGPlotter(db2, ct_colors={0:'rgb(179,179,255)', 1:'lightgray'}, plot_tris=True).draw(output_path+'/mid1_%03d.svg'%i)
            g2.remesh()
            SVGPlotter(db2, ct_colors={0:'rgb(179,179,255)', 1:'lightgray'}, plot_tris=True).draw(output_path+'/mid1r_%03d.svg'%i)
            SVGPlotter(db1, ct_colors={0:'rgb(179,179,255)', 1:'rgb(255,179,179)'}, plot_tris=True, tri_style='stroke: rgb(30,30,30); stroke-width: 0.5').draw(output_path+'/tris0_%03d.svg'%i)
            SVGPlotter(db2, ct_colors={0:'rgb(179,179,255)', 1:'lightgray'}, plot_tris=True, tri_style='stroke: rgb(30,30,30); stroke-width: 0.5').draw(output_path+'/tris1_%03d.svg'%i)

    SVGPlotter(db1, color='rgb(255,179,179)').draw(output_path+'/end0.svg')
    SVGPlotter(db2, color='rgb(179,179,255)').draw(output_path+'/end1.svg')



main()
import matplotlib.pylab as plt
plt.show()
