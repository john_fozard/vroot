
import matplotlib
matplotlib.use("Agg")

import sys, os, errno
import cPickle

def main():
    #Define/open tissue

    from vroot2D.geometry.import_tissue import import_tissue
    db=import_tissue('data/fullroot2.zip', scale=10.0)

    output_path = 'output/fullroot_epi'

#    try:
 #       os.makedirs(output_path)
 #   except OSError as exception:
 #       if exception.errno != errno.EEXIST:
 #           raise

    # Initialize scheduler
    from openalea.scheduler import Scheduler,Task
    sch = Scheduler()

###########################################################################


    from parameters_fullroot import setSimParameters
    setSimParameters(db)

 
    from vroot2D.genenetworks.diffuse_ss import  DiffuseInhibitorSS, DiffusePromoterSS
#    ssi = DiffuseInhibitorSS(db)
    ssp = DiffusePromoterSS(db)
#    sch.register(Task(ssi.step, 1, 11, 'Inhibitor'))
    sch.register(Task(ssp.step, 1, 10, 'Promoter'))
#    ssi.step()
    ssp.step()

############################################################################

    # Time update
    db.set_property('time', 0)
    db.set_description('time', 'current simulation time')
    from vroot2D.utils.db_utilities import get_parameters
    def update_time():
        t=db.get_property('time')
        t+=get_parameters(db, 'timestep')
        db.set_property('time', t)
    sch.register(Task(update_time, 1, 2, "update time"))

    # Growth task
    from vroot2D.growth.growth_nl import Growth, EpidermalGrowthModel
    g=Growth(db, {'mesh_tri_area':100.0, 'spring_phiinv':0.1, 'epidermis_phiinv':10.0, 'mu1':0.2 }, model=EpidermalGrowthModel(), use_old_vels=True)
    sch.register(Task(g.step, 1, 8, "growth"))

    from vroot2D.model_output.expansion_rates import MeasureExpansion

    me = MeasureExpansion(db)
    sch.register(Task(me.step, 1, 3, "measureexpansion"))

    from vroot2D.model_output.measure_midline import MeasureMidline
    mm = MeasureMidline(db, fn=output_path+'/midline')
    sch.register(Task(mm.step, 1, 3, "midline"))

    from vroot2D.model_output.measure import  MeasurePropX
    mx = MeasurePropX(db, 'GP', fn = output_path+'/GP')
    sch.register(Task(mx.step, 1, 2, "MeasureGP"))

    from vroot2D.model_output.measure import  MeasurePropX
    mx = MeasurePropX(db, 'expansion', fn = output_path+'/expansion')
    sch.register(Task(mx.step, 1, 2, "MeasureEXP"))

    from vroot2D.model_output.svg_plot import SVGPlotter
    SVGPlotter(db, color='lightgray', plot_fibres=False, fibre_len=5).draw(output_path+'/init.svg')

    from vroot2D.model_output.mpl_simple import MPLSimple
    mpl = MPLSimple([db], 'GP',  limits=((-1100,900),(-750,750)), fn_base=output_path+'/fullroot_GP', cm_name='Greens', format='png', set_aspect=False, log_prop=True) 
    sch.register(Task(mpl.redraw, 10, 2, "redraw"))
#    mpl = MPLSimple([db], 'GI',  limits=((-1100,900),(-750,750)), fn_base=output_path+'/fullroot_GI', cm_name='Blues', format='png', set_aspect=False) 
#    sch.register(Task(mpl.redraw, 1, 2, "redraw"))
    mpl = MPLSimple([db], 'expansion',  limits=((-1100,900),(-750,750)), fn_base=output_path+'/fullroot', cm_name='Greens', format='png', set_aspect=False) 
    sch.register(Task(mpl.redraw, 1, 2, "redraw"))
    spl = SVGPlotter(db, fn_base=output_path+'/fullroot_GP', prop='GP', log_prop=True)
    sch.register(Task(spl.redraw, 1, 2, "redraw"))    
 

    print 'DOF: ', 2*len(db.get_property('position'))
    
 #   db.write(output_path+'/init.zip')
    def dump_state():
#        db.write(output_path+'/fullroot%03d.zip'%(dump_state.count))
        print 'START DUMP'
        cPickle.dump(db, open(output_path+'/fullroot%03d.pkl'%dump_state.count,'w'))
        print 'END DUMP'

        dump_state.count +=1
#    dump_state.count=0
#    sch.register(Task(dump_state, 1, 1, 'dump'))



    for i in range(200):
        print i
        next(sch.run())

  #  mm.write_regr(open(output_path+"/regr", 'w'))

main()
