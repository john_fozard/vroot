


import sys, os, errno
import copy 

import matplotlib
matplotlib.use("Agg")

import copy 
from vroot2D.utils.db_utilities import get_parameters

def main():

    from vroot2D.geometry.cell_array import cell_array_2D, staggered_array_2D
    db1 = staggered_array_2D(True, seed=10)
#    db1 = cell_array_2D(Nx=2, Ny=2, Lx=100, Ly=20, bend=True)
    db2 = copy.deepcopy(db1)

    output_path = 'output/compare_bend5'

    try:
        os.makedirs(output_path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

    from vroot2D.model_output.svg_plot import SVGPlotter
    SVGPlotter(db1, color='lightgray').draw(output_path+'/init0.svg')
    SVGPlotter(db2, color='lightgray').draw(output_path+'/init1.svg')

    from openalea.scheduler import Scheduler,Task
    sch = Scheduler()

###########################################################################

    # Growth task
    from vroot2D.growth.growth_nl import Growth
    g=Growth(db1, {'bend':0.13, 'relaxation_time':0.005, 
                   'spring_phiinv':0.0, 'mesh_tri_area':10.0, 
                   'cross_lambda':0.0, 'cross_phiinv':0.0, 'mu2':20.0 }, use_old_vels=True)
    sch.register(Task(g.step, 1, 8, "growth"))

    p = get_parameters(db1, g.model.name)
    print p

    from vroot2D.growth.growth_midline import GrowthMidline, \
        MidlineSyntheticGrowthModel

    g2 = GrowthMidline(db2, {'relaxation_time': 0.005}, MidlineSyntheticGrowthModel())
    # Change g2 parameters?
    sch.register(Task(g2.step, 1, 8, "growth"))

    p = get_parameters(db2, g2.model.name)
    print p

    
    from vroot2D.model_output.measure_midline import MeasureMidline
    mm = MeasureMidline(db1, fn=output_path+'/midline0', flip_x=True)
    sch.register(Task(mm.step, 1, 10, "midline"))
    from vroot2D.model_output.measure_midline import MeasureMidline
    mm = MeasureMidline(db2, fn=output_path+'/midline1', flip_x=True)
    sch.register(Task(mm.step, 1, 10, "midline"))

    

    from vroot2D.model_output.mpl_simple import MPLSimple
    mpl = MPLSimple([db1, db2], 'cell_type', limits=((-1000, 450), (-1200, 150)), 
                    offset_y = 250, 
                    fn_base=output_path+'/bend', format='png',
                    plot_midline=True,
                    colours=[{0:(0.8, 0.8, 1.0), 1:(0.8, 0.8, 0.8)},{0:(0.8, 0.8, 1.0), 1:(0.8, 0.8, 0.8)}])
    sch.register(Task(mpl.redraw, 1, 9, "redraw"))

    dt = get_parameters(db1, 'SyntheticGrowthModel')['relaxation_time']

    print 'dt', dt

    for i in range(200):
        db1.set_property('time', i*dt)
        db2.set_property('time', i*dt)
        next(sch.run())
        if (i+1)%5==0:
            SVGPlotter(db1, ct_colors={0:'rgb(179,179,255)', 1:'lightgray'}, plot_tris=True).draw(output_path+'/mid0_%03d.svg'%i)
            SVGPlotter(db2, ct_colors={0:'rgb(179,179,255)', 1:'lightgray'}, plot_tris=True).draw(output_path+'/mid1_%03d.svg'%i)
            g.remesh()
            SVGPlotter(db2, ct_colors={0:'rgb(179,179,255)', 1:'lightgray'}, plot_tris=True).draw(output_path+'/mid1r_%03d.svg'%i)
            SVGPlotter(db1, ct_colors={0:'rgb(179,179,255)', 1:'rgb(255,179,179)'}, plot_tris=True, tri_style='stroke: rgb(30,30,30); stroke-width: 0.5').draw(output_path+'/tris0_%03d.svg'%i)
            SVGPlotter(db2, ct_colors={0:'rgb(179,179,255)', 1:'lightgray'}, plot_tris=True, tri_style='stroke: rgb(30,30,30); stroke-width: 0.5').draw(output_path+'/tris1_%03d.svg'%i)

    SVGPlotter(db1, color='rgb(255,179,179)').draw(output_path+'/end0.svg')
    SVGPlotter(db2, color='rgb(179,179,255)').draw(output_path+'/end1.svg')



main()
import matplotlib.pylab as plt
plt.show()
