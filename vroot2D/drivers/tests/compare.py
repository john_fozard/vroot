
from openalea.celltissue import TissueDB
import sys
import numpy as np

db1 = TissueDB()
db1.read(sys.argv[1])
db2 = TissueDB()
db2.read(sys.argv[2])

prop_name = sys.argv[3]

prop1 = db1.get_property(prop_name)
prop2 = db2.get_property(prop_name)

if not set(prop1)==set(prop2):
    print "FAIL: Tissues have different geometries"
    sys.exit(1)

v1 = np.array([prop1[cid] for cid in prop1])
v2 = np.array([prop2[cid] for cid in prop1])

scale = np.amax(np.abs(v1))
err = np.abs(v1-v2)/scale
if np.amax(err)>0.01:
    print "FAIL: Difference - ", np.amax(err), scale
    
    exit(1)

print "PASS : Max error - ", np.amax(err), scale

exit(0)
