
from openalea.celltissue import TissueDB
import sys
import numpy as np
from cPickle import load

db1 = TissueDB()
db1.read(sys.argv[1])
f = open(sys.argv[2])
prop2 = load(f)

prop_name = sys.argv[3]

prop1 = db1.get_property(prop_name)

if not set(prop1)==set(prop2):
    print "FAIL: Tissues have different geometries"
    sys.exit(1)


for cid in prop1:
    prop1[cid] -= prop2[cid]

db1.write("diff.zip")
