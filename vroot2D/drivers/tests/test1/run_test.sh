DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "Testing whether different auxin transport modules give same results"

set -x
#python $DIR/test1ss.py
#python $DIR/test1gn.py
#python $DIR/test1gncg.py

OUT_DIR=$DIR/../../../output/tests/test1

python $DIR/../compare.py $OUT_DIR/vert_ss.zip $OUT_DIR/vert_gn.zip auxin
python $DIR/../compare.py $OUT_DIR/vert_ss.zip $OUT_DIR/vert_gncg.zip auxin
python $DIR/../compare.py $OUT_DIR/vert_gn.zip $OUT_DIR/vert_gncg.zip auxin


python $DIR/../compare.py $OUT_DIR/horiz_ss.zip $OUT_DIR/horiz_gn.zip auxin
python $DIR/../compare.py $OUT_DIR/horiz_ss.zip $OUT_DIR/horiz_gncg.zip auxin
python $DIR/../compare.py $OUT_DIR/horiz_gn.zip $OUT_DIR/horiz_gncg.zip auxin

python $DIR/../compare.py $OUT_DIR/horiz_gn_5.zip $OUT_DIR/horiz_gncg_5.zip auxin
set +x
