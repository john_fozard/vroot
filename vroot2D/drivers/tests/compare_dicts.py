
import sys
import numpy as np
from cPickle import load

f = open(sys.argv[1], 'r')
g = open(sys.argv[2], 'r')

prop1 = load(f)
prop2 = load(g)

if not set(prop1)==set(prop2):
    print "FAIL: Dictionaries have different keys"
    sys.exit(1)

v1 = np.array([prop1[cid] for cid in prop1])
v2 = np.array([prop2[cid] for cid in prop1])

scale = np.amax(np.abs(v1))
err = np.abs(v1-v2)/scale
if np.amax(err)>0.01:
    print "FAIL: Difference - ", np.amax(err), scale
    
    exit(1)

print "PASS : Max error - ", np.amax(err), scale

exit(0)
