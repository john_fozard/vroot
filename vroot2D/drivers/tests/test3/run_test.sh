DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "Comparison of model outputs vs TPC paper (Band et al 2014)"
echo "Compare output with at_versions/at_cwd_DIIq_aprod_R12/python auxin_transport_fig4.py  ;  revision 487"
echo "Changes need to be made to manipulatePINandAUX.py - AUX1 in S3 columella cells (change cid1 to cid2)"
echo "Note also that mistake in the code used to calculate V in the TPC version"

set -x
python $DIR/test3gn.py
python $DIR/test3gncg.py

OUT_DIR = $DIR/../../../output/tests/test3

python $DIR/../compare.py $OUT_DIR/horiz_gn_DII.zip $OUT_DIR/horiz_TPC_DII.zip auxin
python $DIR/../compare.py $OUT_DIR/horiz_gn_DII.zip $OUT_DIR/horiz_TPC_DII.zip VENUS
python $DIR/../compare.py $OUT_DIR/horiz_gncg_DII.zip $OUT_DIR/horiz_TPC_DII.zip auxin
python $DIR/../compare.py $OUT_DIR/horiz_gncg_DII.zip $OUT_DIR/horiz_TPC_DII.zip VENUS



set +x
