
"""
Initialize parameter values for simulation components
"""

from vroot2D.utils.db_utilities import get_parameters, set_parameters


def setSimParameters(db):
    initial_values = { 'auxin': 0.0, 'VENUS': 0.01 }
    # Initial values for each property

    timestep = 5*60.0 # 3 mins
    # Initial global simulation timestep

    print db.get_config('config').cell_types
    print db.get_config('config').border
    fixed_settings = { 'auxin': [('border[cid]==1 and cell_type[cid] in [5]', 1.0),('border[cid]==1 and cell_type[cid] not in [5]', 0.0)] }
    set_parameters(db, 'initial_values', initial_values)
    set_parameters(db, 'timestep', timestep)
    set_parameters(db, 'fixed', fixed_settings)

