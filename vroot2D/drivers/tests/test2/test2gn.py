

import sys
import random
import numpy as np
from vroot2D.utils.db_utilities import get_mesh, get_graph, set_parameters
from vroot2D.utils.db_geom import centroid
import matplotlib.pylab as plt
import cPickle

ENDO = 3

def add_casparian(db):
    pos = db.get_property('position')
    mesh = get_mesh(db)
    graph = get_graph(db)
    cell_type = db.get_property('cell_type')
    LRC_cells = [cid for cid, ct in cell_type.iteritems() if ct in (6,7,8,9)]
    minx_LRC = min(centroid(mesh, pos, 2, cid)[0] for cid in LRC_cells)    
    casparian = {}
    
    for wid in mesh.wisps(1):
        casparian[wid] = 0
        if mesh.nb_regions(1,wid)==2:    
            cid1, cid2 = mesh.regions(1, wid)
            if (cell_type[cid1]==ENDO and cell_type[cid2]==ENDO and 
                centroid(mesh, pos, 2, cid1)[0]<minx_LRC-500 and  centroid(mesh, pos, 2, cid2)[0]<minx_LRC-500):
                casparian[wid] = 1
    db.set_property('casparian', casparian)
    db.set_description('casparian', '')
    db.get_property('divided_props')['casparian']=('wall', 'property', 0)

def classify_cells(db):
    # Classify cells as upper and lower
    # Ads new property to tissue - side. 
    # side[cid] = {1 = lower, 2 = upper, 3 = "middle" }

    # First locate the QC cells.

    pos = db.get_property('position')
    mesh = get_mesh(db)
    graph = get_graph(db)
    cell_type = db.get_property('cell_type')
    QC_cells = [cid for cid, ct in cell_type.iteritems() if ct==17]
    mid_QC = np.mean([centroid(mesh, pos, 2, cid) for cid in QC_cells], axis=0)
    y_QC = mid_QC[1]
    side = {}
    for cid in mesh.wisps(2):
        y = centroid(mesh, pos, 2, cid)[1]
        if y > y_QC:
            side[cid]=1
        else:
            side[cid]=2
    LRC_cells = [cid for cid, ct in cell_type.iteritems() if ct in (6,7,8,9)]
    minx_LRC = min(centroid(mesh, pos, 2, cid)[0] for cid in LRC_cells)
    meristem = {}
    for cid in mesh.wisps(2):
        x = centroid(mesh, pos, 2, cid)[0]
        if x < minx_LRC:
            meristem[cid] = 0
        else:
            meristem[cid] = 1

    db.set_property('side', side)
    db.set_description('side', '')
    db.set_property('meristem', meristem)
    db.set_description('meristem', '')


def label_cells(db):
    mesh = get_mesh(db)
    pos = db.get_property('position')
    cell_type = db.get_property('cell_type')
    side = db.get_property('side')
    meristem = db.get_property('meristem')
    epi_cells_lower = [(-centroid(mesh, pos, 2, cid)[0], cid) for cid, ct in cell_type.iteritems() if (ct==2 and side[cid]==2 and meristem[cid]==0)]
    print epi_cells_lower
    epi_cells_lower = sorted(epi_cells_lower)[0:10:2]
    epi_cells_upper = [(-centroid(mesh, pos, 2, cid)[0], cid) for cid, ct in cell_type.iteritems() if (ct==2 and side[cid]==1 and meristem[cid]==0)]
    epi_cells_upper = sorted(epi_cells_upper)[0:10:2]

    cell_labels = {}
    for i, (_, cid) in enumerate(epi_cells_upper):
        cell_labels[cid] = 'u'+str(i+1)
    for i, (_, cid) in enumerate(epi_cells_lower):
        cell_labels[cid] = 'l'+str(i+1)
    db.set_property('cell_labels', cell_labels)
    db.set_description('cell_labels', '')



def main():
    #Define/open tissue

    output_path = 'output/tests/test2'

    random.seed(1)

    from vroot2D.geometry.import_tissue import import_tissue, divide_long_cells, extend_tissue
    db=import_tissue('data/R12_3.zip', scale=1.0)

    pos = db.get_property('position')
#    print pos[2177]
#    pos[2177][0] -= 5.0

#    new_cids = extend_tissue(db, 1000)

    classify_cells(db)

    from vroot2D.carriers.PIN_assign import PIN_assign_subset
#    PIN_assign_subset(db, new_cids)

    from vroot2D.model_output.svg_plot import SVGPlotter
    SVGPlotter(db, prop=None).draw('/tmp/init.svg')


    # Initialize scheduler
    from openalea.scheduler import Scheduler,Task
    sch = Scheduler()

###########################################################################


    from parameters_grav import setSimParameters
    setSimParameters(db)
    
    set_parameters(db, 'timestep', 60.0)

    from vroot2D.genenetworks.genenetwork import GeneNetworkJPat, GeneNetwork,\
                                               CombinedModel

    from vroot2D.genenetworks.genenetwork_codegen import CGGeneNetwork, CodeGenModel, AuxinResponseModel, AuxinHSModel, \
                                                       CodeGenTransportModel, CGTransportGeneNetwork,  CGat_cwd, \
                                                       CombinedCGModel

    from vroot2D.genenetworks.AuxinTransportModels import at_cwd, DIIq, Response
    gn = GeneNetworkJPat(db, CombinedModel([at_cwd(), DIIq()]))
#    gn = CGTransportGeneNetwork(db, CodeGenTransportModel(CombinedCGModel([AuxinHSModel(), AuxinResponseModel()]), CGat_cwd()))
#    gn = CGTransportGeneNetwork(db, CodeGenTransportModel(CombinedCGModel([AuxinHSModel()]), CGat_cwd()))

    from vroot2D.utils.db_utilities import get_parameters   

 

    from vroot2D.carriers.manipulate import manipulate

    manipulate(db)
    add_casparian(db)

    from vroot2D.carriers.PIN_angle4 import PINAngle_fullroot
    pa = PINAngle_fullroot(db)
    pa.gv=(1,0)
    pa.step() 
    gn.step_dt(1e6)

    for prop_name in ['theta']:
        db.set_description(prop_name, '')
    db.write(output_path+'vert_gn_DII.zip')
       
    pa.gv=(0,-1)
    pa.step()

    gn.step_dt(3000.0)

    db.write(output_path+'horiz_gn_DII_50.zip')

    gn.step_dt(1e6)

    db.write(output_path+'horiz_gn_DII.zip')
 
main()
