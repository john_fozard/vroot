DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "Testing whether different auxin transport modules give same results"

set -x
#python $DIR/test2gn.py
#python $DIR/test2gncg.py

OUT_DIR=../../../output/tests/test2

python $DIR/../compare.py $OUT_DIR/horiz_gn_DII.zip $OUT_DIR/horiz_gncg_DII.zip auxin 
python $DIR/../compare.py $OUT_DIR/horiz_gn_DII.zip $OUT_DIR/horiz_gncg_DII.zip VENUS
python $DIR/../compare.py $OUT_DIR/horiz_gn_DII_50.zip $OUT_DIR/horiz_gncg_DII_50.zip auxin
python $DIR/../compare.py $OUT_DIR/horiz_gn_DII_50.zip $OUT_DIR/horiz_gncg_DII_50.zip VENUS
python $DIR/../compare.py $OUT_DIR/vert_gn_DII.zip $OUT_DIR/vert_gncg_DII.zip auxin
python $DIR/../compare.py $OUT_DIR/vert_gn_DII.zip $OUT_DIR/vert_gncg_DII.zip VENUS


set +x
