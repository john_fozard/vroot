
from openalea.celltissue import TissueDB
import sys
import numpy as np

db1 = TissueDB()
db1.read(sys.argv[1])
db2 = TissueDB()
db2.read(sys.argv[2])

prop_name = sys.argv[3]

prop1 = db1.get_property(prop_name)
prop2 = db2.get_property(prop_name)

if not set(prop1)==set(prop2):
    print "FAIL: Tissues have different geometries"
    sys.exit(1)


for cid in prop1:
    prop1[cid] -= prop2[cid]

db1.write("diff.zip")
