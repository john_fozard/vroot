
"""
Initialize parameter values for simulation components
"""

from vroot.utils.db_utilities import get_parameters, set_parameters


def setSimParameters(db):
    initial_values = { 'auxin': 0.0, 'water_potential': 0.0,
                       'water_potential_wall': -0.0,
                       'osmotic_potential': 0.3,
                       'growth_rate': 0.2/3600.0,
                       'water_flux_wall_vertex': 0.0, 'ABA': 0.0,
                       'ABA_wall': 0.0}
    # Initial values for each property

    timestep = 60*3 # 3 mins
    # Initial global simulation timestep

    print db.get_config('config').cell_types
    print db.get_config('config').border
    fixed_settings = { 'ABA': [('cell_type[cid]==31', 1.0)],
                       'water_potential': [('cell_type[cid]==31', 0.0)] }    
#                       'water_potential': [('cell_type[cid]==30', -0.0),
#                                           ('cell_type[cid]==31', 0.3)] }
    wall_fixed = { 'ABA_wall': [('mesh.nb_regions(1, wid)==1', 0.0)],
                   'water_potential_wall': [('mesh.nb_regions(1,wid)==1', 0.0)]}

    set_parameters(db, 'initial_values', initial_values)
    set_parameters(db, 'timestep', timestep)
    set_parameters(db, 'fixed', fixed_settings)
    set_parameters(db, 'wall_fixed', wall_fixed)
