
"""
Initialize parameter values for simulation components
"""

from vroot2D.utils.db_utilities import get_parameters, set_parameters


def setSimParameters(db):
    initial_values = { 'auxin': 0.0, 'E2F': 0.0, 'AuxIAA': 0.3, 'IAAm': 0.3,
                      'LBD': 0.3, 'PA1': 0.3, 'PA2': 0.3, 'PLT': 0.02,
                       'PINc': 0.0 }
    # Initial values for each property

    timestep = 60*3 # 3 mins
    # Initial global simulation timestep

    print db.get_config('config').cell_types
    print db.get_config('config').border
    fixed_settings = { 'auxin': [('border[cid]==6 and cell_type[cid] in [3,4]', 2.0),('border[cid]==6 and cell_type[cid] not in [3,4]', 0.0),
                                 ('cell_type[cid]==4', 2.0)] }
    set_parameters(db, 'initial_values', initial_values)
    set_parameters(db, 'timestep', timestep)
    set_parameters(db, 'fixed', fixed_settings)

