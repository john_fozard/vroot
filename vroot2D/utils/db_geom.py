

from geom import area, dist
#from openalea.tissueshape import face_surface_2D, edge_length
from db_utilities import get_mesh, get_graph, refresh_property
from math import atan2
from geom import centroid as g_centroid
import numpy as np

def centroid(mesh, pos, deg, cid):
    wids, pids = ordered_wids_pids(mesh, pos, cid)
    return np.asarray(g_centroid([pos[pid] for pid in pids]))


def wall_angle(db, wid):
    """
    Determine whether a wall is nearer to vertical than it is to
    being horizontal (i.e. making an angle with the horizontal of
    between pi/4 and 3pi/4, or between -3pi/4 and -pi/4)
    :param db: Tissue database
    :type db: TissueDB
    :param wid: Wall Id
    :type wid: int
    """
    pos=db.get_property('position')
    mesh=get_mesh(db)
    pts=[pos[pid] for pid in mesh.borders(1,wid)]
    return abs(pts[1][1]-pts[0][1])>abs(pts[1][0]-pts[0][0])

def wall_angle_ordered(db, cid, wid):
    """
    Determine whether a wall is nearer to vertical than it is to
    being horizontal (i.e. making an angle with the horizontal of
    between pi/4 and 3pi/4, or between -3pi/4 and -pi/4)
    :param db: Tissue database
    :type db: TissueDB
    :param wid: Wall Id
    :type wid: int
    """
    pos=db.get_property('position')
    mesh=get_mesh(db)
    wids, pids = ordered_wids_pids(mesh, pos, cid)
    i = wids.index(wid)
    d = pos[pids[(i+1)%len(pids)]]-pos[pids[i]]
    return atan2(d[1],d[0])

def ordered_wids_pids(mesh, pos, cid):
    wid_set = set(mesh.borders(2,cid))
    wid_borders = dict((wid, set(mesh.borders(1,wid))) for wid in wid_set)
    wid = wid_set.pop()
    o_wids = [wid]
    o_pids = list(wid_borders[wid])
    end = o_pids[-1]
    while wid_set:
        for wid in wid_set:
            if end in wid_borders[wid]:
                wid_set.discard(wid)
                o_wids.append(wid)
                end = (wid_borders[wid] - set([end])).pop()
                o_pids.append(end)
                break
    o_pids.pop()
    if area([pos[pid] for pid in o_pids])<0:
        o_wids.reverse()
        o_pids=[o_pids[0]]+o_pids[-1:0:-1]
    return o_wids, o_pids



def updateVS(db):
    """ 
    Calculate cell volumes and surface area between cells
    (updating these in the TissueDB properties V and S). 
    :param db: Tissue Database (modified in place)
    :type db: TissueDB
    """
#    graph = get_graph(db)

    mesh = get_mesh(db)
    pos = db.get_property('position')
    wall = db.get_property('wall')

    # Area of each cell
    V = refresh_property(db, 'V')
    for cid in mesh.wisps(2):
        V[cid]=face_surface_2D(mesh, pos, cid)
    
    # Lengths of each wall
    S = refresh_property(db, 'S')
    for wid in mesh.wisps(1):
        S[wid] = edge_length(mesh, pos, wid)

def face_surface_2D(mesh, pos, cid):
    o_wids, o_pids = ordered_wids_pids(mesh, pos, cid)
    return area([pos[pid] for pid in o_pids]) 

def edge_length(mesh, pos, wid):
    pid0, pid1 = mesh.borders(1, wid)
    return dist(pos[pid0], pos[pid1])
