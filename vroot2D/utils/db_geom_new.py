
"""
Geometrical utilities that operate on the whole tissue
(mesh and or graph).
"""


from geom import area
from openalea.tissueshape import face_surface_2D, edge_length
from db_utilities import get_mesh, get_graph, refresh_property

def ordered_wids_pids(mesh, pos, cid):
    """
    For a mesh, generate lists of the walls and points surrounding
    the cell with id cid
   
    pid0   
    :param mesh: Topomesh for the tissue
    :param pos: Dictionary mapping point ids in the 
                mesh into (cartesian) positions
    :param cid: Id number of the cell
    :returns: o_wids, o_pids - ordered lists of walls and points
              describing the cell points in an anti-clockwise order
            o_wid[0]        o_wid[1]         ...          o_wid[-1]
    o_pid[0]        o_pid[1]        o_pid[2] ... o_pid[-1]         o_pid[0]
    """
    wid_set = set(mesh.borders(2,cid))
    wid = wid_set.pop()
    o_wids = [wid]
    o_pids = list(mesh.borders(1, wid))
    end = o_pids[-1]
    # Loop over all walls
    while wid_set:
        # While walls remain in wid_set
        for wid in wid_set:
            # Find wall in wid_set which contains the point end
            if end in mesh.borders(1, wid):
                # Add it to the ordered list of walls
                wid_set.discard(wid)
                o_wids.append(wid)
                # Set end to be the other end-point of the wall
                end = (set(mesh.borders(1, wid)) - set([end])).pop()
                o_pids.append(end)
                break
    o_pids.pop()
    # Reverse the points and walls if they describe the cell in a
    # clockwise order (area has wrong sign).
    if area([pos[pid] for pid in o_pids])<0:
        o_wids.reverse()
        o_pids=[o_pids[0]]+o_pids[-1:0:-1]
    return o_wids, o_pids




def updateVS(db):
    """ 
    Calculate cell volumes and surface area between cells
    (updating these in the TissueDB properties V and S). 
    :param db: Tissue Database (modified in place)
    :type db: TissueDB
    """
    
#    graph = get_graph(db)
    mesh = get_mesh(db)
    pos = db.get_property('position')
    wall = db.get_property('wall')

    # Area of each cell
    V = refresh_property(db, 'V')
    for cid in mesh.wisps(2):
        V[cid]=face_surface_2D(mesh, pos, cid)
    
    # Lengths of each wall
    S = refresh_property(db, 'S')
    for wid in mesh.wisps(1):
        S[wid] = edge_length(mesh, pos, wid)

