
from vroot2D.utils.utils import sort_pair
from collections import defaultdict

"""
Utilities for manipulating tissue databases
(Generally wrappers around small sections of OpenAlea code.)
"""

def set_cparameters(db, cname, value):
    """
    Sets a parameter value, given a composite string as a name. The function splits the parameter name using '.' as divider and sets the value by browsing recursively in the parameters dictionary
    """
    l = cname.split('.')
    pars = db.get_property('parameters')
 
    if len(l) == 2:
        t = pars [l[0]]
        try:
            t[l[1]] = float(value)
        except ValueError:
 	    t[l[1]] = value
        #print "set_cparameters: " + cname + " - " + str(value)
    else:
        if len(l) == 1:
            set_parameters(db, cname, value)
        else:
            raise Exception('Error in set_cparamters: parameter<' + cname + '> should be composed only of two names')            

def set_parameters(db, name, value):
    """
    Set parameter set with name 'name' from TissueDB
    :param db: The tissue database
    :type db: TissueDB
    :param name: Name of parameter set
    :param value: Parameter values (dictionary of name-value pairs)
    :type value: dict
    :type name: str
    """
    
    if 'parameters' in db.properties():
        parameters = db.get_property('parameters')
    else:
        parameters = {}
        db.set_property('parameters', parameters)
        db.set_description('parameters', "Model parameters")
    parameters[name]=value

def get_parameters(db, name):
    """
    Extract parameter set with name 'name' from TissueDB
    :param db: The tissue database
    :type db: TissueDB
    :param name: Name of parameter
    :type name: str
    :returns: Parameter set dictionary
    """
    return db.get_property('parameters').get(name)

def get_mesh(db):
    """
    Extract graph from TissueDB
    :param db: The tissue database
    :type db: TissueDB
    :returns: Mesh
    """
    t = db.tissue()
    cfg = db.get_config('config')
    return t.relation(cfg.mesh_id)

def get_graph(db):
    """
    Extract graph from TissueDB
    :param db: The tissue database
    :type db: TissueDB
    :returns: graph
    """
    t = db.tissue()
    cfg = db.get_config('config')
    return t.relation(cfg.graph_id)

def refresh_property(db, prop_name):
    """ 
    Look in the db - if there is a property called prop_name, clear it.
    Otherwise, create an empty property of that name
    We don't want to call db.set_property with a new dictionary, as
    other references to that property (particularly in the viewer)
    will still point to the old dictionary
    :param db: The tissue database
    :type db: TissueDB
    :param prop_name: Name of the property
    :type prop_name: str
    :returns: Empty property
    """
    try:
        prop = db.get_property(prop_name)
        prop.clear() # Empty the dictionary
    except KeyError:
        prop = {}
        db.set_property(prop_name, prop) # Create an empty property of that name
    return prop

def get_wall_decomp(db):
    """ 
    Calculate the 'wall decomposition', which maps each wall in the mesh
    seperating a pair of cells to its corresponding pair of edges in the
    graph
    :param db: Tissue Database
    :type db: TissueDB
    :returns dict: Wall decomposition. Map from wall ids to the
                   corresponding pair of edge ids.
    """
    wall_decomposition = {}
    wall=db.get_property('wall')
    for eid in wall:
        if wall[eid] not in wall_decomposition:
            wall_decomposition[wall[eid]] = [eid]
        else:
            wall_decomposition[wall[eid]].append(eid)
    return wall_decomposition

def get_tissue_maps(db):
    """
    Calculate the 'cell_map' and 'edge_map':
    maps between the id numbers in the graph and their
    position in the list of the cells and edges
    :param db: Tissue Database (modified in place)
    :type db: TissueDB
    :returns: {'cell': cell_map, 'edge': edge_map}
              cell_map is a dictionary mapping cell ids
              to offsets. Similarly for edge_map.
    """
    t = db.tissue()
    cfg = db.get_config('config')
    graph = t.relation(cfg.graph_id) # directed cell to cell graph
    mesh = t.relation(cfg.mesh_id)
    cell_map={}
    for ind, cid in enumerate(mesh.wisps(2)) :
        cell_map[cid] = ind
    edge_map={}
    for ind, eid in enumerate(graph.edges()) :
        edge_map[eid] = ind
    wall_map={}
    for ind, wid in enumerate(mesh.wisps(1)) :
        wall_map[wid] = ind
    return {'cell': cell_map, 'edge': edge_map, 'wall': wall_map }

def add_cell_property(db, name, init_value):
    """
    Add a property to the tissue database, with every cell
    having the value init_value
    :param db: Tissue Database
    :type db: TissueDB
    :param name: Name of parameter
    :type name: str
    :param init_value: Default initial value for the parameter
    :type init_value: float, int (any?)
    """
    mesh=get_mesh(db)
    p = dict( (cid, init_value) for cid in mesh.wisps(2) )
    db.set_property(name, p)
    db.set_description(name, '')
    return p


def iter_prop_type(db, prop_type):
    """
    Generate an iterator over all the objects of a given type
    in the TissueDB
    :param db: TissueDB - the tissue database
    :param prop_type: str - type of object to iterate over.
    """
    if prop_type == 'cell':
        mesh=get_mesh(db)
        return mesh.wisps(2)
    if prop_type == 'wall':
        mesh = get_mesh(db)
        return mesh.wisps(1)
    if prop_type == '(cell, wall)':
        mesh = get_mesh(db)
        return cell_walls(mesh)
    if prop_type == 'point':
        mesh = get_mesh(db)
        return mesh.wisps(0)
    if prop_type == 'edge':
        graph = get_graph(db)
        return graph.edges()
    if prop_type == '(cell, tri)':
        mesh = get_mesh(db)
        cell_triangles = db.get_property('cell_triangles')
        return ((cid, t) for cid, l in cell_triangles.iteritems() for t in l)
    if prop_type == '(wall,point)':
        mesh = get_mesh(db)
        return ((wid, pid) for wid in mesh.wisps(1) for pid in mesh.borders(1, wid))


def add_property(db, prop_name, prop_type, default_value):
    """
    Add a property to the tissue database, with every cell
    having the value init_value
    :param db: Tissue Database
    :type db: TissueDB
    :param prop_name: Name of parameter
    :type prop_name: str
    :param prop_type: Type of mesh element property is defined upon
    :type prop_type: str
    :param init_value: Default initial value for the parameter
    :type init_value: float, int (any?)
    """

    p = {}
    for wid in iter_prop_type(db, prop_type):
        p[wid] = default_value
    db.set_property(prop_name, p)
    db.set_description(prop_name, '')
    return p

def cell_walls(mesh):
    """
    Extract all (cell, wall) pairs in a Topomesh 
    :param mesh: The mesh
    :type mesh: Topomesh
    :returns: generator of (cell, wall) id pairs
    """
    for wid in mesh.wisps(1):
        for cid in mesh.regions(1, wid):
            yield (cid, wid)

def gen_edge_groups(db):
    graph = get_graph(db)
    groups = defaultdict(list)
    for eid in graph.edges():
        p = (graph.source(eid), graph.target(eid))
        groups[p].append(eid)
    return groups
