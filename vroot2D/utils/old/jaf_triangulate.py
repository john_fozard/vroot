
#import meshpy.triangle as triangle
#from utils import pairs
from geom import centroid, ear_clip
#import numpy as np

"""
Wrapper for the ear-clipping triangulation function
(to be called by OpenAlea internals)
"""

def jaf_triangulate_polygon (pids, pos) :
#	quit()
	pts = [pos[pid] for pid in pids]
	return [tuple(pids[i] for i in t) for t in ear_clip(pts)]
	"""
	facets = [ pp for pp in pairs(range(len(pts))) ]
	info = triangle.MeshInfo()
	info.set_points(pts)
	info.set_facets(facets)
	c=centroid(pts)
	#help(info)
	#info.set_regions([( c[0], c[1], 1, 0.1) ])
	triangle_output = triangle.build(info, allow_boundary_steiner=False)
	new_elements=np.array(triangle_output.elements, dtype='int')
	print new_elements
	print len(pts)
	return new_elements
	"""
