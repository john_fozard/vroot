import sys
import random
import numpy as np
from vroot2D.utils.db_utilities import get_mesh, get_graph, set_parameters
from vroot2D.utils.db_geom import centroid
import matplotlib.pylab as plt
import cPickle
from math import log, sqrt

def main():
    from vroot2D.geometry.import_tissue import import_tissue, divide_long_cells, extend_tissue
    db=import_tissue('data/R12_3.zip', scale=1.0)

    auxin1 = cPickle.load(open('auxin.pkl'))
    db.set_property('auxin1', auxin1)

#    auxin2 = cPickle.load(open('auxin_LRB_AUX1_only.pkl'))
    auxin2 = cPickle.load(open('auxin_LRB.pkl'))
    db.set_property('auxin2', auxin2)
    
#    auxin_diff = dict((cid, log(max(abs(auxin1[cid] - auxin_LRB[cid]), 1e-6))) for cid in auxin1)


    auxin_diff = dict((cid, auxin2[cid] - auxin1[cid]) for cid in auxin2)

  #  print auxin_diff

    db.set_property('auxin_diff', auxin_diff)

    from vroot2D.model_output.svg_plot import SVGPlotter
    SVGPlotter(db, prop='auxin1', thresh_prop=False).draw('auxin1.svg')
    SVGPlotter(db, prop='auxin2', thresh_prop=False).draw('auxin2.svg')
    SVGPlotter(db, prop='auxin_diff', thresh_prop=False).draw('auxin_diff.svg')


    VENUS1 = cPickle.load(open('VENUS.pkl'))
    db.set_property('VENUS1', VENUS1)

#    VENUS2 = cPickle.load(open('VENUS_LRB_AUX1_only.pkl'))
    VENUS2 = cPickle.load(open('VENUS_LRB.pkl'))
    db.set_property('VENUS2', VENUS2)
    
#    VENUS_diff = dict((cid, log(max(abs(VENUS1[cid] - VENUS_LRB[cid]), 1e-6))) for cid in VENUS1)

   # for cid in VENUS2:
   #     print cid, VENUS1[cid], VENUS2[cid], VENUS1[cid] - VENUS2[cid]

    VENUS_diff = dict((cid, VENUS2[cid] - VENUS1[cid] if VENUS2[cid]<100 else 0) for cid in VENUS2)
    
    print max(VENUS_diff.itervalues()), min(VENUS_diff.itervalues())

  #  print VENUS_diff

    db.set_property('VENUS_diff', VENUS_diff)

    from vroot2D.model_output.svg_plot import SVGPlotter
    SVGPlotter(db, prop='VENUS1', thresh_prop=False).draw('VENUS1.svg')
    SVGPlotter(db, prop='VENUS2', thresh_prop=False).draw('VENUS2.svg')
    SVGPlotter(db, prop='VENUS_diff', thresh_prop=False).draw('VENUS_diff.svg')

main()
