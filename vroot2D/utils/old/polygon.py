#!/usr/local/python

import sys
import time
from math import sqrt, sin, cos, pi
from random import random
import matplotlib
from matplotlib.pylab import *
import matplotlib.pyplot as plt
import matplotlib.collections as coll
import matplotlib.patches as pat
import numpy

[2, 0, 1]
# Based on a post by Peter Otten on comp.lang.python 2004/09/04.
# This uses the 'key' option to sort and the 'sorted' function, both
# new in Python 2.4.
# http://code.activestate.com/recipes/306862/

def permutation_indices(data):
    return sorted(range(len(data)), key = data.__getitem__)

def draw_poly(p) :

#    fig=plt.figure()
    
 #   a=fig.add_axes([0,0,1,1])
    cla()
    a=gca()
    
  
    if is_simple_n2(p) :
        patch=pat.Polygon(p, fc='none')
    else :
        patch=pat.Polygon(p)

    a.add_patch(patch)
    
    siz=[10 for v in p]
    col=coll.CircleCollection(siz, offsets=p, transOffset=a.transData)
    a.add_collection(col, autolim=True)
    a.autoscale_view()
 
    draw()


def draw_polys(pp) :

#    fig=plt.figure()
    
 #   a=fig.add_axes([0,0,1,1])
    cla()
    a=gca()
    
    for p in pp :
        if is_simple_n2(p) :
            patch=pat.Polygon(p, fc='none')
        else :
            patch=pat.Polygon(p)
        a.add_patch(patch)

    a.autoscale_view()
 
    draw()

# http://stackoverflow.com/questions/1257413/iterate-over-pairs-in-a-list-circular-fashion-in-python
def g_pairs(lst) :
     i = iter(lst)
     first = prev = i.next()
     for item in i :
         yield prev, item
         prev = item
     yield item, first

def clip_cmp(x, val):
    eps=1e-6
    return (x>val+eps)-(x<val-eps)

def ccw(a,b,c) :
    # test for some of the degenerate cases (two point coninciding)
    # ignore collinear but not coincident points
    if all(a==b) or all(a==c) or all(b==c) :
        return 0
    # rule from 
    # http://compgeom.cs.uiuc.edu/~jeffe/teaching/algorithms/notes/xn-convexhull.pdf
    else :
        return clip_cmp((b[0]-a[0])*(c[1]-b[1])-(b[1]-a[1])*(c[0]-b[0]),0.0)

def intersects(e, f) :
# http://compgeom.cs.uiuc.edu/~jeffe/teaching/373/notes/x06-sweepline.pdf
# return CCW(a, c, d) != CCW(b, c, d) and CCW(a, b, c) != CCW(a, b, d)
# a=e[0] b=e[1] c=f[0] d=f[1]
# Note that this probably doesn't handle degenerate cases well.

# Needs to be made more robust - particularly for the colinear case    

 
    if (ccw(e[0],f[0],f[1]) != ccw(e[1],f[0],f[1])) and \
        (ccw(e[0],e[1],f[0]) != ccw(e[0],e[1],f[1])):
        print 'intersects: ', e, f 
        print ccw(e[0], f[0], f[1]), ccw(e[1], f[0], f[1])
        print ccw(e[0], e[1], f[0]), ccw(e[0], e[1], f[1])
        raise
    
    return (ccw(e[0],f[0],f[1]) != ccw(e[1],f[0],f[1])) and \
        (ccw(e[0],e[1],f[0]) != ccw(e[0],e[1],f[1]))

def is_simple_ccw(p) :
    a=p[0]
    r=True
    
#    dir = ( p1>p2 for p1,p2 in g_pairs(p) )
#    if sum( d1!=d2 for d1,d2 in g_pairs(dir) ) > 2 :
#         r = False 
#    else :
    if True :
         for i in range(1,len(p)-1) :
              if (ccw(a, p[i], p[i+1])<0) :
                   r=False
    if r :
        return True
    else :
        return is_simple_n2(p) 
                       

def is_simple_n2(p) :
    # The O(n^2) test for polygon simplicity
    edges=[e for e in g_pairs(p)]
    n=len(edges)
    for i in xrange(0,n-2) :
        if any([intersects(edges[i], e) for e in edges[i+2:n-(i==0)]]) :
            return False
    return True

def above(v,e,p) :
     # for a point v to the right of e[0], test whether it lies above
     # the line through e[0] and e[1]
     #   if p[v]>max(p[e[0]][1],p[e[1]][1]) :
     #        return True
     #   else :
     return ccw(p[e[0]], p[e[1]], p[v])>0

def is_simple_scan(p) :
    # The scan-line algorithm for polygon simplicity
    # Loosely based on the description in CGAL, 
    # include/CGAL/Polygon_2/Polygon_2_simplicity.h

    # for i in ind_p :
    #     if i is not an endpoint of an edge :
    #          insert the pair of right-going edges (in the right order)
    #     if i is the endpoint of exactly one edge e :
    #          check that i still lies between the two edges above and below
    #          swap (*,i) -> (i,*)
    
    #     if i is the endpoint of two edges e1 and e2 :
    #          check that i lies between the two edges above and below
    #          delete these edges

    n=len(p)

    ind_p=permutation_indices(p)

    events=[]

    for i in ind_p :
         # ev = (index of vertex, [edges to add], [edges to remove])
         ev=(i, [], [])
         i_prev=(i-1)%n
         i_next=(i+1)%n
         if (p[i_prev]>p[i]) :
              ev[2].append( (i, i_prev) )
         else :
              ev[1].append( (i_prev, i) )
         if (p[i_next]>p[i]) :
              ev[2].append( (i, i_next) )
         else :
              ev[1].append( (i_next, i) )
              
         if len(ev[2])==2 and above(i_next, (i, i_prev), p) :
              ev[2].reverse()
                       
         events.append(ev)

#    print events

    sl = []

    for ev in events :
         v=ev[0]
         if len(ev[1])==0 :
              # New
              # find position to insert edges in sl
              for i in range(len(sl)) :
                   if not above(v,sl[i],p) :
                        sl.insert(i,ev[2][0])
                        sl.insert(i,ev[2][1])
                        break
              else :                   
                   sl.append(ev[2][1])
                   sl.append(ev[2][0])
                   
         if len(ev[1])==1 :
              # Swap
              i = sl.index(ev[1][0])
              if i>0 and (not above(v,sl[i-1],p)) :
                   return False
              if i<len(sl)-1 and above(v,sl[i+1],p) :
                   return False
              sl[i]=ev[2][0]

         if len(ev[1])==2 :
              # Delete
              i = sl.index(ev[1][0])              
              sl.pop(i)
              i = sl.index(ev[1][1])              
              sl.pop(i)
              if i>0 and (not above(v,sl[i-1],p)) :
                   return False
              if i<len(sl) and above(v,sl[i],p) :
                   return False

         
    return True

def above2(v,e) :
     # for a point v to the right of e[0], test whether it lies above
     # the line through e[0] and e[1]
     #   if p[v]>max(p[e[0]][1],p[e[1]][1]) :
     #        return True
     #   else :
     return ccw(e[0], e[1], v)>0

def is_simple_scan2(p) :
    # The scan-line algorithm for polygon simplicity
    # Loosely based on the description in CGAL, 
    # include/CGAL/Polygon_2/Polygon_2_simplicity.h

    # for i in ind_p :
    #     if i is not an endpoint of an edge :
    #          insert the pair of right-going edges (in the right order)
    #     if i is the endpoint of exactly one edge e :
    #          check that i still lies between the two edges above and below
    #          swap (*,i) -> (i,*)
    
    #     if i is the endpoint of two edges e1 and e2 :
    #          check that i lies between the two edges above and below
    #          delete these edges

    n=len(p)

    ind_p=permutation_indices(p)

    events=[]

    for i in ind_p :
         # ev = (index of vertex, [edges to add], [edges to remove])
         pt=p[i]
         ev=(pt, [], [])
         pt_prev=p[(i-1)%n]
         pt_next=p[(i+1)%n]
         if (pt_prev>pt) :
              ev[2].append( ( pt, pt_prev ) )
         else :
              ev[1].append( ( pt_prev, pt ) )
         if (pt_next>pt) :
              ev[2].append( ( pt, pt_next ) )
         else :
              ev[1].append( ( pt_next, pt ) )
              
         if len(ev[2])==2 and above2( pt_next, ( pt, pt_prev ) ) :
              ev[2].reverse()
                       
         events.append(ev)

#    print events

    sl = []

    for ev in events :
         v=ev[0]
         if len(ev[1])==0 :
              # New
              # find position to insert edges in sl
              for i in range(len(sl)) :
                   if not above2(v,sl[i]) :
                        sl.insert(i,ev[2][0])
                        sl.insert(i,ev[2][1])
                        break
              else :                   
                   sl.append(ev[2][1])
                   sl.append(ev[2][0])
                   
         if len(ev[1])==1 :
              # Swap
              i = sl.index(ev[1][0])
              if i>0 and (not above2(v,sl[i-1])) :
                   return False
              if i<len(sl)-1 and above2(v,sl[i+1]) :
                   return False
              sl[i]=ev[2][0]

         if len(ev[1])==2 :
              # Delete
              i = sl.index(ev[1][0])              
              sl.pop(i)
              i = sl.index(ev[1][1])              
              sl.pop(i)
              if i>0 and (not above2(v,sl[i-1])) :
                   return False
              if i<len(sl) and above2(v,sl[i]) :
                   return False
         
    return True
                  
              
              
              
              
              

   
 

               



def bbox(p) :
    x,y=zip(*p)
    return ((min(x),min(y)),(max(x),max(y)))

def intersect_box(b1,b2) :
    # strictness of overlap?!
    # AABB check
    # http://en.wikipedia.org/wiki/Bounding_volume#Basic_intersection_checks
    return ( b1[1][0] >= b2[0][0] ) and ( b2[1][0] >= b1[0][0] ) and \
                     ( b1[1][1] >= b2[0][1] ) and ( b2[1][1] >= b1[0][1] ) 
                     

def intersect_polys(p1, p2) :
    # detect intersection between two polygons
    b1=bbox(p1)
    b2=bbox(p2)
    if not intersect_box(b1, b2) :
        return False
    return True


def noisy_circle(o,n,eps) :
    return  [(o[0]+cos(i*2*pi/n)+eps*random(), o[1]+sin(i*2*pi/n)+eps*random()) for i in xrange(0,n)]

def test() :
#      p = [(random(), random()) for i in xrange(0,1000)]
    n=100
#    p=[(cos(i*2*pi/n), sin(i*2*pi/n)) for i in xrange(0,n)]
    p=noisy_circle((0,0),n,0.1)
    return is_simple_n2(p)

def test2() :
 #     p = [(random(), random()) for i in xrange(0,1000)]
    n=100
 #   p=[(cos(i*2*pi/n), sin(i*2*pi/n)) for i in xrange(0,n)]
    p=noisy_circle((0,0),n,0.1)
    return is_simple_scan2(p)

def main() :


    p=noisy_circle((0,0),10000,0.1)
 #    p=[ (0,0), (1,0), (0.8,0.5), (1,1), (0.5, 0.8), (0,1) ]

    for i in range(0,1) :
        is_simple_ccw(p)
        is_simple_scan2(p)

#    p=[ (0,0), (1,0), (1,1), (0,1) ]
 #    from timeit import Timer
 #    t = Timer("test()", "from __main__ import test")#, is_simple_n2, ccw, intersects, g_pairs")
#     print(t.timeit(number=100))
#     t = Timer("test2()", "from __main__ import test2")#, is_simple_n2, ccw, intersects, g_pairs")
#     print(t.timeit(number=100))
#


##     ion()
##     for i in range(0,100) :        
##         p=noisy_circle((0,0),20,0.2)
##         p2=noisy_circle((2*random(),2*random()),20,0.2)
##         is_simple_scan(p)
##         draw_polys([p,p2])
##         if intersect_polys(p,p2) :
##             print 'IS'
##             time.sleep(0.8)
##         time.sleep(0.2)
##         time.sleep(0.5)
##     plt.show()


if __name__ == "__main__":
#    import cProfile
#    import psyco
#    psyco.full()
    main()
#    
#    cProfile.run("main()",filename="poly.prof") 
#    import pstats
#    stats = pstats.Stats("poly.prof")
#    stats.print_stats()
#    stats.strip_dirs().sort_stats('time').print_stats(20)  
#     main()
