from cPickle import load
import numpy as np
import scipy.linalg as la
import matplotlib.pylab as plt
from math import sqrt, atan2, asin
from scipy.interpolate import interp1d

import sys

def dist(p1, p2):
    return sqrt((p1[0]-p2[0])**2+(p1[1]-p2[1])**2)

kappa_history = []
l_history = []
kdot_history = []
regr_history = []
s_history = []

with open(sys.argv[1]) as f:
    kappa =[]
    l = []
    kdot = []
    regr = []
    for line in f:
        if not line.strip():
            kappa_history.append(np.array(kappa))
            l_history.append(l)
            kdot_history.append(kdot)
            regr_history.append(regr)
            s_history.append(np.array([0] + list(np.cumsum(l))))
            kappa =[]
            l = []
            kdot = []
            regr = []
        else:
            v = map(float, line.strip().split())
            kappa.append(v[0])
            l.append(v[1])
            kdot.append(v[2])
            regr.append(v[3])
xi = np.linspace(0, 1700, 1000)
yi = np.arange(len(kappa_history))
kappa_new = []
for s, k in zip(s_history, kappa_history):
    print k[-1]
    ki = interp1d(s[-1]-0.5*(s[:0:-1]+s[-2::-1]), k[::-1], bounds_error=False, fill_value=0.0)
    kappa_new.append(np.array([ki(x) for x in xi]))
zi = np.asarray(kappa_new)
fig = plt.figure(3)
cs=plt.contourf(xi,yi,-zi,20, cmap=plt.cm.jet)  
print np.max(zi), np.min(zi)
plt.colorbar()
for i, s in enumerate(s_history):
    plt.plot(s[-1]-s, i*np.ones(s.shape), 'kx')


plt.show()
# Trace out history of material points - want this also. Code in the old midline stuff.
