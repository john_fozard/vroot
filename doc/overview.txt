
Overview of simulation code
===========================

vroot - 2D simulation code
--------------------------

*vroot/genenetworks*
AuxinTransportModels.py -> at_cwd, at_cwd_casp, at_cwd_casp_pH, DIIq, Response
AuxinTransportModels_old.py -> AuxinTransportAllCarriersPlusApo, at_cwd, DIIq
diffuse_ss.py -> DiffusePromoterSS, DiffuseInhibitorSS, AuxinSS, acwdSS, acwdSSpH, AuxinWallsSS
     AuxinSS = original auxin model without apoplastic diffusion
     AuxinWallsSS = params are A1, A2 etc ...
      acwd = voltage params, has different production in QCinit and elsewhere, casp strip, 
fullrootmodels.py -> GrowthRegulators, AuxinDelay
genenetwork -> CombinedModel, GeneNetwork, GeneNetworkJPat
genenetwork_codegen.py ->CombinedCGModel AuxinHSModel DIIModel 
	AuxinResponseModel ConstantPropagator VariablePropagator
	ParameterPropagator YPropagator MeanExtractor CGat_cwd CodeGenModel CodeGenTransportModel
	CGGeneNetwork CGTransportGeneNetwork
(julienModels.py)
models.py ->  AuxinTransportAUX1 at_cwd at_ncwd  DIIq (think these are all old?)
SBML.py -> old (unfinished code) for models in SBML functions
STM.py -> STM STM_eqns

vroot3D - 3D simulation code
----------------------------

Calculating JPat (lil_matrix), and updateVS (openalea area/volume functions) are very slow!

*vroot3D/geometry*
cube.py
cube_array.py
import_tissue_3D.py

*vroot3D/genenetworks* - suspect these don't work!

AuxinTransportModels.py
diffuse_ss.py
fullrootModels.py
genenetwork.py -> fine (tissue_maps['cell'])
models.py

*vroot3D/model_output*
oa_ply_3d.py - output to ply mesh

*vroot3D/growth*
division.py
division_rule.py
growth_3d.py
py_slice.py
tri_contact.py

*vroot3D/utils*
