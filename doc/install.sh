sudo apt-get update
sudo apt-get -y upgrade
sudo apt-get -y install g++ build-essential python-dev python-numpy python-scipy scons git python-matplotlib

sudo apt-get -y install libboost-python-dev libsundials-serial-dev libsuitesparse-dev libopenblas-dev

sudo apt-get -y install libxml2 python-lxml gfortran

sudo apt-get -y install python-pip
sudo pip install meshpy
sudo apt-get -y install libglu1-mesa libsm6

sudo apt-get clean

git clone https://code.plant-images.org/vroot2D

cd vroot2D/src/cpp
wget "bitbucket.org/eigen/eigen/get/3.2.5.tar.gz"
tar zxvf 3.2.5.tar.gz 
mv eigen-eigen-bdd17ee3b1b3/ eigen
cd ~

mkdir Downloads
cd Downloads/
wget "http://geuz.org/gmsh/bin/Linux/gmsh-2.9.3-Linux64.tgz"
tar zxvf gmsh-2.9.3-Linux64.tgz 
rm gmsh-2.9.3-Linux64.tgz 
cd ..


cd vroot2D
scons .


cd /usr/local/lib/python2.7/dist-packages/meshpy
sudo sed -i "s/pzj/npzj/" triangle.py

cd ~
git clone https://code.plant-images.org/odesparse
mv odesparse odesparse_src
cd odesparse_src
sudo python setup.py install

# rename odesparse directory



