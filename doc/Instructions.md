
# Using VRoot2D

## Acquiring a root geometry

These simulations of root growth and gravitropism require a 
suitable vertex-based representation of the geometry of the cells.

Whilst there may be other ways to obtain such a geometry, work 
at CPIB
has generally used templates which are from confocal microscopy. The
walls need to be made to fluoresce, either with Propidium Iodide
staining, or preferably using a membrane associated marker such as
Lti6a. The StackProject tool may be useful to acquire a suitable
section in cases where the root midplane is not parallel to the
z-plane of the confocal microscope; it permits the selection of a
suitable surface from a confocal stack.

This planar image then needs to be segmented and converted into a
vertex-based geometry. CellSeT is a tool which is capable of helping
with this, providing a semi-automatic segmentation of the two
dimensional image. We refer the user to the CellSeT documentation
for further information about this process. However, we further
note the importance of the annotation of particular cell types
for the correct positioning of auxin carriers within the tissue.
These should follow the scheme indicated in the figure below.

The tissue needs to be exported as an xml file, with options " ".

## Informal summary of the gravitropism model.



## How the bits of the simulation fit together.





