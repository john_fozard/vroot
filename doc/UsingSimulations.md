
# Using these simulations - a guide

## About OpenAlea

These simulations were built on top of the OpenAlea TissueDB, which provides a
datastructure for storing and representing vertex-based tissues.
Owing to the simultaneous development of OpenAlea and the current project,
we have opted to distribute a frozen version of a small subset of the OpenAlea
project that suffices for the purpose of our simulations.
Official documentation for the TissueDB class is available from the OpenAlea
project, but we give a brief description of some of its properties here.
This datastructure is very flexible, facilitiating the development of the simulations,
but we have adopted certain conventions.

The TissueDB can contain a varying number of different components. However, those
considered in the current model will contain at least the following:
1. A `Topomesh` object. This is simplicial complex representation of a
   vertex-based tissue. It contains objects (`wisp`s) of different dimensions;
   points are 0-dimensional objects, walls are 1-dimensional objects, cells
   are 2-dimensional objects.
2. A `Graph` object. This is a directed graph between pairs of cells.
A pair of edges (one in each direction) is present between each wall.
3. A `properties` dictionary. This contains the

### The properties dictionary

This is where most of our conventional extensions to the tissue reside.

## Directories and python modules

###  `vroot.geometry`

This contains 
